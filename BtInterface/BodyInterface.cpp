#include "BodyInterface.h"
#include "ObjectMotionState.h"
#include "convertVM.h"
#include "../simplefc/settings.h"
#include "../simplefc/trigger.h"
#include "WorldInterface.h"
#include "../level.h"
#include "../gameData.h"

#include "../comps/Magnets.h"
#include "../simplefc/trigger.h"

BodyInterface::BodyInterface(btCollisionShape *shape, Object *parent, btQuaternion rot, btVector3 pos, btScalar mass, btScalar bounce, btScalar friction, btScalar rollingFriction, bool static_hold, bool magnetic, TriggerData* pushable): btRigidBody([&](){
		ObjectMotionState* state = new ObjectMotionState(btTransform(rot, pos), parent);
		btVector3 inertia = btVector3(0,0,0);
		shape->calculateLocalInertia(mass, inertia);
		btRigidBody::btRigidBodyConstructionInfo bodyCI(mass, state, shape, inertia);
		bodyCI.m_restitution = bounce;
		bodyCI.m_friction = friction;
		bodyCI.m_rollingFriction = rollingFriction;
		return bodyCI;
	}()), angular_factor(btVector3(1, 1, 1)), staticed(0), magnetic(magnetic), static_hold(static_hold), pushable(pushable), parent_obj(parent), parent_comp(0), registered(0){
	setSleepingThresholds(SLEEPING_THRESHOLD);
	setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
	activate(true);

	if(getInvMass() == 0){
		setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );
		setActivationState(DISABLE_DEACTIVATION);
	}else if(parent_obj) mvableObjs[this] = parent_obj;
	if(magnetic)addMagneticObj(this);
}

BodyInterface::BodyInterface(btCollisionShape *shape, ObjectComponent *parent, btQuaternion rot, btVector3 pos, btScalar mass, btScalar bounce, btScalar friction, btScalar rollingFriction, bool static_hold, bool magnetic, TriggerData* pushable): btRigidBody([&](){
		ObjectMotionState* state = new ObjectMotionState(btTransform(rot, pos), 0);
		btVector3 inertia = btVector3(0,0,0);
		shape->calculateLocalInertia(mass, inertia);
		btRigidBody::btRigidBodyConstructionInfo bodyCI(mass, state, shape, inertia);
		bodyCI.m_restitution = bounce;
		bodyCI.m_friction = friction;
		bodyCI.m_rollingFriction = rollingFriction;
		return bodyCI;
	}()), angular_factor(btVector3(1, 1, 1)), staticed(0), magnetic(magnetic), static_hold(static_hold), pushable(pushable), parent_obj(0), parent_comp(parent), registered(0){
	setSleepingThresholds(SLEEPING_THRESHOLD);
	setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
	activate(true);

	if(getInvMass() == 0){
		setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );
		setActivationState(DISABLE_DEACTIVATION);
	}
	if(magnetic)addMagneticObj(this);
}

BodyInterface::BodyInterface(BodyInterface& o):btRigidBody([&](){
		ObjectMotionState* state = new ObjectMotionState((ObjectMotionState*)o.getMotionState());
		float mass = o.getInvMass();
		mass = mass == 0 ?0:1./mass;
		btCollisionShape *shape = o.getCollisionShape();
		btVector3 inertia = btVector3(0,0,0);
		shape->calculateLocalInertia(mass, inertia);
		btRigidBody::btRigidBodyConstructionInfo bodyCI(mass, state, shape, inertia);
		bodyCI.m_restitution = o.getRestitution();
		bodyCI.m_friction = o.getFriction();
		bodyCI.m_rollingFriction = o.getRollingFriction();
		return bodyCI;
	}()), angular_factor(btVector3(1, 1, 1)), staticed(0), magnetic(o.magnetic), static_hold(o.static_hold), parent_obj(o.parent_obj), parent_comp(o.parent_comp), registered(0){
	if(!o.pushable)pushable = 0;
	else{
		pushable = new TriggerData(*o.pushable);
		triggerRedirectFind(pushable->tCall, pushable);
	}

	setCollisionFlags(o.getCollisionFlags());
	setSleepingThresholds(SLEEPING_THRESHOLD);
	activate(true);

	if(getInvMass() == 0){
		setActivationState(DISABLE_DEACTIVATION);
	}else if(parent_obj) mvableObjs[this] = parent_obj;

	regist(o.registered);
	if(magnetic)addMagneticObj(this);
}

BodyInterface::~BodyInterface(){
	if(magnetic)remMagneticObj(this);
	deregister();
	delete getMotionState();

	auto it = mvableObjs.find(this);
	if(it != mvableObjs.end())mvableObjs.erase(it);
}

void BodyInterface::enregister(){
	if(!registered){
		if(parent_obj)world_interface->add_rigid(this);
		if(parent_comp)world_interface->add_rigid(this);
		registered = 1;
	}
}

void BodyInterface::deregister(){
	if(registered){
		world_interface->remove_rigid(this);
		registered = 0;
	}
}
void BodyInterface::regist(bool reg){
	if(reg!=registered){
		if(reg)enregister();
		else deregister();
	}

	if(reg)set_static(statical || static_hold);
}

void BodyInterface::set_static(bool set){
	//if(set == staticed)return;
	if(set){
		setLinearVelocity(btVector3(0,0,0));
		setAngularVelocity(btVector3(0,0,0));
		setGravity(btVector3(btVector3(0,0,0)));
		setLinearFactor(btVector3(0,0,0));
		setAngularFactor(btVector3(0,0,0));
		staticed = 1;
	}else{
		setGravity(GRAVACC);
		setLinearFactor(btVector3(1,1,1));
		setAngularFactor(angular_factor);
		activate(true);
		staticed = 0;
	}
}

BodyInterface* BodyInterface::set_angular_factor(btVector3 factor){
	angular_factor = factor;
	if(!staticed)setAngularFactor(angular_factor);
	return this;
}




void BodyInterface::requestTriggers(){
	if(pushable)pushable->requestTrigger();
}



map<const btCollisionObject*, ObjectComponent*> coll_ghosts;

Object *getObjColl(const btCollisionObject *collObj, int bitColl){
	BodyInterface* body = (BodyInterface*)collObj;
	const auto it = coll_ghosts.find(collObj);
	if((bitColl & 1) && it==coll_ghosts.end() && body->get_parent_obj())return body->get_parent_obj();
	if(bitColl & 2){
		if(body->get_parent_comp())return body->get_parent_comp()->getParent();
		if(it != coll_ghosts.end())return it->second->getParent();
	}
	return 0;
}
