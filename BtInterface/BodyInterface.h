#pragma once
#include "includeBullet.h"

class Object;
class ObjectComponent;
class TriggerData;

class BodyInterface: public btRigidBody{
public:
	BodyInterface(btCollisionShape *shape, Object *parent, btQuaternion rot, btVector3 pos, btScalar mass, btScalar bounce, btScalar friction, btScalar rollingFriction, bool static_hold = 0, bool magnetic = 0, TriggerData* pushable = 0);
	BodyInterface(btCollisionShape *shape, ObjectComponent *parent, btQuaternion rot, btVector3 pos, btScalar mass, btScalar bounce, btScalar friction, btScalar rollingFriction, bool static_hold = 0, bool magnetic = 0, TriggerData* pushable = 0);
	BodyInterface(BodyInterface& o);

	~BodyInterface();

	void enregister();
	void deregister();

	void regist(bool reg);

	Object* get_parent_obj(){return parent_obj;}
	ObjectComponent* get_parent_comp(){return parent_comp;}

	void set_static(bool set);
	bool is_static_hold(){return static_hold;}

	bool is_magnetic(){return magnetic;}

	TriggerData* get_pushable(){return pushable;}

	BodyInterface* set_angular_factor(btVector3 factor);
	void requestTriggers();
private:
	btVector3 angular_factor;



	bool staticed;

	bool magnetic;
	bool static_hold;

	TriggerData* pushable;

	Object *parent_obj;
	ObjectComponent *parent_comp;

	bool registered;
};

extern map<const btCollisionObject*, ObjectComponent*> coll_ghosts;

//1 Object
//2 ObjectComponent
Object *getObjColl(const btCollisionObject *collObj, int bitColl = 0xFFFF);
