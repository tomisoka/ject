#pragma once

#include "../Object.h"
#include "includeBullet.h"
#include "convertVM.h"

class ObjectMotionState: public btMotionState{
public:
	ObjectMotionState(btTransform transform, Object *parent): transform(transform), parent(parent){}
	ObjectMotionState(ObjectMotionState* o): transform(o->transform), parent(o->parent){}
	~ObjectMotionState(){}

	void getWorldTransform(btTransform &out) const override{
		out = transform;
	}

	void setWorldTransform(const btTransform &in) override{
		transform = in;

		/*if(parent){
			parent->setPos(Vec3(in.getOrigin()));
			parent->setRot(Quat(in.getRotation()));
			cout << parent << parent->getPos() << parent->getRot() << endl;
		}*/
	}

protected:
	btTransform transform;

	Object *parent;
};
