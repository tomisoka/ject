#include "Shape.h"
#include "../simplefc/std.h"
#include "../simplefc/basicFcs.h"
#include "convertVM.h"

void saveShape(btCollisionShape *sh, xml_node shape){
	xml_attribute type = shape.append_attribute("Type");

	btBoxShape *shapeB;
	btCylinderShape *shapeC;
	btConvexHullShape *shapeH;
	btSphereShape *shapeS;

	int tp = sh->getShapeType();
	if(tp == BOX_SHAPE_PROXYTYPE){
		type.set_value("Box");
		shapeB = (btBoxShape*)sh;
		btVector3 sz = shapeB->getHalfExtentsWithMargin();

		xmlWVec3(shape, "size", Vec3(sz));
	}else if(tp == CYLINDER_SHAPE_PROXYTYPE){
		type.set_value("Cylinder");
		shapeC = (btCylinderShape*)sh;
		btVector3 sz = shapeC->getHalfExtentsWithMargin();

		xmlWVec3(shape, "size", Vec3(sz));
	}else if(tp == CONVEX_HULL_SHAPE_PROXYTYPE){
		type.set_value("ConvexHull");
		shapeH = (btConvexHullShape*)sh;

		int num = shapeH->getNumPoints();
		btVector3 *points = shapeH->getUnscaledPoints();

		for(int i=0;i<num;++i){
			xmlWVec3(shape, "p", Vec3(points[i]));
		}

	}else if(tp == SPHERE_SHAPE_PROXYTYPE){
		type.set_value("Sphere");
		shapeS = (btSphereShape*)sh;

		xmlWFloat(shape, "radius", shapeS->getRadius());
	}
}

map<float, btSphereShape *> shSpheres;
map<vec3, btBoxShape *> shBoxes;
map<vec3, btCylinderShape *> shCylinders;

vector<btConvexHullShape*> shConHull;

void clearBtShapes(){
	for(auto it = shSpheres.begin(); it != shSpheres.end(); it++)delete it->second;
	shSpheres.clear();
	for(auto it = shBoxes.begin(); it != shBoxes.end(); it++)delete it->second;
	shBoxes.clear();
	for(auto it = shCylinders.begin(); it != shCylinders.end(); it++)delete it->second;
	shCylinders.clear();
	for(auto sh: shConHull)delete sh;
	shConHull.clear();
}

btCollisionShape *loadShape(xml_node shape){
	const char * shType = shape.attribute("Type").as_string();

	vec3 size = vec3(1);
	if(!strcmp(shType, "Sphere")){
		float radius = shape.child("radius").text().as_float();
		auto it = shSpheres.find(radius);
		if(it != shSpheres.end())return it->second;
		else{
			btSphereShape *sh = new btSphereShape(radius);
			shSpheres[radius] = sh;
			return sh;
		}
	}else if(!strcmp(shType, "Box")){
		xml_node sz = shape.child("size");
		xml_node sc = shape.child("scale3");
		if(sz && strToVec(sz.text().as_string(), size))size = vec3(1);
		if(sc && strToVec(sc.text().as_string(), size))size = vec3(1);
		auto it = shBoxes.find(size);
		if(it != shBoxes.end())return it->second;
		else{
			btBoxShape *sh = new btBoxShape(btVec3(size));
			shBoxes[size] = sh;
			return sh;
		}
	}else if(!strcmp(shType, "Cylinder")){
		xml_node sz = shape.child("size");
		xml_node sc = shape.child("scale3");
		if(sz && strToVec(sz.text().as_string(), size))size = vec3(1);
		if(sc && strToVec(sc.text().as_string(), size))size = vec3(1);
		auto it = shCylinders.find(size);
		if(it != shCylinders.end())return it->second;
		else{
			btCylinderShape *sh = new btCylinderShape(btVec3(size));
			shCylinders[size] = sh;
			return sh;
		}
	}else if(!strcmp(shType, "ConvexHull")){
		btConvexHullShape *conHul = new btConvexHullShape();
		vec3 p;
		for(xml_node point: shape.children("p")){
			if(strToVec(point.text().as_string(), p))continue;
			conHul->addPoint(btVec3(p));
		}
		for(xml_node point: shape.children("point")){
			if(strToVec(point.text().as_string(), p))continue;
			conHul->addPoint(btVec3(p));
		}
		shConHull.push_back(conHul);
		return conHul;
	}
	return 0;
}
