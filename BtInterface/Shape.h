#pragma once
#include "../simplefc/xml.h"

class btCollisionShape;

void saveShape(btCollisionShape *sh, xml_node shape);
btCollisionShape *loadShape(xml_node shape);

void clearBtShapes();
