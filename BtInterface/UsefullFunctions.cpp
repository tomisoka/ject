#include "UsefullFunctions.h"
#include "../simplefc/render.h"
#include "../simplefc/std.h"

uint_t ShapeVisualization(size_t& len, btCollisionShape *sh){
	vector<vec3> vertices;

	int tp = sh->getShapeType();
	if(tp == BOX_SHAPE_PROXYTYPE){
		btBoxShape *shapeB = (btBoxShape*)sh;
		btVector3 sz = shapeB->getHalfExtentsWithMargin();

		vertices.push_back(vec3(sz.getX(),sz.getY(),sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),sz.getY(),sz.getZ()));

		vertices.push_back(vec3(sz.getX(),sz.getY(),sz.getZ()));
		vertices.push_back(vec3(sz.getX(),-sz.getY(),sz.getZ()));

		vertices.push_back(vec3(sz.getX(),sz.getY(),sz.getZ()));
		vertices.push_back(vec3(sz.getX(),sz.getY(),-sz.getZ()));



		vertices.push_back(vec3(-sz.getX(),sz.getY(),sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),-sz.getY(),sz.getZ()));

		vertices.push_back(vec3(-sz.getX(),sz.getY(),sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),sz.getY(),-sz.getZ()));



		vertices.push_back(vec3(sz.getX(),-sz.getY(),sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),-sz.getY(),sz.getZ()));

		vertices.push_back(vec3(sz.getX(),-sz.getY(),sz.getZ()));
		vertices.push_back(vec3(sz.getX(),-sz.getY(),-sz.getZ()));



		vertices.push_back(vec3(sz.getX(),sz.getY(),-sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),sz.getY(),-sz.getZ()));

		vertices.push_back(vec3(sz.getX(),sz.getY(),-sz.getZ()));
		vertices.push_back(vec3(sz.getX(),-sz.getY(),-sz.getZ()));



		vertices.push_back(vec3(sz.getX(),-sz.getY(),-sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),-sz.getY(),-sz.getZ()));

		vertices.push_back(vec3(-sz.getX(),sz.getY(),-sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),-sz.getY(),-sz.getZ()));

		vertices.push_back(vec3(-sz.getX(),-sz.getY(),sz.getZ()));
		vertices.push_back(vec3(-sz.getX(),-sz.getY(),-sz.getZ()));

	}else if(tp == CYLINDER_SHAPE_PROXYTYPE){
		btCylinderShape *shapeC = (btCylinderShape*)sh;
		btVector3 sz = shapeC->getHalfExtentsWithMargin();

		for(size_t i=0;i<16;++i){
			float x1, x2;
			float z1, z2;
			float angle1 = i*M_PI/8.f, angle2 = (i+1)*M_PI/8.f;
			x1 = sin(angle1)*sz.getX();
			x2 = sin(angle2)*sz.getX();
			z1 = cos(angle1)*sz.getZ();
			z2 = cos(angle2)*sz.getZ();

			vertices.push_back(vec3(x1, sz.getY(), z1));
			vertices.push_back(vec3(x2, sz.getY(), z2));

			vertices.push_back(vec3(x1, -sz.getY(), z1));
			vertices.push_back(vec3(x2, -sz.getY(), z2));

			vertices.push_back(vec3(x1, sz.getY(), z1));
			vertices.push_back(vec3(x1, -sz.getY(), z1));
		}
	}else if(tp == SPHERE_SHAPE_PROXYTYPE){
		btSphereShape *shapeS = (btSphereShape*)sh;
		float r = shapeS->getRadius();

		for(size_t i=0;i<12;++i){
			float x1, x2;
			float z1, z2;
			float angle1 = i*M_PI/6.f, angle2 = (i+1)*M_PI/6.f;
			x1 = sin(angle1)*r;
			x2 = sin(angle2)*r;
			z1 = cos(angle1)*r;
			z2 = cos(angle2)*r;

			for(size_t j=0;j<6;++j){
				float angle3 = j/6.f*M_PI, angle4 = (j+1)/6.f*M_PI;
				float y1 = cos(angle3)*r;
				float y2 = cos(angle4)*r;


				vertices.push_back(vec3(x1*sin(angle3), y1, z1*sin(angle3)));
				vertices.push_back(vec3(x2*sin(angle3), y1, z2*sin(angle3)));

				vertices.push_back(vec3(x1*sin(angle3), y1, z1*sin(angle3)));
				vertices.push_back(vec3(x1*sin(angle4), y2, z1*sin(angle4)));
			}
		}
	}
	len = vertices.size();
	return render::getBuffer(vertices);
}

void GhostOverlap(btPairCachingGhostObject* ghost, function<bool(const btManifoldPoint&, const btCollisionObject *o, bool)> act, vector<const btCollisionObject*> filter, bool filterOnlyORIgnore, bool ignoreStatic, function<bool(const btCollisionObject*)> check){
	btManifoldArray manifoldArray;
	btBroadphasePairArray& collisionPairs = ghost->getOverlappingPairCache()->getOverlappingPairArray();
	int numObjects = collisionPairs.size();
	for (int i = 0; i < numObjects; ++i){
		manifoldArray.clear();
		const btBroadphasePair& pair = collisionPairs[i];

		btBroadphasePair* collisionPair = dynamicsWorld->getPairCache()->findPair(pair.m_pProxy0,pair.m_pProxy1);

		if (!collisionPair) continue;

		if (collisionPair->m_algorithm)
			collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);
		for (int j=0;j<manifoldArray.size();j++){
			btPersistentManifold* manifold = manifoldArray[j];

			bool isFirstBody = manifold->getBody0() == ghost;
			const btCollisionObject *other = isFirstBody?manifold->getBody1():manifold->getBody0();
			if(filterOnlyORIgnore && find(filter.begin(), filter.end(), other) != filter.end())continue;
			if(!filterOnlyORIgnore && filter.size() && find(filter.begin(), filter.end(),other) == filter.end())continue;
			if(ignoreStatic && other->isStaticObject())continue;

			if(check(other))continue;

			for (int p = 0; p < manifold->getNumContacts(); ++p){
				const btManifoldPoint& pt = manifold->getContactPoint(p);

				if (pt.getDistance() < 0.f){
					int ret = act(pt, other, isFirstBody);
					if(ret == 2)break;
					if(ret == 1)return;
				}
			}
		}
	}
}
