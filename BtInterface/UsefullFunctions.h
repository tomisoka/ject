#pragma once
#include "BodyInterface.h"

uint_t ShapeVisualization(size_t& len, btCollisionShape *sh);

void GhostOverlap(btPairCachingGhostObject* ghost, function<bool(const btManifoldPoint&, const btCollisionObject *o, bool)> act, vector<const btCollisionObject*> filter = {}, bool filterOnlyORIgnore = 1, bool ignoreStatic = 0, function<bool(const btCollisionObject*)> check = [](const btCollisionObject*){return 0;});
