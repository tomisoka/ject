#include "WorldInterface.h"
#include "BodyInterface.h"
#include "convertVM.h"
#include "../simplefc/settings.h"
#include "../Object.h"

struct WorldInterface::internals{
	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btGhostPairCallback *ghostCallback;

	btDiscreteDynamicsWorld* dynamicsWorld;
};

WorldInterface::WorldInterface():m(new internals()){
	m->broadphase = new btDbvtBroadphase();
	m->collisionConfiguration = new btDefaultCollisionConfiguration();
	m->dispatcher = new btCollisionDispatcher(m->collisionConfiguration);
	m->solver = new btSequentialImpulseConstraintSolver();
	m->dynamicsWorld = new btDiscreteDynamicsWorld(m->dispatcher, m->broadphase, m->solver, m->collisionConfiguration);
	m->dynamicsWorld->setGravity(GRAVACC);
	dynamicsWorld = m->dynamicsWorld;
	m->ghostCallback = new btGhostPairCallback();
	m->broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(m->ghostCallback);
}

WorldInterface::~WorldInterface(){
	delete m->dynamicsWorld;
	delete m->solver;
	delete m->dispatcher;
	delete m->collisionConfiguration;
	delete m->broadphase;
	delete m->ghostCallback;

	delete m;
}

void WorldInterface::add_rigid(btRigidBody* b){
	dynamicsWorld->addRigidBody(b);
}

void WorldInterface::remove_rigid(btRigidBody *b){
	dynamicsWorld->removeRigidBody(b);
}

void WorldInterface::add_ghost(btCollisionObject* g, ObjectComponent *comp){
	m->dynamicsWorld->addCollisionObject(g,btBroadphaseProxy::SensorTrigger,btBroadphaseProxy::AllFilter & ~btBroadphaseProxy::SensorTrigger);
	if(comp)coll_ghosts[g] = comp;
}

void WorldInterface::remove_ghost(btCollisionObject *obj){
	m->dynamicsWorld->removeCollisionObject(obj);
	coll_ghosts.erase(obj);
}

void WorldInterface::step(){
	m->dynamicsWorld->stepSimulation(TICK_TIME, 8, TICK_TIME);
}

bool WorldInterface::is_inside(vec3 pos, uint16_t filter_mask){
	return isInside(dynamicsWorld, btVec3(pos), filter_mask);
}

const btCollisionObject* WorldInterface::closest_ray_test(vec3 from, vec3 to, vec3& hit, vec3& normal){
	btCollisionWorld::ClosestRayResultCallback res(btVec3(from), btVec3(to));
	m->dynamicsWorld->rayTest(btVec3(from), btVec3(to), res);
	hit = res.hasHit()?Vec3(res.m_hitPointWorld):vec3(0);
	normal = res.hasHit()?Vec3(res.m_hitNormalWorld):vec3(0);
	return res.hasHit()?res.m_collisionObject:0;
}

vector<const btCollisionObject*> WorldInterface::all_ray_test(vec3 from, vec3 to, vector<vec3>& hit, vector<vec3>& normal){
	btCollisionWorld::AllHitsRayResultCallback res(btVec3(from), btVec3(to));
	m->dynamicsWorld->rayTest(btVec3(from), btVec3(to), res);
	vector<const btCollisionObject*> ret;
	for(int i = 0; i < res.m_hitPointWorld.size(); i++){
		hit.push_back(Vec3(res.m_hitPointWorld[i]));
		normal.push_back(Vec3(res.m_hitNormalWorld[i]));
		ret.push_back(res.m_collisionObjects[i]);
	}
	return ret;
}





WorldInterface* world_interface;
