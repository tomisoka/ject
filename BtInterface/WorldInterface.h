#pragma once
#include "../simplefc/std.h"

class btCollisionObject;
class ObjectComponent;
class Object;
class btRigidBody;

class WorldInterface{
public:
	WorldInterface();
	~WorldInterface();

	void add_rigid(btRigidBody* b);
	void remove_rigid(btRigidBody *b);

	void add_ghost(btCollisionObject* g, ObjectComponent* comp);
	void remove_ghost(btCollisionObject *g);

	bool is_inside(vec3 pos, uint16_t filter_mask);

	const btCollisionObject* closest_ray_test(vec3 from, vec3 to, vec3& hit, vec3& normal);
	vector<const btCollisionObject*> all_ray_test(vec3 from, vec3 to, vector<vec3>& hit, vector<vec3>& normal);

	void step();
private:
	struct internals;
	internals* m;
};

extern WorldInterface* world_interface;
