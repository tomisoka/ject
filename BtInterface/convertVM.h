#pragma once

#include "../simplefc/mathlib.h"
#include "includeBullet.h"

inline vec3 Vec3(btVector3 old){
	return vec3(old.getX(), old.getY(), old.getZ());
}
inline btVector3 btVec3(vec3 old){
	return btVector3(old.getX(), old.getY(), old.getZ());
}
inline quat Quat(btQuaternion old){
	return quat(old.getX(), old.getY(), old.getZ(), old.getW());
}
inline btQuaternion btQuat(quat old){
	return btQuaternion(old.getX(), old.getY(), old.getZ(), old.getW());
}
inline btTransform btTrans(mat4 old){
	return btTransform(btQuat(quat(old)), btVec3(old.getPos()));
}
inline mat4 Mat4(btTransform old){
	return Vec3(old.getOrigin()).toTransM() * Quat(old.getRotation()).toRotMatrix();
}
