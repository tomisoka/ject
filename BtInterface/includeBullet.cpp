#include "includeBullet.h"
#include <time.h>
#include "../simplefc/sounds.h"
#include "convertVM.h"
#include "BodyInterface.h"

btDiscreteDynamicsWorld* dynamicsWorld;

btVector3 normalRadA(btVector3 old){
	if(old.getX() < -M_PI)old.setX(old.getX()+2*M_PI);
	if(old.getY() < -M_PI)old.setY(old.getY()+2*M_PI);
	if(old.getZ() < -M_PI)old.setZ(old.getZ()+2*M_PI);
	if(old.getX() > +M_PI)old.setX(old.getX()-2*M_PI);
	if(old.getY() > +M_PI)old.setY(old.getY()-2*M_PI);
	if(old.getZ() > +M_PI)old.setZ(old.getZ()-2*M_PI);
	return old;
}


struct CollideContactResultCallback : public btCollisionWorld::ContactResultCallback{
	CollideContactResultCallback() : coll(0){}

	btScalar addSingleResult(btManifoldPoint& cp,
		const btCollisionObjectWrapper* colObj0Wrap,
		int partId0,
		int index0,
		const btCollisionObjectWrapper* colObj1Wrap,
		int partId1,
		int index1){
		coll = 1;
		return 0;
	}

	bool coll;
	bool colled(){return coll;}
};

btRigidBody *pointSphere = new BodyInterface(new btSphereShape(0), (Object*)0, btQuaternion(0,0,0,1), btVector3(0,0,0), 0, 0, 0, 0);

bool isInside(btDiscreteDynamicsWorld *world, btVector3 in, uint16_t filter_mask){
	CollideContactResultCallback callback = CollideContactResultCallback();
	callback.m_collisionFilterMask = filter_mask;
	pointSphere->setWorldTransform(btTransform(btQuaternion(0,0,0,1), in));
	world->contactTest(pointSphere, callback);
	return callback.colled();
}



const btCollisionObject *closestObjectIgnore(btVector3 actPos, btVector3 dirPos, int flags, btVector3 &hit, btVector3 &hitNormal){

	btCollisionWorld::AllHitsRayResultCallback res(actPos, dirPos);
	dynamicsWorld->rayTest(actPos, dirPos, res);


	int i;
	int Gind = 0;
	float dist = FLT_MAX;

	for(i=0;i<res.m_collisionObjects.size();++i){
		if(res.m_collisionObjects[i]->getInternalType() & flags){
			btVector3 a = res.m_hitPointWorld[i];

			float len = (actPos - a).length2();
			if(len<dist){
				Gind = i;
				dist = len;
			}
		}
	}

	if(dist == FLT_MAX){
		hit = btVector3(0,0,0);
		hitNormal = btVector3(0,0,0);
		return 0;
	}
	hit = res.m_hitPointWorld[Gind];
	hitNormal = res.m_hitNormalWorld[Gind];
	return res.m_collisionObjects[Gind];
}









//extern ContactProcessedCallback gContactProcessedCallback;
vector<btRigidBody *> SoundIgnoreObjs;
bool HandleContacts(btManifoldPoint& point, btCollisionObject* body0, btCollisionObject* body1) {
	if(!(body0->getInternalType() & 2) || !(body1->getInternalType() & 2)){
		return 1;
	}

	btRigidBody* rigidbody0 = dynamic_cast<btRigidBody*>(body0);
	btRigidBody* rigidbody1 = dynamic_cast<btRigidBody*>(body1);


	if(rigidbody0 && rigidbody1){
		if(find(SoundIgnoreObjs.begin(), SoundIgnoreObjs.end(), rigidbody0) != SoundIgnoreObjs.end())return false;
		if(find(SoundIgnoreObjs.begin(), SoundIgnoreObjs.end(), rigidbody1) != SoundIgnoreObjs.end())return false;

		btVector3 diffVel = rigidbody0->getLinearVelocity() - rigidbody1->getLinearVelocity();
		btVector3 diffRot = rigidbody0->getAngularVelocity() - rigidbody1->getAngularVelocity();
		diffRot = normalRadA(diffRot);

		if(diffVel.length2() > 1/* || diffRot.length2() > 0.2f*/){
			addCollisionSound(collisionID(rigidbody0, rigidbody1), diffVel.length2(), Vec3(point.getPositionWorldOnA()));
		}
	}
	return false;
}
