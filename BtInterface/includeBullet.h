#pragma once
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletSoftBody/btDefaultSoftBodySolver.h>
#include "../simplefc/std.h"

extern btDiscreteDynamicsWorld* dynamicsWorld;

bool isInside(btDiscreteDynamicsWorld *world, btVector3 in, uint16_t filter_mask);

const btCollisionObject *closestObjectIgnore(btVector3 actPos, btVector3 dirPos, int flags, btVector3 &hit, btVector3 &hitNormal);

extern vector<btRigidBody *> SoundIgnoreObjs;
bool HandleContacts(btManifoldPoint& point, btCollisionObject* body0, btCollisionObject* body1);

const btTransform nullBtTransform = btTransform(btQuaternion(0,0,0,1), btVector3(0,0,0));
