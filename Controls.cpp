#include "Controls.h"
#include "simplefc/xml.h"
#include <GLFW/glfw3.h>

using std::to_string;

namespace CTRL{
size_t changing_ctrls = 0;

const int _default[LEN] = {
	'W',
	'A',
	'S',
	'D',
	GLFW_KEY_SPACE,
	'J',
	'L',
	'I',
	'K',
	'U',
	'O',
	'E',

	GLFW_KEY_LEFT_SHIFT,
	'I',
	'K',
	'J',
	'L',
	'U',
	'O',

	'C',
	'X',
	'V',
	'Z',

	'X',
	'Y',
	'Z',
	'V',
	'B',

	'C',
	'G',
	'H',
	'F',
	'.',
	',',

	'N',
	'P',
	'P',
	'T',

	'R',
};

int set[LEN];

const string name[LEN] = {
	"move_forward",
	"move_left",
	"move_back",
	"move_right",
	"jump",
	"rotate_pitch_m",
	"rotate_pitch_p",
	"rotate_roll_m",
	"rotate_roll_p",
	"rotate_yaw_m",
	"rotate_yaw_p",
	"pick_up",

	"ignore_objects",
	"object_up",
	"object_down",
	"object_left",
	"object_right",
	"object_forward",
	"object_back",

	"copy",
	"cut",
	"paste",
	"undo",

	"axis_x",
	"axis_y",
	"axis_z",
	"axis_v",
	"axis_b",

	"create_delete",
	"show_grid",
	"show_ghosts",
	"toggle_glass",
	"rotate_m",
	"rotate_p",

	"playtest",
	"playtest_on_place",
	"set_spawn",
	"teleport",

	"focus_un",
};

const string name_gui[LEN] = {
	"Move forward",
	"Move left",
	"Move back",
	"Move right",
	"Jump",
	"Rotate pitch -",
	"Rotate pitch +",
	"Rotate roll -",
	"Rotate roll +",
	"Rotate yaw -",
	"Rotate yaw +",
	"Pick up",

	"Ignore objects to build cube",
	"Object up",
	"Object down",
	"Object left",
	"Object right",
	"Object forward",
	"Object back",

	"(CTRL) + Copy",
	"(CTRL) + Cut",
	"(CTRL) + Paste",
	"(CTRL) + Undo / (CTRL+SHIFT) + Redo",

	"Axis X lock",
	"Axis Y lock",
	"Axis Z lock",
	"Axis unlock",
	"Axis block",

	"Create/Delete",
	"Show grid",
	"Show ghosts",
	"Toggle glass",
	"Rotate -",
	"Rotate +",

	"Playtest",
	"Playtest on place",
	"Set spawn (in playtest)",
	"Teleport",

	"Focus/Unfocus",
};
const string name_categories_save[CATEGORIES_LEN] = {
	"movement",
	"editing",
	"miscellaneous",
};
const string name_categories[CATEGORIES_LEN] = {
	"Movement",
	"Editing",
	"Miscellaneous",
};
const size_t categories_starts[CATEGORIES_LEN] = {
	0,
	12,
	38,
};

void reset(){
	for(size_t i = 0; i < LEN; i++){
		set[i] = _default[i];
	}
}
void reset_key(size_t ID){
	set[ID] = _default[ID];
}

bool is_diff_key(size_t ID){
	return set[ID] != _default[ID];
}

void load(xml_node controls){
	for(size_t i = 0; i < LEN; i++){
		set[i] = controls ? xmlRInt(controls, name[i].c_str(), _default[i]) : _default[i];
	}
	for(xml_node child: controls.children()){
		for(size_t i = 0; i < LEN; i++){
			set[i] = controls ? xmlRInt(child, name[i].c_str(), set[i]) : set[i];
		}
	}
}
void save(xml_node controls){
	xml_node act_save = controls;
	size_t next_category = 0;
	for(size_t i = 0; i < LEN; i++){
		if(next_category < CATEGORIES_LEN && i == categories_starts[next_category]){
			act_save = controls.append_child(name_categories_save[next_category].c_str());
			next_category++;
		}
		xmlWInt(act_save, name[i].c_str(), set[i]);
	}
}

string get_key_name(int key){
	if(key > 0x20 && key < 0x7F)return string(1, (char)key);
	else if(key == 0x20)return "space";
	else if(key == -1)return "unknown";
	else if(key == 161)return "WORLD_1";
	else if(key == 162)return "WORLD_2";
	else if(key == 256)return "escape";
	else if(key == 257)return "enter";
	else if(key == 258)return "tab";
	else if(key == 259)return "backspace";
	else if(key == 260)return "insert";
	else if(key == 261)return "delete";
	else if(key == 262)return "right";
	else if(key == 263)return "left";
	else if(key == 264)return "down";
	else if(key == 265)return "up";
	else if(key == 266)return "page up";
	else if(key == 267)return "page down";
	else if(key == 268)return "home";
	else if(key == 269)return "end";
	else if(key == 280)return "caps lock";
	else if(key == 281)return "scroll lock";
	else if(key == 282)return "num lock";
	else if(key == 283)return "print screen";
	else if(key == 284)return "pause";
	else if(key == 290)return "F1";
	else if(key == 291)return "F2";
	else if(key == 292)return "F3";
	else if(key == 293)return "F4";
	else if(key == 294)return "F5";
	else if(key == 295)return "F6";
	else if(key == 296)return "F7";
	else if(key == 297)return "F8";
	else if(key == 298)return "F9";
	else if(key == 299)return "F10";
	else if(key == 300)return "F11";
	else if(key == 301)return "F12";
	else if(key == 302)return "F13";
	else if(key == 303)return "F14";
	else if(key == 304)return "F15";
	else if(key == 305)return "F16";
	else if(key == 306)return "F17";
	else if(key == 307)return "F18";
	else if(key == 308)return "F19";
	else if(key == 309)return "F20";
	else if(key == 310)return "F21";
	else if(key == 311)return "F22";
	else if(key == 312)return "F23";
	else if(key == 313)return "F24";
	else if(key == 314)return "F25";
	else if(key == 320)return "numpad 0";
	else if(key == 321)return "numpad 1";
	else if(key == 322)return "numpad 2";
	else if(key == 323)return "numpad 3";
	else if(key == 324)return "numpad 4";
	else if(key == 325)return "numpad 5";
	else if(key == 326)return "numpad 6";
	else if(key == 327)return "numpad 7";
	else if(key == 328)return "numpad 8";
	else if(key == 329)return "numpad 9";
	else if(key == 330)return "numpad decimal";
	else if(key == 331)return "numpad divide";
	else if(key == 332)return "numpad multiply";
	else if(key == 333)return "numpad subtract";
	else if(key == 334)return "numpad add";
	else if(key == 335)return "numpad enter";
	else if(key == 336)return "numpad equal";
	else if(key == 340)return "left shift";
	else if(key == 341)return "left control";
	else if(key == 342)return "left alt";
	else if(key == 343)return "left super";
	else if(key == 344)return "right shift";
	else if(key == 345)return "right control";
	else if(key == 346)return "right alt";
	else if(key == 347)return "right super";
	else if(key == 348)return "menu";
	else return "key_" + to_string(key);
}
}
