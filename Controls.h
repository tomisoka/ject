#pragma once
#include <string>

using std::string;

namespace pugi{class xml_node;}
using pugi::xml_node;

namespace CTRL{
extern size_t changing_ctrls;

const size_t LEN = 39;
extern const string name_gui[LEN];
extern int set[LEN];

const size_t CATEGORIES_LEN = 3;
extern const string name_categories[CATEGORIES_LEN];
extern const size_t categories_starts[CATEGORIES_LEN];

#define MOVE_FORWARD set[0]
#define MOVE_LEFT set[1]
#define MOVE_BACK set[2]
#define MOVE_RIGHT set[3]
#define JUMP set[4]
#define ROTATE_PITCH_M set[5]
#define ROTATE_PITCH_P set[6]
#define ROTATE_ROLL_M set[7]
#define ROTATE_ROLL_P set[8]
#define ROTATE_YAW_M set[9]
#define ROTATE_YAW_P set[10]
#define PICK_UP set[11]

#define IGNORE_OBJECTS set[12]
#define OBJECT_UP set[13]
#define OBJECT_DOWN set[14]
#define OBJECT_LEFT set[15]
#define OBJECT_RIGHT set[16]
#define OBJECT_FORWARD set[17]
#define OBJECT_BACK set[18]

#define COPY set[19]
#define CUT set[20]
#define PASTE set[21]
#define UNDO set[22]

#define AXIS_X set[23]
#define AXIS_Y set[24]
#define AXIS_Z set[25]
#define AXIS_V set[26]
#define AXIS_B set[27]

#define CREATE_DELETE set[28]
#define SHOW_GRID set[29]
#define SHOW_GHOSTS set[30]
#define TOGGLE_GLASS set[31]
#define ROTATE_M set[32]
#define ROTATE_P set[33]

#define PLAYTEST set[34]
#define PLAYTEST_ON_PLACE set[35]
#define SET_SPAWN set[36]
#define TELEPORT set[37]

#define FOCUS_UN set[38]

void reset();
void reset_key(size_t ID);
bool is_diff_key(size_t ID);

void load(xml_node controls);
void save(xml_node controls);

string get_key_name(int key);
}
