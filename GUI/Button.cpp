#include "Button.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"
#include "../simplefc/TemplateBuffers.h"

Button::Button(vec2 ps, vec2 sz, const MutString text, vector<TriggerData*> callData, function<bool()> can_do, bool border): ObjectComponent(ps, quat(), sz), TriggerCaller(callData), can_do(can_do), border(border){
	vertsBack = template_buffer_rectangle_background;
	vertsBorder = border?template_buffer_rectangle_boundaries:0;

	renderer = new RenderText(text, ps, sz.getY()*0.8, 1);
}
Button::Button(vec2 ps, vec2 sz, ObjectComponent* renderer, vector<TriggerData*> callData, function<bool()> can_do, bool border): ObjectComponent(ps, quat(), sz), TriggerCaller(callData), can_do(can_do), border(border), renderer(renderer){
	vertsBack = template_buffer_rectangle_background;
	vertsBorder = border?template_buffer_rectangle_boundaries:0;
}

Button::~Button(){
	delete renderer;
}

void Button::setParent(Object *parent){
	ObjectComponent::setParent(parent);
	renderer->setParent(parent);
}

bool Button::isIn(){
	return mouse_in_rectangle(getTransform());
}

bool Button::input_mouse(bool acted, int button, int action, int mods){
	if(acted || button!=0 || action!=1 || !getActive())return 0;

	if(isIn() && can_do()){
		callit(0);
		return 1;
	}
	return 0;
}

void Button::render(){
	if(!active)return;

	mat4 trans = getTransform();

	render::set2DColor();
	render::setTrans(trans);
	render::setColor(vec3(can_do()?isIn()?0.95:1:0.75));
	render::setAttrPoint(0, 2, vertsBack, GL_FLOAT);
	render::draw(6);

	if(border){
		render::set2DLine();
		render::setTrans(trans);
		render::setColor(vec3(0));
		render::setAttrPoint(0, 2, vertsBorder, GL_FLOAT);
		render::draw(8);
	}

	renderer->render();
}
