#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include "../simplefc/input.h"
#include "../simplefc/render.h"
#include "RenderText.h"

class Button: public ObjectComponent, public InputCallbacks, public TriggerCaller{
public:
	Button(vec2 ps, vec2 sz, const MutString text, vector<TriggerData *> callData, function<bool()> can_do = [](){return true;}, bool border = 1);
	Button(vec2 ps, vec2 sz, ObjectComponent *renderer, vector<TriggerData *> callData, function<bool()> can_do = [](){return true;}, bool border = 1);
	~Button();

	void render() override;

	void setParent(Object *parent) override;
private:
	function<bool()> can_do;

	bool input_mouse(bool acted, int button, int action, int mods) override;
	bool isIn();

	bool border;

	GLuint vertsBack;
	GLuint vertsBorder;

	ObjectComponent* renderer;
};
