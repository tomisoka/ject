#include "Dragger.h"
#include "../simplefc/window.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"
#include "../simplefc/TemplateBuffers.h"

Dragger::Dragger(vec2 pos, vec2 size, float *output, float min, float max, vec3 color):ObjectComponent(pos, quat(), size), color(color), output(output), min(min), max(max){
	vertBuff = template_buffer_rectangle_background;

	dragPos = (*output - min)/(max-min);

	SZ = scale.getY();
}

Dragger::~Dragger(){}

bool Dragger::checkCursor(){
	return mouse_in_rectangle(getTransform());
}

bool Dragger::input_mouse(bool acted, int button, int action, int mods){
	if(!acted && action && checkCursor() && getActive()){
		mat4 transform = parent->getTransform();

		vec2 M = cursorPos();

		vec2 pos = transform * vec3(this->pos);

		vec2 diff = M-pos;
		dragPos = diff.getX()/(2*(scale.getX()-SZ)) + 0.5;
		if(dragPos > 1)dragPos = 1;
		if(dragPos < 0)dragPos = 0;

		*output = min + dragPos * (max-min);
		return 1;
	}
	return 0;
}

bool Dragger::input_scroll(bool acted, double xOff, double yOff){
	if(!acted && checkCursor() && getActive()){
		dragPos += yOff*0.05;
		if(dragPos > 1)dragPos = 1;
		if(dragPos < 0)dragPos = 0;

		*output = min + dragPos * (max-min);
	}
	return 0;
}

void Dragger::render(){
	if(!active)return;

	mat4 tran = vec3(pos + vec2((dragPos-0.5) * 2 * (scale.getX()-SZ), 0)).toTransM();
	mat4 transform = parent->getTransform()*tran*vec3(SZ).toScaleM();

	render::set2DColor();
	render::setTrans(transform);
	render::setColor(color);

	render::setAttrPoint(0, 2, vertBuff, GL_FLOAT);
	render::draw(6);
}
