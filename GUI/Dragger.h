#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/input.h"
#include "../simplefc/render.h"

class Dragger: public ObjectComponent, public InputCallbacks{
public:
	Dragger(vec2 pos, vec2 size, float *output, float min, float max, vec3 color = vec3(0.7));
	~Dragger();

	void render() override;
private:
	bool input_mouse(bool acted, int button, int action, int mods) override;
	bool input_scroll(bool acted, double xOff, double yOff) override;

	bool checkCursor();

	GLuint vertBuff;

	float dragPos;
	float SZ;

	vec3 color;

	float *output;
	float min, max;
};
