#include "Droplist.h"
#include "Button.h"

Droplist::Droplist(vec2 ps, vec2 sz, vector<string> texts, DroplistCallback *callback, size_t issueID):
		Object(ps, quat(), 1, sz), showed(false), callback(callback), issueID(issueID), actText(0), texts(texts){

	actButton = new Button(vec2(0), vec2(1), this->texts[actText], {new TriggerData(this, 0)});
	addComponent(actButton);

	for(size_t i=0; i < texts.size(); i++){
		Button* btn = new Button(vec2(0,-2*float(i+1)), vec2(1), this->texts[i], {new TriggerData(this, 0x10+i)});
		btn->setActive(0);
		addComponent(btn);
		hidableButtons.push_back(btn);
	}
}
bool Droplist::input_key(bool acted, int key, int action, int mods){
	if(showed)show(false);
	return 0;
}
bool Droplist::input_mouse(bool acted, int button, int action, int mods){
	if(acted || action != 1)return 0;
	if(showed)show(false);
	return 0;
}

void Droplist::resetActButton(){
	actButton->delMe();
	actButton = new Button(vec2(0), vec2(1), texts[actText], {new TriggerData(this, 0)});
	addComponent(actButton);
}

void Droplist::show(bool show){
	showed = show;
	for(ObjectComponent *comp: hidableButtons){
		comp->setActive(show);
	}
}

void Droplist::call(int ButtonID){
	if(ButtonID == 0){
		show(!showed);
	}else{
		actText = ButtonID-0x10;
		resetActButton();
		callback->droplist_callback(issueID, texts[actText]);
		show(false);
	}
}
