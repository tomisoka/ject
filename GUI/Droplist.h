#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include "../simplefc/input.h"

class DroplistCallback{
	friend class Droplist;
	virtual void droplist_callback(size_t issueID, string text){}
};

class Droplist: public Object, public Trigger, public InputCallbacks{
public:
	Droplist(vec2 ps, vec2 sz, vector<string> texts, DroplistCallback *callback, size_t issueID);

	void call(int buttonID) override;
private:
	bool input_key(bool acted, int key, int action, int mods) override;
	bool input_mouse(bool acted, int button, int action, int mods) override;

	void resetActButton();

	bool showed;
	void show(bool show);

	DroplistCallback *callback;
	size_t issueID;

	size_t actText;
	vector<string> texts;

	ObjectComponent *actButton;
	vector<ObjectComponent *> hidableButtons;
};
