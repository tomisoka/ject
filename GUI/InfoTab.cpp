#include "InfoTab.h"
#include "RenderText.h"
#include "../simplefc/TemplateBuffers.h"
#include "Button.h"

InfoTab::InfoTab(vec2 pos): Object(vec3(pos)), sz(0.01){}
InfoTab::~InfoTab(){}

void InfoTab::addText(const MutString& text){
	addComponent(new RenderText(text, vec2(0.01, -sz[1]-0.03), 0.04, 0));
	sz[1] += 0.041;
}

void InfoTab::addButton(const MutString& text, vector<TriggerData *> callData){
	addComponent(new Button(vec2(0.01+0.2, -sz[1]-0.042), vec2(0.2, 0.038), text, callData));
	sz[1] += 0.084;
}

void InfoTab::updateSZX(){
	for(ObjectComponent *comp: components){
		vec2o3 comp_sz = comp->getSize();
		float diff_szX = comp_sz[1].getX() - comp_sz[0].getX();
		if(diff_szX > sz.getX())sz.setX(diff_szX + 0.02);
	}
}

bool InfoTab::input_key(bool acted, int key, int action, int mods){
	if(!acted)delMe();
	return 0;
}
bool InfoTab::input_mouse(bool acted, int button, int action, int mods){
	if(!acted && action == 1)delMe();
	return 0;
}
bool InfoTab::input_scroll(bool acted, double xOff, double yOff){
	if(!acted)delMe();
	return 0;
}
bool InfoTab::input_cursor_pos(bool acted, double xd, double yd){
	return 0;
}

void InfoTab::render(){
	if(!active)return;

	updateSZX();
	mat4 trans = getTransform() * vec3(sz).toScaleM() * vec3(0.5).toScaleM() * vec3(1, -1, 0).toTransM();

	render::set2DColor();
	render::setTrans(trans);
	render::setColor(vec3(1));
	render::setAttrPoint(0, 2, template_buffer_rectangle_background, GL_FLOAT);
	render::draw(6);

	render::set2DLine();
	render::setTrans(trans);
	render::setColor(vec3(0));
	render::setAttrPoint(0, 2, template_buffer_rectangle_boundaries, GL_FLOAT);
	render::draw(8);

	Object::render();
}
