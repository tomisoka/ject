#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/input.h"
#include "../simplefc/render.h"
#include "../simplefc/MutString.h"

class TriggerData;

class InfoTab: public Object, public InputCallbacks{
public:
	InfoTab(vec2 pos);
	~InfoTab();

	void addText(const MutString& text);
	void addButton(const MutString& text, vector<TriggerData *> callData);

	void render() override;
private:
	bool input_key(bool acted, int key, int action, int mods) override;
	bool input_mouse(bool acted, int button, int action, int mods) override;
	bool input_scroll(bool acted, double xOff, double yOff) override;
	bool input_cursor_pos(bool acted, double xd, double yd) override;

	void updateSZX();

	vec2 sz;
};
