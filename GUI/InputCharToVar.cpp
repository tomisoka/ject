#include "InputCharToVar.h"
#include <GLFW/glfw3.h>

void InputCharToVar::call(int state){
	if(state == 0)setActive(1);
	if(!getActive())setActive(0);
}

bool InputCharToVar::input_key(bool acted, int key, int action, int mods){
	if(getActive() && action == 1){
		if(can_do() && key != GLFW_KEY_ESCAPE)*output = key;
		setActive(0);
		callit(0);
		return 1;
	}
	return 0;
}
