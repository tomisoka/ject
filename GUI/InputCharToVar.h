#pragma once
#include "../ObjectComponent.h"
#include "../simplefc/input.h"
#include "../simplefc/trigger.h"

class InputCharToVar: public ObjectComponent, public InputCallbacks, public Trigger, public TriggerCaller{
public:
	InputCharToVar(int* output, function<bool()> can_do = [](){return true;}, vector<TriggerData*> callData = {}): TriggerCaller(callData), can_do(can_do), output(output){setActive(0);}

	void call(int state) override;
private:
	function<bool()> can_do;

	int* output;
	bool input_key(bool acted, int key, int action, int mods) override;
};
