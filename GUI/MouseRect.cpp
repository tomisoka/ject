#include "MouseRect.h"
#include "../simplefc/window.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"

bool MouseRect::input_mouse(bool acted, int button, int action, int mods){
	if(acted || this->button!=button || !action || !parent->getActive())return 0;

	if(mouse_in_rectangle(getTransform())){
		callit(0);
		return 1;
	}
	return 0;
}
