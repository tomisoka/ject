#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include "../simplefc/input.h"

class MouseRect: public ObjectComponent, public InputCallbacks, public TriggerCaller{
public:
	MouseRect(vec2 ps, vec2 sz, vector<TriggerData *> callData = {}, int button=0): ObjectComponent(ps, quat(), sz), TriggerCaller(callData), button(button){}

private:
	bool input_mouse(bool acted, int button, int action, int mods) override;

	int button;
};
