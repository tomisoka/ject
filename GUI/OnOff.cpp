#include "OnOff.h"
#include "../simplefc/window.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"
#include "../simplefc/TemplateBuffers.h"

OnOff::OnOff(vec2 ps, vec2 sz, bool *out): ObjectComponent(ps, quat(), sz), out(out){
	vertsBack = template_buffer_rectangle_background;
	vertsBorder = template_buffer_rectangle_boundaries;
	vertsCross = template_buffer_rectangle_cross;
}

bool OnOff::input_mouse(bool acted, int button, int action, int mods){
	if(acted || button != 0 || action!=1 || !getActive())return 0;

	if(mouse_in_rectangle(getTransform())){
		*out = !(*out);
		return 1;
	}
	return 0;
}

void OnOff::render(){
	if(!active)return;
	mat4 trans = getTransform();

	render::set2DColor();
	render::setTrans(trans);
	render::setColor(vec3(1));
	render::setAttrPoint(0, 2, vertsBack, GL_FLOAT);
	render::draw(6);

	render::set2DLine();
	render::setTrans(trans);
	render::setColor(vec3(0));

	render::setAttrPoint(0, 2, vertsBorder, GL_FLOAT);
	render::draw(8);

	if(*out){
		render::setAttrPoint(0, 2, vertsCross, GL_FLOAT);
		render::draw(4);
	}
}
