#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include "../simplefc/input.h"
#include "../simplefc/render.h"

class OnOff: public ObjectComponent, public InputCallbacks{
public:
	OnOff(vec2 ps, vec2 sz, bool *out);

	void render() override;
private:
	bool input_mouse(bool acted, int button, int action, int mods) override;

	bool *out;

	GLuint vertsBack;
	GLuint vertsBorder;
	GLuint vertsCross;
};
