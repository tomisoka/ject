#include "Render2D.h"
#include "../simplefc/render.h"

Render2D::Render2D(vector<float> vertices, vector<float> uvs, uint32_t textureID, vec3 color, vec2 pos, quat rot, vec2 scale): ObjectComponent(pos, rot, scale), to_delete_buffers(1), vertices_n(vertices.size()/2), vertexbuffer(render::getBuffer(vertices)), colorbuffer(render::getBuffer(uvs)), textureID(textureID), color(color){}

Render2D::Render2D(GLuint vertices, GLuint uvs, size_t vertices_n, uint32_t textureID, vec3 color, vec2 pos, quat rot, vec2 scale): ObjectComponent(pos, rot, scale), to_delete_buffers(0), vertices_n(vertices_n), vertexbuffer(vertices), colorbuffer(uvs), textureID(textureID), color(color){}

Render2D::~Render2D(){
	if(to_delete_buffers){
		render::delBuffer(&vertexbuffer);
		render::delBuffer(&colorbuffer);
	}
}

void Render2D::render(){
	if(!active)return;

	if(textureID){
		render::set2D();
		render::setTexture(textureID);
	}else{
		render::set2DColor();
		render::setColor(color);
	}
	render::setTrans(getTransform());

	render::setAttrPoint(0, 2, vertexbuffer, GL_FLOAT);
	if(textureID)render::setAttrPoint(1, 2, colorbuffer, GL_FLOAT);
	render::draw(vertices_n);
}
