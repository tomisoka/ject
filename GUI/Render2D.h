#pragma once

#include "../ObjectComponent.h"

class Render2D: public ObjectComponent{
public:
	Render2D(vector<float> vertices, vector<float> uvs, uint32_t textureID=0, vec3 color=vec3(1), vec2 pos=vec2(0), quat rot=quat(), vec2 scale = vec2(1));
	Render2D(GLuint vertices, GLuint uvs, size_t vertices_n, uint32_t textureID=0, vec3 color=vec3(1), vec2 pos=vec2(0), quat rot=quat(), vec2 scale = vec2(1));
	~Render2D();

	void render() override;
private:
	bool to_delete_buffers;

	size_t vertices_n;

	GLuint vertexbuffer;
	GLuint colorbuffer;

	uint32_t textureID;

	vec3 color;
};
