#include "RenderLines2D.h"
#include "../simplefc/render.h"

RenderLines2D::RenderLines2D(vector<float> vertices, vec3 color, vec2 pos, quat rot, vec2 scale): ObjectComponent(pos, rot, scale), to_delete_buffers(1), vertices_n(vertices.size()/2), vertexbuffer(render::getBuffer(vertices)), color(color){}

RenderLines2D::RenderLines2D(GLuint vertices, size_t vertices_n, vec3 color, vec2 pos, quat rot, vec2 scale): ObjectComponent(pos, rot, scale), to_delete_buffers(0), vertices_n(vertices_n), vertexbuffer(vertices), color(color){}

RenderLines2D::~RenderLines2D(){
	if(to_delete_buffers)render::delBuffer(&vertexbuffer);
}

void RenderLines2D::render(){
	if(!active)return;

	render::set2DLine();
	render::setTrans(getTransform());
	render::setColor(color);
	render::setAttrPoint(0, 2, vertexbuffer, GL_FLOAT);
	render::draw(vertices_n);
}
