#pragma once
#include "../ObjectComponent.h"

class RenderLines2D:public ObjectComponent{
public:
	RenderLines2D(vector<float> vertices, vec3 color = vec3(0), vec2 pos=vec2(0), quat rot = quat(), vec2 scale = vec2(1));
	RenderLines2D(uint_t vertices, size_t vertices_n, vec3 color = vec3(0), vec2 pos=vec2(0), quat rot = quat(), vec2 scale = vec2(1));
	~RenderLines2D();

	void render() override;
private:
	bool to_delete_buffers;

	size_t vertices_n;

	uint_t vertexbuffer;
	vec3 color;
};
