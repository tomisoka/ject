#include "RenderText.h"
#include "../simplefc/Text.h"
#include "../simplefc/render.h"

RenderText::RenderText(const MutString& s, vec2 pos, float multSiz, bool centred, vec3 col):
		ObjectComponent(pos), str(s), multSiz(multSiz), centred(centred), col(col), vertBuff(0), uvsBuff(0){
}

RenderText::~RenderText(){
	render::delBuffer(&vertBuff);
	render::delBuffer(&uvsBuff);
}

void RenderText::updateString(){
	string new_string = str;
	if(new_string != cur_string){
		if(new_string.size()){
			vector<vec2> verts;
			vector<vec2> uvs;
			w = text->getTextVerts(new_string, verts, uvs);

			vertBuff = render::getBuffer(verts);
			uvsBuff = render::getBuffer(uvs);

			vert_sz = verts.size();
		}else w = 0;

		cur_string = new_string;
	}
}

vec2o3 RenderText::getSize(){
	updateString();
	return centred?vec2o3(vec3(-w*multSiz/2, -multSiz/2, 0), vec3(w*multSiz/2, multSiz/2, 0)):
		vec2o3(vec3(0), vec3(w * multSiz, multSiz, 0));
}

void RenderText::render(){
	if(!active)return;

	updateString();
	if(!cur_string.size())return;

	render::set2D_distance_field();
	render::setTexture(text->getTextureID());

	render::setAttrPoint(0, 2, vertBuff, GL_FLOAT);
	render::setAttrPoint(1, 2, uvsBuff, GL_FLOAT);

	mat4 trans = getTransform() * vec3(multSiz).toScaleM() * vec3(centred?-w/2:0, centred?-1/4.:0, 0).toTransM();
	render::setTrans(trans);
	render::setColor(col);

	render::draw(vert_sz);
}
