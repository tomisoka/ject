#pragma once
#include "../ObjectComponent.h"
#include "../simplefc/MutString.h"
#include "../simplefc/settings.h"

class RenderText: public ObjectComponent{
public:
	RenderText(const MutString& s, vec2 pos, float multSiz = 0.04, bool centred=1, vec3 col = TEXT_COLOR);
	~RenderText();

	void render() override;

	vec2o3 getSize() override;
private:
	MutString str;
	float multSiz;
	bool centred;
	vec3 col;

	GLuint vertBuff;
	GLuint uvsBuff;

	size_t vert_sz;

	void updateString();
	float w;
	string cur_string;
};
