#include "SideBar.h"
#include "../simplefc/window.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"
#include "../simplefc/render.h"
#include "../simplefc/TemplateBuffers.h"

SideBar::SideBar(vec2 minP, vec2 maxP, float MultH, float Winact, float *h, float scroll, bool lefted,
		vec3 colInact, vec3 colAct): colInact(colInact), colAct(colAct), minP(minP), maxP(maxP), MultH(MultH), Winact(Winact), scroll(scroll), lefted(lefted), clicked(0), h(h){
	vertexbuffer = template_buffer_rectangle_background;
}

SideBar::~SideBar(){}

bool SideBar::checkCursor(){
	float diff = maxP.getY()-minP.getY();
	float diffA = maxP.getY()+minP.getY();

	vec2 minFP = minP;
	minFP.setY(diffA/2+(*h)*diff/2*(1-MultH)-diff/2*MultH);
	vec2 maxFP = maxP;
	maxFP.setY(diffA/2+(*h)*diff/2*(1-MultH)+diff/2*MultH);

	return mouse_in_rectangle(parent->getTransform(), minFP, maxFP);
}

bool SideBar::checkCursorAll(){
	vec2 minFP = minP;
	vec2 maxFP = maxP;

	if(lefted){
		maxFP.setX(minP.getX()+scroll);
	}else{
		minFP.setX(maxP.getX()-scroll);
	}

	return mouse_in_rectangle(getTransform(), minFP, maxFP);
}

bool SideBar::input_mouse(bool acted, int button, int action, int mods){
	if(acted || !parent->getActive())return 0;
	if(MultH>=1)return 0;

	vec2 MP = cursorPos();

	if(action==1){
		if(checkCursor()){
			clicked=1;
			oldClick=MP;
			return 1;
		}
	}else if (action==0 && clicked){
		float y = (oldClick.getY()-MP.getY())*(maxP.getY()-minP.getY());
		*h-=y;
		if(*h>1)*h=1;
		if(*h<-1)*h=-1;

		clicked=0;
		return 1;
	}
	return 0;
}
bool SideBar::input_scroll(bool acted, double xOff, double yOff){
	if(acted || !parent->getActive())return 0;
	if(MultH>=1)return 0;

	if(!clicked && checkCursorAll()){
		*h+=MultH*yOff*0.1;
		if(*h>1)*h=1;
		if(*h<-1)*h=-1;
	}
	return 0;
}

void SideBar::update(){
	if(MultH>=1)return;
	if(clicked){
		vec2 MP = cursorPos();

		float y = (oldClick.getY()-MP.getY())*(maxP.getY()-minP.getY());
		*h-=y;
		if(*h>1)*h=1;
		if(*h<-1)*h=-1;

		oldClick=MP;
	}
}

void SideBar::render(){
	if(MultH>=1)return;

	mat4 transform = vec3(lefted?minP.getX():maxP.getX(), minP.getY()/2+maxP.getY()/2+(*h)*(maxP.getY()-minP.getY())/2*(1-MultH), 0).toTransM();
	mat4 scaleMat = vec3((maxP.getX()-minP.getX())*((clicked || checkCursor())?1:Winact),(maxP.getY()-minP.getY())/2.f*MultH, 1).toScaleM();
	transform = getTransform() * transform*scaleMat*quat(vec3(0,0,lefted?M_PI:0)).toRotMatrix();

	render::set2DColor();
	render::setTrans(transform);
	render::setColor(clicked?colInact:colAct);
	render::setAttrPoint(0, 2, vertexbuffer, GL_FLOAT);
	render::draw(6);
}
