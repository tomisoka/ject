#pragma once

#include <GL/glew.h>
#include "../ObjectComponent.h"
#include "../simplefc/input.h"

class SideBar: public ObjectComponent, public InputCallbacks{
public:
	SideBar(vec2 minP, vec2 maxP, float MultH, float Winact, float *h, float scroll, bool lefted = 0, vec3 colInact = vec3(0.7f), vec3 colAct = vec3(0.85f));
	~SideBar();

	void render() override;
	void update() override;

	void setMultH(float multH){MultH = multH;}
private:
	bool input_mouse(bool acted, int button, int action, int mods) override;
	bool input_scroll(bool acted, double xOff, double yOff) override;

	bool checkCursor();
	bool checkCursorAll();

	GLuint vertexbuffer;
	GLuint colorbuffer;

	vec3 colInact, colAct;

	vec2 minP, maxP;

	float MultH;

	float Winact;
	float scroll;

	bool lefted;


	bool clicked;

	vec2 oldClick;

	float *h;
};
