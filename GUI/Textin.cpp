#include "Textin.h"
#include "../simplefc/window.h"
#include "../simplefc/render.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/mouse.h"
#include "../simplefc/Text.h"
#include "../simplefc/TemplateBuffers.h"
#include <GLFW/glfw3.h>

Textin::Textin(vec2 pos, vec2 SZ, TextinCallBack* callback, size_t issueID, function<bool(string)> validator, bool editable): ObjectComponent(pos, quat(), SZ), validator(validator), is_valid(true), insertI(0), insertP(0), insertRP(0), is_edit(false), editable(editable), callback(callback), issueID(issueID){
	width = SZ.getX()/SZ.getY();
	inputS.push_back(0);
}

Textin::~Textin(){
	clearInput();
}

void Textin::clearInput(){
	inputS.clear();
	inputSZ.clear();
	inputS.push_back(0);

	insertI=0;
	insertP=0;
	insertRP=0;
	is_valid = true;
}

void Textin::render(){
	mat4 transform = getTransform();

	render::set2DColor();
	render::setTrans(transform);
	render::setColor(is_valid?vec3(is_edit?1:0.9):vec3(1, 0.75, 0.75));
	render::setAttrPoint(0, 2, template_buffer_rectangle_background, GL_FLOAT);
	render::draw(6);

	render::set2DLine();
	render::setTrans(transform);
	render::setColor(vec3(0));
	render::setAttrPoint(0, 2, template_buffer_rectangle_boundaries, GL_FLOAT);
	render::draw(8);



	render::set2D_distance_field();
	render::setTexture(text->getTextureID());
	render::setColor(TEXT_COLOR);

	if(inputSZ.size()){
		vector<vec2> verts;
		vector<vec2> uvs;
		float w = text->getTextVerts(&inputS[0], verts, uvs);

		if(width > w){
			render::setTrans(transform * vec3(-1, 0, 0).toTransM() * vec3(1/width, 1.8, 0).toScaleM() * vec3(0, -1/4., 0).toTransM());
			GLuint vertBuff = render::getBuffer(verts);
			GLuint uvsBuff = render::getBuffer(uvs);

			render::setAttrPoint(0, 2, vertBuff, GL_FLOAT);
			render::setAttrPoint(1, 2, uvsBuff, GL_FLOAT);
			render::draw(verts.size());
			render::delBuffer(&vertBuff);
			render::delBuffer(&uvsBuff);
		}else{
			render::setTrans(transform * vec3(-1, 0, 0).toTransM() * vec3(1/width, 1.8, 0).toScaleM() * vec3(-insertRP+insertP, -1/4., 0).toTransM());
			{
				auto it_low = lower_bound(verts.begin(), verts.end(), vec2((insertRP-insertP)*(width/2 - 1), 0));
				size_t diff = it_low - verts.begin();
				size_t n_del = (diff)/6*6;
				verts.erase(verts.begin(), verts.begin() + n_del);
				uvs.erase(uvs.begin(), uvs.begin() + n_del);
				if(diff%6){
					float min = verts[0].getX();
					float max = verts[5].getX();
					float minUV = uvs[0].getX();
					float maxUV = uvs[5].getX();
					float goal = (insertRP-insertP)*(width/2 - 1);
					float set_uvs = minUV + (goal - min)*(maxUV-minUV)/(max-min);
					for(size_t i = 0; i < 6; i++){
						if(verts[i].getX() < goal){
							verts[i].setX(goal);
							uvs[i].setX(set_uvs);
						}
					}
				}
			}
			{
				auto it_upp = upper_bound(verts.begin(), verts.end(), vec2((insertRP-insertP)*(width/2 - 1) + width*2, 0));
				size_t diff = verts.end() - it_upp;
				size_t n_del = (diff)/6*6;
				verts.erase(verts.end()-n_del, verts.end());
				uvs.erase(uvs.end()-n_del, uvs.end());
				if(diff%6){
					float min = verts[verts.size()-6].getX();
					float max = verts[verts.size()-1].getX();
					float minUV = uvs[uvs.size()-6].getX();
					float maxUV = uvs[uvs.size()-1].getX();
					float goal = (insertRP-insertP)*(width/2 - 1) + width * 2;
					float set_uvs = minUV + (goal - min)*(maxUV-minUV)/(max-min);
					for(size_t i = verts.size() - 6; i < verts.size(); i++){
						if(verts[i].getX() > goal){
							verts[i].setX(goal);
							uvs[i].setX(set_uvs);
						}
					}
				}
			}
			GLuint vertBuff = render::getBuffer(verts);
			GLuint uvsBuff = render::getBuffer(uvs);

			render::setAttrPoint(0, 2, vertBuff, GL_FLOAT);
			render::setAttrPoint(1, 2, uvsBuff, GL_FLOAT);
			render::draw(verts.size());
			render::delBuffer(&vertBuff);
			render::delBuffer(&uvsBuff);
		}
	}

	if(is_edit){
		render::set2DLine();
		render::setTrans(transform);
		render::setColor(TEXT_COLOR);

		vector<vec2> vs = {vec2(insertP/width-0.99, 1), vec2(insertP/width-0.99, -1)};
		GLuint vertBuff = render::getBuffer(vs);
		render::setAttrPoint(0, 2, vertBuff, GL_FLOAT);
		render::draw(vs.size());
		render::delBuffer(&vertBuff);
	}
}

bool Textin::input_char(bool acted, int ch, int mods){
	if(!is_edit || !getActive() || !editable)return 0;
	if(acted){
		is_edit = 0;
		if(is_valid && callback)callback->textin_callback(issueID, string(&inputS[0]));
		return 0;
	}

	if(ch < 128){
		addChar((char) ch);
		is_valid = validator(string(&inputS[0]));
	}
	return 1;
}

bool Textin::input_key(bool acted, int key, int action, int mods){
	if(!is_edit || !getActive())return 0;
	if(acted){
		is_edit = 0;
		if(is_valid && callback)callback->textin_callback(issueID, string(&inputS[0]));
		return 0;
	}

	if(action){
		if(key == GLFW_KEY_LEFT && insertI){
			insertI--;
			insertRP-=inputSZ[insertI];
			insertP-=inputSZ[insertI];
			if(insertP<0)insertP=0;
		}
		if(key == GLFW_KEY_RIGHT && insertI < (int) inputSZ.size()){
			insertRP+=inputSZ[insertI];
			insertP+=inputSZ[insertI];
			insertI++;
			if(insertP>width*2)insertP=width*2;
		}
		if(key == GLFW_KEY_BACKSPACE && editable){
			removeChar();
			is_valid = validator(string(&inputS[0]));
		}if(key == GLFW_KEY_ESCAPE || key == 10 || key == GLFW_KEY_ENTER){
			is_edit=0;
			if(is_valid && callback)callback->textin_callback(issueID, string(&inputS[0]));
		}
		return 1;
	}
	return 0;
}

bool Textin::input_mouse(bool acted, int button, int action, int mods){
	if(button==0 && action==1 && getActive()){
		bool temp = is_edit;
		is_edit = acted ? 0 : isMouseInText();
		if(temp && !is_edit && is_valid && callback)callback->textin_callback(issueID, string(&inputS[0]));
		if(is_edit)return 1;
	}
	return 0;
}

bool Textin::isMouseInText(){
	return mouse_in_rectangle(getTransform());
}

void Textin::addChar(char c){
	inputS.insert(inputS.begin()+insertI, c);

	float w = text->getCharWidth(c);
	inputSZ.insert(inputSZ.begin()+insertI, w);
	insertI++;
	insertRP+=w;
	insertP+=w;
	if(insertP>width*2)insertP=width*2;
}

void Textin::removeChar(){
	if(inputSZ.size() && insertI){
		insertI--;
		insertRP-=inputSZ[insertI];

		float new_width = 0;
		for(float iSZ: inputSZ)new_width+=iSZ;
		new_width-=inputSZ[insertI];

		if(new_width<width)insertP=insertRP;
		else if(insertRP<insertP)insertP-=inputSZ[insertI];

		if(insertP<0)insertP=0;
		inputSZ.erase(inputSZ.begin()+insertI);
		inputS.erase(inputS.begin()+insertI);
	}
}

Textin* Textin::setText(const char *s){
	clearInput();
	while(*s)addChar(*s++);
	is_valid = validator(string(&inputS[0]));
	return this;
}

Textin* Textin::setText(string s){
	setText(s.c_str());
	return this;
}
