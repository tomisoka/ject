#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/input.h"
#include "../simplefc/settings.h"

struct TextinCallBack{
	//friend class Textin;
	virtual void textin_callback(size_t issueID, string input){}
};

class Textin: public ObjectComponent, public InputCallbacks{
public:
	Textin(vec2 pos, vec2 SZ, TextinCallBack *callback, size_t issueID, function<bool(string)> validator = [](string){return true;}, bool editable = 1);
	~Textin();

	void clearInput();

	void render() override;

	Textin* setText(const char *s);
	Textin* setText(string s);

private:
	bool input_key(bool acted, int key, int action, int mods) override;
	bool input_char(bool acted, int ch, int mods) override;
	bool input_mouse(bool acted, int button, int action, int mods) override;


	bool isMouseInText();
	void addChar(char c);
	void removeChar();

	vector<char> inputS;
	vector<float> inputSZ;

	function<bool(string)> validator;
	bool is_valid;

	float insertI, insertP, insertRP;
	bool is_edit;

	float width;

	bool editable;

	TextinCallBack *callback;
	size_t issueID;
};
