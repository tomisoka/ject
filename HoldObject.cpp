#include "HoldObject.h"
#include "comps/Repeller.h"
#include "BtInterface/convertVM.h"
#include "BtInterface/BodyInterface.h"
#include "simplefc/settings.h"

HoldObject::HoldObject(Object *mvParent, quat rot, vec3 pos): ObjectComponent(pos, rot), mvParent(mvParent), oldGoal(0), start(30){}

HoldObject::HoldObject(const HoldObject& o): ObjectComponent(o), mvParent(o.mvParent), oldGoal(0), start(30){}

void HoldObject::delMe(){
	ignoreParent(parent, 0);

	BodyInterface *body = parent->getBtBody();
	if(!body->is_static_hold()){
		btVector3 vel = body->getLinearVelocity() - mvParent->getBtBody()->getLinearVelocity();
		vel*=0.2;
		body->setLinearVelocity(mvParent->getBtBody()->getLinearVelocity() + vel);
		body->setGravity(GRAVACC);
	}else body->set_static(true);
	ObjectComponent::delMe();
}

void HoldObject::postLoaded(){
	BodyInterface *body = parent->getBtBody();

	btCollisionShape *sh = body->getCollisionShape();

	int tp = sh->getShapeType();
	if(tp == BOX_SHAPE_PROXYTYPE){
		btBoxShape* shapeB = (btBoxShape*)sh;
		btVector3 sz = shapeB->getHalfExtentsWithMargin();

		radius = sz.length();
	}else if(tp == CYLINDER_SHAPE_PROXYTYPE){
		btCylinderShape* shapeC = (btCylinderShape*)sh;
		btVector3 sz = shapeC->getHalfExtentsWithMargin();

		radius = sz.length();
	}else if(tp == CONVEX_HULL_SHAPE_PROXYTYPE){
		btConvexHullShape* shapeH = (btConvexHullShape*)sh;

		int num = shapeH->getNumPoints();
		btVector3 *points = shapeH->getUnscaledPoints();

		float r2 = 0;
		for(int i=0;i<num;++i){
			float rT2 = points[num].length2();
			if(rT2>r2)r2 = rT2;
		}
		radius = sqrt(r2);
	}else if(tp == SPHERE_SHAPE_PROXYTYPE){
		btSphereShape* shapeS = (btSphereShape*)sh;

		radius = shapeS->getRadius();
	}

	body->set_static(0);

	ignoreParent(parent, mvParent->getBtBody());
}

void HoldObject::ignoreParent(Object *obj, BodyInterface *toIgnore){
	for(ObjectComponent *comp: obj->getComps()){
		if(comp->getID()==19){
			((Repeller*)comp)->setIgnore(toIgnore);
		}
	}
}


void HoldObject::update(){
	BodyInterface *body = parent->getBtBody();
	body->setGravity(btVector3(0,0,0));
	body->activate();

	btTransform Bodytrans;
	mvParent->getBtBody()->predictIntegratedTransform (TICK_TIME, Bodytrans);
	mat4 trans = Mat4(Bodytrans);



	quat mvParRot = mvParent->getRot();
	quat mvParRotNw = quat(trans);


	resetTransform();
	mat4 relTrans = this->transform;

	mat4 endTrans = trans*relTrans;

	vec3 newPos = endTrans.getPos();
	quat newRot = quat(endTrans);


	vec3 oldPos = parent->getPos();
	vec3 mvPos = newPos-oldPos;

	btTransform tran = body->getWorldTransform();

	if(mvPos.lengthSq()>0.0001){
		if(start){
			vec3 vel = mvPos*0.4*TPS*((30-start)/float(30));
			body->setLinearVelocity(btVec3(vel));
			start--;
			float mv = vel.getY();
			if(mv>0){
				mvParent->getBtBody()->setLinearVelocity(mvParent->getBtBody()->getLinearVelocity()
							- btVector3(0, mv, 0)/parent->getBtBody()->getInvMass()*mvParent->getBtBody()->getInvMass());
			}
		}else{
			if((newPos-oldGoal).lengthSq()>0.0001f){
				body->setLinearVelocity(btVec3((newPos - oldGoal)*TPS));
				if((oldPos - oldGoal).lengthSq()>0.0001){
					body->setLinearVelocity(body->getLinearVelocity() + btVec3(mvPos*0.1*TPS));
				}
			}else{
				body->setLinearVelocity(btVec3(mvPos*0.2*TPS));
			}
		}
	}else{
		start = 0;
		body->setLinearVelocity(btVector3(0,0,0));
		tran.setOrigin(btVec3(newPos));
	}



	body->setWorldTransform(tran);


	quat parRot = parent->getRot();


	quat rotVel2 = mvParRot.diff(mvParRotNw);

	parRot = parRot * rotVel2;


	quat nwRot = parRot.NLerp(newRot, start?0.1:0.2, 1);
	quat rotVel = parRot.diff(nwRot);

	rotVel = rotVel * rotVel2;



	vec3 eulVel = rotVel.getEuler();
	float deg90 = M_PI/2.f * (start?0.1f:0.2f);
	if(eulVel.lengthSq()>deg90*deg90 && radius > 0.5f){
		nwRot = parent->getRot().NLerp(newRot, 1, 1);
		rotVel = parent->getRot().diff(nwRot);
		eulVel = rotVel.getEuler();
	}

	body->setAngularVelocity(btVec3(eulVel * TPS));

	oldGoal = newPos;
}
