#pragma once
#include "ObjectComponent.h"

class HoldObject: public ObjectComponent{
public:
	HoldObject(Object *mvParent, quat rot, vec3 pos);
	HoldObject(const HoldObject& o);

	void delMe();

	void setPos(vec3 pos){this->pos=pos;}

	void postLoaded() override;

	void rotate(quat rot){this->rot = this->rot * rot;}

	uint32_t getID() override{return 17;}
	string getIDName() override{return "HoldObject";}
	void update() override;
private:
	void ignoreParent(Object *obj, BodyInterface *toIgnore);

	Object *mvParent;

	vec3 oldGoal;

	float radius;

	uint32_t start;
};
