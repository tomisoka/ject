#pragma once
#include "simplefc/std.h"
#include "simplefc/basicFcs.h"

template<typename T>
bool edit_vec(size_t& actCode,size_t issueID, string& input, vector<T>& out, function<bool(string& in, T& out)> c){
	for(size_t i = 0; i < out.size();++i){
		if(issueID < actCode)break;
		if(actCode == issueID){
			swap(out[i], out[i-1]);
		}else if(actCode+1 == issueID){
			swap(out[i], out[i+1]);
		}else if(actCode+2 == issueID){
			out.erase(out.begin()+i);
		}else if(actCode+3 == issueID){
			return c(input, out[i]);
		}
		actCode+=4;
	}
	if(actCode==issueID){
		out.push_back(out[out.size()-1]);
	}
	actCode++;
	return 0;
}

template<typename T, uint_t D>
bool edit_mvec(size_t& actCode, size_t issueID, string input, Vector<T, D>& out){
	if(issueID == actCode){
		Vector<T, D> temp;
		if(strToVec(input.c_str(), temp))return 1;
		out = temp;
	}
	actCode++;
	return 0;
}

inline bool edit_float(size_t& actCode, size_t issueID, string input, float& out){
	if(issueID == actCode){
		float temp;
		if(!strToFloat(input.c_str(), temp))return 1;
		out = temp;
	}
	actCode++;
	return 0;
}

inline bool edit_int(size_t& actCode, size_t issueID, string input, int& out){
	if(issueID == actCode){
		int temp;
		if(!strToInt(input.c_str(), temp))return 1;
		out = temp;
	}
	actCode++;
	return 0;
}

inline bool edit_bool(size_t& actCode, size_t issueID, string input, bool& out){
	if(issueID == actCode){
		int temp;
		if(!strToInt(input.c_str(), temp))return 1;
		out = temp;
	}
	actCode++;
	return 0;
}
