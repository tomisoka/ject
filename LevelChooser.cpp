#include "LevelChooser.h"
#include "GUI/InfoTab.h"
#include "simplefc/xml.h"
#include "simplefc/basicFcs.h"
#include "gameData.h"
#include <dirent.h>

struct file{
	string name;
	string full_name;
};

LevelChooser::LevelChooser(InfoTab *tab, string level_dir): tab(tab){
	DIR *extDir;
	struct dirent *extFile;
	extDir = opendir(level_dir.c_str());
	if(extDir){
		while ((extFile = readdir(extDir)) != 0){
			xml_document doc;
			if(doc.load_file((level_dir + string(extFile->d_name)).c_str())){
				xml_node nameN = doc.child("name");
				if(nameN)files.push_back(new file{string(nameN.text().as_string()), level_dir + string(extFile->d_name)});
				else files.push_back(new file{string(extFile->d_name), level_dir + string(extFile->d_name)});
			}
		}
		closedir(extDir);
	}
	sort(files.begin(), files.end(), [](file* a, file* b){return a->name < b->name;});

	tab->addText("Levels:");

	for(size_t i = 0; i < files.size(); i++){
		if(files[i]->name == "hidden_level" || ends_with(files[i]->full_name, ".temp"))continue;
		tab->addButton(files[i]->name, {new TriggerData(this, i)});
	}
}

LevelChooser::~LevelChooser(){
	for(file *f: files)delete f;
}

void LevelChooser::call(int code){
	level_to_load = files[code]->full_name;
	tab->delMe();
}
