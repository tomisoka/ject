#pragma once
#include "simplefc/trigger.h"
#include "ObjectComponent.h"

class InfoTab;
class file;

class LevelChooser: public ObjectComponent, public Trigger{
public:
	LevelChooser(InfoTab *tab, string level_dir);
	~LevelChooser();

	void call(int code) override;

	vec2o3 getSize() override{return vec2o3(0, 0);}
private:
	InfoTab *tab;

	vector<file*> files;
};
