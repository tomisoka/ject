NODEPS = clean

SOURCES = $(patsubst ./%,%,$(shell find ./ -name "*.cpp"))

OBJS = $(patsubst %.cpp,o/%.o,$(SOURCES))
DEPS = $(patsubst %.cpp,d/%.d,$(SOURCES))

INCLUDES = /usr/include/bullet /usr/include/freetype2 ../


CXXFLAGS = -std=c++17 -O2 -Wno-unused-result -Wall -Wextra -Wno-unused-parameter -Wcast-align -Wwrite-strings -Wlogical-op -Wredundant-decls $(addprefix -I, $(INCLUDES)) -g

CXXFLAGSOUT = -std=c++17 -O2 -Wno-unused-result -lGLEW -lGL -lglfw -lpng -lfreetype -lpugixml -lsfml-audio -lsfml-system -lBulletDynamics -lBulletCollision -lLinearMath -L../freetype-gl/build/ -lfreetype-gl

all: $(OBJS)
	$(CXX) -o run $^ $(CXXFLAGSOUT)

d/%.d: %.cpp
	$(CXX) $(addprefix -isystem , $(INCLUDES)) $(CXXFLAGS) -MM -MT '$(patsubst %.cpp,o/%.o,$<)' $< -MF $@
	echo "	$(CXX) $(CXXFLAGS) -o $(patsubst %.cpp,o/%.o,$<) -c $<" >> $@

include $(DEPS)

clean:
	find ./ -name "*.d" -delete
	find ./ -name "*.o" -delete
	rm run

#to update dependences if changed in header
update:
	find ./ -name "*.d" -delete

.PRECIOUS: $(DEPS)
