#include "MenuController.h"
#include "gameData.h"
#include "controls.h"
#include "level.h"
#include "simplefc/window.h"
#include "simplefc/TemplateBuffers.h"
#include "edit/edit.h"
#include "GUI/MouseRect.h"
#include "GUI/InfoTab.h"
#include "LevelChooser.h"
#include "Controls.h"

MenuController::MenuController(){
	addComponent(background = new Render2D(template_buffer_rectangle_background, template_buffer_rectangle_positive_background, 6, 1, vec3(0), vec2(0)));


	mainMenu = new Menu(this, 0);
	mainMenu->addButton(vec2(0,0.2f), "Start Game", 0);
	mainMenu->addButton(vec2(0.35,0.2f), "?", 0x10, vec2(0.08));
	mainMenu->addButton(vec2(0,0), "Options", 1);
	mainMenu->addButton(vec2(0,-0.2f), "Edit", 2);
	mainMenu->addButton(vec2(0,-0.6f), "End Game", 3);
	addChild(mainMenu);

	optionsMenu = new Menu(this, 1);
	optionsMenu->addOnOff(vec2(0, 0.8f), "Sounds:", &soundsOn);
	optionsMenu->addOnOff(vec2(0, 0.7f), "Inverted colors:", &render::invertCol);
	optionsMenu->addOnOff(vec2(0, 0.6f), "Fullscreen:", &fullscreen);
	optionsMenu->addButton(vec2(0,-0.6f), "Controls", 1);
	optionsMenu->addButton(vec2(0, -0.8f), "OK", 0);
	//optionsMenu->addDragger(vec2(0, 0.5f), "Font resolution:", &fontRes, 16, 64);
	addChild(optionsMenu);

	inGameMenu = new Menu(this, 2);
	// ignores other clicks -> e.g. to edit;
	inGameMenu->addComponent(new MouseRect(vec2(0), vec2(1), {new TriggerData(inGameMenu, 0x000FFFF)}));
	inGameMenu->addButton(vec2(0,0.3f), "Continue", 0);
	inGameMenu->addButton(vec2(0,0.1f), "Restart level", 1);
	inGameMenu->addButton(vec2(0,-0.3f), "Exit to menu", 2);
	addChild(inGameMenu);

	controlsMenu = new Menu(this, 3);
	controlsMenu->addButton(vec2(-0.4f, 0.8f), "OK", 0, vec2(0.25f, 0.08f));
	controlsMenu->addButton(vec2(0.4f, 0.8f), "Reset all", 1, vec2(0.25f, 0.08f));
	controlsMenu->addControlsList(vec2(0, 0.6f));
	addChild(controlsMenu);

	mainMenu->setActive(1);
}

MenuController::~MenuController(){}

void MenuController::call(int code){
	int ID = code >> 8;
	int param = code & 0xFF;

	if(ID==0){
		if(param==0){
			inCam=1;
			editing=0;
			statical = 0;

			levelPar->setActive(1);
			control->reset();
			CameraObj->setPos(vec3(0));
			levelPar->addChild(actLevel = new Level(level_to_load, 0));
			hideCursor(1);


			mainMenu->setActive(0);
			background->setActive(0);
		}else if(param==1){
			mainMenu->setActive(0);
			optionsMenu->setActive(1);
		}else if(param==2){
			editing=1;
			statical = 1;
			inCam=0;

			levelPar->setActive(1);
			control->reset();
			levelPar->addChild(actLevel = new Level(level_to_load, 1));
			hideCursor(1);
			position = vec3(0);
			resetEdit(1);


			mainMenu->setActive(0);
			background->setActive(0);
		}else if(param==3){
			toEnd=1;
		}else if(param==0x10){ //choose level
			InfoTab *level_chooser = new InfoTab(vec2(0.45, 0.9));
			addChild(level_chooser);

			level_chooser->addComponent(new LevelChooser(level_chooser, LEVEL_DIR));
		}
	}else if(ID == 1){
		if(param == 0){
			mainMenu->setActive(1);
			optionsMenu->setActive(0);
		}else if(param == 1){
			controlsMenu->setActive(1);
			optionsMenu->setActive(0);
		}
	}else if(ID == 2){
		if(param == 0){
			hideCursor(1);
		}else if(param == 1){
			hideCursor(1);

			if(!editing){
				position = vec3(0);
				horizontalAngle = 0;
				verticalAngle = 0;
			}
			control->reset();
			CameraObj->setPos(vec3(0));

			string file = actLevel->getXmlFile();
			actLevel->delMe();
			levelPar->addChild(actLevel = new Level(file, editing));
		}else if(param == 2){
			levelPar->setActive(0);
			control->reset();
			actLevel->delMe();
			resetEdit(0);
			horizontalAngle = 0;
			verticalAngle = 0;


			mainMenu->setActive(1);
			background->setActive(1);
		}
		inGameMenu->setActive(0);
		paused = 0;
	}else if(ID == 3){
		CTRL::changing_ctrls = 0;
		if(param == 0){
			controlsMenu->setActive(0);
			optionsMenu->setActive(1);
		}else if(param == 1){
			CTRL::reset();
		}
	}
}

bool MenuController::input_key(bool acted, int key, int action, int mods){
	if(getActive() && action == 1 && !acted){
		if(key == 256 && (!editing || statical)){
			if(mainMenu->getActive()){
			}else if(optionsMenu->getActive()){
			}else if(controlsMenu->getActive()){
			}else if(inGameMenu->getActive()){
				inGameMenu->setActive(0);
				hideCursor(1);
				paused = 0;
			}else{
				inGameMenu->setActive(1);
				hideCursor(0);
				paused = 1;
			}
			return true;
		}
	}
	return false;
}

void MenuController::render(){
	if(!active)return;
	Object::render();
}

MenuController *menuControl;
