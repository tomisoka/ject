#pragma once
#include "menu.h"
#include "simplefc/input.h"
#include "GUI/Render2D.h"
#include "simplefc/trigger.h"

class MenuController: public Object, public InputCallbacks, public Trigger{
public:
	MenuController();
	~MenuController();

	void call(int code) override;

	void render() override;
protected:
	bool input_key(bool acted, int key, int action, int mods) override;

	Menu *mainMenu;
	Menu *optionsMenu;
	Menu *inGameMenu;
	Menu *controlsMenu;

	Render2D *background;
};

extern MenuController *menuControl;
