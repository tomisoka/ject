#include "Object.h"
#include "ObjectComponent.h"
#include "gameData.h"
#include "BtInterface/convertVM.h"
#include "simplefc/basicFcs.h"
#include "simplefc/log.h"
#include "BtInterface/BodyInterface.h"
#include "simplefc/trigger.h"

Object::Object(vec3 pos, quat rot, bool active, vec3 scale):name(""), parent(0), changed(1), active(active), scale(scale), rotation(rot), pos(pos), body(0), editMode(0), editPos(vec3(0)), editRot(quat()), loaded(0){
	resetTransform(0, 0);
}

Object::Object(const Object& o): name(o.name), parent(0), changed(1), active(o.active), scale(o.scale), rotation(o.rotation), pos(o.pos), editMode(o.editMode), editPos(o.editPos), editRot(o.editRot), loaded(0){
	body = o.body?new BodyInterface(*o.body):0;


	for(Object *child: o.children){
		addChild(new Object(*child));
	}
	for(ObjectComponent *comp: o.components){
		ObjectComponent *nw = comp->Copy();
		if(nw->getID() != 0){
			addComponent(nw);
		}else if(comp->getID() != 0){
			printLogWarn("Couldn't copy " + to_string(comp->getID()) + ") " + comp->getIDName());
		}
	}
}

Object *Object::Copy(bool disActive) const{
	Object *copy = new Object(*this);
	copy->setActive(disActive);
	return copy;
}

Object::~Object(){
	delComponents();
	delChildren();

	if(body)delete body;
}

void Object::delMe(){
	removeParent();
	delete this;
}

void Object::removeParent(){
	if(parent)parent->remChild(this);
	parent = 0;
}

void Object::setBtBody(BodyInterface *body){
	if(this->body)delete this->body;

	this->body=body;

	if(body)body->regist(1);
}

void Object::resetTransform(bool par, bool resCh){
	if(resCh)changed=0;
	mat4 result = pos.toTransM() * scale.toScaleM() * rotation.toRotMatrix();
	if(par && parent)transformMat = parent->getTransform() * result;
	else transformMat = result;
}

Object* Object::addChild(Object *child){
	if(!child)return this;
	children.push_back(child);
	child->setParent(this);
	return this;
}

void Object::remChild(Object *child){
	erase(children, child);
}
void Object::delChildren(){
	for(let obj: children)delete obj;
	children.clear();
}

Object* Object::addComponent(ObjectComponent *component){
	if(!component)return this;
	components.push_back(component);
	component->setParent(this);
	return this;
}

void Object::remComponent(ObjectComponent *component){
	erase(components, component);
}
void Object::delComponents(){
	for(let comp: components)delete comp;
	components.clear();
}

void Object::postLoaded(){
	if(loaded)return;
	loaded=1;

	resetTransform(1, 0);

	for(Object *child: children)
		child->postLoaded();
	for(ObjectComponent *comp: components)
		comp->postLoaded();
}

void Object::actBtBodyCh(bool act){
	if(body)body->regist(act);
	for(Object *child: children)
		child->actBtBodyCh(act);
	for(ObjectComponent *comp: components)
		comp->actBtBody(act);
}

void Object::setEditMode(bool editMode){
	if(body)body->regist(1);
	setActive(1);

	if(this->editMode!=editMode){
		if(editMode == 0){
			editPos = pos;
			editRot = rotation;
		}else{
			pos = editPos;
			rotation = editRot;
			changed = 1;
		}
		this->editMode = editMode;
	}
	for(Object *child: children)
		child->setEditMode(editMode);
	for(ObjectComponent *comp: components)
		comp->setEditMode(editMode);
}



void Object::setEditColor(vec3 color){
	for(ObjectComponent *comp: components)
		comp->setEditColor(color);
	for(Object *child: children)
		child->setEditColor(color);
}
void Object::unsetEditColor(){
	for(ObjectComponent *comp: components)
		comp->unsetEditColor();
	for(Object *child: children)
		child->unsetEditColor();
}

map<string, string> Object::propagateStringChange() const{
	map<string, string> stringChange;
	for(Object *child: children){
		auto sc = child->propagateStringChange();
		auto it = sc.begin();
		while(it != sc.end()){
			stringChange[it->first] = it->second;
			++it;
		}
	}
	for(ObjectComponent *comp: components){
		auto sc = comp->propagateStringChange();
		auto it = sc.begin();
		while(it != sc.end()){
			stringChange[it->first] = it->second;
			++it;
		}
	}
	return stringChange;
}

void Object::updateTriggerNames(){
	if(name != "")triggPref.push_back(name);
	for(Object *child: children)
		child->updateTriggerNames();
	if(name != "")triggPref.pop_back();
	for(ObjectComponent *comp: components)
		comp->updateTriggerNames();
}

void Object::registTriggers(){
	for(Object *child: children)
		child->registTriggers();
	for(ObjectComponent *comp: components)
		comp->registTriggers();
}

void Object::requestTriggers(){
	for(Object *child: children)
		child->requestTriggers();
	for(ObjectComponent *comp: components)
		comp->requestTriggers();
	if(body)body->requestTriggers();
}

vector<Object*> Object::getAllChildren(){
	vector<Object *> ret = children;
	for(Object *child: children){
		vector<Object *> r = child->getAllChildren();
		ret.insert(ret.end(), r.begin(), r.end());
	}
	return ret;
}

string Object::getFreeNameHexObj(string base) const{
	string TEMP;
	for(size_t i=0;;++i){
		TEMP = base + intToHex(i);
		for(Object *child: children){
			if(child->name == TEMP){
				goto alreadyInUse;
			}
		}
		break;
		alreadyInUse:;
	}
	return TEMP;
}

string Object::getFreeNameHexComp(string base) const{
	string TEMP;
	for(size_t i=0;;++i){
		TEMP = base + intToHex(i);
		for(Object *child: children){
			for(ObjectComponent* comp: child->getComps()){
				if(comp->name == TEMP)goto alreadyInUse;
			}
		}
		break;
		alreadyInUse:;
	}
	return TEMP;
}


void Object::render(){
	if(!active)return;

	for(ObjectComponent *comp: components)
		comp->render();
	for(Object *child: children)
		child->render();
}

void Object::resPosTotal(){
	if(!active)return;
	bool changed = this->changed;
	if(body){
		btTransform trans;
		if(body->getInvMass() && !statical){
			trans = body->getWorldTransform();
			setPos(Vec3(trans.getOrigin()));
			setRot(Quat(trans.getRotation()));
			resetTransform(0);
			changed = 1;
		}else if((changed && !body->getInvMass()) || statical){
			resetTransform();
			trans = btTrans(transformMat);
			btMotionState *motSt = body->getMotionState();
			motSt->setWorldTransform(trans);
			body->setMotionState(motSt);
		}
	}else if(changed)
		resetTransform();

	for(Object *child: children){
		if(changed)child->change();
		child->resPosTotal();
	}
}

void Object::update(){
	if(!active)return;
	for(Object *child: children)
		child->update();
	for(ObjectComponent *comp: components)
		comp->update();
}
