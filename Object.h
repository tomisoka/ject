#pragma once

#include "simplefc/std.h"

class btCollisionObject;
class BodyInterface;
class ObjectComponent;

class Object{
public:
	Object(vec3 pos, quat rot=quat(), bool active=1, vec3 scale=1);
	Object(bool active=1, vec3 scale=1):Object(vec3(0), quat(), active, scale){}
	Object(const Object& o);

	Object *Copy(bool disActive = 1) const;
	//call setActive; actBtBody; postLoaded;

	virtual ~Object();

	void delMe();
	void removeParent();

	Object* addChild(Object *child);
	void remChild(Object *child);
	void delChildren();
	Object* addComponent(ObjectComponent *component);
	void remComponent(ObjectComponent *component);
	void delComponents();
	void setParent(Object *parent){this->parent=parent;}
	Object *getParent() const {return parent;}
	mat4 getTransform() const {return transformMat;}

	virtual void render();
	virtual void postLoaded();
	virtual void update();

	virtual void resPosTotal();

	void setBtBody(BodyInterface *body);
	BodyInterface *getBtBody(){return body;}
	void setPos(vec3 newPos){changed=1; pos=newPos;}
	void setRot(quat newRot){changed=1; rotation=newRot;}
	vec3 getPos() const {return pos;}
	quat getRot() const {return rotation;}

	bool getActive() const {return active  && (parent?parent->getActive():1);}

	void setActive(bool active){this->active=active;}
	void change(){changed=1;}

	void actBtBodyCh(bool act);
	void setEditMode(bool editMode);
	void setStartEditMode(bool editMode){this->editMode = editMode;}

	void setEditColor(vec3 color);
	void unsetEditColor();

	virtual map<string, string> propagateStringChange() const;
	virtual void updateTriggerNames();
	virtual void registTriggers();
	virtual void requestTriggers();

	string getFreeNameHexObj(string base) const;
	string getFreeNameHexComp(string base) const;

	vector<Object*>& getChildren(){return children;}
	vector<ObjectComponent*>& getComps(){return components;}

	vector<Object*> getAllChildren();

	string name;
protected:
	void resetTransform(bool par=1, bool resCh=1);

	Object *parent;
	vector<Object*> children;
	vector<ObjectComponent*> components;

	bool changed;
	bool active;
	vec3 scale;

	quat rotation;
	vec3 pos;
	BodyInterface *body;

	mat4 transformMat;

	bool editMode;
	vec3 editPos;
	quat editRot;

	bool loaded;
};
