#include "ObjectComponent.h"
#include "simplefc/xml.h"

ObjectComponent::ObjectComponent(xml_node xml): name(""), active(1), editMode(0), editColor(0), parent(0),actBt(1){
	scale = xmlRVec3(xml, "scale3", vec3(1));
	scale = xmlRVec3(xml, "size", scale);
	pos = xmlRVec3(xml, "pos", vec3(0));
	pos = xmlRVec3(xml, "pos3", pos);
	rot = xmlRRot(xml, "rot", quat());
	rot = xmlRRot(xml, "rot4", rot);

	resetTransform();
}
void ObjectComponent::saveXml(xml_node xml){
	if(scale!=vec3(0))xmlWVec3(xml, "scale3", scale);
	if(pos!=vec3(0))xmlWVec3(xml, "pos3", pos);
	if(rot!=quat())xmlWRot(xml, "rot4", rot);
}
