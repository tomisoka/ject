#pragma once

#include "Object.h"

namespace pugi{class xml_node;}
using pugi::xml_node;

class ObjectComponent{
public:
	ObjectComponent(vec3 pos = vec3(0), quat rot=quat(), vec3 scale = vec3(1)): name(""), active(1), editMode(0), editColor(0), editColorR(vec3(1)), parent(0), actBt(1), pos(pos), rot(rot), scale(scale){resetTransform();}
	ObjectComponent(const ObjectComponent& o): name(o.name), active(o.active), editMode(o.editMode), editColor(o.editColor), editColorR(o.editColorR), parent(0), actBt(o.actBt), pos(o.pos), rot(o.rot), scale(o.scale){resetTransform();}
	ObjectComponent(xml_node xml);
	virtual ~ObjectComponent(){}
	virtual void delMe(){parent->remComponent(this); delete this;}

	virtual ObjectComponent *Copy(){return new ObjectComponent(*this);}

	virtual void setParent(Object *parent){this->parent=parent;}
	Object *getParent(){return parent;}

	virtual void update(){}
	virtual void render(){}
	virtual void renderReg(){}
	virtual void postLoaded(){}
	virtual void actBtBody(bool act){actBt=act;}

	virtual uint32_t getID(){return 0;}
	virtual string getIDName(){return "";}

	virtual void saveXml(xml_node xml);

	virtual void setEditColor(vec3 color){editColorR = color; editColor = 1;}
	virtual void unsetEditColor(){editColor = 0;}
	virtual bool isEditColor(){return editColor;}

	virtual map<string, string> propagateStringChange(){return map<string, string>();}
	virtual void updateTriggerNames(){}
	virtual void registTriggers(){}
	virtual void requestTriggers(){}

	mat4 getTransform(){return parent->getTransform() * transform;}

	void setActive(bool active){this->active=active;}
	bool getActive(){return active && parent->getActive();}

	virtual void setEditMode(bool editMode){}
	void setStartEditMode(bool editMode){this->editMode = editMode;}

	vec3 getPos(){return pos;}
	quat getRot(){return rot;}
	vec3 getScale(){return scale;}
	virtual vec2o3 getSize(){return vec2o3(-scale, scale);}

	string name;
protected:
	void resetTransform(){
		transform = pos.toTransM() * rot.toRotMatrix() * scale.toScaleM();
	}

	bool active;

	bool editMode;
	bool editColor;
	vec3 editColorR;

	Object *parent;

	bool actBt;

	vec3 pos;
	quat rot;
	vec3 scale;
	mat4 transform;
};

/*IDs:
0		ObjectComponent
1		Render3D
2		Render3DLine
3		LaserCollect
4		LaserEmit
5		Mover
6		Rotator
7		AudioComponent
8		LevelLoader
9		Coll
10	EditExtension
11	DecayComp
12	Screen (computer)
16	CubeCompound
17	HoldObject
18	TriggerConductor
19	Repeller
20  Magnet
21	TrackMover
22	TrackRotor
23  Portal
24  Force
25  Render3DSpecial
*/
