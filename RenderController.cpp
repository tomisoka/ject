#include "RenderController.h"
#include "simplefc/render.h"
#include "comps/Render3D.h"
#include "comps/Render3DLine.h"
#include "comps/Screen.h"
#include "cubecompound.h"
#include "simplefc/Text.h"

void RenderController::addRendC(Render3D *r){
	if(r->isInverted())RendPI3D.push_back(r);
	else if(r->getTexture())RendTexP3D.push_back(r);
	else RendP3D.push_back(r);
}
void RenderController::addRendC(Render3DLines *r){
	RendL3D.push_back(r);
}
void RenderController::addRendC(Screen *r){
	comp3D.push_back(r);
}
void RenderController::addRendC(CubeCompound *r){
	CubeCom3D.push_back(r);
}

void RenderController::addRendC(Portal *r){
	Portals.push_back(r);
}

void RenderController::remRendC(Render3D *r){
	erase(RendPI3D, r);
	erase(RendTexP3D, r);
	erase(RendP3D, r);
}

void RenderController::remRendC(Render3DLines *r){
	erase(RendL3D, r);
}

void RenderController::remRendC(Screen *r){
	erase(comp3D, r);
}

void RenderController::remRendC(CubeCompound *r){
	erase(CubeCom3D, r);
}

void RenderController::remRendC(Portal *r){
	erase(Portals, r);
}

void RenderController::render(){
	renderScene();
}

void RenderController::renderScene(){
	render::setLine();
	for(auto rend: RendL3D)rend->renderReg();

	render::setColor();
	for(auto rend: RendP3D)rend->renderReg();

	render::setNormal();
	for(auto rend: RendTexP3D)rend->renderReg();

	//computers start
	render::set3D_distance_field();
	render::setIgnoreLowAlpha(true);
	render::setTexture(text->getTextureID());

	for(auto rend: comp3D)rend->renderReg();
	render::setIgnoreLowAlpha(false);
	//computers end

	for(size_t i=CubeCom3D.size();i-- > 0;){
		CubeCom3D[i]->renderReg();
	}

	render::setColor();
	for(auto rend: RendPI3D)rend->renderReg();
}

RenderController *Rcontrol;
