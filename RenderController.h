#pragma once

#include <vector>
using std::vector;

class Render3D;
class Render3DLines;
class Screen;
class CubeCompound;
class Portal;

class RenderController{
public:
	void render();
	void addRendC(Render3D *r);
	void addRendC(Render3DLines *r);
	void addRendC(Screen *r);
	void addRendC(CubeCompound *r);
	void addRendC(Portal *r);

	void remRendC(Render3D *r);
	void remRendC(Render3DLines *r);
	void remRendC(Screen *r);
	void remRendC(CubeCompound *r);
	void remRendC(Portal *r);
private:
	void renderScene();

	vector<Render3D *> RendP3D;
	vector<Render3D *> RendPI3D;//inverted
	vector<Render3D *> RendTexP3D;
	vector<CubeCompound *> CubeCom3D;

	vector<Screen *> comp3D;

	vector<Render3DLines *> RendL3D;

	vector<Portal *> Portals;
};

extern RenderController *Rcontrol;
