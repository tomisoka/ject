#version 330 core

out vec4 color;

uniform vec3 col;
uniform bool invert_color;

void main(){
	color = vec4(col, 1);

	if(invert_color){
		color.x = 1-color.x;
		color.y = 1-color.y;
		color.z = 1-color.z;
	}
}
