#version 330 core

layout(location = 0) in vec3 vert;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 transform;

void main(){
	vec4 res = view * transform * vec4(vert,1);
	gl_Position = projection * res;
}
