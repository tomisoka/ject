#version 330 core

out vec4 color;

uniform vec3 col;
uniform bool fade;
uniform bool invert_color;

uniform float zNear = 1/8.f;
uniform float zFar = 65536.f;

// depthSample from depthTexture.r, for instance
float linearDepth(float depthSample)
{
    depthSample = 2.0 * depthSample - 1.0;
    float zLinear = 2.0 * zNear * zFar / (zFar + zNear - depthSample * (zFar - zNear));
    return zLinear;
}

// result suitable for assigning to gl_FragDepth
float depthSample(float linearDepth)
{
    float nonLinearDepth = (zFar + zNear - 2.0 * zNear * zFar / linearDepth) / (zFar - zNear);
    nonLinearDepth = (nonLinearDepth + 1.0) / 2.0;
    return nonLinearDepth;
}

void main(){

	float linDist = linearDepth(gl_FragCoord.z);

	linDist -= 0.005 + linDist*0.0004;

	gl_FragDepth = depthSample(linDist);

	//gl_FragDepth = gl_FragCoord.z - 1/65536.;
	if(fade){
		float diff = (1/gl_FragCoord.w)*0.02;
		if(diff>0.5)diff=0.5;
		color = vec4( col+vec3(diff), 1);
	}else color = vec4(col, 1);

	if(invert_color){
		color.x = 1-color.x;
		color.y = 1-color.y;
		color.z = 1-color.z;
	}
}
