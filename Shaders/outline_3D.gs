#version 330 core
layout (triangles_adjacency) in;
layout (triangle_strip, max_vertices = 18) out;

#define M_PI 3.1415926535897932384626433832795

uniform mat4 projection;

vec3 up(vec3 tr1, vec3 tr2, vec3 tr3){
	return normalize(cross(tr2 - tr3, tr1 - tr3));
}

float angle(vec3 a, vec3 b){
	return acos(dot(a, b)/(length(a) * length(b)));
}

float angle_diff =  M_PI*0.125;

float ds_LineWidth = 3;
vec2 ds_ViewportSize = vec2(1200, 750);

void draw_outline(vec4 v0, vec4 v1, float mult){
	vec3 ndc0 = v0.xyz / v0.w;
	vec3 ndc1 = v1.xyz / v1.w;

	float width = ds_LineWidth * mult;

	vec2 lineScreenForward = normalize(ndc1.xy - ndc0.xy);
	vec2 lineScreenRight = vec2(-lineScreenForward.y, lineScreenForward.x);
	vec2 lineScreenOffset = (vec2(width) / ds_ViewportSize) * lineScreenRight;

	gl_Position = vec4(v0.xy + lineScreenOffset * v0.w, v0.z, v0.w);
	EmitVertex();

	gl_Position = vec4(v0.xy - lineScreenOffset * v0.w, v0.z, v0.w);
	EmitVertex();

	gl_Position = vec4(v1.xy + lineScreenOffset * v1.w, v1.z, v1.w);
	EmitVertex();

	gl_Position = vec4(v1.xy - lineScreenOffset * v1.w, v1.z, v1.w);
	EmitVertex();

	EndPrimitive();
}

void main(){
	vec4 v0 = gl_in[0].gl_Position;
	vec4 v1 = gl_in[2].gl_Position;
	vec4 v2 = gl_in[4].gl_Position;

	vec4 adj0 = gl_in[1].gl_Position;
	vec4 adj1 = gl_in[3].gl_Position;
	vec4 adj2 = gl_in[5].gl_Position;

	vec3 up0 = up(vec3(v0), vec3(v1), vec3(v2));

	float a0 = angle(up0, vec3(v0));

	if(a0 > M_PI/2)return;

	vec3 up1 = up(vec3(v0), vec3(adj0), vec3(v1));
	vec3 up2 = up(vec3(v1), vec3(adj1), vec3(v2));
	vec3 up3 = up(vec3(v2), vec3(adj2), vec3(v0));

	vec4 vo0 = projection * v0;
	vec4 vo1 = projection * v1;
	vec4 vo2 = projection * v2;

	if(angle(up1, vec3(adj0)) > M_PI/2){
		draw_outline(vo0, vo1, 1);
	}else if(angle(up0, up1) > angle_diff){
		draw_outline(vo0, vo1, 0.5);
	}
	if(angle(up2, vec3(adj1)) > M_PI/2){
		draw_outline(vo1, vo2, 1);
	}else if(angle(up0, up2) > angle_diff){
		draw_outline(vo1, vo2, 0.5);
	}
	if(angle(up3, vec3(adj2)) > M_PI/2){
		draw_outline(vo2, vo0, 1);
	}else if(angle(up0, up3) > angle_diff){
		draw_outline(vo2, vo0, 0.5);
	}
}
