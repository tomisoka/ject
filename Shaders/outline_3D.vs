#version 330 core

layout(location = 0) in vec3 vert;

uniform mat4 view;
uniform mat4 transform;

void main(){
	gl_Position = view * transform * vec4(vert,1);
}
