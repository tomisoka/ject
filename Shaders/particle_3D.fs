#version 330 core

in vec4 col;
out vec4 color;
uniform bool invert_color;

void main(){
	color = col;

	if(invert_color){
		color.x = 1-color.x;
		color.y = 1-color.y;
		color.z = 1-color.z;
	}
}
