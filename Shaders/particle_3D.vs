#version 330 core


layout(location = 0) in vec3 vert;
layout(location = 1) in vec4 xyzs;
layout(location = 2) in vec4 color;

out vec4 col;
uniform mat4 MVP;

uniform vec3 CamRight;
uniform vec3 CamUp;

void main(){
	float particleSize = xyzs.w;
	vec3 particleCenter = xyzs.xyz;

	vec3 vertex_res = particleCenter
		+ CamRight * vert.x * particleSize
		+ CamUp * vert.y * particleSize;

	gl_Position = MVP * vec4(vertex_res, 1.0f);

	col = color;
}
