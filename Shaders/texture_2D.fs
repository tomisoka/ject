#version 330 core

in vec2 UV;
out vec4 color;

uniform sampler2D tex;
uniform float alpha;
uniform bool invert_color;

void main(){
	color = texture( tex, UV);

	color.w *= alpha;
	if(invert_color){
		color.x = 1-color.x;
		color.y = 1-color.y;
		color.z = 1-color.z;
	}
}
