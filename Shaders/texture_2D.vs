#version 330 core

layout(location = 0) in vec2 vert;
layout(location = 1) in vec2 vertUV;

out vec2 UV;
uniform mat4 transform;

void main(){
	vec4 pos = transform * vec4(vert,0,1);
	gl_Position = pos;
	
	UV = vertUV;
}
