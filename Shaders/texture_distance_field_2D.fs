#version 330 core

in vec2 UV;
out vec4 color;

uniform sampler2D tex;
uniform vec3 col;
uniform bool invert_color;

void main(){
	float alpha  = texture(tex, UV).x;

	if(alpha > 0.5)
		color = vec4(col, 1);
	else if(alpha > 0.25)
		color = vec4(col, (alpha - 0.25)*2.);
	else
		color = vec4(col, 0);

	if(invert_color){
		color.x = 1-color.x;
		color.y = 1-color.y;
		color.z = 1-color.z;
	}
}
