#include "AudioComponent.h"
#include "../simplefc/xml.h"

AudioComponent::AudioComponent(xml_node xml):audio(0), playing(0), Spatialization(1){
	for(xml_node audio: xml.children("audio")){
		xml_node file = audio.child("file");
		addAudio(string(file.text().as_string()));
		dur.push_back(xmlRFloat(audio, "duration", 0));
	}
}

AudioComponent::AudioComponent(const AudioComponent& o): ObjectComponent(o), Trigger(o), audio(0), audioFiles(o.audioFiles), dur(o.dur), playing(0), Spatialization(o.Spatialization){}

AudioComponent::~AudioComponent(){
	if(playing)delete audio;
}
void AudioComponent::saveXml(xml_node xml){
	for(size_t i=0;i<audioFiles.size();++i){
		xml_node audioN = xml.append_child("audio");
		xmlWString(audioN, "file", audioFiles[i]);
		xmlWFloat(audioN, "duration", dur[i]);
	}
}

void AudioComponent::update(){
	if(playing){
		if(Spatialization){
			vec3 pos = parent->getTransform().getPos();
			audio->setPosition(pos.getX(), pos.getY(), pos.getZ());
		}

		if(audioLen==0){
			if(audio->getStatus() == sf::Music::Stopped){
				delete audio;
				audio=0;
				playing=0;
			}
		}else{

			if(audio->getPlayingOffset().asSeconds()>=audioLen){
				delete audio;
				audio=0;
				playing=0;
			}else if(audio->getStatus() == sf::Music::Stopped){
				audioLen-=audio->getPlayingOffset().asSeconds();
				audio->play();
			}
		}
	}else if(audio){
		delete audio;
		audio=0;
	}
}

void AudioComponent::call(int state){
	int audioNum = state;
	if(audioNum==-1)return;

	if(playing){
		audioLen = dur[audioNum-1]-(audioLen-audio->getPlayingOffset().asSeconds());
		if(audioNum!=0){
			delete audio;
			audio=0;
		}else{
			playing=0;
		}
	}else{
		if(audioNum==0 || audioNum > (int)dur.size())return;
		audioLen = dur[audioNum-1];
	}

	if(audioNum==0)return;
	audioNum--;

	if(audioNum>=(int)audioFiles.size())return;

	audio = new sf::Music();

	if(!audio->openFromFile(audioFiles[audioNum]))
		return;

	audio->play();

	playing=1;
}

void AudioComponent::setEditMode(bool editMode){
	if(editMode){
		if(playing){
			delete audio;
			audio = 0;
			playing = 0;
		}
	}
}
