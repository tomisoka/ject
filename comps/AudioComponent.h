#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include <SFML/Audio.hpp>

class AudioComponent:public ObjectComponent, public Trigger{
public:
	AudioComponent(xml_node xml);
	AudioComponent(const AudioComponent& o);
	~AudioComponent();

	ObjectComponent *Copy() override{return new AudioComponent(*this);}

	void addAudio(string audioFile){audioFiles.push_back(audioFile);}

	void update() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 7;}
	string getIDName() override{return "Audio";}

	void saveXml(xml_node xml) override;
private:
	sf::Music *audio;

	vector<string> audioFiles;
	vector<float> dur;

	float audioLen;

	bool playing;

	bool Spatialization;
};
