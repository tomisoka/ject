#include "Coll.h"
#include "../gameData.h"
#include "../simplefc/render.h"
#include "../controls.h"
#include "../BtInterface/convertVM.h"
#include "../BtInterface/UsefullFunctions.h"
#include "../BtInterface/BodyInterface.h"
#include "../BtInterface/Shape.h"
#include "../BtInterface/WorldInterface.h"
#include "../simplefc/settings.h"
#include "../simplefc/xml.h"

Coll::Coll(vec3 relPos, quat relRot, btCollisionShape *shape, uint32_t tickActive): maxTicksActive(tickActive),
	relPos(relPos), relRot(relRot){

	playerOnly = 0;
	state = 0;
	first = 1;
	actTicksActive=0;
	actBt=1;

	ghost = new btPairCachingGhostObject();
	ghost->setCollisionShape(shape);
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	world_interface->add_ghost(ghost, this);

	initVertices();
}

Coll::Coll(const Coll& o): ObjectComponent(o), TriggerCaller(o), playerOnly(o.playerOnly), maxTicksActive(o.maxTicksActive), relPos(o.relPos), relRot(o.relRot){
	state = 0;
	first = 1;
	actTicksActive=0;
	actBt = o.actBt;

	ghost = new btPairCachingGhostObject();
	ghost->setCollisionShape(o.ghost->getCollisionShape());
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);

	if(actBt)world_interface->add_ghost(ghost, this);
	initVertices();
}

Coll::Coll(xml_node xml){
	btCollisionShape *shape = loadShape(xml.child("collShape"));
	relPos = xmlRVec3(xml, "relPos", vec3(0));
	relRot = xmlRRot(xml, "relRot", quat());
	maxTicksActive = int(xmlRFloat(xml, "delay", 0)*60);
	playerOnly = xmlRBool(xml, "player_only", 0);

	ghost = new btPairCachingGhostObject();
	ghost->setCollisionShape(shape);
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	world_interface->add_ghost(ghost, this);

	if(playerOnly)only.push_back(CameraObj->getBtBody());
	state = 0;
	first = 1;
	actTicksActive=0;
	actBt=1;

	init(xml);
	initVertices();
}

Coll::~Coll(){
	if(actBt)world_interface->remove_ghost(ghost);
	delete ghost;
	render::delBuffer(&vertexbuffer);
}

void Coll::actBtBody(bool act){
	if(act==actBt)return;

	if(act)world_interface->add_ghost(ghost, this);
	else world_interface->remove_ghost(ghost);

	ObjectComponent::actBtBody(act);
}

void Coll::saveXml(xml_node xml){
	xml_node collShape = xml.append_child("collShape");
	saveShape(ghost->getCollisionShape(), collShape);
	if(relPos!=vec3(0))xmlWVec3(xml, "relPos", relPos);
	if(relRot!=quat())xmlWRot(xml, "relRot", relRot);
	if(maxTicksActive)xmlWFloat(xml, "delay", maxTicksActive * TICK_TIME);
	if(playerOnly)xmlWBool(xml, "player_only", playerOnly);
	save(xml);
}

void Coll::setEditMode(bool editMode){
	if(editMode){
		state = 0;
		actTicksActive = 0;
	}
}

void Coll::update(){
	bool actState=0;

	GhostOverlap(ghost, [&](const btManifoldPoint&, const btCollisionObject*, bool){
		actState = 1;
		return 1;
	}, only, 0, 1);

	if(!statical){
		if(actState){
			if(first)first=0;
			else if(actState!=state){
				state=1;
				callit(state);
			}
			actTicksActive = maxTicksActive;
		}else if(state){
			if(actTicksActive)actTicksActive--;
			else{
				state = 0;
				callit(state);
			}
		}
	}

	btTransform tr;

	tr.setOrigin(btVec3(relPos));
	tr.setRotation(btQuat(relRot));

	btTransform trans;
	if(parent->getBtBody() && !statical)parent->getBtBody()->predictIntegratedTransform (1.f/TPS, trans);
	else{
		trans = btTrans(parent->getTransform());
	}
	ghost->setWorldTransform(trans*tr);
}

void Coll::initVertices(){
	vertexbuffer = ShapeVisualization(vertex_len, ghost->getCollisionShape());
}

void Coll::render(){
	if(!showGhosts || !vertex_len)return;
	mat4 transform = parent->getTransform()*relPos.toTransM()*relRot.toRotMatrix();

	render::setLine();

	render::setTrans(transform);
	render::setColor(vec3(1, 1, 0));

	render::setAttrPoint(0, 3, vertexbuffer);
	render::draw(vertex_len);
}
