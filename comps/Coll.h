#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"

class btPairCachingGhostObject;
class btCollisionShape;

class Coll: public ObjectComponent, public TriggerCaller{
public:
	Coll(vec3 relPos, quat relRot, btCollisionShape *shape, uint32_t tickActive = 0);
	Coll(const Coll& o);
	Coll(xml_node xml);
	~Coll();

	ObjectComponent *Copy() override{return new Coll(*this);}

	void update() override;
	void render() override;

	void addOnly(btCollisionObject *obj){only.push_back(obj);}

	void setEditMode(bool editMode) override;
	map<string, string> propagateStringChange() override{return stringChangeF();}
	void requestTriggers() override{requestTriggerAClear();}

	uint32_t getID() override{return 9;}
	string getIDName() override{return "Coll";}

	void saveXml(xml_node xml) override;
	void actBtBody(bool act) override;
private:
	vector<const btCollisionObject *> only;
	bool playerOnly;

	void initVertices();

	uint_t vertexbuffer;
	size_t vertex_len;

	uint32_t maxTicksActive;
	uint32_t actTicksActive;

	btPairCachingGhostObject *ghost;
	bool state;
	vec3 relPos;
	quat relRot;

	bool first;
};
