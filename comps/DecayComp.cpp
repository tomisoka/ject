#include "DecayComp.h"
#include "Render3D.h"
#include "../particles.h"
#include "../gameData.h"
#include "../simplefc/rand.h"
#include "../BtInterface/includeBullet.h"
#include "../BtInterface/BodyInterface.h"
#include "../simplefc/xml.h"

DecayComp::DecayComp(uint32_t laserTicks, TriggerData *rendD): laserTicksMax(laserTicks), laserTicksAct(laserTicks), rendD(rendD){
	if(!laserTicksAct)decayed();
}

DecayComp::DecayComp(const DecayComp& o): LaserCollect(o), laserTicksMax(o.laserTicksMax), laserTicksAct(o.laserTicksAct){
	if(o.rendD){
		rendD = new TriggerData(*o.rendD);
		triggerRedirectFind(rendD->tCall, rendD);
	}else rendD = 0;
}

void DecayComp::saveXmlLas(xml_node xml){
	xmlWFloat(xml, "decay", laserTicksMax/60.f);
	if(rendD)xmlWString(xml, "rend", rendD->nameCall);
	save(xml);
}

void DecayComp::addParticles(Object *obj){
	uint32_t i;
	float num;
	vec3 psP, mvP;
	float size = 0.015;
	float angle, distP;

	BodyInterface *body = obj->getBtBody();

	mat4 tran = obj->getTransform();

	if(body){
		btCollisionShape *sh = body->getCollisionShape();

		btBoxShape *shapeB;
		btCylinderShape *shapeC;
		btSphereShape *shapeS;

		int tp = sh->getShapeType();
		if(tp == BOX_SHAPE_PROXYTYPE){
			shapeB = (btBoxShape*)sh;
			btVector3 sz = shapeB->getHalfExtentsWithMargin();

			num = sz.getX()*sz.getY()*sz.getZ()*3000.f;
			while(num > 5000){
				if(num>10000)num -= 5000;
				else num = 5000;
				size += 0.001;
			}

			for(i=0;i<num;++i){
				psP = vec3(getRandDouble2()*sz.getX(), getRandDouble2()*sz.getY(), getRandDouble2()*sz.getZ());
				psP = tran*psP;
				addParticle(new Particle(psP, vec3(getRandDouble2(), getRandDouble2()+0.5f, getRandDouble2()), vec4(0.7, 0.7, 0.7, 0.8), size, getRandDouble()*2, 1));
			}
		}else if(tp == CYLINDER_SHAPE_PROXYTYPE){
			shapeC = (btCylinderShape*)sh;
			btVector3 sz = shapeC->getHalfExtentsWithMargin();

			num = sz.getX()*sz.getY()*sz.getZ()*2500.f;
			while(num > 5000){
				if(num>10000)num -= 5000;
				else num = 5000;
				size += 0.001;
			}

			for(i=0;i<num;++i){
				angle = getRandDouble()*M_PI*2;

				distP = getRandDouble() + getRandDouble();
				if(distP>1)distP = 2-distP;

				psP = vec3(getRandDouble2()*sin(angle)*sz.getX() * distP, getRandDouble2()*sz.getY(), getRandDouble2()*sz.getZ()*cos(angle)*distP);
				psP = tran*psP;
				addParticle(new Particle(psP, vec3(getRandDouble2(), getRandDouble2()+0.5f, getRandDouble2()), vec4(0.7, 0.7, 0.7, 0.8), size, getRandDouble()*2, 1));
			}
		}else if(tp == SPHERE_SHAPE_PROXYTYPE){
			shapeS = (btSphereShape*)sh;
			float angle2;
			float r = shapeS->getRadius();
			num = r*r*r*2000.f;
			while(num > 5000){
				if(num>10000)num -= 5000;
				else num = 5000;
				size += 0.001;
			}

			for(i=0;i<num;++i){
				angle = getRandDouble()*M_PI*2;
				angle2 = getRandDouble()*M_PI*2;

				distP = getRandDouble() + getRandDouble();
				if(distP>1)distP = 2-distP;
				distP *= r;

				psP = vec3(getRandDouble2()*sin(angle) * distP, getRandDouble2()*sin(angle2)*distP, getRandDouble2()*cos(angle)*distP);
				psP = tran*psP;
				addParticle(new Particle(psP, vec3(getRandDouble2(), getRandDouble2()+0.5f, getRandDouble2()), vec4(0.7, 0.7, 0.7, 0.8), size, getRandDouble()*2, 1));
			}
		}
	}

	vector<Object*>& children = obj->getChildren();
	for(i=0;i<children.size();++i){
		addParticles(children[i]);
	}
}

void DecayComp::setEditMode(bool editMode){
	if(editMode){
		laserTicksAct = laserTicksMax;
		if(rendD && rendD->tCall){
			float col = (0.6f + float(laserTicksAct)/float(laserTicksMax)*0.4f);

			((Render3D *) rendD->tCall)->setColor(vec3(col));
		}
	}
}
map<string, string> DecayComp::propagateStringChange(){
	map<string, string> s;
	if(rendD->nameCall != rendD->oldnameCall){
		s[rendD->oldnameCall] = rendD->nameCall;
	}
	return s;
}

void DecayComp::decayed(){
	addParticles(parent);

	parent->actBtBodyCh(0);
	parent->setActive(0);
}

void DecayComp::update(){
	if(state!=lastState)callit(state);
	lastState=state;
	state=0;
}

void DecayComp::collect(){
	if(statical)return;
	state=1;
	laserTicksAct--;
	if(!laserTicksAct){
		if(lastState)callit(0);
		decayed();
	}else{
		if(rendD && rendD->tCall){
			float col = (0.6f + float(laserTicksAct)/float(laserTicksMax)*0.4f);
			((Render3D *)rendD->tCall)->setColor(vec3(col));
		}
	}
}
