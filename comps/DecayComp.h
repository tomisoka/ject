#pragma once

#include "Laser.h"
#include "../Object.h"

class ObjectRenderer;

class DecayComp: public LaserCollect{
public:
	DecayComp(uint32_t laserTicks, TriggerData *rend=0);
	DecayComp(const DecayComp& o);

	ObjectComponent *Copy() override{return new DecayComp(*this);}

	void update() override;
	void collect() override;

	uint32_t getID() override{return 11;}
	string getIDName() override{return "DecayComp";}

	void setEditMode(bool editMode) override;

	map<string, string> propagateStringChange() override;
	void requestTriggers() override{rendD->requestTrigger();}

	void saveXmlLas(xml_node xml) override;
private:
	void decayed();
	void addParticles(Object *obj);

	uint32_t laserTicksMax;
	uint32_t laserTicksAct;
	TriggerData *rendD;
};
