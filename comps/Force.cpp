#include "Force.h"
#include "../BtInterface/BodyInterface.h"
#include "../BtInterface/convertVM.h"
#include "../InputEdit.h"
#include "../edit/edit.h"
#include "../gameData.h"

Force::Force(): dir(vec3(0, 1, 0)), acc(1), forcing(0), impulse(0){}

Force::Force(xml_node xml): forcing(0), impulse(0){
	dir = xmlRVec3(xml, "direction3", vec3(0, 1, 0)).normalized();
	acc = xmlRFloat(xml, "acc", 1);
	impulse = xmlRBool(xml, "impulseB", 0);
}

void Force::saveXml(xml_node xml){
	xmlWVec3(xml, "direction3", dir);
	xmlWFloat(xml, "acc", acc);
	xmlWBool(xml, "impulseB", impulse);
}

void Force::call(int code){
	if(statical)textin_callback(code, "");
	else{
		if(code && impulse && !forcing)act();
		forcing = code;
	}
}

void Force::act(){
	vector<Object*> objs = parent->getAllChildren();
	for(Object* o: objs){
		BodyInterface *body = o->getBtBody();
		if(body && body->getInvMass())body->applyForce(btVec3(dir * acc / body->getInvMass()), btVector3(0, 0, 0));
	}
}

void Force::update(){
	if(forcing && !impulse)act();
}

void Force::setEditMode(bool editMode){
	if(editMode)forcing = 0;
}

void Force::textin_callback(size_t issueID, string input){
	size_t actCode = 0;

	if(issueID == actCode)delMe();
	actCode++;

	if(edit_mvec(actCode, issueID, input, dir))return;
	if(edit_float(actCode, issueID, input, acc))return;
	if(edit_bool(actCode, issueID, input, impulse))return;

	edit->call(0x7FFF0000);
}
