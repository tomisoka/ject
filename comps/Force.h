#pragma once
#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"
#include "../simplefc/xml.h"
#include "../GUI/Textin.h"

class Force: public ObjectComponent, public Trigger, public TextinCallBack{
public:
	Force();
	Force(xml_node xml);

	Force* Copy() override{return new Force(*this);}

	void update() override;

	void saveXml(xml_node xml) override;

	void call(int code) override;
	void textin_callback(size_t issueID, string input) override;

	uint32_t getID() override {return 24;}
	string getIDName() override {return "Force";}

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	void setEditMode(bool editMode)override;

	vec3 getDir(){return dir;}
	float getAcc(){return acc;}
	bool getImpulse(){return impulse;}
private:
	void act();

	vec3 dir;
	float acc;

	bool forcing;
	bool impulse;
};
