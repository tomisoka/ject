#include "Laser.h"
#include "../gameData.h"
#include "../BtInterface/convertVM.h"
#include "../BtInterface/BodyInterface.h"
#include "../simplefc/xml.h"
#include "../simplefc/render.h"

LaserCollect::LaserCollect(const LaserCollect& o):ObjectComponent(o), TriggerCaller(o), state(0), lastState(0){}

LaserCollect::~LaserCollect(){
	btCollisionObject *toDel = parent->getBtBody();
	removeObjLaserData(toDel);
}

void LaserCollect::postLoaded(){
	btCollisionObject *body = parent->getBtBody();
	if(body) addObjLaserData(body, this);
}

void LaserCollect::saveXmlLas(xml_node xml){
	save(xml);
}

void LaserCollect::update(){
	if(statical)return;
	if(state!=lastState)callit(state);
	lastState=state;
	state=0;
}

void LaserCollect::collect(){
	state=1;
}

void LaserCollect::setEditMode(bool editMode){
	if(editMode){
		lastState = 0;
		state = 0;
	}
}

map<const btCollisionObject*, LaserCollect*> laserData;

void addObjLaserData(btCollisionObject *obj, LaserCollect* data){
	laserData[obj] = data;
}

void removeObjLaserData(btCollisionObject *obj){
	laserData.erase(obj);
}

LaserEmit::LaserEmit(vec3 dir, vec3 col, vec3 relPos, quat rot):
		Render3DLines({},col,relPos,rot), dir(dir){
}

LaserEmit::LaserEmit(const LaserEmit& o): Render3DLines(o), Trigger(o), states(o.states), dir(o.dir){}

LaserEmit::LaserEmit(xml_node xml):Render3DLines({},vec3(1,0,0)){
	pos = xmlRVec3(xml, "pos", vec3(0));
	dir = xmlRVec3(xml, "dir", vec3(1));
	xmlRBool(xml, "stateX", states);
	if(!states.empty())active = states[0];
	resetTransform();
}

void LaserEmit::saveXml(xml_node xml){
	xmlWVec3(xml, "pos", pos);
	xmlWVec3(xml, "dir", dir);
	xmlWBool(xml, "stateX", states);
}

void LaserEmit::update(){
	if(!active)return;
	uint32_t ref=10;

	btVector3 actPos=btVec3(getTransform().getPos());
	btVector3 dirPos=btVec3(getTransform()*(dir*1000000.f));

	bool reflect;

	vertices.clear();

	vector<btVector3> v;
	do{
		reflect=0;

		btVector3 hit, btHitNormal;
		const btCollisionObject *colObj = closestObjectIgnore(actPos, dirPos, 0x2, hit, btHitNormal);

		if(colObj){
			v.push_back(actPos);
			v.push_back(hit);

			auto it = laserData.find(colObj);
			LaserCollect *dataL=0;
			if(it!=laserData.end()){
				dataL = it->second;
			}

			if(dataL){
				if(ref && dataL->getReflect()){
					ref--;
					reflect=1;


					btVector3 norm = btHitNormal.normalized();

					norm.normalize();
					btVector3 temp = (actPos-hit) - norm*((actPos-hit).dot(norm))*2;
					temp.normalize();
					temp*=-1000.f;
					temp+=hit;

					actPos=hit;
					dirPos=temp;
				}
				if(!statical)dataL->collect();
			}
		}else{
			v.push_back(actPos);
			v.push_back(dirPos);
		}
	}while(reflect);
	for(btVector3 vc: v)vertices.push_back(Vec3(vc));
}

void LaserEmit::call(int state){
	if((int)states.size() < state){
		active = states[state];
	}else active = state;
}

void LaserEmit::setEditMode(bool editMode){
	if(editMode){
		if(!states.empty())active = states[0];
		else active = 1;
	}
}

void LaserEmit::renderReg(){
	if(!vertices.size() || !active)return;

	vertexbuffer = render::getBuffer(vertices);

	render::setTrans(vec3(0).toTransM());
	render::setColor(color);
	render::setAttrPoint(0, 3, vertexbuffer);
	render::draw(vertices.size());
	render::delBuffer(&vertexbuffer);
}
