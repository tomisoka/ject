#pragma once

#include "../simplefc/trigger.h"
#include "../ObjectComponent.h"
#include "Render3DLine.h"

class LaserCollect:public ObjectComponent, public TriggerCaller{
public:
	LaserCollect(bool reflect = 0, bool state = 0):state(state), lastState(state), reflect(reflect){}
	LaserCollect(const LaserCollect& o);
	~LaserCollect();

	ObjectComponent *Copy() override{return new LaserCollect(*this);}

	void postLoaded() override;

	void update() override;
	virtual void collect();

	virtual void saveXmlLas(xml_node xml);

	void setEditMode(bool editMode) override;
	map<string, string> propagateStringChange() override{return stringChangeF();}
	void requestTriggers() override{requestTriggerAClear();}

	bool getReflect(){return reflect;}

	uint32_t getID() override{return 3;}
	string getIDName() override{return "LaserCollect";}
protected:

	bool state;
	bool lastState;

	bool reflect;
};

extern map<const btCollisionObject*, LaserCollect*> laserData;

void addObjLaserData(btCollisionObject *obj, LaserCollect* data);
void removeObjLaserData(btCollisionObject *obj);

class LaserEmit: public Render3DLines, public Trigger{
public:
	LaserEmit(vec3 dir, vec3 col=vec3(1,0,0), vec3 relPos=vec3(0), quat rot=quat());
	LaserEmit(const LaserEmit& o);
	LaserEmit(xml_node xml);

	ObjectComponent *Copy() override{return new LaserEmit(*this);}

	void update() override;
	void renderReg() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 4;}
	string getIDName() override{return "LaserEmit";}

	void saveXml(xml_node xml) override;
protected:
	vector<bool> states;
	vec3 dir;
};
