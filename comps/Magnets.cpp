#include "Magnets.h"
#include "../BtInterface/convertVM.h"
#include "../BtInterface/BodyInterface.h"
#include "../BtInterface/UsefullFunctions.h"
#include "../BtInterface/WorldInterface.h"
#include "../simplefc/settings.h"
#include "../simplefc/xml.h"

vector<BodyInterface *> magneticObjs;

void addMagneticObj(BodyInterface *obj){
	push_new(magneticObjs, obj);
}

void remMagneticObj(BodyInterface *obj){
	erase(magneticObjs, obj);
}

Magnet::Magnet():act_acc(0){
	accs.push_back(1);
}

Magnet::Magnet(const Magnet& o): ObjectComponent(o), Trigger(o), accs(o.accs), act_acc(o.act_acc){}

Magnet::Magnet(xml_node xml):act_acc(0){
	xmlRFloat(xml, "accX", accs);
}

Magnet::~Magnet(){
	if(ghost){
		if(actBt)world_interface->remove_ghost(ghost);
		delete ghost;
	}
}

void Magnet::postLoaded(){
	btRigidBody *body = parent->getBtBody();
	if(body){
		ghost = new btPairCachingGhostObject();
		ghost->setCollisionShape(body->getCollisionShape());
		shape = body->getCollisionShape();
		ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
		world_interface->add_ghost(ghost, this);
	}else{
		ghost = 0;
	}
}

void Magnet::saveXml(xml_node xml){
	xmlWFloat(xml, "accX", accs);
}

void Magnet::actBtBody(bool act){
	if(actBt == act)return;
	if(ghost){
		if(actBt)world_interface->remove_ghost(ghost);
		else world_interface->add_ghost(ghost, this);
	}
	actBt=act;
}

btVector3 Magnet::getClosestPoint(btVector3 p){

	btRigidBody *body = parent->getBtBody();
	if(!body)return btVec3(parent->getTransform().getPos());

	btTransform trans = body->getWorldTransform();
	btVector3 relP = trans.inverse() * p;

	btVector3 closest;

	btBoxShape *shapeB;
	btCylinderShape *shapeC;
	btSphereShape *shapeS;
	int tp = shape->getShapeType();
	if(tp == BOX_SHAPE_PROXYTYPE){
		shapeB = (btBoxShape*)shape;
		btVector3 sz = shapeB->getHalfExtentsWithMargin();

		if(relP.getX() > sz.getX())closest.setX(sz.getX());
		else if(relP.getX() < -sz.getX())closest.setX(-sz.getX());
		else closest.setX(relP.getX());
		if(relP.getY() > sz.getY())closest.setY(sz.getY());
		else if(relP.getY() < -sz.getY())closest.setY(-sz.getY());
		else closest.setY(relP.getY());
		if(relP.getZ() > sz.getZ())closest.setZ(sz.getZ());
		else if(relP.getZ() < -sz.getZ())closest.setZ(-sz.getZ());
		else closest.setZ(relP.getZ());
	}else if(tp == CYLINDER_SHAPE_PROXYTYPE){
		shapeC = (btCylinderShape*)shape;
		btVector3 sz = shapeC->getHalfExtentsWithMargin();

		if(relP.getY() > sz.getY())closest.setY(sz.getY());
		else if(relP.getY() < -sz.getY())closest.setY(-sz.getY());
		else closest.setY(relP.getY());

		vec2 relP2 = vec2(relP.getX(), relP.getZ());
		if(relP2.lengthSq() < sz.getX() * sz.getZ()){
			closest.setX(relP2.getX());
			closest.setZ(relP2.getY());
		}else{
			relP2.normalize();
			closest.setX(relP2.getX() * sz.getX());
			closest.setZ(relP2.getY() * sz.getZ());
		}
	}else if(tp == SPHERE_SHAPE_PROXYTYPE){
		shapeS = (btSphereShape*)shape;

		float r = shapeS->getRadius();

		closest = relP.normalized() * r;
	}
	return trans*closest;
}

void Magnet::call(int index){
	if(index >= (int) accs.size()){
		printLogWarn("Invalid index (" + to_string(index) + ") send to Magnet (max " + to_string(accs.size())+ ")");
	}else{
		act_acc = index;
	}
}

void Magnet::update(){
	for(BodyInterface *body: colledObj)body->set_static(0);
	colledObj.clear();


	GhostOverlap(ghost, [&](const btManifoldPoint& pt, const btCollisionObject* o, bool first){
		const btVector3& normalOnB = pt.m_normalWorldOnB * (first?1:-1);

		BodyInterface *body = (BodyInterface *)o;
		if(body->getGravity().length2() < 0.1f)return 2;

		float acc = accs[act_acc]/30;
		if(acc > 3)acc = 3;

		if(accs[act_acc] >= 2.5)body->setGravity(btVector3(0,0,0));
		body->setLinearFactor(btVector3(1,1,1)/3);
		body->setAngularFactor(btVector3(1,1,1)/3);
		body->applyCentralImpulse(normalOnB / body->getInvMass() * acc);
		colledObj.push_back(body);
		return 2;
	}, {}, 1, 1, [](const btCollisionObject* o){
		Object *obj = getObjColl(o, 1);
		if(!obj || !obj->getBtBody()->is_magnetic())return 1;
		return 0;
	});

	for(BodyInterface *body: magneticObjs){
		bool continue_for = 0;
		for(BodyInterface *colled: colledObj){
			if(body == colled){
				continue_for = 1;
				break;
			}
		}
		if(continue_for)continue;

		btVector3 bdPos = body->getWorldTransform().getOrigin();
		btVector3 btPos = getClosestPoint(bdPos);

		btVector3 diffPos = btPos-bdPos;

		float finalAcc = 1 / diffPos.length2() * accs[act_acc] * TICK_TIME;

		if(finalAcc > 20)finalAcc = 20;

		body->applyCentralImpulse(diffPos.normalized() / body->getInvMass() * finalAcc);
	}

	if(ghost)ghost->setWorldTransform(parent->getBtBody()->getWorldTransform());
}
