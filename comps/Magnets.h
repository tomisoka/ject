#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"

class btVector3;
class btPairCachingGhostObject;
class btCollisionShape;
class BodyInterface;

void addMagneticObj(BodyInterface *obj);
void remMagneticObj(BodyInterface *obj);

class Magnet: public ObjectComponent, public Trigger{
public:
	Magnet();
	Magnet(const Magnet& o);
	Magnet(xml_node xml);
	~Magnet();

	ObjectComponent *Copy() override{return new Magnet(*this);}

	void update() override;
	void actBtBody(bool act) override;

	void call(int index);

	uint32_t getID() override{return 20;}
	string getIDName() override{return "Magnet";}

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	void postLoaded() override;

	void saveXml(xml_node xml) override;
private:
	btVector3 getClosestPoint(btVector3 p);

	btPairCachingGhostObject *ghost;
	btCollisionShape *shape;


	vector<BodyInterface *> colledObj;

	vector<float> accs;
	uint32_t act_acc;
};
