#include "MoveComponent.h"
#include "../gameData.h"
#include "../edit/edit.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/xml.h"
#include "../InputEdit.h"

MoveComponent::MoveComponent(vec3 targPos): act_pos(vec3(0)), blocked(0), speed(1){
	targPses.push_back(targPos);
	active = 0;
}

MoveComponent::MoveComponent(const MoveComponent& o):ObjectComponent(o), Trigger(o), act_pos(o.act_pos), blocked(o.blocked), targPs(o.targPs), targPses(o.targPses), speed(o.speed){
	active = 0;
}

MoveComponent::MoveComponent(xml_node xml): act_pos(vec3(0)), blocked(0){
	speed = xmlRFloat(xml, "speed", 1);
	xmlRVec3(xml, "goalX3", targPses);
	xmlRVec3(xml, "goal", targPses);
	if(!targPses.size())targPses.push_back(vec3(0));
	active = 0;
}


void MoveComponent::saveXml(xml_node xml){
	xmlWFloat(xml, "speed", speed);
	xmlWVec3(xml, "goalX3", targPses);
}

void MoveComponent::textin_callback(size_t issueID, string input){
	size_t actCode = 0;

	if(issueID == actCode)delMe();
	actCode++;

	if(edit_float(actCode, issueID, input, speed))return;
	if(edit_vec(actCode, issueID, input, targPses, function<bool(string&, vec3&)>([=](string& input, vec3& out){
		vec3 in;
		if(strToVec(input.c_str(), in))return 1;
		out = in - (rel_abs?parent->getPos():vec3(0));
		return 0;
	})))return;

	edit->call(0x7FFF0000);
}


void MoveComponent::update(){
	if(active && !statical){
		float mv = speed * TICK_TIME;
		vec3 targ = targPses[targPs];

		if((act_pos - targ).lengthSq()<mv*mv){
			parent->setPos(parent->getPos() + targ - act_pos);
			act_pos = targ;
			active=0;
		}else{
			vec3 relPos = targ - act_pos;
			relPos.normalize();
			act_pos += relPos*mv;
			parent->setPos(parent->getPos()+relPos*mv);
		}
	}
}

void MoveComponent::call(int state){
	if(statical){
		textin_callback(state, "");
	}else{
		int size = targPses.size();
		if(state < size){
			if(!blocked){
				targPs = state;
				active=1;
			}
		}else if(state < size*2){
			targPs = state - size;
			active=1;
			blocked = 1;
		}else if(state < size*3){
			targPs = state - size*2;
			active=1;
			blocked = 0;
		}
	}
}

void MoveComponent::setEditMode(bool editMode){
	if(editMode){
		active = 0;
		blocked = 0;
		act_pos = vec3(0);
	}
}


RotComponent::RotComponent(quat targRot): act_rot(quat()), blocked(0){
	speed = 1;
	targRots.push_back(targRot);
	active = 0;
}

RotComponent::RotComponent(const RotComponent& o): ObjectComponent(o), Trigger(o), act_rot(o.act_rot), blocked(o.blocked), targRot(o.targRot), targRots(o.targRots), speed(o.speed){
	active = 0;
}

RotComponent::RotComponent(xml_node xml): act_rot(quat()), blocked(0){
	speed = xmlRFloat(xml, "speed", 1);
	vec3 goalV3;
	xmlRRot(xml, "goalX4", targRots);
	xmlRRot(xml, "goalX3", targRots);
	xmlRRot(xml, "goal", targRots);
	if(!targRots.size())targRots.push_back(quat());
	active = 0;
}


void RotComponent::saveXml(xml_node xml){
	xmlWFloat(xml, "speed", speed);
	xmlWRot(xml, "goalX4", targRots);
}

void RotComponent::textin_callback(size_t issueID, string input){
	size_t actCode = 0;

	if(issueID == actCode)delMe();
	actCode++;

	if(edit_float(actCode, issueID, input, speed))return;
	if(edit_vec(actCode, issueID, input, targRots, function<bool(string&, quat&)>([=](string& input, quat& out){
		quat in;
		if(quat_euler){
			vec3 inV;
			if(strToVec(input.c_str(), inV))return 1;
			in = quat(inV * M_PI/180);
		}else if(strToVec(input.c_str(), in))return 1;
		if(rel_abs)out = parent->getRot().diff(in);
		else out = in;
		return 0;
	})))return;


	edit->call(0x7FFF0000);
}

void RotComponent::update(){
	if((active || targRots.size()==1) && !statical){
		float mv = speed * TICK_TIME;

		vec3 axis;
		float angle;
		if(targRots.size()>1){
			quat targ = targRots[targRot];

			quat diff = act_rot.diff(targ);
			diff.toAxis(axis, angle);
			if(angle < mv){
				parent->setRot(parent->getRot() * diff);
				act_rot = targ;
				active = 0;
			}else{
				act_rot = act_rot * quat(axis, mv);
				parent->setRot(parent->getRot() * quat(axis, mv));
			}
		}else if(targRots.size()==1){
			quat targ = targRots[0];
			if(targ == quat())return;

			targ.toAxis(axis, angle);

			parent->setRot(parent->getRot() * quat(axis, mv));
		}
	}
}

void RotComponent::call(int state){
	if(statical){
		textin_callback(state, "");
	}else{
		int size = targRots.size();
		if(state < size){
			if(!blocked){
				targRot = state;
				active=1;
			}
		}else if(state < size*2){
			targRot = state - size;
			active=1;
			blocked = 1;
		}else if(state < size*3){
			targRot = state - size*2;
			active=1;
			blocked = 0;
		}
	}
}

void RotComponent::setEditMode(bool editMode){
	if(editMode){
		active = 0;
		blocked = 0;
		act_rot = quat();
	}
}
