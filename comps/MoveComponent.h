#pragma once

#include "../simplefc/trigger.h"
#include "../ObjectComponent.h"
#include "../GUI/Textin.h"

class MoveComponent: public ObjectComponent, public Trigger, public TextinCallBack{
public:
	MoveComponent(vec3 targPos = vec3(0));
	MoveComponent(const MoveComponent& o);
	MoveComponent(xml_node xml);

	ObjectComponent *Copy() override{return new MoveComponent(*this);}

	void saveXml(xml_node xml) override;

	void update() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 5;}
	string getIDName() override{return "Mover";}

	float getSpeed(){return speed;}
	vector<vec3>& getGoals(){return targPses;}
private:
	void textin_callback(size_t issueID, string input) override;

	vec3 act_pos;

	bool blocked;

	uint32_t targPs;
	vector<vec3> targPses;
	float speed;
};


class RotComponent: public ObjectComponent, public Trigger, public TextinCallBack{
public:
	RotComponent(quat targRot = quat());
	RotComponent(const RotComponent& o);
	RotComponent(xml_node xml);

	ObjectComponent *Copy(){return new RotComponent(*this);}

	void saveXml(xml_node xml) override;

	void update() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 6;}
	string getIDName() override{return "Rotator";}

	float getSpeed(){return speed;}
	vector<quat>& getGoals(){return targRots;}
private:
	void textin_callback(size_t issueID, string input) override;

	quat act_rot;

	bool blocked;

	uint32_t targRot;
	vector<quat> targRots;
	float speed; //rad
};
