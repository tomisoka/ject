#include "Portal.h"

Portal::Portal(vec3 pos, quat rot): ObjectComponent(pos, rot), connected(0){
	vec3 p[] = {
		vec3( 0.5, 1, 0),
		vec3(-0.5, 1, 0),
		vec3(-0.5,-1, 0),
		vec3( 0.5,-1, 0),
	};
	vector<vec3> v = {
		p[0], p[1],
		p[1], p[2],
		p[2], p[3],
		p[3], p[0],
	};
	vertsb = render::getBuffer(v);
}

Portal::~Portal(){
	render::delBuffer(&vertsb);
}

void Portal::render(){
	render::setLine();
	render::setTrans(getTransform());
	render::setColor(vec3(0,0,1));
	render::setAttrPoint(0, 3, vertsb);
	render::draw(8);
}
