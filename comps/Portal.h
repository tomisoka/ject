#pragma once
#include "../ObjectComponent.h"
#include "../simplefc/render.h"

class Portal: public ObjectComponent{
public:
	Portal(vec3 pos = vec3(0), quat rot = quat());
	~Portal();

	void set_connected(Portal* connected){this->connected = connected;}

	uint32_t getID() override {return 23;}
	string getIDName() override {return "Portal";}

	void render() override;
private:
	Portal *connected;

	GLuint vertsb;
};
