#include "Render3D.h"
#include "../RenderController.h"
#include "../simplefc/objLoad.h"
#include "../simplefc/xml.h"
#include "../simplefc/render.h"

Render3D::Render3D(string file, uint32_t textureID, vec3 color, vec3 pos, quat rot, vec3 scale): ObjectComponent(pos, rot, scale), invert(0), textureID(textureID), color(color), file(file){
	cls.push_back(color);
	loadFaceObj(file.c_str(), vertices, indices);
	init();
}

Render3D::Render3D(const Render3D& o): ObjectComponent(o), Trigger(o), invert(o.invert), cls(o.cls), textureID(o.textureID), vertices(o.vertices), uvs(o.uvs), indices(o.indices), file(o.file){
	init();
}

Render3D::Render3D(xml_node xml): ObjectComponent(xml), textureID(0), uvs(0){
	file = xmlRString(xml, "obj", "obj/cube.obj");
	invert = xmlRBool(xml, "invertColor", 0);

	loadFaceObj(file.c_str(), vertices, indices);

	xmlRVec3(xml, "colorX3", cls);
	xmlRVec3(xml, "color", cls);

	init();
}

void Render3D::init(){
	vertexbuffer = render::getBuffer(vertices);
	if(textureID)colorbuffer = render::getBuffer(uvs);
	indexbuffer = render::getBuffer(indices);

	if(!cls.size())cls.push_back(vec3(1));
	color = cls[0];

	Rcontrol->addRendC(this);
}

Render3D::~Render3D(){
	render::delBuffer(&vertexbuffer);
	if(textureID)render::delBuffer(&colorbuffer);
	render::delBuffer(&indexbuffer);
	Rcontrol->remRendC(this);
}

void Render3D::saveXml(xml_node xml){
	xmlWString(xml, "obj", file);
	if(invert)xmlWBool(xml, "invertColor", invert);
	xmlWVec3(xml, "colorX3", cls);

	ObjectComponent::saveXml(xml);
}

void Render3D::call(int index){
	if(index<(int)cls.size())color = cls[index];
}

void Render3D::setEditMode(bool editMode){
	if(editMode)color = cls[0];
}

void Render3D::renderReg(){
	if(!getActive())return;
	if(invert)render::setInvert(true);

	render::setTrans(getTransform());
	if(textureID)render::setTexture(textureID);
	else render::setColor(color);
	render::setAttrPoint(0, 3, vertexbuffer);
	if(textureID)render::setAttrPoint(1, 2, colorbuffer);
	render::bind_ibo(indexbuffer);
	render::draw_elements(indices.size() * 3);

	if(invert)render::setInvert(false);
}
