#pragma once
#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"

class Render3D: public ObjectComponent, public Trigger{
public:
	Render3D(string file, uint32_t textureID=0, vec3 color=vec3(0), vec3 relPos=vec3(0), quat rot=quat(), vec3 scale = vec3(1));
	Render3D(const Render3D& o);
	Render3D(xml_node xml);
	~Render3D();

	ObjectComponent *Copy() override{return new Render3D(*this);}

	void renderReg() override;

	void call(int index);

	uint32_t getID() override{return 1;}
	string getIDName() override{return "Render3D";}

	void saveXml(xml_node par) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getTexture(){return textureID;}
	bool isInverted(){return invert;}

	void setColor(vec3 color){this->color = color;}

protected:
	bool invert;

	vector<vec3> cls;

	void init();

	uint_t vertexbuffer;
	uint_t colorbuffer;
	uint_t indexbuffer;

	uint32_t textureID;

	vector<vec3> vertices;
	vector<vec2> uvs;
	vector<vec3i> indices;

	vec3 color;

	//edit stuff:
	string file;
};
