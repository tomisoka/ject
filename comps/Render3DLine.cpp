#include "Render3DLine.h"
#include "../RenderController.h"
#include "../simplefc/objLoad.h"
#include "../simplefc/xml.h"
#include "../simplefc/render.h"

Render3DLines::Render3DLines(string file, vec3 color, vec3 pos, quat rot, vec3 scale): ObjectComponent(pos, rot, scale), color(color), file(file){
	loadLineObj(file.c_str(), vertices, indices);
	init();
}

Render3DLines::Render3DLines(const Render3DLines& o): ObjectComponent(o), vertices(o.vertices), color(o.color), file(o.file){
	init();
}

Render3DLines::Render3DLines(xml_node xml): ObjectComponent(xml), color(vec3(0)){
	file = xmlRString(xml, "obj", "obj/cubeLine.obj");
	loadLineObj(file.c_str(), vertices, indices);
	init();
}
void Render3DLines::init(){
	vertexbuffer = vertices.size() ? render::getBuffer(vertices) : 0;
	indexbuffer = render::getBuffer(indices);
	Rcontrol->addRendC(this);
}

Render3DLines::~Render3DLines(){
	render::delBuffer(&vertexbuffer);
	render::delBuffer(&indexbuffer);
	Rcontrol->remRendC(this);
}

void Render3DLines::saveXml(xml_node xml){
	xmlWString(xml, "obj", file);
	ObjectComponent::saveXml(xml);
}

void Render3DLines::renderReg(){
	if(!vertices.size() || !getActive())return;

	render::setTrans(getTransform());
	render::setColor(editColor?editColorR:color);
	render::setAttrPoint(0, 3, vertexbuffer);
	render::bind_ibo(indexbuffer);
	render::draw_elements(indices.size() * 2);
}
