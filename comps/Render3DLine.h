#pragma once
#include "../ObjectComponent.h"

class Render3DLines: public ObjectComponent{
public:
	Render3DLines(){}
	Render3DLines(string file, vec3 color=vec3(0.5f),vec3 pos=vec3(0),quat rot = quat(),vec3 scale=vec3(1));
	Render3DLines(const Render3DLines& o);
	Render3DLines(xml_node xml);
	virtual ~Render3DLines();

	ObjectComponent *Copy() override{return new Render3DLines(*this);}

	void renderReg() override;

	uint32_t getID() override{return 2;}
	string getIDName() override{return "Render3DLine";}

	void saveXml(xml_node xml) override;
protected:
	void init();

	vector<vec3> vertices;
	vector<vec2i> indices;

	uint_t vertexbuffer;
	uint_t indexbuffer;
	vec3 color;

	//edit stuff:
	string file;
};
