#include "Render3DSpecial.h"
#include "../simplefc/objLoad.h"
#include "../simplefc/render.h"

Render3DSpecial::Render3DSpecial(vec3 color, vec3 pos, quat rot, vec3 scale): ObjectComponent(vec3(2), rot, 0.04), color(color){
	//loadFaceObj("obj/char.obj", vertices, indices);
	//loadFaceObj("obj/bojack_model_test.obj", vertices, indices);
	vector<Color> cols;
	loadFaceObj("models/bojack_test.obj", vertices, indices, cols);
	//loadFaceObj("models/test.obj", vertices, indices, cols);
	vector<Color> cls;
	distribute_colors_per_face(indices, cols, cls);
	for(Color c: cls)colors.push_back(c.getColor3F());

	vertexbuffer = render::getBuffer(vertices);
	colorbuffer = render::getBuffer(colors);
	indexbuffer = render::getBuffer(indices, GL_ELEMENT_ARRAY_BUFFER);
	build_adjacent_indices(indices, indices_adjacent);
	index_adjacentbuffer = render::getBuffer(indices_adjacent, GL_ELEMENT_ARRAY_BUFFER);
}

Render3DSpecial::Render3DSpecial(const Render3DSpecial& o): ObjectComponent(o), vertices(o.vertices), indices(o.indices), indices_adjacent(o.indices_adjacent){
	vertexbuffer = render::getBuffer(vertices);
	colorbuffer = render::getBuffer(colors);
	indexbuffer = render::getBuffer(indices, GL_ELEMENT_ARRAY_BUFFER);
	index_adjacentbuffer = render::getBuffer(indices_adjacent, GL_ELEMENT_ARRAY_BUFFER);
}

Render3DSpecial::~Render3DSpecial(){
	render::delBuffer(&vertexbuffer);
	render::delBuffer(&colorbuffer);
	render::delBuffer(&indexbuffer);
	render::delBuffer(&index_adjacentbuffer);
}

void Render3DSpecial::render(){
	render::setOutline();
	render::setTrans(getTransform());
	render::setColor(vec3(0));
	render::setAttrPoint(0, 3, vertexbuffer);
	render::bind_ibo(index_adjacentbuffer);
	render::draw(indices_adjacent.size() * 6);

	render::set_color_per_face_3D();
	render::setTrans(getTransform());
	render::setAttrPoint(0, 3, vertexbuffer);
	render::setAttrPoint(1, 3, colorbuffer);
	render::bind_ibo(indexbuffer);
	render::draw_elements(indices.size() * 3);
}
