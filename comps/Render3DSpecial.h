#pragma once
#include "../ObjectComponent.h"

class Render3DSpecial: public ObjectComponent{
public:
	Render3DSpecial(vec3 color=vec3(0), vec3 relPos=vec3(0), quat rot=quat(), vec3 scale = vec3(1));
	Render3DSpecial(const Render3DSpecial& o);
	~Render3DSpecial();

	ObjectComponent *Copy() override{return new Render3DSpecial(*this);}

	void render() override;

	uint32_t getID() override{return 25;}
	string getIDName() override{return "Render3DSpecial";}
protected:
	uint_t vertexbuffer;
	uint_t colorbuffer;
	uint_t indexbuffer;
	uint_t index_adjacentbuffer;

	vector<vec3> vertices;
	vector<vec3> colors;
	vector<vec3i> indices;
	vector<Vector<int, 6>> indices_adjacent;

	vec3 color;
};
