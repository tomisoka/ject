#include "Repeller.h"
#include "../gameData.h"
#include "../particles.h"
#include "../simplefc/settings.h"
#include "../simplefc/rand.h"
#include "../BtInterface/convertVM.h"
#include "../BtInterface/BodyInterface.h"
#include "../simplefc/xml.h"
#include "../BtInterface/UsefullFunctions.h"
#include "../BtInterface/WorldInterface.h"

Repeller::Repeller(float acc, float height, float radius, vec3 relPos, quat relRot){
	accs.push_back(acc);
	init(height, radius, relPos, relRot);
}

Repeller::Repeller(const Repeller& o): ObjectComponent(o), Trigger(o), accs(o.accs){
	init(o.height, o.radius, o.relPos, o.relRot);
	actBtBody(o.actBt);
}

Repeller::Repeller(xml_node xml){
	relPos = xmlRVec3(xml, "relPos", vec3(0));
	relRot = xmlRRot(xml, "relRot", quat());
	height = xmlRFloat(xml, "height", 1);
	radius = xmlRFloat(xml, "radius", 1);

	xmlRFloat(xml, "accX", accs);
	xmlRFloat(xml, "acc", accs);

	init(height, radius, relPos, relRot);
}

void Repeller::init(float height, float radius, vec3 relPos, quat relRot){
	this->height = height;
	this->radius = radius;
	this->relPos = relPos;
	this->relRot = relRot;
	act_acc = 0;

	shape = new btCylinderShape(btVector3(radius, height/2.f, radius));

	ghost = new btPairCachingGhostObject();
	ghost->setCollisionShape(shape);
	ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
	world_interface->add_ghost(ghost, this);

	actBt=1;
	actHeight = height;

	ignore = 0;
}

Repeller::~Repeller(){
	if(actBt)world_interface->remove_ghost(ghost);
	delete ghost->getCollisionShape();
	delete ghost;
}

void Repeller::actBtBody(bool act){
	if(act==actBt)return;

	if(act)world_interface->add_ghost(ghost, this);
	else world_interface->remove_ghost(ghost);

	ObjectComponent::actBtBody(act);
}

void Repeller::saveXml(xml_node xml){
	if(relPos!=vec3(0))xmlWVec3(xml, "relPos", relPos);
	if(relRot!=quat())xmlWRot(xml, "relRot", relRot);
	xmlWFloat(xml, "height", height);
	xmlWFloat(xml, "radius", radius);
	xmlWFloat(xml, "accX", accs);
}

void Repeller::call(int index){
	if(index >= (int) accs.size()){
		printLogWarn("Invalid index (" + to_string(index) + ") send to Repeller (max " + to_string(accs.size()) + ")");
	}else{
		act_acc = index;
	}
}
void Repeller::setEditMode(bool editMode){
	if(editMode){
		act_acc = 0;
	}
}

void Repeller::update(){
	mat4 relTran = relPos.toTransM() * relRot.toRotMatrix();


	vector<vec3> hit;
	vector<vec3> normal;
	vec3 actPos= parent->getTransform() * relPos;
	mat4 checkH = parent->getTransform() * relTran;
	vec3 dirPos = checkH * vec3(0,height,0);
	actPos -= (dirPos - actPos).normalized() * 0.02f;

	vector<const btCollisionObject *> hit_objs = world_interface->all_ray_test(actPos, dirPos, hit, normal);


	float distC = FLT_MAX;
	float distTmp;

	for(size_t i=0;i<hit_objs.size();++i){
		const btCollisionObject *obj = hit_objs[i];
		obj->activate();
		if(obj->isStaticObject() || (obj->getInternalType() == 2 && !((btRigidBody*) obj)->getInvMass())){
			distTmp = (actPos - hit[i]).length();
			if(distTmp<distC)distC = distTmp;
		}
	}


	distC += 0.01f;
	if(distC<height){
		float aH = distC;
		if(actHeight!=aH){
			actHeight=aH;
			shape->setLocalScaling(btVector3(1,aH/height,1));
		}
	}else if(actHeight!=height){
		actHeight = height;
		shape->setLocalScaling(btVector3(1,1,1));
	}






	relTran.initTranslation(relPos + vec3(0,actHeight/2.f,0));
	relTran = relTran * relRot.toRotMatrix();

	mat4 allTran = parent->getTransform()*relTran;


	vector<btRigidBody*> others;

	if(!statical)GhostOverlap(ghost, [&](const btManifoldPoint&, const btCollisionObject *o, bool){
		if(o->getInternalType() == 2)push_new(others, (btRigidBody *)o);
		return 2;
	}, {parent->getBtBody()}, 1, 1);

	for(size_t i=0;i<others.size();++i){
		btRigidBody *otherR = others[i];
		if(otherR->getInvMass()==0 || otherR == ignore)continue;

		vec3 accV = vec3(0, (accs[act_acc])/otherR->getInvMass() * TICK_TIME, 0);
		accV = allTran * accV - allTran.getPos();

		otherR->applyCentralImpulse(btVec3(accV));
	}

	//spawn particles

	if(accs[act_acc]){
		double parts = TICK_TIME * radius * actHeight * 200.f * nwParticMult;
		double randM = getRandDouble();
		float lifeP = 0.8f;
		float speedP = sqrt((accs[act_acc])/4.f);
		vec3 mvP = allTran * vec3(0,speedP,0) - allTran.getPos();

		size_t i=0;
		while(parts > randM && i*lifeP<8000){
			double angle = getRandDouble() * M_PI*2;

			double distP = getRandDouble() + getRandDouble();
			if(distP>1)distP = 2-distP;

			float hP = actHeight * getRandDouble();
			lifeP = 0.3f + getRandDouble();

			float tempP = (speedP * lifeP + hP) - actHeight;
			if(tempP > 0)lifeP -= tempP/speedP * 0.9;

			vec3 psP = vec3(sin(angle)*distP*radius, hP-actHeight/2.f, cos(angle)*distP*radius);
			psP = allTran * psP + mvP * getRandDouble() * TICK_TIME;

			//addParticle(new Particle(psP, mvP, vec4(0.7, 0.7, 1, 0.8), 0.007, lifeP));
			addParticle(new Particle(psP, mvP, vec4(0.3, 0.3, 1, 0.8), 0.007, lifeP));

			parts-=randM;
			randM = getRandDouble();
			i++;
		}
	}





	btTransform tr = btTransform();

	tr.setOrigin(btVec3(relPos + vec3(0,actHeight/2.f,0)));
	tr.setRotation(btQuat(relRot));

	btTransform trans;
	if(parent->getBtBody() && !statical)parent->getBtBody()->predictIntegratedTransform (TICK_TIME, trans);
	else{
		trans = btTrans(parent->getTransform());
	}
	ghost->setWorldTransform(trans*tr);
}
