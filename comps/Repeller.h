#pragma once

#include "../ObjectComponent.h"
#include "../simplefc/trigger.h"

class btPairCachingGhostObject;
class btCollisionShape;

class Repeller: public ObjectComponent, public Trigger{
public:
	Repeller(float acc, float height, float radius, vec3 relPos = vec3(0), quat relRot = quat());
	Repeller(const Repeller& o);
	Repeller(xml_node xml);
	~Repeller();

	ObjectComponent *Copy() override{return new Repeller(*this);}

	void setIgnore(BodyInterface *ignore){this->ignore = ignore;}

	void call(int index);

	void update() override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 19;}
	string getIDName() override{return "Repeller";}


	void saveXml(xml_node xml) override;
	void actBtBody(bool act) override;
private:
	void init(float height, float radius, vec3 relPos, quat relRot);

	BodyInterface *ignore;

	vec3 relPos;
	quat relRot;

	btPairCachingGhostObject *ghost;
	btCollisionShape *shape;

	vector<float> accs;
	uint32_t act_acc;

	float height;
	float actHeight;

	float radius;
};
