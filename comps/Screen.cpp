#include "Screen.h"
#include "../RenderController.h"
#include "../simplefc/render.h"
#include "../simplefc/Text.h"
#include "../gameData.h"
#include "../BtInterface/convertVM.h"
#include "../BtInterface/UsefullFunctions.h"
#include "../BtInterface/WorldInterface.h"
#include "../simplefc/xml.h"

const float h = 0.1;

Screen::Screen(string file, vec3 pos, quat rot, float scale, bool lefted): ObjectComponent(pos, rot, scale), scale(1), lefted(lefted), file(file), to_update(0), vertBuff(0), uvsBuff(0), ghostVerticesBuff(0){
	init();
}

Screen::Screen(xml_node xml): ObjectComponent(xml), to_update(0), vertBuff(0), uvsBuff(0), ghostVerticesBuff(0){
	file = xmlRString(xml, "file", "levels/screen/tutorialWASDE");
	lefted = xmlRBool(xml, "lefted", 0);
	scale = xmlRFloat(xml, "scale", 1);
	ObjectComponent::scale = vec3(1);

	resetTransform();

	init();
}

Screen::Screen(const Screen& o): ObjectComponent(o), Trigger(o), scale(o.scale), lefted(o.lefted), file(o.file), lines(o.lines), to_update(1), vertBuff(0), uvsBuff(0), ghostVerticesBuff(0){
	init(0);
}

Screen::~Screen(){
	if(ghost){
		if(actBt)world_interface->remove_ghost(ghost);
		delete ghost->getCollisionShape();
		delete ghost;
	}

	render::delBuffer(&vertBuff);
	render::delBuffer(&uvsBuff);
	render::delBuffer(&ghostVerticesBuff);
	Rcontrol->remRendC(this);
}



void Screen::init(bool load){
	if(load){
		static char temp[256];

		FILE *Fin;
		if(file.length()){
			if(!(Fin = fopen(file.c_str(), "r"))){
				printLogErr("Cannon open screen file ("+file+")!");
				exit(1);
			}

			while(fgets(temp, 256, Fin))addLine(temp);

			fclose(Fin);
		}
	}

	if(editing){
		actBt=1;
		ghost = new btPairCachingGhostObject();
		btCollisionShape * sh = new btBoxShape(btVector3(1,1,0.1)*scale);
		ghost->setCollisionShape(sh);
		ghost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);
		world_interface->add_ghost(ghost, this);

		ghostVerticesBuff = ShapeVisualization(ghostVerticesSize, sh);
	}else{
		ghost = 0;
	}
	Rcontrol->addRendC(this);
}

void Screen::saveXml(xml_node xml){
	xmlWString(xml, "file", file);
	if(pos!=vec3(0))xmlWVec3(xml, "pos3", pos);
	if(rot!=quat())xmlWRot(xml, "rot4", rot);
	if(scale!=0)xmlWFloat(xml, "scale", scale);
	if(lefted)xmlWBool(xml, "lefted", lefted);
}

void Screen::actBtBody(bool act){
	if(!ghost || act==actBt)return;

	if(act)world_interface->add_ghost(ghost, this);
	else world_interface->remove_ghost(ghost);

	ObjectComponent::actBtBody(act);
}

void Screen::update(){
	if(!ghost)return;

	btTransform tr;

	tr.setOrigin(btVec3(pos));
	tr.setRotation(btQuat(rot));

	btTransform trans = btTrans(parent->getTransform());
	ghost->setWorldTransform(trans*tr);
}

void Screen::update_vertices(){
	render::delBuffer(&vertBuff);
	render::delBuffer(&uvsBuff);

	if(!lines.size())return;

	vector<vec3> end_verts;
	vector<vec2> end_uvs;

	for(size_t i = 0; i < lines.size(); i++){
		vector<vec2> verts;
		float w = text->getTextVerts(lines[i], verts, end_uvs);
		for(vec2 v: verts){
			end_verts.push_back(vec3(
				h*(lefted?(v.getX()-1):(v.getX() - w/2)),
				1+h*(v.getY() - i - 3/4.),
				0
			));
		}
	}


	vertBuff = render::getBuffer(end_verts);
	uvsBuff = render::getBuffer(end_uvs);
	vert_sz = end_verts.size();

	to_update = 0;
}

void Screen::renderReg(){
	if(!getActive())return;

	if(showGhosts && ghostVerticesBuff){
		render::setLine();

		render::setTrans(getTransform());
		render::setColor(vec3(1, 1, 0));

		render::setAttrPoint(0, 3, ghostVerticesBuff);
		render::draw(ghostVerticesSize);
	}

	if(to_update)update_vertices();

	if(!vertBuff)return;

	render::setAttrPoint(0, 3, vertBuff, GL_FLOAT);
	render::setAttrPoint(1, 2, uvsBuff, GL_FLOAT);

	render::setTrans(getTransform() * vec3(scale).toScaleM());
	render::setColor(vec3(2/5.));

	render::draw(vert_sz);
}

void Screen::addLine(string s){
	if(text->getTextWidth(s) * h < 2)lines.push_back(s);
	else{
		vector<char> cs;
		float w = 0;
		const char* str = s.c_str();
		for(int32_t i = 0; i < (int) s.size(); i++){
			if(w > 2){
				size_t back = 0;

				while(text->getTextWidth(string(&cs[0])) * h > 2 && cs.size() > 1){
					back++;
					cs.pop_back();
				}

				w = 0;
				lines.push_back(string(&cs[0]));
				cs.clear();
				i -= back - 1;
			}else{
				w += text->getCharWidth(str[i]) * h;
				cs.push_back(str[i]);
			}
		}
	}
	to_update = 1;
}
