#pragma once

#include "../simplefc/trigger.h"
#include "../ObjectComponent.h"

class btPairCachingGhostObject;

class Screen: public ObjectComponent, public Trigger{
public:
	Screen(string file, vec3 pos=vec3(0), quat rot=quat(), float scale=1, bool lefted = 0);
	Screen(xml_node file);
	Screen(const Screen& o);
	~Screen();

	ObjectComponent *Copy() override{return new Screen(*this);}

	void clear(){lines.clear(); to_update = 1;}

	uint32_t getID() override{return 12;}
	string getIDName() override{return "Screen";}

	void update() override;
	void renderReg() override;
	void saveXml(xml_node xml) override;

	void actBtBody(bool act) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	void addLine(string s);
private:
	void init(bool load = 1);
	void update_vertices();

	btPairCachingGhostObject *ghost;

	float scale;

	bool lefted;
	string file;

	vector<string> lines;
	bool to_update;

	uint_t vertBuff;
	uint_t uvsBuff;

	size_t vert_sz;


	uint_t ghostVerticesBuff;
	size_t ghostVerticesSize;
};
