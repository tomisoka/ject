#include "TrackComponent.h"
#include "../gameData.h"
#include "../edit/edit.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/xml.h"
#include "../InputEdit.h"

TrackMover::TrackMover(vec3 targPos):act_pos(vec3(0)), act_targ(0), act_speed(0), loops(1){
	targPses.push_back(targPos);
	speeds.push_back(0);
}

TrackMover::TrackMover(const TrackMover& o): ObjectComponent(o), Trigger(o), act_pos(o.act_pos), targPses(o.targPses), act_targ(o.act_targ), speeds(o.speeds), act_speed(o.act_speed), loops(o.loops){}

TrackMover::TrackMover(xml_node xml):act_pos(vec3(0)), act_targ(0), act_speed(0){
	loops = xmlRBool(xml, "loops", 1);
	xmlRFloat(xml, "speedX", speeds);
	if(!speeds.size())speeds.push_back(0);
	xmlRVec3(xml, "goalX3", targPses);
	if(!targPses.size())targPses.push_back(vec3(0));
}


void TrackMover::saveXml(xml_node xml){
	xmlWBool(xml, "loops", loops);
	xmlWFloat(xml, "speedX", speeds);
	xmlWVec3(xml, "goalX3", targPses);
}

void TrackMover::textin_callback(size_t issueID, string input){
	size_t actCode = 0;

	if(issueID == actCode)delMe();
	actCode++;

	if(edit_bool(actCode, issueID, input, loops))return;
	if(edit_vec(actCode, issueID, input, speeds, function<bool(string&, float&)>([=](string& input, float& out){
		float in;
		if(strToFloat(input.c_str(), in))return 1;
		out = in;
		return 0;
	})))return;
	if(edit_vec(actCode, issueID, input, targPses, function<bool(string&, vec3&)>([=](string& input, vec3& out){
		vec3 in;
		if(strToVec(input.c_str(), in))return 1;
		out = in - (rel_abs?parent->getPos():vec3(0));
		return 0;
	})))return;

	edit->call(0x7FFF0000);
}


void TrackMover::update(){
	if(active && !statical){
		float mv = fabs(speeds[act_speed] * TICK_TIME);
		if(mv == 0){
			active = 0;
			return;
		}

		size_t ind;
		if(speeds[act_speed] >= 0){
			ind = act_targ;
		}else if(act_targ == 0){
			if(!loops){
				active = 0;
				return;
			}else{
				ind = targPses.size()-1;
			}
		}else{
			ind = act_targ-1;
		}
		vec3 targ = targPses[ind];

		if((act_pos - targ).lengthSq()<mv*mv){
			parent->setPos(parent->getPos() + targ - act_pos);
			act_pos = targ;
			if(speeds[act_speed] >= 0){
				if(act_targ == targPses.size() - 1){
					if(!loops){
						active = 0;
						return;
					}
					act_targ = 1;
				}else act_targ++;
			}else{
				if(act_targ == 0)act_targ = targPses.size();
				act_targ--;
			}
		}else{
			vec3 relPos = targ - act_pos;
			relPos.normalize();
			act_pos += relPos*mv;
			parent->setPos(parent->getPos()+relPos*mv);
		}
	}
}

void TrackMover::call(int state){
	if(statical){
		textin_callback(state, "");
	}else{
		if(state < (int) speeds.size()){
			act_speed = state;
			active = 1;
		}
	}
}

void TrackMover::setEditMode(bool editMode){
	if(editMode){
		act_speed = 0;
		act_targ = 0;
		act_pos = vec3(0);
		active = 1;
	}
}


TrackRotor::TrackRotor(quat targRot): act_rot(quat()), act_targ(0), act_speed(0), loops(1){
	speeds.push_back(0);
	targRots.push_back(targRot);
}

TrackRotor::TrackRotor(const TrackRotor& o): ObjectComponent(o), Trigger(o), act_rot(quat()), act_targ(o.act_targ), act_speed(o.act_speed), loops(o.loops){
	for(quat targ: o.targRots){
		targRots.push_back(targ);
	}
	for(float sp: o.speeds){
		speeds.push_back(sp);
	}
}

TrackRotor::TrackRotor(xml_node xml):act_rot(quat()), act_targ(0), act_speed(0){
	xmlRFloat(xml, "speedX", speeds);
	if(!speeds.size())speeds.push_back(0);
	xmlRRot(xml, "goalX3", targRots);
	xmlRRot(xml, "goalX4", targRots);
	if(!targRots.size())targRots.push_back(quat());
}


void TrackRotor::saveXml(xml_node xml){
	xmlWFloat(xml, "speedX", speeds);
	xmlWRot(xml, "goalX4", targRots);
}

void TrackRotor::textin_callback(size_t issueID, string input){
	size_t actCode = 0;
	if(issueID == actCode)delMe();
	actCode++;

	if(edit_bool(actCode, issueID, input, loops))return;
	if(edit_vec(actCode, issueID, input, speeds, function<bool(string&, float&)>([=](string& input, float& out){
		float in;
		if(strToFloat(input.c_str(), in))return 1;
		out = in;
		return 0;
	})))return;
	if(edit_vec(actCode, issueID, input, targRots, function<bool(string&, quat&)>([=](string& input, quat& out){
		quat in;
		if(quat_euler){
			vec3 inV;
			if(strToVec(input.c_str(), inV))return 1;
			in = quat(inV * M_PI/180);
		}else if(strToVec(input.c_str(), in))return 1;
		if(rel_abs)out = parent->getRot().diff(in);
		else out = in;
		return 0;
	})))return;

	edit->call(0x7FFF0000);
}

void TrackRotor::update(){
	if((active || targRots.size()==1) && !statical){
		float mv = fabs(speeds[act_speed] * TICK_TIME);
		if(mv == 0){
			active = 0;
			return;
		}

		if(targRots.size()>1){
			size_t ind;
			if(speeds[act_speed] >= 0){
				ind = act_targ;
			}else if(act_targ == 0){
				if(!loops){
					active = 0;
					return;
				}else{
					ind = targRots.size()-1;
				}
			}else{
				ind = act_targ-1;
			}
			quat targ = targRots[ind];

			quat diff = act_rot.diff(targ);
			vec3 axis;
			float angle;
			diff.toAxis(axis, angle);
			if(angle < mv){
				act_rot = targ;
				parent->setRot(parent->getRot() * diff);
				if(speeds[act_speed] >= 0){
					if(act_targ == targRots.size() - 1){
						if(!loops){
							active = 0;
							return;
						}
						act_targ = 1;
					}else act_targ++;
				}else{
					if(act_targ == 0)act_targ = targRots.size();
					act_targ--;
				}
			}else{
				act_rot = act_rot * quat(axis, mv);
				parent->setRot(parent->getRot() * quat(axis, mv));
			}
		}else if(targRots.size() == 1){
			vec3 axis;
			float angle;
			targRots[0].toAxis(axis, angle);
			parent->setRot(parent->getRot() * quat(axis, mv));
		}
	}
}

void TrackRotor::call(int state){
	if(statical){
		textin_callback(state, "");
	}else{
		if(state < (int) speeds.size()){
			act_speed = state;
			active = 1;
		}
	}
}

void TrackRotor::setEditMode(bool editMode){
	if(editMode){
		act_speed = 0;
		act_targ = 0;
		act_rot = vec3(0);
		active = 1;
	}
}
