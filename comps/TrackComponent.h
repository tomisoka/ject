#pragma once

#include "../simplefc/trigger.h"
#include "../ObjectComponent.h"
#include "../GUI/Textin.h"

class TrackMover: public ObjectComponent, public Trigger, public TextinCallBack{
public:
	TrackMover(vec3 targPos = vec3(0));
	TrackMover(const TrackMover& o);
	TrackMover(xml_node xml);

	ObjectComponent *Copy() override{return new TrackMover(*this);}

	void saveXml(xml_node xml);

	void update() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 21;}
	string getIDName() override{return "TrackMover";}

	bool getLoops(){return loops;}
	vector<vec3>& getGoals(){return targPses;}
	vector<float>& getSpeeds(){return speeds;}
private:
	void textin_callback(size_t issueID, string input) override;

	vec3 act_pos;

	vector<vec3> targPses;
	uint32_t act_targ;
	vector<float> speeds;
	uint32_t act_speed;

	bool loops;
};


class TrackRotor: public ObjectComponent, public Trigger, public TextinCallBack{
public:
	TrackRotor(quat targRot = quat());
	TrackRotor(const TrackRotor& o);
	TrackRotor(xml_node xml);

	ObjectComponent *Copy() override{return new TrackRotor(*this);}

	void saveXml(xml_node xml);

	void update() override;
	void call(int state) override;

	void setEditMode(bool editMode) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}

	uint32_t getID() override{return 22;}
	string getIDName() override{return "TrackRotor";}

	bool getLoops(){return loops;}
	vector<quat>& getGoals(){return targRots;}
	vector<float>& getSpeeds(){return speeds;}
private:
	void textin_callback(size_t issueID, string input) override;

	quat act_rot;

	vector<quat> targRots;
	uint32_t act_targ;
	vector<float> speeds; //rad
	uint32_t act_speed;

	bool loops;
};
