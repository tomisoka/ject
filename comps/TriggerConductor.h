#pragma once
#include "../simplefc/trigger.h"

class TriggerConductor: public ObjectComponent, public Trigger, public TriggerCaller{
public:
	TriggerConductor(const TriggerConductor& o):ObjectComponent(o), Trigger(o), TriggerCaller(o){}
	TriggerConductor(xml_node xml){init(xml);}

	ObjectComponent *Copy() override{return new TriggerConductor(*this);}

	void call(int state) override{
		callit(state);
	}

	uint32_t getID() override{return 18;}
	string getIDName() override{return "TriggerConductor";}

	void updateTriggerNames() override{updateTriggerName(name);}
	map<string, string> propagateStringChange() override{return stringChangeF();}
	void requestTriggers() override{requestTriggerAClear();}
	void registTriggers() override{registTriggerSelf();}
};
