#include "controls.h"
#include "gameData.h"
#include "level.h"
#include "HoldObject.h"
#include "simplefc/window.h"
#include "simplefc/mouse.h"
#include "MenuController.h"
#include "edit/edit.h"
#include "comps/Render3DLine.h"
#include "comps/Render3D.h"
#include "GUI/RenderLines2D.h"
#include "simplefc/sounds.h"
#include "BtInterface/convertVM.h"
#include "BtInterface/BodyInterface.h"
#include "BtInterface/UsefullFunctions.h"
#include "BtInterface/WorldInterface.h"
#include "Controls.h"

controls *control;

Object *CameraObj;
btPairCachingGhostObject *playerGhost;

bool inCam=1;
bool lastNotMoved=0;
float last2Mv=0;

vec3 position = vec3(0);
float horizontalAngle = 0;
float verticalAngle = 0;

float speed = 100.0f;
float mouseSpeed = 0.005f;
float lastNormalY = 0;

vec3 get_direction(){
	return vec3(
		cos(verticalAngle) * sin(horizontalAngle+M_PI/2),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle+M_PI/2)
	);
}

vec3 getRightVec(){
	return vec3(sin(horizontalAngle), 0, cos(horizontalAngle));
}

void setPlayer(){
	if(CameraObj->getActive()){
		if(!CameraObj->getBtBody()){
			world_interface->add_ghost(playerGhost, 0);

			BodyInterface *playerB = new BodyInterface(new btCapsuleShape(0.45f,0.9f), CameraObj, btQuaternion(0,0,0,1), btVector3(0,0,0), 90,1, 0, 0);
			playerB->setSleepingThresholds(0.0, 0.0);
			playerB->set_angular_factor(btVector3(0,0,0));
			CameraObj->setBtBody(playerB);
			playerB->set_static(0);

			if(find(SoundIgnoreObjs.begin(), SoundIgnoreObjs.end(), playerB) == SoundIgnoreObjs.end())SoundIgnoreObjs.push_back(playerB);
		}
	}else{
		if(CameraObj->getBtBody()){
			world_interface->remove_ghost(playerGhost);
			BodyInterface *body = CameraObj->getBtBody();

			let it = find(SoundIgnoreObjs.begin(), SoundIgnoreObjs.end(), body);
			if(it != SoundIgnoreObjs.end())SoundIgnoreObjs.erase(it);

			delete body->getCollisionShape();

			CameraObj->setBtBody(0);
		}
	}
}

controls::controls(): View(45.), pickup(0), hold(0){
	CameraObj = new Object();
	levelPar->addChild(CameraObj);
	CameraObj->addComponent(new Render3D("obj/char.obj", 0, vec3(1)));
	CameraObj->addComponent(new Render3DLines("obj/charLine.obj", vec3(0)));
	CameraObj->setBtBody(0);

	playerGhost = new btPairCachingGhostObject();
	btConvexShape* capsule = new btCapsuleShape(0.45f,0.9f);
	playerGhost->setCollisionShape(capsule);
	playerGhost->setCollisionFlags(btCollisionObject::CF_NO_CONTACT_RESPONSE);

	const float radi = 0.005f;
	const float verts[] = {
		radi , 0, 0, radi,
		-radi, 0, 0, radi,
		radi , 0, 0,-radi,
		-radi, 0, 0,-radi,
	};
	levelPar->addChild((new Object())->addComponent(new RenderLines2D(vector<float>(verts, verts+16))));
}

controls::~controls(){
	setPlayer();
	delete playerGhost->getCollisionShape();
	delete playerGhost;
}

bool controls::input_key(bool acted, int key, int action, int mods){
	if(CameraObj->getActive() && !acted){
		if(action){
			if(key=='1')inCam=!inCam;
			else if(key=='2')speed*=2;
			else if(key=='3')speed/=2;
			else if(key == CTRL::PICK_UP){
				if(hold){
					let it = find(SoundIgnoreObjs.begin(), SoundIgnoreObjs.end(), hold->getParent()->getBtBody());
					if(it != SoundIgnoreObjs.end())SoundIgnoreObjs.erase(it);
					hold->delMe();
					hold = 0;
				}else{
					btVector3 btHit, btHitNormal;
					const btCollisionObject *colObj;
					vec3 cam_pos = get_current_view()->get_position();
					colObj = closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos+get_current_view()->get_direction()*1000000.f), 0x2, btHit, btHitNormal);

					if(colObj){
						auto it = mvableObjs.find(colObj);

						if(it!=mvableObjs.end()){
							Object *pickUpObj = it->second;
							vec3 picPos = pickUpObj->getPos();
							vec3 camPos = CameraObj->getPos();
							if(abs(picPos.getY() - camPos.getY()) < 0.9f+0.7f && (vec2(picPos.getX(), picPos.getZ()) - vec2(camPos.getX(), camPos.getZ())).lengthSq() < 5 && pickUpObj->getBtBody() && pickUpObj->getBtBody()->getInvMass()>0.01 && pickUpObj->getBtBody()->getInvMass()<10){
								pickup=pickUpObj;
							}
						}else{
							BodyInterface* body = (BodyInterface*) colObj;
							TriggerData* dat = body->get_pushable();
							if(dat && dat->tCall){
								if(dat->retState.size())dat->tCall->call(dat->retState[0]);
								else dat->tCall->call(0);
							}
						}
					}
					return true;
				}
			}else if(key == CTRL::FOCUS_UN)hideCursor(!isHiddenCursor());
		}
	}
	return false;
}

void controls::doit(){
	if(pickup){
		pickup->addComponent(hold = new HoldObject(CameraObj, quat(vec3(0,-M_PI/2,0)), vec3(1.1, 0, 0)));
		SoundIgnoreObjs.push_back(pickup->getBtBody());
		hold->postLoaded();
		pickup=0;
	}
	if(hold){
		float angle = verticalAngle;
		if(angle>0.5)angle=0.5f;
		if(angle<-0.5)angle=-0.5f;
		vec3 nwPos = vec3(cos(angle)*1.1, sin(angle)*1.1+0.5, 0);
		hold->setPos(nwPos);


		if (isPressed(CTRL::ROTATE_YAW_M))hold->rotate(quat(vec3(0,-0.1,0)));
		if (isPressed(CTRL::ROTATE_YAW_P))hold->rotate(quat(vec3(0, 0.1,0)));
		if (isPressed(CTRL::ROTATE_ROLL_M))hold->rotate(quat(vec3(0,0,-0.1)));
		if (isPressed(CTRL::ROTATE_ROLL_P))hold->rotate(quat(vec3(0,0, 0.1)));
		if (isPressed(CTRL::ROTATE_PITCH_M))hold->rotate(quat(vec3(-0.1,0,0)));
		if (isPressed(CTRL::ROTATE_PITCH_P))hold->rotate(quat(vec3( 0.1,0,0)));
	}
}

void controls::resetKill(){
	if(hold){
		hold->delMe();
		hold = 0;
	}
	setPlayer();
}

void controls::reset(){
	hold = 0;
	setPlayer();
}


int stepC = 0;

void computeMatricesFromInputs(){
	btRigidBody *camB = CameraObj->getBtBody();

	if(inCam)position = CameraObj->getPos();

	double middleX = gwinWidth()/2., middleY = gwinHeight()/2.;

	float deltaTime = TICK_TIME;

	vec2i m_pos = cursorPosIE();

	if(isHiddenCursor()){
		set_cursor_to_middle();
		float diffX = float(middleX - m_pos.getX());
		float diffY = float(middleY - m_pos.getY());
		if(fabs(diffX) < 1)diffX = 0;
		if(fabs(diffY) < 1)diffY = 0;
		horizontalAngle += mouseSpeed * diffX;
		float mem = verticalAngle + mouseSpeed * diffY;
		if(mem<M_PI/2 && mem>-M_PI/2)verticalAngle=mem;
	}

	vec3 direction = get_direction();
	vec3 right = getRightVec();


	float normalY=0;
	bool flying = 1;

	GhostOverlap(playerGhost, [&](const btManifoldPoint& pt, const btCollisionObject*, bool first){
		const btVector3& normal = pt.m_normalWorldOnB  * (first?1:-1);
		if(normal.getY()>normalY)normalY=normal.getY();
		return 0;
	}, {camB});
	if(normalY>0.6f)flying = 0;

	normalY = floor(normalY*1000.f)/1000.f;


	vec3 dir=direction;
	vec3 rightN=right;
	if(inCam){
		dir.setY(0);
		dir = dir.normalized();
		rightN.setY(0);
		rightN = rightN.normalized();
	}

	bool notMoved = 1;

	btVector3 newVel = btVector3(0,0,0);

	if(isHiddenCursor()){
		if (isPressed(CTRL::JUMP) && inCam && (normalY>0.6f||lastNotMoved) && !flying && camB->getLinearVelocity().getY()<1){
			camB->setLinearVelocity(btVector3(0,4.f,0)+camB->getLinearVelocity());
			btTransform tr = camB->getWorldTransform();
			tr.setOrigin(tr.getOrigin()+btVector3(0,0.01f,0));
			camB->setWorldTransform(tr);
			notMoved=0;
		}
		bool canMove = (normalY==0 && (lastNormalY==0||lastNormalY>0.6f)) || normalY>0.6f || (lastNormalY!=normalY && lastNormalY!=0 && normalY!=0);
		if (isPressed(CTRL::MOVE_FORWARD)){
			if(!inCam)position += direction * deltaTime * speed/10.f;
			else if(canMove)newVel += btVec3(dir);
		}
		if (isPressed(CTRL::MOVE_BACK)){
			if(!inCam)position -= direction * deltaTime * speed/10.f;
			else if(canMove)newVel -= btVec3(dir);
		}
		if (isPressed(CTRL::MOVE_RIGHT)){
			if(!inCam)position += right * deltaTime * speed/10.f;
			else if(canMove)newVel += btVec3(rightN);
		}
		if (isPressed(CTRL::MOVE_LEFT)){
			if(!inCam)position -= right * deltaTime * speed/10.f;
			else if(canMove)newVel -= btVec3(rightN);
		}
	}

	if(newVel.length()){
		newVel.normalize();
		notMoved=0;
	}
	btTransform newTrans;
	if(!statical){
		camB->setLinearVelocity(btVector3(camB->getLinearVelocity().getX()*(flying?0.98f:0.8f),camB->getLinearVelocity().getY(),camB->getLinearVelocity().getZ()*(flying?0.98f:0.8f)));
		camB->setLinearVelocity(newVel/float(TPS)*speed*(flying?0.1f:1)+camB->getLinearVelocity());
		if(notMoved && (normalY>0.6f || (lastNotMoved && normalY==0)) && camB->getLinearVelocity().length2()<0.5f && !flying && !(lastNotMoved && camB->getLinearVelocity().length2()>0.02f)){
			float nw2Mv = camB->getLinearVelocity().length2();
			if(nw2Mv*0.999f<=last2Mv || nw2Mv<0.000001f || !lastNotMoved){
				camB->setLinearVelocity(btVector3(0,0,0));
			}
			last2Mv = nw2Mv;

			lastNotMoved=1;
			camB->setGravity(btVector3(0,0,0));
		}else{
			lastNotMoved=0;
			camB->setGravity(GRAVACC);
		}

		camB->predictIntegratedTransform (1.f/TPS, newTrans);
		newTrans.setOrigin(newTrans.getOrigin() + btVector3(0,-0.01,0));

		playerGhost->setWorldTransform(newTrans);
	}

	lastNormalY = normalY;



	if(!flying && camB->getLinearVelocity().length2() > 1){
		if(stepC == 0){
			playStep();
			stepC = 30;
		}else stepC--;
	}else stepC = 0;

	resetView();
}
void resetView(){
	btRigidBody *camB = CameraObj->getBtBody();

	vec3 direction = get_direction();
	vec3 right = getRightVec();
	vec3 up = right.cross(direction);

	vec3 pos = position;

	if(inCam){
		btTransform trans = camB->getWorldTransform();

		pos = position = Vec3(trans.getOrigin());
		pos[0]+=sin(horizontalAngle+M_PI/2)*0.24f;
		pos[1]+=0.68f;
		pos[2]+=cos(horizontalAngle+M_PI/2)*0.24f;

		trans.setRotation(btQuat(quat(vec3(0, horizontalAngle,0))));
		camB->setWorldTransform(trans);
	}
	control->set_pdu(pos, direction, up);

	soundSetPosDirUp(pos, direction, up);
	soundSetVolume(soundsOn?100:0);
}
