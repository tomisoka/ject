#pragma once

#include "simplefc/input.h"
#include "simplefc/View.h"

class Object;
class HoldObject;

void computeMatricesFromInputs();
void resetView();

extern vec3 position;
extern float horizontalAngle;
extern float verticalAngle;

extern Object *CameraObj;

extern bool inCam;

class controls: public InputCallbacks, public View{
public:
	controls();
	~controls();

	void doit();

	void resetKill();
	void reset();
private:
	bool input_key(bool acted, int key, int action, int mods) override;

	Object *pickup;

	HoldObject *hold;
};

extern controls *control;
