#include "cubecompound.h"
#include "simplefc/render.h"
#include "RenderController.h"
#include "simplefc/basicFcs.h"
#include "BtInterface/convertVM.h"
#include "BtInterface/BodyInterface.h"
#include "simplefc/xml.h"

vector <const btCollisionObject *> cubeCompoundCollObjs;

bool Line::collideNew(const Line c, vector<Line> &out){

	if(min >= c.max || max <= c.min)return 0;

	bool Xmin = min < c.min;
	bool Xmax = max > c.max;

	if(min < c.min)out.push_back(Line(min, c.min, glass));
	else if(c.min < min)out.push_back(Line(c.min, min, c.glass));

	if(max > c.max)out.push_back(Line(c.max, max, glass));
	else if(c.max > max)out.push_back(Line(max, c.max, c.glass));

	if(glass != c.glass)out.push_back(Line(Xmin?c.min:min, Xmax?c.max:max, 1));

	return 1;
}

bool Square::collide(const Square c, vector<Square> &out){
	if(min.getX() >= c.max.getX() || min.getY() >= c.max.getY() || max.getX() <= c.min.getX() || max.getY() <= c.min.getY()){
		out.push_back(*this);
		return 0;
	}

	bool Xmin = min.getX() < c.min.getX();
	bool Xmax = max.getX() > c.max.getX();

	if(Xmin)out.push_back(Square(min, vec2i(c.min.getX(), max.getY())));
	if(Xmax)out.push_back(Square(vec2i(c.max.getX(), min.getY()), max));

	if(min.getY() < c.min.getY())out.push_back(Square(vec2i(Xmin?c.min.getX():min.getX(), min.getY()), vec2i(Xmax?c.max.getX():max.getX(), c.min.getY())));

	if(max.getY() > c.max.getY())out.push_back(Square(vec2i(Xmin?c.min.getX():min.getX(), c.max.getY()), vec2i(Xmax?c.max.getX():max.getX(), max.getY())));

	return 1;
}

Cube::~Cube(){
	deactivate();
}

void Cube::activate(CubeCompound *parent){
	if(!body){
		this->parent = parent;
		vec3 centroid = vec3(min+max)/2.f*scale;
		vec3 size = vec3(max-min)/2.f*scale;
		body = new BodyInterface(new btBoxShape(btVec3(size)), parent, btQuat(quat()), btVec3(centroid), 0, 0, 1, 0);
		if(parent != WorldCubeCompound){
			body->setCollisionFlags( body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT );
			body->setActivationState(DISABLE_DEACTIVATION);
		}
		body->enregister();
		cubeCompoundCollObjs.push_back(body);
		bodyInW = 1;
	}
}

void Cube::deactivate(){
	if(body){
		if(bodyInW)erase(cubeCompoundCollObjs, (const btCollisionObject*)body);
		delete body->getCollisionShape();
		delete body;
		body = 0;
	}
}

void Cube::updateBody(){
	if(body){
		vec3 centroid = vec3(min+max)/2.f*scale;

		mat4 result = parent->getTransform() * centroid.toTransM();
		btTransform tran = btTrans(result);

		btMotionState *motSt = body->getMotionState();
		motSt->setWorldTransform(tran);
		body->setMotionState(motSt);
	}
}
void Cube::actBtBody(bool act){
	if(body){
		if(act != bodyInW){
			if(act){
				body->enregister();
				cubeCompoundCollObjs.push_back(body);
			}else{
				erase(cubeCompoundCollObjs, (const btCollisionObject*)body);
				body->deregister();
			}

			bodyInW = act;
		}
	}
}

char *Cube::toString(char *out){
	out = floatStr(out, min.getX(), 3);
	*out++ = ';';
	out = floatStr(out, min.getY(), 3);
	*out++ = ';';
	out = floatStr(out, min.getZ(), 3);
	*out++ = ';';
	out = floatStr(out, max.getX(), 3);
	*out++ = ';';
	out = floatStr(out, max.getY(), 3);
	*out++ = ';';
	out = floatStr(out, max.getZ(), 3);
	if(glass){
		*out++ = ';';
		*out++ = '0' + glass;
	}
	return out;
}

bool Cube::collide(Cube& c, vector<Cube>& out){
	if(min.getX() >= c.max.getX() || min.getY() >= c.max.getY() || min.getZ() >= c.max.getZ() ||
				max.getX() <= c.min.getX() || max.getY() <= c.min.getY() || max.getZ() <= c.min.getZ()){
		return 0;
	}

	bool Xmin = min.getX() < c.min.getX();
	bool Xmax = max.getX() > c.max.getX();

	bool Ymin = min.getY() < c.min.getY();
	bool Ymax = max.getY() > c.max.getY();

	bool Zmin = min.getZ() < c.min.getZ();
	bool Zmax = max.getZ() > c.max.getZ();

	if(Xmin){
		out.push_back(Cube(vec3i(min.getX(), min.getY(), min.getZ()), vec3i(c.min.getX(), max.getY(), max.getZ()), glass));
	}
	if(Xmax){
		out.push_back(Cube(vec3i(c.max.getX(), min.getY(), min.getZ()), vec3i(max.getX(), max.getY(), max.getZ()), glass));
	}

	if(Ymin){
		out.push_back(Cube(vec3i(Xmin?c.min.getX():min.getX(), min.getY(), min.getZ()), vec3i(Xmax?c.max.getX():max.getX(), c.min.getY(), max.getZ()), glass));
	}
	if(Ymax){
		out.push_back(Cube(vec3i(Xmin?c.min.getX():min.getX(), c.max.getY(), min.getZ()), vec3i(Xmax?c.max.getX():max.getX(), max.getY(), max.getZ()), glass));
	}

	if(Zmin){
		out.push_back(Cube(vec3i(Xmin?c.min.getX():min.getX(), Ymin?c.min.getY():min.getY(), min.getZ()), vec3i(Xmax?c.max.getX():max.getX(), Ymax?c.max.getY():max.getY(), c.min.getZ()), glass));
	}
	if(Zmax){
		out.push_back(Cube(vec3i(Xmin?c.min.getX():min.getX(), Ymin?c.min.getY():min.getY(), c.max.getZ()), vec3i(Xmax?c.max.getX():max.getX(), Ymax?c.max.getY():max.getY(), max.getZ()), glass));
	}

	return 1;
}

bool Cube::collideInside(Cube& c, vector<Cube>& out){
	if(min.getX() >= c.max.getX() || min.getY() >= c.max.getY() || min.getZ() >= c.max.getZ() ||
			max.getX() <= c.min.getX() || max.getY() <= c.min.getY() || max.getZ() <= c.min.getZ()){
		return 0;
	}

	bool Xmin = min.getX() < c.min.getX();
	bool Xmax = max.getX() > c.max.getX();

	bool Ymin = min.getY() < c.min.getY();
	bool Ymax = max.getY() > c.max.getY();

	bool Zmin = min.getZ() < c.min.getZ();
	bool Zmax = max.getZ() > c.max.getZ();

	out.push_back(Cube(
		vec3i(Xmin?c.min.getX():min.getX(), Ymin?c.min.getY():min.getY(), Zmin?c.min.getZ():min.getZ()),
		vec3i(Xmax?c.max.getX():max.getX(), Ymax?c.max.getY():max.getY(), Zmax?c.max.getZ():max.getZ()),
		glass));
	return 1;
}


Cube *Cube::merge(Cube& c){
	if(min.getX() == c.min.getX() && max.getX() == c.max.getX() &&
			min.getY() == c.min.getY() && max.getY() == c.max.getY()){
		if(min.getZ() == c.max.getZ()){
			return new Cube(vec3i(min.getX(), min.getY(), c.min.getZ()), vec3i(max.getX(), max.getY(), max.getZ()),
				glass);
		}
		if(c.min.getZ() == max.getZ()){
			return new Cube(vec3i(min.getX(), min.getY(), min.getZ()), vec3i(max.getX(), max.getY(), c.max.getZ()),
				glass);
		}
	}
	if(min.getX() == c.min.getX() && max.getX() == c.max.getX() &&
			min.getZ() == c.min.getZ() && max.getZ() == c.max.getZ()){
		if(min.getY() == c.max.getY()){
			return new Cube(vec3i(min.getX(), c.min.getY(), min.getZ()), vec3i(max.getX(), max.getY(), max.getZ()),
				glass);
		}
		if(c.min.getY() == max.getY()){
			return new Cube(vec3i(min.getX(), min.getY(), min.getZ()), vec3i(max.getX(), c.max.getY(), max.getZ()),
				glass);
		}
	}
	if(min.getY() == c.min.getY() && max.getY() == c.max.getY() &&
			min.getZ() == c.min.getZ() && max.getZ() == c.max.getZ()){
		if(min.getX() == c.max.getX()){
			return new Cube(vec3i(c.min.getX(), min.getY(), min.getZ()), vec3i(max.getX(), max.getY(), max.getZ()),
				glass);
		}
		if(c.min.getX() == max.getX()){
			return new Cube(vec3i(min.getX(), min.getY(), min.getZ()), vec3i(c.max.getX(), max.getY(), max.getZ()),
				glass);
		}
	}

	return 0;
}

void getRenderLine(vector<Line> temp, vector<Line> &outA){
	while(temp.size()){
		bool valid = 1;
		for(size_t i=1;i<temp.size();++i){
			//why constructor? passed wrong value; IDK&IDC
			if(Line(temp[0]).collideNew(temp[i], temp)){
				temp.erase(temp.begin()+i);
				temp.erase(temp.begin());
				valid = 0;
				break;
			}
		}
		if(valid){
			outA.push_back(temp[0]);
			temp.erase(temp.begin());
		}
	}
}

void getRenderSquare(const vector<Square>& a,const vector<Square>& b, vector<Square> &outA, vector<Square> &outB){
	vector<Square> temp1;
	vector<Square> temp2;

	for(const auto it: a)temp1.push_back(it);

	for(size_t j = 0; j < b.size();++j){
		for(size_t i = 0; i < temp1.size();++i){
			temp1[i].collide(b[j], temp2);
		}
		temp1.clear();

		swap(temp1, temp2);
	}

	for(const auto it: temp1)outA.push_back(it);
	temp1.clear();


	for(const auto it: b)temp1.push_back(it);

	for(size_t j = 0; j < a.size();++j){
		for(size_t i = 0; i < temp1.size();++i){
			temp1[i].collide(a[j], temp2);
		}
		temp1.clear();

		swap(temp1, temp2);
	}

	for(const auto it: temp1)outB.push_back(it);
}

CubeCompound::CubeCompound(xml_node xml):CubeCompound(){
	xml_node scaleN = xml.child("scale");
	if(scaleN)scale = scaleN.text().as_float();

	int32_t sh[6];
	for(xml_node cubeN: xml.children("cube")){
		const char *in = cubeN.text().as_string();
		bool glass = 0;

		in = strToInt(in, sh[0], ';')+1;
		in = strToInt(in, sh[1], ';')+1;
		in = strToInt(in, sh[2], ';')+1;
		in = strToInt(in, sh[3], ';')+1;
		in = strToInt(in, sh[4], ';')+1;
		in = strToInt(in, sh[5], ';');
		if(*in >= 0x20)glass = *in-'0';

		//printf("cuba: %d %d %d %d %d %d\n", sh[0], sh[1], sh[2], sh[3], sh[4], sh[5]);
		cubes.push_back(new Cube(vec3i(sh[0], sh[1], sh[2]), vec3i(sh[3], sh[4], sh[5]), glass, scale));
		cubesRestore.push_back(Cube(vec3i(sh[0], sh[1], sh[2]), vec3i(sh[3], sh[4], sh[5]), glass, scale));
	}
	cubeSZ = cubes.size();
	reRender = 1;
	Rcontrol->addRendC(this);
}

CubeCompound::CubeCompound(const CubeCompound& o): ObjectComponent(o), oldPos(vec3(0)), oldRot(quat()), mained(0), scale(o.scale), vertexbufferL(0), vertexbuffer(0), vertexbufferGlass(0), cubeSZ(o.cubeSZ), isMain(0), reRender(1), rendered(0){
	for(const Cube& cube: o.cubesRestore){
		cubes.push_back(new Cube(cube));
		cubesRestore.push_back(Cube(cube));
	}
	Rcontrol->addRendC(this);
}

CubeCompound::~CubeCompound(){
	for(size_t i=0;i<cubeSZ;++i){
		if(!isMain && WorldCubeCompound)WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
		if(cubes[i]->renderCopy)delete cubes[i]->renderCopy;
		delete cubes[i];
	}
	if(WorldCubeCompound == this){
		WorldCubeCompound=0;
	}

	cleanRender();
	Rcontrol->remRendC(this);
}

void CubeCompound::merge(vector<Cube *>* mergedCubes){
	reRender = 1;
	if(!isMain && WorldCubeCompound){
		size_t i;
		for(i=0;i<cubeSZ;++i){
			WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
			if(cubes[i]->renderCopy){
				if(mergedCubes)mergedCubes->push_back(new Cube(*cubes[i]->renderCopy));
				WorldCubeCompound->addNewCube(*cubes[i]->renderCopy);
			}else{
				mat4 trans = parent->getTransform();
				vec3i min = cubes[i]->min;
				vec3i max = cubes[i]->max;
				min = ((trans*(vec3(min)*scale))/scale).roundI();
				max = ((trans*(vec3(max)*scale))/scale).roundI();
				Cube *cube = new Cube(min, max, cubes[i]->glass, scale);

				if(mergedCubes)mergedCubes->push_back(new Cube(*cube));
				WorldCubeCompound->addNewCube(*cube);
			}

			delete cubes[i];
		}
		cubeSZ = 0;
		for(;i<cubes.size();++i){
			WorldCubeCompound->addCube(cubes[i]);
		}
		cubes.clear();
		delMe();
	}
}

void CubeCompound::getCopyInSpace(Cube& space, vector<Cube>& cubesInside){
	space.checkMinMax();

	for(size_t i=0;i<cubeSZ;++i){
		cubes[i]->collideInside(space, cubesInside);
	}
}

void CubeCompound::addNewCube(Cube& cube){
	cube.checkMinMax();
	reRender = 1;

	vector<Cube> newCubes;

	newCubes.push_back(cube);
	for(size_t i=0;i<cubes.size();++i){
		size_t size = newCubes.size();
		for(size_t j=0;j<size;++j){
			if(newCubes[j].collide(*cubes[i], newCubes)){
				newCubes.erase(newCubes.begin()+j);
				size--;
				j--;
			}
		}
	}
	for(auto& c: newCubes)tryMerge(c);
}

void CubeCompound::remCube(Cube& cube){
	cube.checkMinMax();
	reRender = 1;

	vector<Cube> newCubes;

	for(size_t i=0;i<cubeSZ;++i){
		if(cubes[i]->collide(cube, newCubes)){
			delete cubes[i];
			cubes.erase(cubes.begin()+i);
			cubeSZ--;
			i--;
		}
	}
	for(auto& c: newCubes)tryMerge(c);
}

void CubeCompound::restoreCubes(){
	for(size_t i = 0;i < cubeSZ;++i)delete cubes[i];
	cubes.erase(cubes.begin(), cubes.begin() + cubeSZ);

	for(cubeSZ = 0; cubeSZ < cubesRestore.size();++cubeSZ){
		Cube *cube = new Cube(cubesRestore[cubeSZ]);
		cubes.insert(cubes.begin()+cubeSZ, cube);
		cube->activate(this);
		cube->updateBody();
	}

	reRender = 1;
}

vector<Cube> CubeCompound::copyCubes(){
	vector<Cube> ret;
	for(size_t i=0;i < cubeSZ;++i){
		ret.push_back(Cube(*cubes[i]));
	}
	return ret;
}

void CubeCompound::tryMerge(Cube& c){
	for(size_t i=0;i<cubeSZ;++i){
		if(c.glass != cubes[i]->glass)continue;
		Cube *mrg = cubes[i]->merge(c);
		if(mrg){
			delete cubes[i];
			cubes.erase(cubes.begin()+i);
			cubeSZ--;
			c = *mrg;
			i=-1;
		}
	}

	c.checkMinMax();
	vec3i min = c.min;
	vec3i max = c.max;
	c.scale = scale;

	Cube *cp = new Cube(c);
	cubes.insert(cubes.begin()+cubeSZ, cp);
	cubeSZ++;

	if(!isMain){
		mat4 trans = parent->getTransform();

		min = ((trans*(vec3(min)*scale))/scale).roundI();
		max = ((trans*(vec3(max)*scale))/scale).roundI();

		Cube *cube = new Cube(min, max, cp->glass, scale);
		cp->renderCopy = cube;
		WorldCubeCompound->addCube(cube);
	}
	cp->activate(this);
	cp->updateBody();
}

void CubeCompound::postLoaded(){
	if(!WorldCubeCompound){
		WorldCubeCompound = this;
		isMain = 1;
		active = 1;
	}else{
		isMain = 0;
		active = 0;
	}

	oldPos = parent->getPos();
	oldRot = parent->getRot();

	for(size_t i=0;i<cubeSZ;++i){
		cubes[i]->activate(this);
		cubes[i]->updateBody();
	}

	if(!isMain){
		if(canBeMained()){
			mained = 1;
			mat4 trans = parent->getTransform();

			for(size_t i=0;i<cubeSZ;++i){
				vec3i min = cubes[i]->min;
				vec3i max = cubes[i]->max;

				min = ((trans*(vec3(min)*scale))/scale).roundI();
				max = ((trans*(vec3(max)*scale))/scale).roundI();

				if(cubes[i]->renderCopy){
					WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
					delete cubes[i]->renderCopy;
				}

				Cube *cube = new Cube(min, max, cubes[i]->glass, scale);
				cubes[i]->renderCopy = cube;
				WorldCubeCompound->addCube(cube);
			}
		}else{
			active = 1;
		}
	}
}


char Temp[100];
void CubeCompound::saveXml(xml_node xml){
	if(!compDouble(scale, 0.2f))xmlWFloat(xml, "scale", scale);

	for(size_t i=0;i<cubeSZ;++i){
		xml_node cubeN = xml.append_child("cube");
		*(cubes[i]->toString(Temp))=0;
		cubeN.text().set(Temp);
	}
}

void CubeCompound::actBtBody(bool act){
	for(size_t i=0;i<cubeSZ;++i)cubes[i]->actBtBody(act);
}

void CubeCompound::update(){
	mat4 trans = getTransform();
	vec3 posNow = trans.getPos();
	quat rotNow = quat(trans);

	if(posNow != oldPos || rotNow != oldRot){

		if(!isMain){
			if(canBeMained()){
				if(mained){
					for(size_t i=0;i<cubeSZ;++i){
						if(cubes[i]->renderCopy){
							vec3i min = cubes[i]->min;
							vec3i max = cubes[i]->max;

							min = ((trans*(vec3(min)*scale))/scale).roundI();
							max = ((trans*(vec3(max)*scale))/scale).roundI();

							cubes[i]->renderCopy->min = min;
							cubes[i]->renderCopy->max = max;
							cubes[i]->renderCopy->checkMinMax();
							cubes[i]->updateBody();
						}
					}
					WorldCubeCompound->setReRender();
				}else{
					mained = 1;
					active = 0;
					for(size_t i=0;i<cubeSZ;++i){
						vec3i min = cubes[i]->min;
						vec3i max = cubes[i]->max;

						min = ((trans*(vec3(min)*scale))/scale).roundI();
						max = ((trans*(vec3(max)*scale))/scale).roundI();

						if(cubes[i]->renderCopy){
							WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
							delete cubes[i]->renderCopy;
						}
						Cube *cube = new Cube(min, max, cubes[i]->glass, scale);
						cubes[i]->renderCopy = cube;
						WorldCubeCompound->addCube(cube);
						cubes[i]->updateBody();
					}
				}

			}else{
				if(mained){
					mained = 0;
					active = 1;
					for(size_t i=0;i<cubeSZ;++i){
						WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
						if(cubes[i]->renderCopy){
							delete cubes[i]->renderCopy;
							cubes[i]->renderCopy = 0;
						}
					}
				}
				for(size_t i=0;i<cubeSZ;++i)cubes[i]->updateBody();
			}
		}
	}
	oldPos = posNow;
	oldRot = rotNow;
}


void CubeCompound::setEditColor(vec3 color){
	editColor = 1;
	editColorR = color;


	if(mained){
		mained = 0;
		active = 1;
		for(size_t i=0;i<cubeSZ;++i){
			if(cubes[i]->renderCopy){
				WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
				delete cubes[i]->renderCopy;
				cubes[i]->renderCopy = 0;
			}
		}
	}
}
void CubeCompound::unsetEditColor(){
	if(!editColor)return;
	editColor = 0;
	if(!mained && canBeMained()){
		mained = 1;
		active = 0;
		mat4 trans = getTransform();

		for(size_t i=0;i<cubeSZ;++i){
			vec3i min = cubes[i]->min;
			vec3i max = cubes[i]->max;

			min = ((trans*(vec3(min)*scale))/scale).roundI();
			max = ((trans*(vec3(max)*scale))/scale).roundI();

			if(cubes[i]->renderCopy){
				WorldCubeCompound->remAddedCube(cubes[i]->renderCopy);
				delete cubes[i]->renderCopy;
			}

			Cube *cube = new Cube(min, max, cubes[i]->glass, scale);
			cubes[i]->renderCopy = cube;
			WorldCubeCompound->addCube(cube);
		}
	}
}

bool CubeCompound::canBeMained(){
	mat4 trans = getTransform();
	vec3 posNow = trans.getPos();
	quat rotNow = quat(trans);
	vec3i testPos = (posNow/scale * 100).roundI();
	vec3 rotE = rotNow.getEuler()/M_PI*180;
	vec3i rotI = (rotE*20).roundI();

	if(compDouble(scale, WorldCubeCompound->getScale()) &&
		!(rotI.getX() % 1800) &&
		!(rotI.getY() % 1800) &&
		!(rotI.getZ() % 1800) &&
		!(testPos.getX() % 100) &&
		!(testPos.getY() % 100) &&
		!(testPos.getZ() % 100)){
		return 1;
	}
	return 0;
}

void CubeCompound::renderReg(){
	if(!getActive())return;
	if(reRender)resetRender();

	mat4 transform = parent->getTransform()*vec3(scale).toScaleM();

	if(vertexbufferL){
		render::setLine();
		render::setTrans(transform);
		vec3 color = editColor?editColorR:vec3(0);
		render::setColor(color);
		render::setAttrPoint(0, 3, vertexbufferL);
		render::draw(verticesL.size());
	}

	if(vertexbuffer){
		render::setColor();
		render::setTrans(transform);
		render::setColor(vec3(1));
		render::setAttrPoint(0, 3, vertexbuffer);
		render::draw(vertices.size());
	}

	if(vertexbufferGlass){
		render::setColorTranspar();
		render::setTrans(transform);
		render::setColor(vec4(1, 1, 1, 0.7));
		render::setAttrPoint(0, 3, vertexbufferGlass);
		render::draw(verticesGlass.size());
	}

	rendered = 1;

}

void CubeCompound::resetRender(){
	vector<int> vertX;
	vector<vector<Square>> vertiX1;
	vector<vector<Square>> vertiX2;

	vector<int> vertY;
	vector<vector<Square>> vertiY1;
	vector<vector<Square>> vertiY2;

	vector<int> vertZ;
	vector<vector<Square>> vertiZ1;
	vector<vector<Square>> vertiZ2;


	vector<int> vertXG;
	vector<vector<Square>> vertiX1G;
	vector<vector<Square>> vertiX2G;

	vector<int> vertYG;
	vector<vector<Square>> vertiY1G;
	vector<vector<Square>> vertiY2G;

	vector<int> vertZG;
	vector<vector<Square>> vertiZ1G;
	vector<vector<Square>> vertiZ2G;


	vector<int> vertXY;
	vector<vector<int>> vertXYdeep;
	vector<vector<vector<Line>>> vertiXY;

	vector<int> vertXZ;
	vector<vector<int>> vertXZdeep;
	vector<vector<vector<Line>>> vertiXZ;

	vector<int> vertYZ;
	vector<vector<int>> vertYZdeep;
	vector<vector<vector<Line>>> vertiYZ;

	if(rendered)cleanRender();


	for(Cube *cube: cubes){
		vec3i min = cube->min;
		vec3i max = cube->max;

		static auto vert_push = [](vector<int>& vert, int cmp, vec2i min, vec2i max,
			vector<vector<Square>>& verti, vector<vector<Square>>& verti2){
			let it = find(vert.begin(), vert.end(), cmp);
			size_t ps = it - vert.begin();
			if(it==vert.end()){
				vert.push_back(cmp);
				verti.push_back(vector<Square>());
				verti2.push_back(vector<Square>());
			}
			verti[ps].push_back(Square(min, max));
		};

		if(cube->glass){
			vert_push(vertXG, min.getX(), min.getYZ(), max.getYZ(), vertiX1G, vertiX2G);
			vert_push(vertXG, max.getX(), min.getYZ(), max.getYZ(), vertiX2G, vertiX1G);
			vert_push(vertYG, min.getY(), min.getXZ(), max.getXZ(), vertiY1G, vertiY2G);
			vert_push(vertYG, max.getY(), min.getXZ(), max.getXZ(), vertiY2G, vertiY1G);
			vert_push(vertZG, min.getZ(), min.getXY(), max.getXY(), vertiZ1G, vertiZ2G);
			vert_push(vertZG, max.getZ(), min.getXY(), max.getXY(), vertiZ2G, vertiZ1G);
		}else{
			vert_push(vertX, min.getX(), min.getYZ(), max.getYZ(), vertiX1, vertiX2);
			vert_push(vertX, max.getX(), min.getYZ(), max.getYZ(), vertiX2, vertiX1);
			vert_push(vertY, min.getY(), min.getXZ(), max.getXZ(), vertiY1, vertiY2);
			vert_push(vertY, max.getY(), min.getXZ(), max.getXZ(), vertiY2, vertiY1);
			vert_push(vertZ, min.getZ(), min.getXY(), max.getXY(), vertiZ1, vertiZ2);
			vert_push(vertZ, max.getZ(), min.getXY(), max.getXY(), vertiZ2, vertiZ1);
		}

		static auto vertAB_push = [](vector<int>& vertAB, int cmpA, vec2i minMaxB, vec2i minMaxC,
			vector<vector<int>>& vertABdeep, vector<vector<vector<Line>>>& vertiAB, bool glass = 0){

			let it = find(vertAB.begin(), vertAB.end(), cmpA);
			int ps = it-vertAB.begin();
			if(it==vertAB.end()){
				vertAB.push_back(cmpA);
				vertABdeep.push_back(vector<int>());
				vertiAB.push_back(vector<vector<Line>>());
			}

			let it2 = find(vertABdeep[ps].begin(), vertABdeep[ps].end(), minMaxB[0]);
			size_t ps2 = it2 - vertABdeep[ps].begin();
			if(it2==vertABdeep[ps].end()){
				vertABdeep[ps].push_back(minMaxB[0]);
				vertiAB[ps].push_back(vector<Line>());
			}
			vertiAB[ps][ps2].push_back(Line(minMaxC, glass));

			let it3 = find(vertABdeep[ps].begin(), vertABdeep[ps].end(), minMaxB[1]);
			let ps3 = it3 - vertABdeep[ps].begin();
			if(it3==vertABdeep[ps].end()){
				vertABdeep[ps].push_back(minMaxB[1]);
				vertiAB[ps].push_back(vector<Line>());
			}
			vertiAB[ps][ps3].push_back(Line(minMaxC, glass));
		};

		bool G = cube->glass;
		vertAB_push(vertXY,min.getX(),vec2i(min.getY(),max.getY()),vec2i(min.getZ(),max.getZ()),vertXYdeep,vertiXY,G);
		vertAB_push(vertXZ,min.getX(),vec2i(min.getZ(),max.getZ()),vec2i(min.getY(),max.getY()),vertXZdeep,vertiXZ,G);
		vertAB_push(vertYZ,min.getY(),vec2i(min.getZ(),max.getZ()),vec2i(min.getX(),max.getX()),vertYZdeep,vertiYZ,G);
		vertAB_push(vertXY,max.getX(),vec2i(min.getY(),max.getY()),vec2i(min.getZ(),max.getZ()),vertXYdeep,vertiXY,G);
		vertAB_push(vertXZ,max.getX(),vec2i(min.getZ(),max.getZ()),vec2i(min.getY(),max.getY()),vertXZdeep,vertiXZ,G);
		vertAB_push(vertYZ,max.getY(),vec2i(min.getZ(),max.getZ()),vec2i(min.getX(),max.getX()),vertYZdeep,vertiYZ,G);
	}



	auto vertices_push = [](vector<int>& vertA,
		vector<vector<Square>>& vertiA1, vector<vector<Square>> vertiA2,
		function<void(int,vec2i,vec2i)> pushS1, function<void(int,vec2i,vec2i)> pushS2){

		for(size_t i=0;i<vertA.size();++i){
			vector<Square> S1;
			vector<Square> S2;

			getRenderSquare(vertiA1[i], vertiA2[i], S1, S2);

			for(Square S: S1)pushS1(vertA[i], S.min, S.max);
			for(Square S: S2)pushS2(vertA[i], S.min, S.max);
		}
	};

	vertices_push(vertX, vertiX1, vertiX2,
		[this](int X, vec2i min, vec2i max){
			vertices.push_back(vec3(X, min.getX(), min.getY()));
			vertices.push_back(vec3(X, min.getX(), max.getY()));
			vertices.push_back(vec3(X, max.getX(), min.getY()));

			vertices.push_back(vec3(X, max.getX(), min.getY()));
			vertices.push_back(vec3(X, min.getX(), max.getY()));
			vertices.push_back(vec3(X, max.getX(), max.getY()));
		},
		[this](int X, vec2i min, vec2i max){
			vertices.push_back(vec3(X, min.getX(), min.getY()));
			vertices.push_back(vec3(X, max.getX(), min.getY()));
			vertices.push_back(vec3(X, min.getX(), max.getY()));

			vertices.push_back(vec3(X, min.getX(), max.getY()));
			vertices.push_back(vec3(X, max.getX(), min.getY()));
			vertices.push_back(vec3(X, max.getX(), max.getY()));
		}
	);
	vertices_push(vertY, vertiY1, vertiY2,
		[this](int Y, vec2i min, vec2i max){
			vertices.push_back(vec3(min.getX(), Y, min.getY()));
			vertices.push_back(vec3(max.getX(), Y, min.getY()));
			vertices.push_back(vec3(min.getX(), Y, max.getY()));

			vertices.push_back(vec3(min.getX(), Y, max.getY()));
			vertices.push_back(vec3(max.getX(), Y, min.getY()));
			vertices.push_back(vec3(max.getX(), Y, max.getY()));
		},
		[this](int Y, vec2i min, vec2i max){
			vertices.push_back(vec3(min.getX(), Y, min.getY()));
			vertices.push_back(vec3(min.getX(), Y, max.getY()));
			vertices.push_back(vec3(max.getX(), Y, min.getY()));

			vertices.push_back(vec3(max.getX(), Y, min.getY()));
			vertices.push_back(vec3(min.getX(), Y, max.getY()));
			vertices.push_back(vec3(max.getX(), Y, max.getY()));
		}
	);

	vertices_push(vertZ, vertiZ1, vertiZ2,
		[this](int Z, vec2i min, vec2i max){
			vertices.push_back(vec3(min.getX(), min.getY(), Z));
			vertices.push_back(vec3(min.getX(), max.getY(), Z));
			vertices.push_back(vec3(max.getX(), min.getY(), Z));

			vertices.push_back(vec3(max.getX(), min.getY(), Z));
			vertices.push_back(vec3(min.getX(), max.getY(), Z));
			vertices.push_back(vec3(max.getX(), max.getY(), Z));
		},
		[this](int Z, vec2i min, vec2i max){
			vertices.push_back(vec3(min.getX(), min.getY(), Z));
			vertices.push_back(vec3(max.getX(), min.getY(), Z));
			vertices.push_back(vec3(min.getX(), max.getY(), Z));

			vertices.push_back(vec3(min.getX(), max.getY(), Z));
			vertices.push_back(vec3(max.getX(), min.getY(), Z));
			vertices.push_back(vec3(max.getX(), max.getY(), Z));
		}
	);




	vertices_push(vertXG, vertiX1G, vertiX2G,
		[this](int X, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(X, min.getX(), min.getY()));
			verticesGlass.push_back(vec3(X, min.getX(), max.getY()));
			verticesGlass.push_back(vec3(X, max.getX(), min.getY()));

			verticesGlass.push_back(vec3(X, max.getX(), min.getY()));
			verticesGlass.push_back(vec3(X, min.getX(), max.getY()));
			verticesGlass.push_back(vec3(X, max.getX(), max.getY()));
		},
		[this](int X, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(X, min.getX(), min.getY()));
			verticesGlass.push_back(vec3(X, max.getX(), min.getY()));
			verticesGlass.push_back(vec3(X, min.getX(), max.getY()));

			verticesGlass.push_back(vec3(X, min.getX(), max.getY()));
			verticesGlass.push_back(vec3(X, max.getX(), min.getY()));
			verticesGlass.push_back(vec3(X, max.getX(), max.getY()));
		}
	);
	vertices_push(vertYG, vertiY1G, vertiY2G,
		[this](int Y, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(min.getX(), Y, min.getY()));
			verticesGlass.push_back(vec3(max.getX(), Y, min.getY()));
			verticesGlass.push_back(vec3(min.getX(), Y, max.getY()));

			verticesGlass.push_back(vec3(min.getX(), Y, max.getY()));
			verticesGlass.push_back(vec3(max.getX(), Y, min.getY()));
			verticesGlass.push_back(vec3(max.getX(), Y, max.getY()));
		},
		[this](int Y, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(min.getX(), Y, min.getY()));
			verticesGlass.push_back(vec3(min.getX(), Y, max.getY()));
			verticesGlass.push_back(vec3(max.getX(), Y, min.getY()));

			verticesGlass.push_back(vec3(max.getX(), Y, min.getY()));
			verticesGlass.push_back(vec3(min.getX(), Y, max.getY()));
			verticesGlass.push_back(vec3(max.getX(), Y, max.getY()));
		}
	);

	vertices_push(vertZG, vertiZ1G, vertiZ2G,
		[this](int Z, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(min.getX(), min.getY(), Z));
			verticesGlass.push_back(vec3(min.getX(), max.getY(), Z));
			verticesGlass.push_back(vec3(max.getX(), min.getY(), Z));

			verticesGlass.push_back(vec3(max.getX(), min.getY(), Z));
			verticesGlass.push_back(vec3(min.getX(), max.getY(), Z));
			verticesGlass.push_back(vec3(max.getX(), max.getY(), Z));
		},
		[this](int Z, vec2i min, vec2i max){
			verticesGlass.push_back(vec3(min.getX(), min.getY(), Z));
			verticesGlass.push_back(vec3(max.getX(), min.getY(), Z));
			verticesGlass.push_back(vec3(min.getX(), max.getY(), Z));

			verticesGlass.push_back(vec3(min.getX(), max.getY(), Z));
			verticesGlass.push_back(vec3(max.getX(), min.getY(), Z));
			verticesGlass.push_back(vec3(max.getX(), max.getY(), Z));
		}
	);



	for(size_t i=0;i<vertXY.size();++i){
		for(size_t j=0;j<vertXYdeep[i].size();++j){
			vector<Line> L;
			getRenderLine(vertiXY[i][j], L);

			for(size_t k=0;k<L.size();++k){
				verticesL.push_back(vec3(vertXY[i], vertXYdeep[i][j], L[k].min));
				verticesL.push_back(vec3(vertXY[i], vertXYdeep[i][j], L[k].max));
			}
		}
	}
	for(size_t i=0;i<vertXZ.size();++i){
		for(size_t j=0;j<vertXZdeep[i].size();++j){
			vector<Line> L;
			getRenderLine(vertiXZ[i][j], L);

			for(size_t k=0;k<L.size();++k){
				verticesL.push_back(vec3(vertXZ[i], L[k].min, vertXZdeep[i][j]));
				verticesL.push_back(vec3(vertXZ[i], L[k].max, vertXZdeep[i][j]));
			}
		}
	}
	for(size_t i=0;i<vertYZ.size();++i){
		for(size_t j=0;j<vertYZdeep[i].size();++j){
			vector<Line> L;
			getRenderLine(vertiYZ[i][j], L);

			for(size_t k=0;k<L.size();++k){
				verticesL.push_back(vec3(L[k].min, vertYZ[i], vertYZdeep[i][j]));
				verticesL.push_back(vec3(L[k].max, vertYZ[i], vertYZdeep[i][j]));
			}
		}
	}
	vertexbufferL = verticesL.empty()?0:render::getBuffer(verticesL);
	vertexbuffer = vertices.empty()?0:render::getBuffer(vertices);
	vertexbufferGlass = verticesGlass.empty()?0:render::getBuffer(verticesGlass);

	reRender = 0;
}

void CubeCompound::cleanRender(){
	vertices.clear();
	verticesL.clear();
	verticesGlass.clear();
	render::delBuffer(&vertexbufferL);
	render::delBuffer(&vertexbuffer);
	render::delBuffer(&vertexbufferGlass);
}
CubeCompound *WorldCubeCompound=0;
