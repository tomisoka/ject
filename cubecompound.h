#pragma once

#include "ObjectComponent.h"

extern vector<const btCollisionObject *> cubeCompoundCollObjs;

struct Line{
	Line(const Line& o):min(o.min), max(o.max), glass(o.glass){}
	Line(int min, int max, bool glass = 0):min(min), max(max), glass(glass){}
	Line(vec2i sz, bool glass = 0):min(sz[0]), max(sz[1]), glass(glass){}

	bool collideNew(Line c, vector<Line> &out);

	int min, max;
	bool glass;
};

struct Square{
	Square(const Square& o):min(o.min), max(o.max){}
	Square(vec2i min, vec2i max):min(min), max(max){}

	bool collide(Square c, vector<Square> &out);

	vec2i min, max;
};

class CubeCompound;

struct Cube{
	Cube(vec3i min, vec3i max, bool glass = 0, float scale = 0): scale(scale), body(0), bodyInW(0), min(min), max(max), glass(glass), renderCopy(0), parent(0){
		checkMinMax();
	}
	Cube(const Cube& cube): scale(cube.scale), body(0), bodyInW(0), min(cube.min), max(cube.max),
		glass(cube.glass), renderCopy(0), parent(0){
		checkMinMax();
	}
	~Cube();

	void activate(CubeCompound *parent);
	void deactivate();

	void updateBody();
	void actBtBody(bool act);

	bool collide(Cube& c, vector<Cube>& out);
	bool collideInside(Cube& c, vector<Cube>& out);
	Cube *merge(Cube& c);

	void checkMinMax(){
		max.swapMax(min);
	}

	float scale;
	char *toString(char *out);

	BodyInterface *body;
	bool bodyInW;

	vec3i min, max;

	bool glass;

	Cube *renderCopy;
	CubeCompound *parent;
};

class CubeCompound: public ObjectComponent{
public:
	CubeCompound(): oldPos(vec3(0)), oldRot(quat()), mained(0), scale(0.2f), vertexbufferL(0), vertexbuffer(0), vertexbufferGlass(0), cubeSZ(0), isMain(0), reRender(0), rendered(0){}
	CubeCompound(const CubeCompound& o);
	CubeCompound(xml_node xml);
	~CubeCompound();

	ObjectComponent* Copy() override{return new CubeCompound(*this);}

	void postLoaded() override;

	void merge(vector<Cube *>* mergedCubes = 0); //to WorldCubeCompound
	void getCopyInSpace(Cube& space, vector<Cube>& cubesInside);

	void addCube(Cube *cube){cubes.push_back(cube); reRender=1;}
	void remAddedCube(Cube *cube){
		for(size_t i=0;i<cubes.size();++i)if(cube == cubes[i])cubes.erase(cubes.begin() + i--);
		reRender=1;
	}
	void addNewCube(Cube& cube);
	void remCube(Cube& cube);

	void restoreCubes();

	void saveXml(xml_node xml);

	uint32_t getID() override{return 16;}
	string getIDName() override{return "CubeCompound";}

	void actBtBody(bool act);

	void update() override;
	void renderReg() override;

	float getScale(){return scale;}

	void setEditColor(vec3 color);
	void unsetEditColor();

	bool isMained(){return mained;}

	vector<Cube> copyCubes();
private:
	void setReRender(){reRender = 1;}

	void resetRender();
	void cleanRender();

	vec3 oldPos;
	quat oldRot;

	bool mained;
	bool canBeMained();

	void tryMerge(Cube& c);

	float scale;

	vector<vec3> verticesL;
	vector<vec3> vertices;
	vector<vec3> verticesGlass;

	uint_t vertexbufferL;
	uint_t vertexbuffer;
	uint_t vertexbufferGlass;


	vector<Cube *> cubes;
	size_t cubeSZ;

	vector<Cube> cubesRestore;


	bool isMain;

	bool reRender;
	bool rendered;
};

extern CubeCompound *WorldCubeCompound;
