#include "edit.h"
#include "../simplefc/render.h"
#include "../controls.h"
#include "../simplefc/window.h"
#include "../gameData.h"
#include "editExtension.h"
#include <GLFW/glfw3.h>
#include "../level.h"
#include "../predoneFcs.h"
#include "editComp.h"
#include "editHistory.h"
#include "editHistoryChunks.h"
#include "../components.h"
#include "../simplefc/basicFcs.h"
#include <limits.h>
#include "../BtInterface/BodyInterface.h"
#include "../BtInterface/convertVM.h"
#include "../GUI/Button.h"
#include "../Controls.h"

char glassC[] = "- Glass";
char lockC[10] = "lock";
char quat_e[6] = "Quat";
char rel_a[9] = "Relative";

void setQuatEuler(bool q_e){
	quat_euler = q_e;
	strcpy(quat_e, q_e?"Euler":"Quat");
}

void setRelAbs(bool r_a){
	rel_abs = r_a;
	strcpy(rel_a, r_a?"Absolute":"Relative");
}

//editable comps
const size_t editComps = 5;
const uint32_t editCompIDs[editComps] = {5, 6, 21, 22, 24};
const string editCompNames[editComps] = {"Mover", "Rotor", "TrackMover", "TrackRotor", "Force"};
ObjectComponent *getEditComp(size_t n){
	switch (n){
		case 0: return new MoveComponent();
		case 1: return new RotComponent();
		case 2: return new TrackMover();
		case 3: return new TrackRotor();
		case 4: return new Force();
	}
	return 0;
}

Edit *edit;

void resetEdit(bool acting){
	edit->reset();
	edit->setActive(acting);
}

Edit::Edit(bool active){
	this->active = active;
	addExt = 0;
	addObj = 0;
	addObjV = 0;
	RSBar = 0;
	LSBar = 0;
	editHistory = 0;
	locked = 0;

	this->addComponent(createEdgeRect(vec2(0), vec2(0.75f, 1.1f)));

	this->addComponent(createInsideRect(vec2(-0.875f, 0), vec2(0.125f, 1)));
	this->addComponent(createInsideRect(vec2(0.875f, 0), vec2(0.125f, 1)));

	this->addChild(LBar = new Object(vec3(-0.875f, 0, 0)));
	this->addChild(RBar = new Object(vec3(0.875f, 0, 0)));
	RBar->addChild(rButs= new Object(0));
	RBar->addChild(rObj = new Object(1));
	LBar->addChild(lButs= new Object(1));
	LBar->addChild(lObj = new Object(0));
	addLeftObjs();

	leftOffset = 0.0001;

	printLog("Edit - reset");
	reset();

	lButtOffset = 0.04;
	actLeft = 0;

	printLog("Loading extensions");
	getExtensions("levels/extension/", extensions);

	printLog("Edit - adding ext buttons");
	for(const auto& ext: extensions){
		let pos = vec2(0, 1-lButtOffset);
		let size = vec2(0.12f, 0.03f);
		lButs->addComponent(new Button(pos, size, ext->name.c_str(), {new TriggerData(this, actLeft++)}));
		lButtOffset+=0.07f;
	}

	printLog("Edit - adding buttons");

	addRightInput(vec2(0, 1-0*0.125f), "Pos:", this, 0);
	addRightInput(vec2(0, 1-1*0.125f), "Rot:", this, 1);

	Roffset=1;
	Loffset=1;

	printLog("Edit - adding sidebars");
	this->addChild((new Object())
		->addComponent(RSBar=new SideBar(vec2(1-0.015625,-1), vec2(1),1,0.5f,&Roffset,0.25f,0,vec3(0.7),vec3(0.8)))
		->addComponent(LSBar=new SideBar(vec2(-1),vec2(-1+0.015625,1),1,0.5f,&Loffset,0.25f,1,vec3(0.7),vec3(0.8))));

	leftOffset = lButtOffset;
	if(leftOffset < 0.0001)leftOffset = 0.0001;
	LSBar->setMultH(2.f/leftOffset);
}

Edit::~Edit(){
	if(editHistory)delete editHistory;
	deleteAddObjV();
}

void Edit::addRightInput(vec2 pos, const char *name, TextinCallBack *output, int32_t outCode){
	Object *butObject = new Object(pos);
	rButs->addChild(butObject);

	butObject->addComponent(new RenderText(name, vec2(-0.12, -0.03), 0.04, 0));

	Textin *in = new Textin(vec2(0, -0.08), vec2(0.12, 0.03), output, outCode);
	butObject->addComponent(in);
	inputs.push_back(in);
}


void Edit::reset(){
	if(addObj && addObj == CameraObj){
		if(locked)addObj->actBtBodyCh(1);
	}
	addObj = 0;
	addExt = 0;
	bpos=0;
	axisLock=0;
	creDel=0;
	addAngle=0;
	toReset = 0;
	calling = 0;
	grid = 1;
	glass = 0;
	glassC[0] = '-';
	gridad = 5;
	showGhosts=0;
	ignoreGhosts=1;
	choose.clear();

	if(editHistory)delete editHistory;
	editHistory = new EditHistory();
	resetEObj(0);
}

void Edit::setLocked(bool locked){
	this->locked = locked;
	strcpy(lockC, locked?"Locked":"Unlocked");
}

void Edit::addLeftObjs(){
	let size = vec2(0.12f, 0.03f);

	float offset = 0.04;
	lObj->addComponent(new Button(vec2(0, 1-offset), size, quat_e, {new TriggerData(this, 0x7FFE0000)}));
	offset+=0.07f;
	lObj->addComponent(new Button(vec2(0, 1-offset), size, rel_a, {new TriggerData(this, 0x7FFE0001)}));
	offset+=0.07f;
	lObj->addComponent(new Button(vec2(0, 1-offset), size, lockC, {new TriggerData(this, 0x7FFE0002)}));
	offset+=0.07f;
}

void Edit::resetEObj(bool act){
	if(choose.size()){
		act = 0;
		if(chooseID!=0)setCalling(0, 0);
		hideCursor(0);
	}else{
		setCalling(0, 0);
	}

	if(act){
		inputs[0]->setText(vecToS(addObj->getPos()).c_str());
		inputs[1]->setText((quat_euler? vecToS(addObj->getRot().getEuler()*180/M_PI):
																		vecToS(addObj->getRot(), ';', 6)).c_str());

		lButs->setActive(0);
		lObj->setActive(1);
	}else if(!choose.size()){
		deleteAddObj();
		lObj->setActive(0);
		lButs->setActive(1);
		leftOffset = lButtOffset;
	}

	rButs->setActive(act);
	rObj->delChildren();

	vec2 size = vec2(0.12f, 0.025f);
	rObjOffset = 1;
	if(choose.size()){
		rObjOffset = 0;
		Object *adderObj = new Object(vec3(0, 1-rObjOffset, 0));
		rObj->addChild(adderObj);
		adderObj->addComponent(new RenderText("Choose call:", vec2(0, -0.03), 0.04));

		float height = 0.06f;
		for(size_t i = 0; i < choose.size(); ++i){
			let pos = vec2(0, -0.03f-height);
			adderObj->addComponent(new Button(pos, size, choose[i], {new TriggerData(this, 0x7FFF8000+i)}));
			height += 0.06f;
		}
	}else if(addObj){if(addExt){
		rObjOffset = 0.25;

		bool lined = 0;

		float height = 0;
		for(ObjectComponent* comp: addObj->getComps()){
			Object *editComp = CompEditObj(comp, height);

			if(editComp){
				editComp->setPos(vec3(0, 1-rObjOffset, 0));

				rObj->addChild(editComp);

				rObjOffset += height;

				if(!lined){
					editComp->addComponent(createLine(vec2(-0.125f, 0), vec2(0.125f, 0)));
					lined=1;
				}

				editComp->addComponent(createLine(vec2(-0.125f, -height), vec2(0.125f, -height)));
			}
		}
		height = 0;

		Object *adderObj = new Object(vec3(0, 1-rObjOffset, 0));
		rObj->addChild(adderObj);
		adderObj->addComponent(new RenderText("Add", vec2(0, -0.03), 0.04));

		height += 0.06f;
		for(size_t i=0;i<editComps;++i){
			let pos = vec2(0, -0.03f-height);
			adderObj->addComponent(new Button(pos, size, editCompNames[i], {new TriggerData(this, 0x7FFFF000+i)}));
			height += 0.06f;
		}

		rObjOffset += height;
	}}else if(RBar && actLevel){
		rObjOffset = 0.0625f;

		Object *save = new Object(vec3(0,1-rObjOffset,0));
		rObj->addChild(save);

		save->addComponent((new Textin(vec2(0, -0.03), vec2(0.12, 0.03), this, 0xFFFF0000))->setText(actLevel->getXmlFile()));

		vec2 size = vec2(0.058f, 0.03f);
		save->addComponent(new Button(vec2(-0.0625, -0.1  ), size, "Save", {new TriggerData(this, 0x7FFF0001)}));
		save->addComponent(new Button(vec2( 0.0625, -0.1  ), size, "Load", {new TriggerData(this, 0x7FFF0002)}));
		save->addComponent(new Button(vec2(-0.0625, -0.175), size, glassC, {new TriggerData(this, 0x7FFF0003)}));


		rObjOffset+=0.125f;
	}
	if(2.>=rObjOffset)Roffset = 1;
	if(2.>=leftOffset)Loffset = 1;

	if(LSBar)LSBar->setMultH(2./leftOffset);
	if(RSBar)RSBar->setMultH(2./rObjOffset);
}

void Edit::deleteAddObjV(){
	cubesV.clear();
	if(addObjV)delete addObjV;
}

void Edit::deleteAddObj(){
	if(!addObj)return;
	if(addExt){
		addObj->delMe();
		addExt=0;
	}else if(addObj != CameraObj){
		deleteAddObjV();
		for(ObjectComponent *comp: addObj->getComps()){
			if(comp->getID() == 16){
				CubeCompound *cCom = (CubeCompound *)comp;
				cubesV = cCom->copyCubes();
				comp->delMe();
			}
		}
		addObjV = addObj;
		addObjV->removeParent();
	}
	Roffset = 1;
	addObj = 0;
}

void Edit::placeAddObj(){
	if(!addObj)return;
	if(!addExt && addObj != CameraObj){
		if(!fisInt(addAngle/90))return;
		deleteAddObjV();
		for(ObjectComponent *comp: addObj->getComps()){
			if(comp->getID() == 16){
				CubeCompound *cCom = (CubeCompound *)comp;
				cubesV = cCom->copyCubes();
				vector<Cube *> mergedCubes;
				cCom->merge(&mergedCubes);
				EHistChCubeCompound *cubeHist = new EHistChCubeCompound();
				for(Cube *cube: mergedCubes)cubeHist->addAddCube(*cube);
				editHistory->addHistory(cubeHist, 0);
			}
		}
		vector<Object *> os;
		for(Object *obj: addObj->getChildren()){
			Object *o = obj->Copy();
			o->setPos(addObj->getTransform() * o->getPos());
			o->setRot(addObj->getRot() * o->getRot());
			o->name = actLevel->getFreeNameHexObj("Object:");
			actLevel->addChild(o);
			o->postLoaded();
			o->actBtBodyCh(1);
			o->updateTriggerNames();
			os.push_back(o);
		}
		triggerRedirectFindAClear();

		for(Object *o: os){
			auto sc = o->propagateStringChange();
			for(ObjectComponent *comp: o->getComps()){
				if(comp->getID() == 10){
					((EditExtension *)comp)->updateNames(sc);
				}
			}
		}
		addObjV = addObj;
		addObjV->removeParent();
	}else{
		addObj->actBtBodyCh(1);
	}

	Roffset = 1;
	addObj=0;
	addExt=0;
	resetEObj(0);
}

bool Edit::input_key(bool acted, int key, int action, int mods){
	if(!getActive() || acted)return false;

	if(action && statical){
		if(
			key == CTRL::OBJECT_UP || key == CTRL::OBJECT_DOWN ||
			key == CTRL::OBJECT_LEFT || key == CTRL::OBJECT_RIGHT ||
			key == CTRL::OBJECT_FORWARD || key == CTRL::OBJECT_BACK){

			int mult = 1;
			if(mods & 1)mult *= 5;
			if(mods & 2)mult *= 10;

			vec3i mv = vec3i(0);
			if(key == CTRL::OBJECT_UP)mv = vec3i(0,1,0);
			else if(key == CTRL::OBJECT_DOWN)mv = vec3i(0,-1,0);
			else if(key == CTRL::OBJECT_LEFT)mv = vec3i(-1,0,0);
			else if(key == CTRL::OBJECT_RIGHT)mv = vec3i(1,0,0);
			else if(key == CTRL::OBJECT_FORWARD)mv = vec3i(0,0,-1);
			else if(key == CTRL::OBJECT_BACK)mv = vec3i(0,0,1);

			mv *= mult;

			if(bpos){
				axisLock = axisLock | 4;
				fpos2 += mv;
			}

			if(addObj){
				setLocked(0);
				addObj->setPos(addObj->getPos() + vec3(mv)/5);
			}
			return true;
		}
	}

	if(action == 1){
		if(key==CTRL::SHOW_GHOSTS){
			ignoreGhosts=!ignoreGhosts;
			showGhosts=!showGhosts;
			return true;
		}

		if(!statical){
			if(key==CTRL::PLAYTEST || key==256){
				statical = 1;
				inCam = 0;
				CameraObj->setPos(startingPos);
				CameraObj->setRot(quat(vec3(0,startingHorA,0)));
				control->resetKill();
				actLevel->setEditMode(1);

				position = oldPos;
				horizontalAngle = oldHA;
				verticalAngle = oldVA;
				return true;
			}else if(key==CTRL::SET_SPAWN){
				startingPos = CameraObj->getPos();
				startingHorA = CameraObj->getRot().getEuler().getY();
			}else if(key==CTRL::TELEPORT){
				btVector3 btHit, btHitNormal;

				vec3 cam_pos = get_current_view()->get_position();
				if(closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos + get_current_view()->get_direction()*1000000.f), ignoreGhosts?0x2:(0xFFFFFFFF), btHit, btHitNormal)){
					btTransform trans = CameraObj->getBtBody()->getWorldTransform();
					trans.setOrigin(btHit + btVector3(0,0.9,0));
					CameraObj->getBtBody()->setWorldTransform(trans);
				}
			}

		}else{
			if(key == CTRL::UNDO){
				if(!addObj && !bpos){
					if(mods == 2){
						editHistory->undo();
					}else if(mods == 3){
						editHistory->redo();
					}
				}
			}

			if(mods == 2){//CTRL
				if(key == CTRL::PASTE){
					if(!addObj){
						addObj = addObjV;
						addObjV = 0;
						actLevel->addChild(addObj);
						CubeCompound *cCom = new CubeCompound();
						addObj->addComponent(cCom);
						for(Cube& c: cubesV)cCom->addNewCube(c);
						cubesV.clear();

						cCom->actBtBody(0);

						setLocked(1);
						resetEObj(1);

						bpos=0;
					}
				}else if(key == CTRL::COPY || key == CTRL::CUT){
					bool cut = key == CTRL::CUT;

					if(!addObj && bpos){
						vec3i min = fpos;
						vec3i max = fpos2;

						max.swapMax(min);

						vector<Cube> cubesInside;
						Cube space = Cube(min, max+1);
						WorldCubeCompound->getCopyInSpace(space, cubesInside);

						int CMinMax[] = {INT_MAX, INT_MAX, INT_MAX, INT_MIN, INT_MIN, INT_MIN};
						for(Cube& cube: cubesInside){
							for(size_t i = 0; i < 3; ++i){
								if(cube.min[i] < CMinMax[i])CMinMax[i] = cube.min[i];
								if(cube.max[i] > CMinMax[i+3])CMinMax[i+3] = cube.max[i];
							}
						}

						vec3i Coffset = (	vec3i(CMinMax[0], CMinMax[1], CMinMax[2]) +
															vec3i(CMinMax[3], CMinMax[4], CMinMax[5])) / 2;

						float minMax[] = {FLT_MAX, FLT_MAX, FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX};

						vector<Object *> levelChild = actLevel->getChildren();
						vector<Object *> objInSpace;
						for(Object *child: levelChild){
							vec3 pos = child->getPos();
							vec3 min = (vec3(space.min) - 0.5)/5;
							vec3 max = (vec3(space.max) + 0.5)/5;
							if(child->name != "World" && pos.isInSpace(min, max)){
								for(size_t i = 0; i < 3; ++i){
									if(pos[i] < minMax[i])minMax[i] = pos[i];
									if(pos[i] > minMax[i+3])minMax[i+3] = pos[i];
								}
								objInSpace.push_back(child);
							}
						}

						if(cubesInside.size() || objInSpace.size()){
							vec3 offset = cubesInside.size() ?
								vec3(Coffset)/5.:((vec3(minMax[0],minMax[1],minMax[2])+vec3(minMax[3],minMax[4],minMax[5]))/2);

							for(Cube& cube: cubesInside){
								cube.min -= Coffset;
								cube.max -= Coffset;
							}

							addObj = new Object();
							addObj->setStartEditMode(1);

							CubeCompound *copied = new CubeCompound();
							addObj->addComponent(copied);
							for(Cube& cube: cubesInside)copied->addNewCube(cube);
							copied->postLoaded();

							vector<Object *> os;
							for(Object *obj: objInSpace){
								Object *o = obj->Copy(1);
								o->setPos(o->getPos() - offset);
								o->name = addObj->getFreeNameHexObj("Object:");
								addObj->addChild(o);
								os.push_back(o);

								if(cut)obj->delMe();
							}
							addObj->updateTriggerNames();
							triggerRedirectFindAClear();

							for(Object *o: os){
								auto sc = o->propagateStringChange();
								for(ObjectComponent *comp: o->getComps()){
									if(comp->getID() == 10){
										((EditExtension *)comp)->updateNames(sc);
									}
								}
							}

							if(cut)editHistory->addHistory(new EHistChCubeCompound(space, 1));


							actLevel->addChild(addObj);
							addObj->actBtBodyCh(0);

							setLocked(1);
							resetEObj(1);

							bpos=0;
							return true;
						}
					}
				}
			}
			else if(key==CTRL::AXIS_X)axisLock=1;
			else if(key==CTRL::AXIS_Y)axisLock=2;
			else if(key==CTRL::AXIS_Z)axisLock=3;
			else if(key==CTRL::AXIS_V)axisLock=0;
			else if(key==CTRL::AXIS_B)axisLock=4;
			else if(key==CTRL::CREATE_DELETE)creDel=!creDel;
			else if(key==CTRL::SHOW_GRID)grid=!grid;
			else if(key==CTRL::TOGGLE_GLASS){
				glass=!glass;
				glassC[0] = glass?'+':'-';
			}else if(key=='-' || key==GLFW_KEY_KP_SUBTRACT){
				if(gridad>1)gridad--;
			}else if(key=='+' || key==GLFW_KEY_KP_ADD){
				if(gridad<50)gridad++;
			}else if(key==CTRL::ROTATE_P){
				addAngle+=22.5;
				if(addAngle>360)addAngle-=360;
			}else if(key==CTRL::ROTATE_M){
				addAngle-=22.5;
				if(addAngle<0)addAngle+=360;
			}else if(key==CTRL::PLAYTEST){
				#define TO_PLAYTEST(){\
					oldPos = position;\
					oldHA = horizontalAngle;\
					oldVA = verticalAngle;\
					statical = 0;\
					inCam = 1;\
					verticalAngle = 0;\
					control->resetKill();\
					actLevel->setEditMode(0);\
				}
				if(!addObj){
					TO_PLAYTEST();
					horizontalAngle = startingHorA;
				}
			}else if(key==CTRL::PLAYTEST_ON_PLACE){
				if(!addObj){
					TO_PLAYTEST();

					btVector3 btHit, btHitNormal;
					vec3 cam_pos = get_current_view()->get_position();
					if(closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos+get_current_view()->get_direction()*1000000.f), ignoreGhosts?0x2:(0xFFFFFFFF), btHit, btHitNormal)){
						btTransform trans = CameraObj->getBtBody()->getWorldTransform();
						trans.setOrigin(btHit + btVector3(0,0.9,0));
						CameraObj->getBtBody()->setWorldTransform(trans);
					}
				}
			}else if(key == CTRL::TELEPORT){
				btVector3 btHit, btHitNormal;
				vec3 cam_pos = get_current_view()->get_position();
				if(closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos+get_current_view()->get_direction()*1000000.f), ignoreGhosts?0x2:(0xFFFFFFFF), btHit, btHitNormal)){
					position = Vec3(btHit);
				}
			}else if(key==256 && action == 1){
				bool is_addObj = addObj;
				if(choose.size()){
					choose.clear();
					toReset = 1;
					hideCursor(1);
				}else{
					placeAddObj();
				}
				if(is_addObj)return true;
			}
		}
	}
	return false;
}
bool Edit::input_mouse(bool acted, int button, int action, int mods){
	if(!statical || !getActive() || choose.size() || acted)return false;

	if(!isHiddenCursor()){
	}else if(action == 1 && button==1){
		deleteAddObj();
		resetEObj(0);

		bpos=0;
		return true;
	}else if(action == 1 && button==0){
		btVector3 btHit, btHitNormal;
		vec3 cam_pos = get_current_view()->get_position();
		const btCollisionObject *colObj = closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos+get_current_view()->get_direction()*1000000.f), ignoreGhosts?0x2:(0xFFFFFFFF), btHit, btHitNormal);


		if(addObj && colObj){
			if(calling){
				if(Object *extObj = getObjColl(colObj)){
					while(extObj && extObj->getParent()!=actLevel)extObj = extObj->getParent();

					vector<string> calls;
					if(extObj)
					for(ObjectComponent *comp: extObj->getComps()){
						uint32_t ID = comp->getID();
						if(ID==10){
							for(string canCall: ((EditExtension*) comp)->canCall){
								calls.push_back(extObj->name + "/" + canCall);
							}
						}else if(std::find(editCompIDs, editCompIDs + editComps, ID) != editCompIDs + editComps){
							calls.push_back(comp->name);
						}
					}
					if(calls.size() == 1){
						calling->textin_callback(callingCode, calls[0]);
						calling = 0;
					}else if(calls.size() > 1){
						choose = calls;
						chooseID = 0;
						toReset = 1;
					}
				}
			}else{
				placeAddObj();
			}
		}else if(bpos){
			vec3i min = fpos;
			vec3i max = fpos2;

			max.swapMax(min);
			editHistory->addHistory(new EHistChCubeCompound(Cube(min, max+1, glass), creDel));
			bpos=0;
		}else if(colObj){
			bool canCube = find(cubeCompoundCollObjs.begin(), cubeCompoundCollObjs.end(), colObj) != cubeCompoundCollObjs.end();

			BodyInterface *b_obj = (BodyInterface *)colObj;
			if(canCube && b_obj->get_parent_comp() && b_obj->get_parent_comp() != WorldCubeCompound)canCube = 0;

			if(isPressed(CTRL::IGNORE_OBJECTS) || canCube){
				vec3 hit = Vec3(btHit);

				hit = (hit - (hit-get_current_view()->get_position()).normalized()/100.f*(creDel?-1:1));
				hit*=5;

				vec3i cubePs = (hit-0.5f).roundI();

				fpos = cubePs;
				Fpos = (vec3(fpos)+0.5f)/5.f;
				bpos = 1;

				if(axisLock & 4)axisLock = axisLock & 3;
			}else if(Object *extObj = getObjColl(colObj)){
				while(extObj && extObj->getParent()!=actLevel && extObj != CameraObj)extObj = extObj->getParent();
				if(extObj == CameraObj){
					addObj = extObj;
					addExt = 0;
					setLocked(0);
					resetEObj(1);
				}else if(extObj){
					for(ObjectComponent *comp: extObj->getComps()){
						if(comp->getID()==10){
							addExt = (EditExtension*)comp;
							addObj = extObj;
							setLocked(0);
							resetEObj(1);
							break;
						}
					}
				}
			}
		}
		return true;
	}
	return false;
}

void Edit::update(){
	if(!statical || !getActive())return;

	if(toReset){
		actLevel->updateTriggerNames();
		actLevel->registTriggers();
		actLevel->requestTriggers();
		triggersClear();

		if(addObj && locked){
			addObj->actBtBodyCh(1);
			addObj->actBtBodyCh(0);
		}
		resetEObj(!!addObj);
		toReset=0;
	}


	RBar->setPos(vec3(RBar->getPos().getX(), (Roffset-1)/2.f*(2-rObjOffset), 0));
	LBar->setPos(vec3(LBar->getPos().getX(), (Loffset-1)/2.f*(2-leftOffset), 0));



	if(!isHiddenCursor()){
		bpos=0;
	}

	Object::update();

	btVector3 btHit, btHitNormal;
	vec3 cam_pos = get_current_view()->get_position();
	const btCollisionObject *colObj = closestObjectIgnore(btVec3(cam_pos), btVec3(cam_pos+get_current_view()->get_direction()*1000000.f), ignoreGhosts?0x2:(0xFFFFFFFF), btHit, btHitNormal);

	if(addObj && locked){
		if(colObj){
			vec3 hit = Vec3(btHit);

			vec3 up = Vec3(btHitNormal.normalized());

			int axisRound = 0;

			if(addExt)hit+=up*addExt->standOff;

			quat relRot(vec3(0,1,0), addAngle*M_PI/180);
			if(addExt && addExt->rotatable){
				quat rota;
				if(fabs(up.getX()) < 0.01 && fabs(up.getY()) < 0.01){
					axisRound = 3;
				}else if(fabs(up.getY()) < 0.01 && fabs(up.getZ()) < 0.01){
					axisRound = 6;
				}
				if(fabs(up.getX()) < 0.01 && fabs(up.getZ()) < 0.01){
					if(up.getY()>0)rota = vec3(0,0,0);
					else rota = vec3(M_PI,0,0);
					axisRound = 5;
				}else{
					vec3 right = up.cross(vec3(0, 1, 0)).normalized();
					vec3 f = up.cross(right).normalized();

					mat4 rt;
					rt.initRotationFromVectors(right*-1, up, f);
					rota = quat(rt);
				}
				addObj->setRot(relRot * rota);
			}else{
				if(addObj!=CameraObj){
					addObj->setRot(relRot);
					if(!addExt)axisRound = 7;
				}
			}

			if(grid && axisRound){
				hit*=5;
				if(axisRound & 1)hit.setX(round(hit.getX()));
				if(axisRound & 2)hit.setY(round(hit.getY()));
				if(axisRound & 4)hit.setZ(round(hit.getZ()));
				hit/=5;
			}

			addObj->setPos(hit);
			if(addObj == CameraObj){
				addObj->setPos(addObj->getPos() + vec3(0, 0.9f, 0));
				startingPos = addObj->getPos();
			}
		}


	}else if(bpos){
		View* view = get_current_view();
		if(axisLock==0){
			if(colObj){
				vec3 hit = Vec3(btHit);

				hit = (hit - (hit-view->get_position()).normalized()/100.f*(creDel?-1:1));
				hit*=5;
				fpos2 = (hit-0.5f).roundI();
			}
		}else if(axisLock < 4){
			float diff = Fpos[axisLock-1]-view->get_position()[axisLock-1];
			float dirN = view->get_direction()[axisLock-1];

			vec3 dir = view->get_direction()/dirN;
			vec3 hit = view->get_position() + dir*diff;

			hit*=5;
			fpos2 = (hit-0.5).roundI();
		}
	}
}

void Edit::call(int state){
	if(!getActive() || !statical)return;

	if(state<10000){
		if(!addObj){
			addObj = new Object();
			addObj->setStartEditMode(1);

			addObj->name = actLevel->getFreeNameHexObj("Object:");

			addExt = new EditExtension(extensions[state]->file);
			addExt->initArgs();
			addObj->addComponent(addExt);

			actLevel->addChild(addObj);


			triggPref.push_back(addObj->name);
			actLevel->loadLevel(addObj, addObj, nullBtTransform, addExt);
			triggPref.pop_back();

			addObj->actBtBodyCh(0);

			setLocked(1);
			toReset = 1;
		}
	}else if(state == 0x7FFE0000){//quat/euler
		setQuatEuler(!quat_euler);
		toReset = 1;
	}else if(state == 0x7FFE0001){//rel/abs
		setRelAbs(!rel_abs);
		toReset = 1;
	}else if(state == 0x7FFE0002){//lock/unlocked
		setLocked(!locked);
		addObj->actBtBodyCh(!locked);
	}else if(state == 0x7FFF0000){
		toReset = 1;
	}else if(state == 0x7FFF0001){//save
		actLevel->save();
	}else if(state == 0x7FFF0002){//load
		Level *lv = actLevel;
		WorldCubeCompound = 0;
		Object *parent = lv->getParent();
		actLevel = new Level(lv->getXmlFile(), 1, vec3(0));
		parent->addChild(actLevel);
		lv->setXmlFile("");
		lv->delMe();
		control->reset();
	}else if(state == 0x7FFF0003){//glass
		glass = !glass;
		glassC[0] = glass?'+':'-';
	}else if(state >= 0x7FFF8000 && state < 0x7FFFF000){
		if(chooseID == 0){
			calling->textin_callback(callingCode, choose[state-0x7FFF8000]);
			setCalling(0, 0);
		}
		hideCursor(1);
		choose.clear();
		toReset = 1;
	}else if(state >= 0x7FFFF000 && addObj){//add Component
		ObjectComponent *comp = getEditComp(state - 0x7FFFF000);
		if(comp){
			comp->name = actLevel->getFreeNameHexComp(comp->getIDName()+":");
			addObj->addComponent(comp);
			toReset = 1;
		}
	}
}

void Edit::textin_callback(size_t issueID, string input){
	if(issueID==0){
		vec3 pos;
		if(strToVec(input.c_str(), pos)){
			printLogWarn("edit: invalid pos!");
			return;
		}

		if(addObj == CameraObj){
			startingPos = pos;
		}

		addObj->setPos(pos);
	}else if(issueID==1){
		if(quat_euler){
			vec3 rot;
			if(strToVec(input.c_str(), rot)){
				printLogWarn("edit: invalid rot!");
				return;
			}

			if(addObj == CameraObj){
				if(rot.getX()!=0 || rot.getZ() != 0)return;
				startingHorA = rot.getY()*M_PI/180;
			}

			addObj->setRot(quat(rot*M_PI/180));
		}else{
			quat rot;
			if(strToVec(input.c_str(), rot)){
				printLogWarn("edit: invalid rot!");
				return;
			}
			rot.normalize();

			if(addObj == CameraObj){
				vec3 rota = rot.getEuler();
				if(rota.getX()!=0 || rota.getZ() != 0)return;
				startingHorA = rota.getY()*M_PI/180;
			}

			addObj->setRot(rot);
		}
	}else if(issueID == 0xFFFF0000){
		actLevel->setXmlFile(input);
		toReset = 1;
	}
}

void Edit::setCalling(TextinCallBack *calling, int32_t code){
	if(actLevel){
		vector<Object *>& children = actLevel->getChildren();
		if(calling){
			this->calling = calling;
			for(Object *child: children){
				for(ObjectComponent *comp: child->getComps()){
					uint32_t ID = comp->getID();
					if((ID == 10 && ((EditExtension*)comp)->canCall.size()) ||
							(find(editCompIDs, editCompIDs + editComps, ID) != editCompIDs + editComps)){
						child->setEditColor(vec3(0,0,1));
						break;
					}
				}
			}
		}else if(this->calling){
			this->calling = 0;
			for(Object *child: children){
				child->unsetEditColor();
			}
		}
	}else this->calling = 0;
	callingCode = code;
}


void Edit::render(){
	static vec3 verts[24];
	if(getActive() && statical){
		if(bpos){
			mat4 transform;
			transform.initIdentity();

			vec3i mini = fpos;
			vec3i maxi = fpos2;
			maxi.swapMax(mini);

			float creDelOffS = 0.005*(creDel?-1:1);
			vec3 min = vec3(mini)/5 + creDelOffS;
			vec3 max = vec3(maxi+1)/5 - creDelOffS;

			uint32_t vert_len = 0;

			verts[vert_len++] = min;
			verts[vert_len++] = vec3(max.getX(), min.getY(), min.getZ());

			verts[vert_len++] = min;
			verts[vert_len++] = vec3(min.getX(), max.getY(), min.getZ());

			verts[vert_len++] = min;
			verts[vert_len++] = vec3(min.getX(), min.getY(), max.getZ());


			verts[vert_len++] = max;
			verts[vert_len++] = vec3(min.getX(), max.getY(), max.getZ());

			verts[vert_len++] = max;
			verts[vert_len++] = vec3(max.getX(), min.getY(), max.getZ());

			verts[vert_len++] = max;
			verts[vert_len++] = vec3(max.getX(), max.getY(), min.getZ());


			verts[vert_len++] = vec3(max.getX(), min.getY(), min.getZ());
			verts[vert_len++] = vec3(max.getX(), max.getY(), min.getZ());

			verts[vert_len++] = vec3(max.getX(), min.getY(), min.getZ());
			verts[vert_len++] = vec3(max.getX(), min.getY(), max.getZ());


			verts[vert_len++] = vec3(min.getX(), max.getY(), min.getZ());
			verts[vert_len++] = vec3(max.getX(), max.getY(), min.getZ());

			verts[vert_len++] = vec3(min.getX(), max.getY(), min.getZ());
			verts[vert_len++] = vec3(min.getX(), max.getY(), max.getZ());


			verts[vert_len++] = vec3(min.getX(), min.getY(), max.getZ());
			verts[vert_len++] = vec3(max.getX(), min.getY(), max.getZ());

			verts[vert_len++] = vec3(min.getX(), min.getY(), max.getZ());
			verts[vert_len++] = vec3(min.getX(), max.getY(), max.getZ());


			render::setLine(0);

			GLuint vertexbuffer = render::getBuffer(verts, vert_len*3);
			render::setTrans(transform);
			render::setColor(creDel?vec3(1, 0, 0):vec3(0, 1, 0));

			render::setAttrPoint(0, 3, vertexbuffer);
			render::draw(vert_len);
			render::delBuffer(&vertexbuffer);

			if(grid){
				vector<vec3> vertsGrid;

				vec3 CamPos = get_current_view()->get_position();
				bool xB = fpos.getX()<fpos2.getX();
				bool yB = fpos.getY()<fpos2.getY();
				bool zB = fpos.getZ()<fpos2.getZ();

				int32_t i;
				int iter = 0;
				for(i=fpos.getX()+(xB?gridad:-gridad+1);xB?(i<=fpos2.getX()):(i>fpos2.getX());i+=(xB?gridad:-gridad)){
					iter++;
					if(iter > 500)break;
					float x = i/5.f;
					if(CamPos.getY()<min.getY()){
						vertsGrid.push_back(vec3(x, min.getY(), min.getZ()));
						vertsGrid.push_back(vec3(x, min.getY(), max.getZ()));
					}

					if(CamPos.getZ()>max.getZ()){
						vertsGrid.push_back(vec3(x, min.getY(), max.getZ()));
						vertsGrid.push_back(vec3(x, max.getY(), max.getZ()));
					}

					if(CamPos.getY()>max.getY()){
						vertsGrid.push_back(vec3(x, max.getY(), max.getZ()));
						vertsGrid.push_back(vec3(x, max.getY(), min.getZ()));
					}

					if(CamPos.getZ()<min.getZ()){
						vertsGrid.push_back(vec3(x, max.getY(), min.getZ()));
						vertsGrid.push_back(vec3(x, min.getY(), min.getZ()));
					}
				}

				iter = 0;
				for(i=fpos.getY()+(yB?gridad:-gridad+1);yB?(i<=fpos2.getY()):(i>fpos2.getY());i+=(yB?gridad:-gridad)){
					iter++;
					if(iter > 500)break;
					float y = i/5.f;
					if(CamPos.getX()<min.getX()){
						vertsGrid.push_back(vec3(min.getX(), y, min.getZ()));
						vertsGrid.push_back(vec3(min.getX(), y, max.getZ()));
					}

					if(CamPos.getZ()>max.getZ()){
						vertsGrid.push_back(vec3(min.getX(), y, max.getZ()));
						vertsGrid.push_back(vec3(max.getX(), y, max.getZ()));
					}

					if(CamPos.getX()>max.getX()){
						vertsGrid.push_back(vec3(max.getX(), y, max.getZ()));
						vertsGrid.push_back(vec3(max.getX(), y, min.getZ()));
					}

					if(CamPos.getZ()<min.getZ()){
						vertsGrid.push_back(vec3(max.getX(), y, min.getZ()));
						vertsGrid.push_back(vec3(min.getX(), y, min.getZ()));
					}
				}

				iter = 0;
				for(i=fpos.getZ()+(zB?gridad:-gridad+1);zB?(i<=fpos2.getZ()):(i>fpos2.getZ());i+=(zB?gridad:-gridad)){
					iter++;
					if(iter > 500)break;
					float z = i/5.f;
					if(CamPos.getX()<min.getX()){
						vertsGrid.push_back(vec3(min.getX(), min.getY(), z));
						vertsGrid.push_back(vec3(min.getX(), max.getY(), z));
					}

					if(CamPos.getY()>max.getY()){
						vertsGrid.push_back(vec3(min.getX(), max.getY(), z));
						vertsGrid.push_back(vec3(max.getX(), max.getY(), z));
					}

					if(CamPos.getX()>max.getX()){
						vertsGrid.push_back(vec3(max.getX(), max.getY(), z));
						vertsGrid.push_back(vec3(max.getX(), min.getY(), z));
					}

					if(CamPos.getY()<min.getY()){
						vertsGrid.push_back(vec3(max.getX(), min.getY(), z));
						vertsGrid.push_back(vec3(min.getX(), min.getY(), z));
					}
				}

				vertexbuffer = render::getBuffer(vertsGrid);

				render::setColor(vec3(0.7));
				render::setAttrPoint(0, 3, vertexbuffer);
				render::draw(vertsGrid.size());
				render::delBuffer(&vertexbuffer);
			}
		}
		Object::render();
	}
}
