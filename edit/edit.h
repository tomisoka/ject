#pragma once
#include "../simplefc/input.h"
#include "../simplefc/trigger.h"
#include "../Object.h"
#include "extensions.h"
#include "../GUI/Textin.h"
#include "../GUI/SideBar.h"

class Cube;
class EditHistory;
class RenderText;
class EditExtension;

class btDiscreteDynamicsWorld;
extern btDiscreteDynamicsWorld* editWorld;

void resetEdit(bool acting);

class Edit: public InputCallbacks, public Object, public Trigger, public TextinCallBack{
public:
	Edit(bool active = 0);
	~Edit();

	void reset();

	void call(int state) override;
	void update() override;
	void render() override;

	void setCalling(TextinCallBack *calling, int32_t code);
protected:
	bool input_key(bool acted, int key, int action, int mods) override;
	bool input_mouse(bool acted, int button, int action, int mods) override;

	void textin_callback(size_t issueID, string input) override;

	void addRightInput(vec2 pos, const char *name, TextinCallBack *output, int32_t outCode);

	void addLeftObjs();

	void setLocked(bool locked);

	void resetEObj(bool act);

	void deleteAddObj();
	void placeAddObj();

	vector<unique_ptr<extension>> extensions;


	float leftOffset;
	uint32_t actLeft;

	TextinCallBack *calling;
	int32_t callingCode;


	Object *rButs;
	vector <Textin *> inputs;
	Object *LBar;
	Object *RBar;

	SideBar *LSBar, *RSBar;
	float Loffset, Roffset;

	Object *lButs, *lObj;
	float lButtOffset;

	Object *rObj;
	float rObjOffset;




	vec3i fpos, fpos2;
	vec3 Fpos;
	bool bpos;

	bool creDel; //0=CREATE; 1=DELETE
	bool grid;
	bool glass;
	int gridad;

	uint8_t axisLock; //0=NONE; 1=X; 2=Y; 3=Z; >=4 -> IGNORE_MOUSE;

	Object *addObj;
	EditExtension *addExt;
	float addAngle; //degrees
	bool locked;
	Object *addObjV;
	vector<Cube> cubesV;
	void deleteAddObjV();

	bool toReset;

	bool ignoreGhosts;

	EditHistory *editHistory;

	vector<string> choose;
	int chooseID;

	vec3 oldPos;
	float oldHA;
	float oldVA;
};

extern Edit *edit;
