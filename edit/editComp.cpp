#include "editComp.h"
#include "../predoneFcs.h"
#include "../comps/Render3D.h"
#include "../GUI/RenderLines2D.h"
#include "../GUI/RenderText.h"
#include "../GUI/MouseRect.h"
#include "editExtension.h"
#include "../components.h"
#include "../gameData.h"
#include "../simplefc/basicFcs.h"
#include "../simplefc/TemplateBuffers.h"
#include "../GUI/Button.h"

void addButton(Object *parent, TriggerData *call, vec2 pos, uint_t verts, uint32_t vert_sz, vec3 color, bool upsideDown = 0){
	vec2 sz = vec2(0.015f, 0.0225f);
	if(upsideDown)sz[1] *= -1;
	parent->addComponent(new Button(pos, sz, new RenderLines2D(verts, 12, color, pos, quat(), sz), {call}, [](){return true;}, 0));
}

void addButtons(int act, int max, Object *parent, Trigger *call, float height, int32_t& callCode, vec3 color){
	if(act)addButton(parent, new TriggerData(call, callCode), vec2(0.03, -0.025-height), template_buffer_editcomp_moveup, 12, color);
	callCode++;

	if(act<max-1)addButton(parent, new TriggerData(call, callCode), vec2(0.065, -0.025-height), template_buffer_editcomp_moveup, 12, color, 1);
	callCode++;

	if(max-1)addButton(parent, new TriggerData(call, callCode), vec2(0.1, -0.025-height), template_buffer_editcomp_del, 12, color);
	callCode++;
}


string getName(const char* orgText){
	vector<char> name;

	for(size_t i=0;i<strlen(orgText);i++){
		if(!isdigit(orgText[i]) && !isupper(orgText[i])){
			if(orgText[i] == '_')name.push_back(' ');
			else name.push_back(orgText[i]);
		}
	}
	name.push_back(':');
	name[0] = toupper(name[0]);
	name.push_back(0);
	return string(&name[0]);
}

inline void multiple_vars(Object *ret, EditExtension *ext, vector<xml_node>& args, bool& first, float& height, int32_t& callCode, uint32_t num, const char *name){
	uint32_t act = 0;

	for(size_t i = 0; i < args.size();++i){
		if(!strcmp(args[i].name(), name)){
			if(!first){
				ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.5f)));
			}else first = 0;
			ret->addComponent(new RenderText(getName(name), vec2(-0.12f, -height-0.03), 0.04, 0));
			addButtons(act, num, ret, ext, height, callCode, vec3(0.7f));
			height+=0.055f;
			ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), ext, callCode))->setText(args[i].text().as_string()));
			height += 0.07f;
			height += 0.01f;
			callCode++;
			act++;
		}
	}
	ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.8f)));
	vec2 pos = vec2(0, -0.025f-height);
	addButton(ret, new TriggerData(ext, callCode), pos, template_buffer_editcomp_plus, 12, vec3(0.7f));
	callCode++;
	height+=0.065f;
}

#define CREATE_BUTTONS(TITLE, VEC){\
	for(size_t i = 0; i < VEC.size();++i){\
		ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f),vec2(0.125f, -height+1/128.f),vec3(i?0.5f:0.8f)));\
		ret->addComponent(new RenderText(TITLE, vec2(-0.12, -height-0.03), 0.04, 0));\
		addButtons(i, VEC.size(), ret, comp, height, callCode, vec3(0.7f));\
		height+=0.055f;\
		ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), comp, callCode))->setText(as_str(VEC[i])));\
		height += 0.07f;\
		height += 0.01f;\
		callCode++;\
	}\
	ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.8f)));\
	pos = vec2(0, -0.025f-height);\
	addButton(ret, new TriggerData(comp, callCode), pos, template_buffer_editcomp_plus, 12, vec3(0.7f));\
	callCode++;\
	height+=0.06f;\
}

Object *getEditPart(EditExtension *ext, vector<xml_node>& args, uint32_t code, float& height){
	Object *ret = new Object();
	static char TEMP[1024];
	vec2 pos;

	int32_t callCode = code*0x00100000;

	height = 0.01f;

	bool first = 1;

	uint32_t callDatas = 0;

	vector<string> xmlNames;
	vector<uint32_t> xmlNums;

	for(size_t i = 0; i < args.size();++i){
		float oldH = height;

		const char *name = args[i].name();

		if(!strcmp(name, "goal"))args[i].set_name("goalX3");
		else if(!strcmp(name, "acc"))args[i].set_name("accX");
		else if(!strcmp(name, "color"))args[i].set_name("colorX3");
		else if(!strcmp(name, "playerOnly"))args[i].set_name("player_only");

		if(!strcmp(name, "callData"))callDatas++;
		else if(strstr(name, "X")){
			bool isNew = 1;
			for(size_t j = 0;j < xmlNames.size();++j){
				if(xmlNames[j] == string(name)){
					xmlNums[j]++;
					isNew = 0;
					break;
				}
			}
			if(isNew){
				xmlNames.push_back(string(name));
				xmlNums.push_back(1);
			}
		}else{
			ret->addComponent(new RenderText(getName(name), vec2(0, -0.0275-height), 0.04));
			height+=0.055f;
			ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), ext, callCode))->setText(args[i].text().as_string()));
			height += 0.07f;
			height += 0.01f;

			callCode++;

			if(!first){
				ret->addComponent(createLine(vec2(-0.125f, -oldH+1/128.f), vec2(0.125f, -oldH+1/128.f), vec3(0.5f)));
			}else first = 0;
		}
	}

	uint32_t actCallDatas = 0;
	if(callDatas)for(xml_node arg: args){
		if(!strcmp(arg.name(), "callData")){
			if(!first){
				ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.5f)));
			}else first = 0;


			ret->addComponent(new RenderText("Call:", vec2(-0.12, -height-0.03), 0.04, 0));

			if(callDatas-1){
				pos = vec2(0.1, -0.025f-height);
				addButton(ret, new TriggerData(ext, callCode), pos, template_buffer_editcomp_del, 12, vec3(0.7f));
			}
			callCode++;

			pos = vec2(0, -0.025f-height);
			addButton(ret, new TriggerData(ext, callCode), pos, template_buffer_editcomp_dot, 12, vec3(0.7f));
			callCode++;

			callCode++;
			height+=0.055f;


			strcpy(TEMP, arg.text().as_string());

			size_t j;
			for(j=0;j<strlen(TEMP);++j){
				if(TEMP[j]==';'){
					TEMP[j]=0;
					j++;
					break;
				}
			}

			ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), 0, callCode, [](string a){return true;}, 0))->setText(TEMP));
			height += 0.07f;

			ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), ext, callCode))->setText(TEMP+j));
			height += 0.07f;
			height += 0.01f;

			callCode++;

			actCallDatas++;
		}
	}
	if(callDatas){
		ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.8f)));
		pos = vec2(0, -0.025f-height);
		addButton(ret, new TriggerData(ext, callCode), pos, template_buffer_editcomp_plus, 12, vec3(0.7f));
		callCode++;
		height+=0.065f;
	}

	for(size_t i = 0;i < xmlNames.size();++i){
		multiple_vars(ret, ext, args, first, height, callCode, xmlNums[i], xmlNames[i].c_str());
	}

	return ret;
}



Object *CompEditObj(EditExtension *ext, float& height){
	Object *ret = new Object();

	height = 0;

	for(size_t i=0;i<ext->argsNum;++i){
		if(i)ret->addComponent(createLine(vec2(-0.125f, -height+1/128.f), vec2(0.125f, -height+1/128.f), vec3(0.25f)));

		float relH;

		Object *editPart = getEditPart(ext, ext->args[i], i, relH);
		editPart->setPos(vec3(0, -height, 0));

		ret->addChild(editPart);

		height += relH;
	}

	return ret;
}

#define COMP_EDIT_BASICS(NAME){\
	height = 0.01f;\
	callCode = 0;\
	pos = vec2(0.1, -0.025f-height);\
	ret->addComponent(new RenderText(NAME, vec2(-0.12, -height-0.03), 0.035, 0));\
	addButton(ret, new TriggerData(comp, callCode), pos, template_buffer_editcomp_del, 12, vec3(0.7f));\
	callCode++;\
	height += 0.065f;\
}

#define ADD_VAR_BUTTON(TEXT, VAR){\
	ret->addComponent(new RenderText(TEXT, vec2(0, -0.0275-height), 0.04));\
	height+=0.055f;\
	ret->addComponent((new Textin(vec2(0, -0.03f-height), vec2(0.12f, 0.03f), comp, callCode))->setText(\
		as_str(VAR)\
	));\
	height += 0.07f;\
	callCode++;\
	height += 0.01f;\
}

#define CREATE_BUTTONS_POS(TEXT, POSES){\
	if(rel_abs){\
		vector<vec3> pss;\
		for(vec3 g: POSES)pss.push_back(g + comp->getParent()->getPos());\
		CREATE_BUTTONS(TEXT, pss);\
	}else{\
		CREATE_BUTTONS(TEXT, POSES);\
	}\
}
#define CREATE_BUTTONS_ROT(TEXT, ROTS){\
	vector<quat> copy;\
	for (quat g: ROTS){\
		if(rel_abs)copy.push_back(comp->getParent()->getRot()*g);\
		else copy.push_back(g);\
	}\
	if (quat_euler){\
		vector<vec3> gs;\
		for(quat g: copy)gs.push_back(g.getEuler()*180/M_PI);\
		CREATE_BUTTONS(TEXT, gs);\
	}else{\
		CREATE_BUTTONS(TEXT, copy);\
	}\
}

int32_t callCode;
vec2 pos;
Object *CompEditObj(MoveComponent *comp, float& height){
	Object *ret = new Object();
	COMP_EDIT_BASICS("Mover:");
	ADD_VAR_BUTTON("Speed:", comp->getSpeed());
	CREATE_BUTTONS_POS("Goal:", comp->getGoals());
	return ret;
}

Object *CompEditObj(RotComponent *comp, float& height){
	Object *ret = new Object();
	COMP_EDIT_BASICS("Rotor:");
	ADD_VAR_BUTTON("Speed:", comp->getSpeed());
	CREATE_BUTTONS_ROT("Goal:", comp->getGoals());
	return ret;
}

Object *CompEditObj(TrackMover *comp, float& height){
	Object *ret = new Object();
	COMP_EDIT_BASICS("TrackMover:");
	ADD_VAR_BUTTON("Loopable:", comp->getLoops());
	CREATE_BUTTONS("Speed:", comp->getSpeeds());
	CREATE_BUTTONS_POS("GOAL:", comp->getGoals());
	return ret;
}

Object *CompEditObj(TrackRotor *comp, float& height){
	Object *ret = new Object();
	COMP_EDIT_BASICS("TrackRotor:");
	ADD_VAR_BUTTON("Loopable:", comp->getLoops());
	CREATE_BUTTONS("Speed:", comp->getSpeeds());
	CREATE_BUTTONS_ROT("Goal:", comp->getGoals());
	return ret;
}

Object *CompEditObj(Force *comp, float& height){
	Object *ret = new Object();
	COMP_EDIT_BASICS("Force:");
	ADD_VAR_BUTTON("Direction:", comp->getDir());
	ADD_VAR_BUTTON("Acceleration:", comp->getAcc());
	ADD_VAR_BUTTON("Impulse:", comp->getImpulse());
	return ret;
}




Object *CompEditObj(ObjectComponent *comp, float& height){
	height = 0;

	switch(comp->getID()){
		case 5: return CompEditObj((MoveComponent *) comp, height);
		case 6: return CompEditObj((RotComponent *) comp, height);
		case 10:return CompEditObj((EditExtension*) comp, height);
		case 21:return CompEditObj((TrackMover*) comp, height);
		case 22:return CompEditObj((TrackRotor*) comp, height);
		case 24:return CompEditObj((Force*) comp, height);
	}
	return 0;
}
