#include "editExtension.h"
#include "edit.h"
#include "../level.h"
#include "../simplefc/basicFcs.h"
#include "../BtInterface/includeBullet.h"

EditExtension::EditExtension(string extF):file(extF), settingCalling(0){
	xml_document doc;
	if(!doc.load_file(file.c_str())){
		printLogErr("Error when loading world file extension!( " + file + ")");
		exit(1);
	}

	standOff = xmlRFloat(doc, "standOff", 0);
	rotatable = xmlRBool(doc, "rotatable", 1);
	argsNum = xmlRInt(doc, "argsNum", 0);
	xmlRString(doc, "call", canCall);
}

EditExtension::EditExtension(const EditExtension& o): ObjectComponent(o), Trigger(o), file(o.file), canCall(o.canCall), argsNum(o.argsNum), standOff(o.standOff), rotatable(o.rotatable), settingCalling(o.settingCalling){
	for(size_t i=0;i<argsNum;++i){
		args.push_back(vector<xml_node>());

		for(xml_node child: o.args[i]){
			doc_args.append_copy(child);
			args[i].push_back(doc_args.last_child());
		}
	}
}

EditExtension::EditExtension(xml_node xml, string extF):EditExtension(extF){
	static char CHRTMP[10]="a";
	for(size_t i=0;i<argsNum;++i){
		sprintf(CHRTMP+1, "%ld", i);
		xml_node arg = xml.child(CHRTMP);

		args.push_back(vector<xml_node>());

		if(arg){
			for(xml_node child: arg.children()){
				doc_args.append_copy(child);
				args[i].push_back(doc_args.last_child());
			}
		}
	}
}

void EditExtension::initArgs(){
	if(!argsNum)return;

	xml_document doc;
	if(!doc.load_file(file.c_str())){
		printLogErr("Error when loading world file extension!( " + file + ")");
		exit(1);
	}

	xml_node argas = doc.child("args");

	static char CHRTMP[10]="a";
	for(size_t i=0;i<argsNum;++i){
		sprintf(CHRTMP+1, "%ld", i);
		xml_node arg = argas.child(CHRTMP);

		args.push_back(vector<xml_node>());

		if(arg){
			for(xml_node child: arg.children()){
				doc_args.append_copy(child);
				args[i].push_back(doc_args.last_child());
			}
		}
	}
}

void EditExtension::postLoaded(){
	loadObjs();
}
void EditExtension::loadObjs(){
	objs.clear();
	for(Object *obj: parent->getChildren()){
		objs.push_back(obj);
	}
}

void EditExtension::saveXml(xml_node xml){
	xml_node fileN = xml.append_child("file");
	fileN.text().set(file.c_str());

	static char CHRTMP[10]="a";
	for(size_t i=0;i<args.size();++i){
		if(!args[i].size())continue;
		sprintf(CHRTMP+1, "%ld", i);
		xml_node arg = xml.append_child(CHRTMP);

		for(size_t j=0;j<args[i].size();++j){
			xml_node arga = args[i][j];
			arg.append_copy(arga);
		}
	}
}

void EditExtension::updateNames(map<string, string>& ch){
	static char TEMPOut[512];
	auto it = ch.begin();
	while(it != ch.end()){
		for(vector<xml_node>& argsa: args){
			for(xml_node arg: argsa){
				const char *tempCont = arg.text().as_string();
				size_t j;
				for(j=0;j<strlen(tempCont);++j){
					if(tempCont[j]==';')break;
				}

				if(!strncmp(tempCont, it->first.c_str(), j)){
					strcpy(TEMPOut, it->second.c_str());
					strcpy(TEMPOut+strlen(TEMPOut), tempCont+j);

					arg.text().set(TEMPOut);
				}
			}
		}
		it++;
	}
}

void EditExtension::textin_callback(size_t issueID, string input){
	int32_t argC = issueID/0x00100000;
	int32_t code = issueID%0x00100000;
	int32_t actCode = 0;

	int tempInI;
	static char TEMPOut[512];

	bool settedCalling = 0;

	if(settingCalling){
		settedCalling = 1;
		edit->setCalling(0, 0);
		settingCalling = 0;
	}

	uint32_t callDatas = 0;

	vector<string> xmlNames;
	vector<uint32_t> xmlNums;
	#define LIST_OF_VECS_INTS\
		X(1)\
		X(2)\
		X(3)\
		X(4)

	for(size_t i = 0; i < args[argC].size();++i){
		string name = args[argC][i].name();
		if(name == "goal"){ //backwards compatibility
			args[argC][i].set_name("goalX3");
		}else if(name == "acc"){
			args[argC][i].set_name("accX3");
		}else if(name == "color"){//end of backwards compatibility
			args[argC][i].set_name("colorX3");
		}


		if(name == "lefted" || name == "player_only"){
			if(actCode == code){
				if(!strToInt(input.c_str(), tempInI))return;
				if(tempInI!=0 && tempInI!=1)return;
				numStr(TEMPOut, tempInI);
				args[argC][i].text().set(TEMPOut);
			}
			actCode++;
		}else if(name == "file"){
			if(actCode == code){
				args[argC][i].text().set(input.c_str());
			}
			actCode++;
		}else if(name == "size" || name == "scale3"){
			if(actCode == code){
				vec3 tempInV3;
				if(strToVec(input.c_str(), tempInV3))return;
				*vecToStr(TEMPOut, tempInV3, ';') = 0;
				args[argC][i].text().set(TEMPOut);
			}
			actCode++;
		}else if(name == "callData"){
			callDatas++;
		}else if(strstr(name.c_str(), "X")){
			bool isNew = 1;
			for(size_t j = 0;j < xmlNames.size();++j){
				if(xmlNames[j] == string(name)){
					xmlNums[j]++;
					isNew = 0;
					break;
				}
			}
			if(isNew){
				xmlNames.push_back(string(name));
				xmlNums.push_back(1);
			}
		}else{
			if(actCode == code){
				char num = name[strlen(name.c_str())-1];
				if(num < '0' || num > '9')num = '1';
				switch (num){
					#define X(name) case name + '0':{\
						Vector<float, name> in;\
						if(strToVec(input.c_str(), in))return;\
						*vecToStr(TEMPOut, in, ';')=0;\
						break;\
					}
						LIST_OF_VECS_INTS
					#undef X
				}
				args[argC][i].text().set(TEMPOut);
			}
			actCode++;
		}
	}

	if(callDatas){
		uint32_t lastCallData=0;

		for(size_t i = 0; i < args[argC].size();++i){
			xml_node callDataN = args[argC][i];
			if(!strcmp(callDataN.name(), "callData")){
				if(actCode == code){
					callDataN.parent().remove_child(callDataN);
					args[argC].erase(args[argC].begin()+i);
				}else if(actCode+1 == code){
					if(!settedCalling){
						edit->setCalling(this, code+1);
						settingCalling = 1;
						return;
					}
				}else if(actCode+2 == code){
					for(size_t j=0;j<input.size();++j){
						if(input[j] == ';')return;
					}

					const char *tempCont = callDataN.text().as_string();
					size_t cutOrg;
					for(cutOrg=0;cutOrg<strlen(tempCont);++cutOrg){
						if(tempCont[cutOrg]==';')break;
					}

					strcpy(TEMPOut, input.c_str());
					strcpy(TEMPOut+strlen(TEMPOut), tempCont+cutOrg);

					callDataN.text().set(TEMPOut);
				}else if(actCode+3 == code){
					if(!input.size())return;
					for(size_t j=0;j<input.size();++j){
						if((input[j]<'0' || input[j] > '9') && input[j] != ';')return;
						if(j && input[j]==';' && input[j-1]==';')return;
					}

					strcpy(TEMPOut, callDataN.text().as_string());
					strcpy(find(TEMPOut, TEMPOut + strlen(TEMPOut), ';')+1, input.c_str());

					callDataN.text().set(TEMPOut);
				}
				actCode+=4;
				lastCallData = i;
			}
		}
		if(actCode==code){
			xml_node callDataN = doc_args.append_child("callData");
			sprintf(TEMPOut, args[argC][lastCallData].text().as_string());
			char *tempOut = TEMPOut;
			while(*tempOut != ';' && *tempOut != 0)tempOut++;
			callDataN.text().set(tempOut);
			args[argC].insert(args[argC].begin()+lastCallData+1, callDataN);
		}
		actCode++;
	}

	bool toSwap = 0;
	for(size_t j = 0;j < xmlNames.size(); ++j){
		uint32_t last = 0;
		for(size_t i = 0; i < args[argC].size();++i){
			xml_node colN = args[argC][i];
			if(!strcmp(colN.name(), xmlNames[j].c_str())){
				if(actCode == code || toSwap){
					args[argC][i] = args[argC][last];
					args[argC][last] = colN;
					colN = args[argC][i];
					toSwap = 0;
				}else if(actCode+1 == code){
					toSwap = 1;
				}else if(actCode+2 == code){
					colN.parent().remove_child(colN);
					args[argC].erase(args[argC].begin()+i);
				}else if(actCode+3 == code){
					char num = xmlNames[j].c_str()[strlen(xmlNames[j].c_str())-1];
					if(num < '0' || num > '9')num = '1';
					switch (num){
						#define X(name) case name + '0':{\
							Vector<float, name> in;\
							if(strToVec(input.c_str(), in))return;\
							*vecToStr(TEMPOut, in, ';')=0;\
							break;\
						}
							LIST_OF_VECS_INTS
						#undef X
					}
					colN.text().set(TEMPOut);
				}
				actCode+=4;
				last = i;
			}
		}
		if(actCode==code){
			xml_node colN = doc_args.append_child(xmlNames[j].c_str());
			colN.text().set(args[argC][last].text().as_string());
			args[argC].insert(args[argC].begin()+last+1, colN);
		}
		actCode++;
	}

	this->reset();
	edit->call(0x7FFF0000);
}


void EditExtension::reset(){
	for(Object *obj: objs)obj->delMe();
	objs.clear();

	if(!triggPref.size()){
		triggPref.push_back(parent->name);
		actLevel->loadLevel(parent, parent, nullBtTransform, this);
		triggPref.pop_back();
	}else{
		actLevel->loadLevel(parent, parent, nullBtTransform, this);
	}
	loadObjs();
}
