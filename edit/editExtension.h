#pragma once

#include "../simplefc/trigger.h"
#include "../ObjectComponent.h"
#include "../GUI/Textin.h"
#include "../simplefc/xml.h"

class EditExtension:public ObjectComponent, public TextinCallBack, public Trigger{
public:
	EditExtension(string extF);
	EditExtension(const EditExtension& o);
	EditExtension(xml_node xml, string extF);
	void initArgs();

	void saveXml(xml_node xml) override;

	void postLoaded() override;

	void call(int state) override {textin_callback(state, "");}

	void reset();

	string file;
	vector<Object *> objs;
	vector<string> canCall;

	uint32_t getID() override {return 10;}
	string getIDName() override {return "Extension";}

	void updateNames(map<string, string>& ch);
	void updateTriggerNames() override {updateTriggerName(name);}

	xml_document doc_args;
	vector<vector<xml_node>> args;

	uint32_t argsNum;
	float standOff;
	bool rotatable;
protected:
	void textin_callback(size_t issueID, string input) override;

	bool settingCalling;

	void loadObjs();
};
