#include "editHistory.h"
#include "../cubecompound.h"

EditHistory::EditHistory(): actHist(0){
}

void EditHistory::addHistory(EditHistoryChunk *historyCh, bool doit){
	while(actHist!=(int)history.size()){
		delete history.back();
		history.pop_back();
	}
	history.push_back(historyCh);
	actHist++;
	if(doit)history.back()->doit();
}

void EditHistory::undo(){
	if(actHist == 0)return;
	else actHist--;
	WorldCubeCompound->restoreCubes();

	for(int i = 0; i < actHist;++i){
		history[i]->doit();
	}
}

void EditHistory::redo(){
	if(actHist >= (int) history.size())return;
	else actHist++;

	history[actHist-1]->doit();
}
