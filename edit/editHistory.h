#pragma once
#include <vector>

struct EditHistoryChunk{
	virtual ~EditHistoryChunk(){}
	virtual void doit(){}
};

class EditHistory{
public:
	EditHistory();

	void undo();
	void redo();

	void addHistory(EditHistoryChunk *historyCh, bool doit = 1);
private:
	std::vector<EditHistoryChunk *> history;
	int actHist;
};
