#include "editHistoryChunks.h"
#include "../cubecompound.h"

EHistChCubeCompound::EHistChCubeCompound(Cube cube, bool addRem){
	if(addRem)remCubes.push_back(cube);
	else addCubes.push_back(cube);
}

EHistChCubeCompound::~EHistChCubeCompound(){}

void EHistChCubeCompound::doit(){
	for(Cube& cube: remCubes)WorldCubeCompound->remCube(cube);
	for(Cube& cube: addCubes)WorldCubeCompound->addNewCube(cube);
}
