#pragma once
#include "editHistory.h"
#include <vector>
using std::vector;

class Cube;

struct EHistChCubeCompound: public EditHistoryChunk{
public:
	EHistChCubeCompound(){}
	EHistChCubeCompound(Cube cube, bool addRem); //0 -> add; 1 -> remove
	~EHistChCubeCompound();

	virtual void doit();

	void addRemCube(Cube& cube){remCubes.push_back(cube);}
	void addAddCube(Cube& cube){addCubes.push_back(cube);}

private:
	vector<Cube> remCubes;
	vector<Cube> addCubes;
};
