#include "extensions.h"
#include <dirent.h>
#include "../simplefc/xml.h"
#include "../simplefc/log.h"

int cmpExtensionsAlphaB(const void *a, const void *b){
	extension *A = *(extension**)a;
	extension *B = *(extension**)b;

	return A->name.compare(B->name);
}

void getExtensions(string dir, vector<unique_ptr<extension>>& output){
	DIR *extDir;
	struct dirent *extFile;
	extDir = opendir(dir.c_str());
	if(extDir){
		printLog("Opening extension folder");
		while ((extFile = readdir(extDir)) != 0){
			printLog((string("Ext to load: ") + extFile->d_name).c_str());
			xml_document doc;
			if(doc.load_file((dir + string(extFile->d_name)).c_str())){
				xml_node nameN = doc.child("name");
				if(nameN){
					output.push_back(make_unique<extension>(extension(string(nameN.text().as_string()), dir + string(extFile->d_name))));
				}
			}
		}
		printLog("Closing extension folder");
		closedir(extDir);
	}
	qsort(&output[0], output.size(), sizeof(extension*), cmpExtensionsAlphaB);
}
