#pragma once
#include <vector>
#include <string>
#include <memory>

using namespace std;

struct extension{
	string name;
	string file;

	extension(string name, string file):name(name), file(file){}
};

int cmpExtensionsAlphaB(const void *a, const void *b);
void getExtensions(string dir, vector<unique_ptr<extension>>& output);
