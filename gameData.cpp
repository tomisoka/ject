#include "gameData.h"
#include "simplefc/xml.h"
#include "simplefc/render.h"
#include "Controls.h"

string level_to_load = "levels/default";

bool soundsOn = 1;

bool editing=0;
bool showGhosts = 0;
bool statical = 0;
bool paused = 0;
bool quat_euler = 0;
bool rel_abs = 0;

bool toEnd=0;

void setupData(){
	xml_document doc;
	if(!doc.load_file(".config"))return;

	soundsOn = xmlRBool(doc, "soundsOn", 1);
	render::invertCol = xmlRBool(doc, "invertCol", 0);
	fullscreen = xmlRBool(doc, "fullscreen", 0);
	level_to_load = xmlRString(doc, "level_to_load", level_to_load);

	xml_node controlsN = doc.child("controls");
	CTRL::load(controlsN);
}

void cleanupData(){
	xml_document doc;

	xmlWBool(doc, "soundsOn", soundsOn);
	xmlWBool(doc, "invertCol", render::invertCol);
	xmlWBool(doc, "fullscreen", fullscreen);
	xmlWString(doc, "level_to_load", level_to_load);

	xml_node controlsN = doc.append_child("controls");
	CTRL::save(controlsN);

	doc.save_file(".config");
}
