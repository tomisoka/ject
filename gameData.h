#pragma once
#include "simplefc/std.h"

extern bool invertCol;
extern bool fullscreen;


extern string level_to_load;



extern bool soundsOn;

extern bool editing;
extern bool showGhosts;
extern bool statical;
extern bool paused;
extern bool quat_euler;
extern bool rel_abs;

extern bool toEnd;

void setupData();
void cleanupData();
