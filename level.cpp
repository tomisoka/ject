#include "level.h"
#include "controls.h"
#include "edit/editExtension.h"
#include "particles.h"
#include "components.h"
#include "BtInterface/convertVM.h"
#include "simplefc/log.h"
#include "simplefc/xml.h"
#include "BtInterface/Shape.h"
#include "BtInterface/BodyInterface.h"

vec3 startingPos = vec3(0);
float startingHorA = 0;

map<const btCollisionObject*, Object *> mvableObjs;

ObjectComponent *loadComponent(Level *level, xml_node component, Object *parent, Object *act, btTransform old){
	ObjectComponent* comp=0;
	const char * compType = component.attribute("Type").as_string();

	if(!strcmp(compType, "Render3D"))comp = new Render3D(component);
	else if(!strcmp(compType, "Render3DLine"))comp = new Render3DLines(component);
	else if(!strcmp(compType, "LevelLoader"))comp = new LevelLoader(component);
	else if(!strcmp(compType, "LaserEmit"))comp = new LaserEmit(component);
	else if(!strcmp(compType, "Audio"))comp = new AudioComponent(component);
	else if(!strcmp(compType, "Mover"))comp = new MoveComponent(component);
	else if(!strcmp(compType, "Rotator"))comp = new RotComponent(component);
	else if(!strcmp(compType, "CubeCompound"))comp = new CubeCompound(component);
	else if(!strcmp(compType, "Repeller"))comp = new Repeller(component);
	else if(!strcmp(compType, "Screen"))comp = new Screen(component);
	else if(!strcmp(compType, "Coll"))comp = new Coll(component);
	else if(!strcmp(compType, "TriggerConductor"))comp = new TriggerConductor(component);
	else if(!strcmp(compType, "Magnet"))comp = new Magnet(component);
	else if(!strcmp(compType, "TrackMover"))comp = new TrackMover(component);
	else if(!strcmp(compType, "TrackRotor"))comp = new TrackRotor(component);
	else if(!strcmp(compType, "Force"))comp = new Force(component);
	//back compatibility
	else if(!strcmp(compType, "MeshRenderer"))comp = new Render3D(component);
	else if(!strcmp(compType, "LineRenderer"))comp = new Render3DLines(component);
	//end of back compatibility
	else if(!strcmp(compType, "Extension")){
		xml_node fileN = component.child("file");
		if(fileN){
			const char *file = fileN.text().as_string();
			EditExtension *ext = new EditExtension(component, string(file));
			level->loadLevel(parent, act, old, ext);
			comp = ext;
		}
	}else printLogErr("Invalid component type! (" + string(compType) + ")");

	comp->name = xmlARString(component, "Name", "");
	return comp;
}

void Level::loadLevel(Object *parent, btTransform old, xml_node objects, bool xml, EditExtension *ext){
	for(xml_node object: objects.children("object")){
		xml_attribute objName = object.attribute("Name");

		Object *newObj = new Object();
		if(edit)newObj->setStartEditMode(1);

		if(objName){
			if(xml){
				if(objsM.find(string(objName.as_string()))!=objsM.end()){
					printLogErr(string("multiple declaration of: ") + objName.as_string());
				}
				objsM[string(objName.as_string())]=newObj;
			}
			newObj->name = string(objName.as_string());
		}

		vec3 pos = xmlRVec3(object, "pos", vec3(0));
		quat rot = xmlRRot(object, "rot", quat());


		btTransform trans = old * btTransform(btQuat(rot), btVec3(pos));

		newObj->setPos(pos);
		newObj->setRot(rot);


		parent->addChild(newObj);

		for(xml_node component: object.children("objectComponent")){
			triggPref.push_back(newObj->name);
			ObjectComponent * comp = loadComponent(this, component, parent, newObj, trans);
			if(edit)comp->setStartEditMode(1);
			newObj->addComponent(comp);
			triggPref.pop_back();
		}

		xml_node collision = object.child("collision");
		if(collision){
			xml_node shape = collision.child("collShape");
			btCollisionShape *sh = loadShape(shape);


			float bounce = xmlRFloat(collision, "bounce", DEFAULT_BOUNCE);
			float friction = xmlRFloat(collision, "friction", DEFAULT_FRICTION);
			float rollingFriction = xmlRFloat(collision, "rollingFriction", DEFAULT_ROLLING_FRICTION);
			float mass = xmlRFloat(collision, "mass", DEFAULT_MASS);
			bool staticHold = xmlRBool(collision, "staticHold", 0);
			bool magnetic = xmlRBool(collision, "magnetic", 0);

			//"fixes" 1. collision with cubecompound problem
			btVector3 pos = trans.getOrigin() + ((mass==0 || edit)?btVector3(0,0,0):btVector3(0,0.01,0));

			TriggerData* call_push = 0;
			xml_node callN = collision.child("callData");
			if(callN)call_push = new TriggerData(callN,
				accumulate(triggPref.begin(),triggPref.end(),string(""),[](string s,string a){return s+a+"/";}));

			newObj->setBtBody(new BodyInterface(sh,newObj,trans.getRotation(),pos,mass,bounce,friction,rollingFriction, staticHold, magnetic, call_push));

			xml_node laserD = collision.child("laserData");
			if(laserD){
				bool reflect = xmlRBool(laserD, "reflect", 0);

				xml_node calls = laserD.child("callData");

				LaserCollect *collect=0;

				xml_node decay = laserD.child("decay");

				if(decay){
					float decayT = decay.text().as_float();
					xml_node RendLas = laserD.child("rend");
					TriggerData *triggD = 0;
					if(RendLas){
						triggD = new TriggerData();
						triggD->nameCall=accumulate(triggPref.begin(),triggPref.end(),string(""),[](string s,string a){return s+a+"/";}) + RendLas.text().as_string();
					}

					collect = new DecayComp(uint32_t(decayT*TPS), triggD);
				}else{
					collect = new LaserCollect(reflect);
				}
				newObj->addComponent(collect);

				if(calls)collect->init(laserD);
			}
		}

		xml_node child = object.child("child");
		if(child)loadLevel(newObj, trans, child, 0);

		if(loaded)newObj->postLoaded();
	}
}

void extToXml(EditExtension *ext, xml_node node){
	static char Temp[32];
	for(xml_node child: node.children()){
		if(string(child.name())==string("arg")){
			int extInd = child.text().as_int();

			if((int)ext->args.size()>extInd)
			for(size_t i=0;i<ext->args[extInd].size();++i){
				node.append_copy(ext->args[extInd][i]);

				xml_node last = node.last_child();

				xml_attribute args = last.attribute("args");

				size_t argsI = 0;
				if(args){
					argsI = args.as_int();
				}else{
					args = last.append_attribute("args");
				}

				sprintf(Temp, "%ld", argsI+1);

				args.set_value(Temp);
			}
			node.remove_child(child);
		}else{
			extToXml(ext, child);
		}
	}
}

void Level::loadLevel(Object *parent, Object *act, btTransform old, EditExtension *ext){
	xml_document doc;
	if(!doc.load_file(ext->file.c_str())){
		printLogErr("Error when loading world file extension!(" + ext->file + ")");
		exit(1);
	}
	if(ext && ext->args.size())extToXml(ext, doc);

	xml_node objects = doc.child("objects");
	loadLevel(act, old, objects, 0, ext);
}

Level::Level(string xmlFile, bool edit, vec3 oldPos, float oldVertRot){
	setup(xmlFile, edit, oldPos, oldVertRot);
}

void Level::setup(string xmlFile, bool edit, vec3 oldPos, float oldVertRot){
	printLog("Level (" + xmlFile + ") is being loaded");
	WorldCubeCompound = 0;
	this->xmlFile = xmlFile;
	loadedThis = 1;
	this->edit=edit;
	parent = 0;
	mvableObjs.clear();
	clearParticles();


	xml_document doc;
	if(!doc.load_file(xmlFile.c_str())){
		if(!doc.load_file(DEFAULT_LEVEL)){
			printLogErr("Invalid world file!(" + xmlFile + ")");
			exit(1);
		}
	}


	startingPos = xmlRVec3(doc, "startPos", vec3(0));
	startingHorA = xmlRFloat(doc, "startRot", 0)*M_PI/180;


	float angleDiff = startingHorA + oldVertRot;


	if(!edit)horizontalAngle -= angleDiff;

	xml_node objects = doc.child("objects");
	this->loadLevel(this, nullBtTransform, objects, edit, 0);


	btRigidBody *playerB = CameraObj->getBtBody();
	btTransform trans = playerB->getWorldTransform();

	vec3 diffPos = CameraObj->getPos() - oldPos;
	mat4 matRot;
	matRot.initRotationEuler(0, angleDiff, 0);
	diffPos = matRot * diffPos;


	vec3 linVel = Vec3(playerB->getLinearVelocity());
	linVel = matRot * linVel;
	playerB->setLinearVelocity(btVec3(linVel));


	btVector3 PPos = btVec3(startingPos + diffPos);
	trans.setOrigin(PPos);
	trans.setRotation(trans.getRotation() * btQuat(quat(vec3(0,startingHorA,0))));
	playerB->setWorldTransform(trans);

	CameraObj->setPos(startingPos);
	CameraObj->setRot(quat(vec3(0,startingHorA,0)));

	playerB->setCollisionFlags(playerB->getCollisionFlags());

	this->updateTriggerNames();
	this->registTriggers();
	this->requestTriggers();
	triggersClear();

	this->postLoaded();

	this->actBtBodyCh(1);
	printLog("Level (" + xmlFile + ") successfully loaded");

	//addComponent(new Render3DSpecial(vec3(0), vec3(0, 1.1, 0), quat()));

	/*Object* portals = new Object();
	this->addChild(portals);
	Portal *prt1 = new Portal(vec3(0,1, 3));
	Portal *prt2 = new Portal(vec3(0,1,-3), quat(vec3(0,M_PI/2,0)));
	portals->addComponent(prt1);
	portals->addComponent(prt2);
	prt1->set_connected(prt2);
	prt2->set_connected(prt1);*/
}

void Level::save(){
	pugi::xml_document doc;

	if(xmlFile.length()==0)return;

	if(!loadedThis){
		std::ifstream src(xmlFile, std::ios::binary);
		if(!src.fail()){
			std::ofstream dst(xmlFile + ".temp", std::ios::binary);
			dst << src.rdbuf();
		}
		loadedThis = 1;
	}

	if(startingPos!=vec3(0))xmlWVec3(doc, "startPos", startingPos);
	if(startingHorA!=0)xmlWFloat(doc, "startRot", startingHorA*180/M_PI);

	xml_node objects = doc.append_child("objects");

	for(Object *child: children){
		xml_node object = objects.append_child("object");

		if(child->name!="")xmlAWString(object, "Name", child->name);

		if(child->getPos()!=vec3(0))xmlWVec3(object, "pos", child->getPos());
		if(child->getRot()!=quat())xmlWRot(object, "rot", child->getRot());

		BodyInterface *body = child->getBtBody();
		if(body){
			xml_node bodyN = object.append_child("collision");

			xml_node collShape = bodyN.append_child("collShape");
			saveShape(body->getCollisionShape(), collShape);

			if(body->getRestitution()!=0)xmlWFloat(bodyN, "bounce", body->getRestitution());
			if(body->getFriction()!=0)xmlWFloat(bodyN, "friction", body->getFriction());
			if(body->getRollingFriction()!=0)xmlWFloat(bodyN, "rollingFriction", body->getRollingFriction());
			if(body->getInvMass()!=0)xmlWFloat(bodyN, "mass", 1/body->getInvMass());
			if(body->is_static_hold()!=0)xmlWBool(bodyN, "staticHold", 1);
			if(body->is_magnetic()!=0)xmlWBool(bodyN, "magnetic", 1);

			map<const btCollisionObject*, LaserCollect *>::iterator it = laserData.find(body);
			if(it!=laserData.end()){
				LaserCollect *dataL = it->second;

				xml_node laserData = bodyN.append_child("laserData");
				if(dataL->getReflect())xmlWBool(laserData, "reflect", dataL->getReflect());

				dataL->saveXmlLas(laserData);
			}
		}

		for(ObjectComponent *comp: child->getComps()){
			xml_node compN = object.append_child("objectComponent");
			xmlAWString(compN, "Type", comp->getIDName());
			if(comp->name!="")xmlAWString(compN, "Name", comp->name);
			comp->saveXml(compN);
		}
	}

	extern bool statical;
	if(statical)doc.save_file(xmlFile.c_str());
}

Level::~Level(){
	if(edit)save();
	if(this == actLevel)actLevel = 0;
}

LevelLoader::LevelLoader(xml_node xml):file(string(xmlRString(xml, "file", ""))){}

void LevelLoader::load(){
	control->resetKill();
	Level *lv = actLevel;
	Object *parent = lv->getParent();
	vec3 realPos = getTransform().getPos();
	actLevel = new Level(file, 0, vec3(realPos), quat(getTransform()).getEuler().getY());
	parent->addChild(actLevel);
	lv->delMe();
	control->reset();
}

void LevelLoader::saveXml(xml_node xml){
	xmlWString(xml, "file", file);
}

Level *actLevel = 0;
Object *levelPar = 0;
LevelLoader *levelLoader=0;
