#pragma once

#include "Object.h"
#include "ObjectComponent.h"
#include "simplefc/trigger.h"
#include "gameData.h"

class EditExtension;

extern vec3 startingPos;
extern float startingHorA;

extern map<const btCollisionObject*, Object *> mvableObjs;

void loadTriggerCallData(TriggerCaller *caller, xml_node data);
void extToXml(EditExtension *ext, xml_node node);

class btTransform;

class Level: public Object{
public:
	Level(string xmlFile, bool edit, vec3 oldPos=vec3(0), float oldVertRot = 0);
	~Level() override;

	void loadLevel(Object *parent, btTransform old, xml_node objects, bool xml, EditExtension *ext = 0);
	void loadLevel(Object *parent, Object *act, btTransform old, EditExtension *ext);

	void update() override{if(!paused)Object::update();}

	void save();

	void setup(string xmlFile, bool edit, vec3 oldPos = vec3(0), float oldVertRot = 0);


	string getXmlFile(){return xmlFile;}
	void setXmlFile(string xmlFile){this->xmlFile = xmlFile; loadedThis = 0;}

	map<string, Object *> objsM;
protected:
	string xmlFile;
	bool loadedThis;

	vec3 startPos;

	bool edit;
};

extern Level *actLevel;
extern Object *levelPar;

class LevelLoader;
extern LevelLoader *levelLoader;

class LevelLoader: public ObjectComponent, public Trigger{
public:
	LevelLoader(string file):file(file){}
	LevelLoader(const LevelLoader& o): ObjectComponent(o), Trigger(o), file(o.file){}
	LevelLoader(xml_node xml);

	void call(int index) override{
		if(file!=""){
			levelLoader=this;
		}
	}

	void load();

	uint32_t getID() override{return 8;}
	string getIDName() override{return "LevelLoader";}

	void saveXml(xml_node xml) override;

	void updateTriggerNames() override{updateTriggerName(name);}
	void registTriggers() override{registTriggerSelf();}
private:
	string file;
};

ObjectComponent *loadComponent(Level *level, xml_node component, Object *parent, Object *act, btTransform old);
