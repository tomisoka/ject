#include "BtInterface/WorldInterface.h"
#include "simplefc/basicFcs.h"
#include "simplefc/render.h"
#include "controls.h"
#include "simplefc/window.h"
#include "Object.h"
#include "level.h"
#include "gameData.h"
#include "edit/edit.h"
#include "RenderController.h"
#include <GLFW/glfw3.h>
#include "GUI/RenderText.h"
#include "setups.h"
#include "MenuController.h"
#include "particles.h"

string TIMER_N[10] = {"Sim", "Update", "Render", "Sleep", "Edit", "Control", "Misc"};
double TIMER_T[10] = {0,0,0,0,0,0,0,0,0,0};
double TIMER[10];

void setTimer(size_t i){
	TIMER[i] = glfwGetTime();
}

void endTimer(size_t i){
	TIMER_T[i] += glfwGetTime() - TIMER[i];
}

void resetTimer(){
	for(size_t i = 0; i < 10; i++)TIMER_T[i] = 0;
}

int main(int argv, char **argc){
	setup();

	Rcontrol = new RenderController();

	printLog("Setting up mainObject");
	Object *mainObject = new Object();

	printLog("Setting up level parent");
	mainObject->addChild(levelPar = new Object(false));
	printLog("Setting up edit");
	levelPar->addChild(edit = new Edit());
	printLog("Setting up menuControl");
	mainObject->addChild(menuControl = new MenuController());


	printLog("Setting up controls");
	control = new controls();

	Object *textObject = new Object();

	char FPSBuff[16] = "FPS: ";
	textObject->addComponent(new RenderText(FPSBuff, vec2(0, 0.97f), 0.04f));

	char posBuff[16] = "POS: ";
	textObject->addComponent(new RenderText(posBuff, vec2(0, 0.93f), 0.04f));

	mainObject->addChild(textObject);

	printLog("Starting game loop");

	try{
		int ticks = 0, renders = 0;
		double second=0, temp, lastTime=glfwGetTime(), notTicked = 0;
		bool ticked = 0;

		do{
			temp=glfwGetTime();
			notTicked += temp - lastTime;
			second += temp - lastTime;
			lastTime = temp;

			while(notTicked > 1.0f/TPS){
				glfwPollEvents();

				if(levelLoader && !paused){
					if(!editing)levelLoader->load();
					levelLoader=0;
				}

				if(levelPar->getActive() && !paused){
					setTimer(5);
					computeMatricesFromInputs();
					control->doit();
					endTimer(5);
					setTimer(4);
					edit->update();
					endTimer(4);
				}

				setTimer(1);
				mainObject->resPosTotal();
				mainObject->update();
				endTimer(1);
				if(levelPar->getActive()){
					if(!paused){
						setTimer(0);
						world_interface->step();
						endTimer(0);

						setTimer(1);
						mainObject->resPosTotal(); //fixes how player looks
						updatePartics();
						endTimer(1);

						setTimer(5);
						resetView();
						endTimer(5);
					}

					posBuff[0]='P';
					*vecToStr(posBuff+5, position, ' ') = 0;
				}else
					posBuff[0]=0;

				notTicked -= TICK_TIME;
				ticked = 1;
				ticks++;
			}
			if(ticked){
				ticked = 0;
				renders++;

				setTimer(2);

				render::clear();
				view = control;

				if(levelPar->getActive())renderPartics();
				Rcontrol->render();
				mainObject->render();

				glfwSwapBuffers(window);
				endTimer(2);
			}else this_thread::sleep_for(100us);

			if(second > 1.0f){
				*(numStr(FPSBuff+5, renders))=0;
				cout << "--------------------" << endl;
				for(size_t i = 0; i < 7; i++)cout << TIMER_N[i] << " : " << TIMER_T[i] << endl;
				cout << "All: " << accumulate(TIMER_T, TIMER_T + 7, 0.) << endl;
				resetTimer();
				printf("Ticks : %d, FPS : %d\n", ticks, renders);
				renders = 0;
				ticks = 0;
				second-=1.0f;
			}
		}while(!toEnd && glfwWindowShouldClose(window) == 0);
	}catch(const exception& e){
		printLogErr(string("Catched in main: ") + e.what());
	}

	delete control;
	delete mainObject;
	delete Rcontrol;

	cleanup();

	return 0;
}
