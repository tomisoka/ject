#include "menu.h"
#include "GUI/RenderText.h"
#include "predoneFcs.h"
#include "GUI/OnOff.h"
#include "GUI/Dragger.h"
#include "GUI/Button.h"
#include "Controls.h"
#include "GUI/InputCharToVar.h"
#include "GUI/SideBar.h"
#include "simplefc/TriggerFunction.h"

Menu::Menu(Trigger *parent, size_t ID, bool active): Object(active), size(1), pos(1), ID(ID), parent(parent){
	addChild(child_object = new Object());
	addChild((new Object())
		->addComponent(side_bar=new SideBar(vec2(1-0.015625,-1), vec2(1),1,0.5f,&pos,2,0,vec3(0.7),vec3(0.8))));
}

void Menu::addButton(vec2 pos, string name, int butID, vec2 sz){
	child_object->addComponent(new Button(pos, sz, name, {new TriggerData(this, butID)}));
	let sz_ = 1-pos.getY() + sz.getY() + 0.05;
	if(sz_ > size)size = sz_;
}

void Menu::line_with_name(vec2 pos, const char *name, float sz){
	let size = vec2(0.8f, 0.05f);
	let txt_pos = pos + vec2(0, 0.02) - size;
	createBlankRect(child_object, pos, size);
	child_object->addComponent(new RenderText(name, txt_pos, sz, 0));
}

void Menu::addOnOff(vec2 pos, const char *name, bool *data){
	Object *onOffObj = new Object(pos);
	child_object->addChild(onOffObj);

	line_with_name(pos, name);

	vec2 posB = vec2(0.77, 0);
	vec2 sizeB = vec2(0.03, 0.05);
	onOffObj->addComponent(new OnOff(posB, sizeB, data));
	let sz = 1-pos.getY() + 0.15;
	if(sz > size)size = sz;
}

void Menu::addDragger(vec2 pos, const char *name, float *data, float min, float max){
	Object *DraggerObj = new Object(pos);
	child_object->addChild(DraggerObj);

	line_with_name(pos, name);

	DraggerObj->addComponent(new Dragger(vec2(0.4f, 0), vec2(0.4f, 0.05f), data, min, max));
	DraggerObj->addComponent(createLine(vec2(0,-0.05f), vec2(0, 0.05f), vec3(0.5)));
	let sz = 1-pos.getY() + 0.15;
	if(sz > size)size = sz;
}


//good luck future me!
void Menu::addControlsList(vec2 pos){
	size_t next_category = 0;
	for(size_t i = 0; i < CTRL::LEN; ++i){
		if(next_category < CTRL::CATEGORIES_LEN && CTRL::categories_starts[next_category] == i){
			vec2 ps = pos - vec2(0, (i + next_category)*0.1);
			child_object->addComponent(new RenderText(CTRL::name_categories[next_category], ps, 0.08));
			next_category++;
		}
		vec2 ps = pos - vec2(0, (i + next_category)*0.1);
		line_with_name(ps, CTRL::name_gui[i].c_str(), 0.05);

		InputCharToVar *char_in = new InputCharToVar(CTRL::set + i, [=](){return i+1 == CTRL::changing_ctrls;}
		, {new TriggerData(new TriggerFunction([](int){CTRL::changing_ctrls = 0;}), 0, true)});
		child_object->addComponent(char_in);

		child_object->addComponent(new Button(ps + vec2(0.3f, 0), vec2(0.3f, 0.05f),
			{function<string()>([=](){
				return (CTRL::changing_ctrls == i+1)?"<choose key>":CTRL::get_key_name(CTRL::set[i]);
			})}
		,{new TriggerData(char_in, 0), new TriggerData(new TriggerFunction([=](int){
			CTRL::changing_ctrls = i+1;
		}), 0, true)}));

		child_object->addComponent(new Button(ps + vec2(0.7f, 0), vec2(0.1f, 0.05), "Reset",{
			new TriggerData(new TriggerFunction([=](int){CTRL::changing_ctrls = 0; CTRL::reset_key(i);}), 0, true)
		}, [=](){return CTRL::is_diff_key(i);}));

		let sz = 1-ps.getY() + 0.15;
		if(sz > size)size = sz;
	}
}

void Menu::call(int state){
	parent->call((ID<<8) | state);
}

void Menu::render(){
	if(!active)return;
	if(size > 2)side_bar->setMultH(2./size);
	child_object->setPos(vec3(0, (pos-1)/2.f*(2-size), 0));

	Object::render();
}
