#pragma once
#include "simplefc/trigger.h"
#include "Object.h"

class SideBar;

class Menu: public Object, public Trigger{
public:
	Menu(Trigger *parent, size_t ID, bool active = 0);

	void call(int state) override;

	void render() override;

	void addButton(vec2 pos, string name, int butID, vec2 sz = vec2(0.25, 0.08));
	void addOnOff(vec2 pos, const char *name, bool *data);
	void addDragger(vec2 pos, const char *name, float *data, float min, float max);
	void addControlsList(vec2 pos);
private:
	void line_with_name(vec2 pos, const char *name, float sz = 0.08);

	float size;
	float pos;

	SideBar *side_bar;
	Object *child_object;

	size_t ID;
	Trigger *parent;
};
