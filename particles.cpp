#include "particles.h"
#include "simplefc/render.h"
#include "simplefc/View.h"
#include "simplefc/settings.h"
#include "BtInterface/WorldInterface.h"

double nwParticMult = 1;

Particle *pStart = 0;
Particle *pLast = 0;

uint32_t partics = 0;

void addParticle(Particle *p){
	if(pStart == 0){
		pStart = p;
		pLast = p;
	}else{
		pLast->nextP = p;
		pLast = p;
	}
	partics++;
}

void addParticle(vec3 pos, vec3 mv, vec4 col, float sz, float life, bool grav){
	addParticle(new Particle(pos, mv, col, sz, life, grav));
}

void clearParticles(){
	Particle *p = pStart;
	Particle *oldP;

	while(p){
		oldP = p;
		p = p->nextP;
		delete oldP;
	}


	pStart = 0;
	pLast = 0;
	partics = 0;
}

void updatePartics(){
	Particle *oldP = pStart;
	Particle *p = pStart;
	Particle *next;

	uint32_t actPs = 0;
	bool inside;

	while(p){
		p->life -= TICK_TIME;


		if(p->life > 0){
			inside = world_interface->is_inside(p->pos, 3);

			if(inside){
				goto deleteActParticle;
			}
			//p->pos += p->mv*TICK_TIME;
			p->pos[0] += p->mv[0]*TICK_TIME;
			p->pos[1] += p->mv[1]*TICK_TIME;
			p->pos[2] += p->mv[2]*TICK_TIME;

			if(p->grav)p->mv[1] -= 9.81 * TICK_TIME;

			actPs++;
		}else{
			deleteActParticle:
			next = p->nextP;
			delete p;
			if(p == pStart){
				pStart = next;
				if(!next){
					pLast = 0;
				}
			}else{
				oldP->nextP = next;
				if(!next){
					pLast = oldP;
				}
			}

			p = next;
			partics--;
			continue;
		}



		oldP = p;
		p = p->nextP;
	}
}

float *poses = 0;
float *colrs = 0;
uint32_t actMaxPart = 0;


void renderPartics(){
	Particle *p = pStart;
	if(!p)return;

	if(poses == 0){
		poses = (float *) malloc(sizeof(vec4) * partics);
		colrs = (float *) malloc(sizeof(vec4) * partics);
		actMaxPart = partics;
	}else if(partics > actMaxPart){
		poses = (float *) realloc(poses, sizeof(vec4) * partics);
		colrs = (float *) realloc(colrs, sizeof(vec4) * partics);
		actMaxPart = partics;
	}

	static const float verts[] = {
		-0.5f,-0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
		0.5f,  0.5f, 0.0f,
	};

	uint32_t actPs = 0;
	uint32_t actCl = 0;
	float psDiff;

	View *view = get_current_view();
	while(p){
		psDiff = (view->get_position() - vec3(p->pos)).lengthSq();

		poses[actPs++] = p->pos.getX();
		poses[actPs++] = p->pos.getY();
		poses[actPs++] = p->pos.getZ();
		poses[actPs++] = p->sz*sqrt(sqrt(psDiff));

		colrs[actCl++] = p->col.getX();
		colrs[actCl++] = p->col.getY();
		colrs[actCl++] = p->col.getZ();
		colrs[actCl++] = p->col.getW();

		p = p->nextP;
	}
	//printf("%d\n", actPs/4);

	if(partics){
		render::setParticle(view->get_up(), view->get_right());

		GLuint vertexbuff = render::getBuffer(verts, sizeof(verts)/sizeof(float));
		GLuint posbuff = render::getBuffer(poses, partics * 4);
		GLuint colbuff = render::getBuffer(colrs, partics * 4);

		render::setAttrPoint(0, 3, vertexbuff);
		render::setAttrPoint(1, 4, posbuff);
		render::setAttrPoint(2, 4, colbuff);

		render::setAttrDiv(0, 0);
		render::setAttrDiv(1, 1);
		render::setAttrDiv(2, 1);

		render::drawInstanced(4, partics);

		render::setAttrDiv(1, 0);
		render::setAttrDiv(2, 0);

		render::delBuffer(&vertexbuff);
		render::delBuffer(&posbuff);
		render::delBuffer(&colbuff);
	}
}

void startPartics(){
}

void endPartics(){
	free(poses);
	free(colrs);
	poses = 0;
	colrs = 0;
}
