#pragma once

#include "simplefc/mathlib.h"

extern double nwParticMult;

struct Particle{
public:
	vec3 pos;
	vec3 mv;
	vec4 col;
	float sz;
	float life;
	bool grav;

	Particle *nextP;

	Particle():
		pos(vec3(0)), mv(vec3(0)), col(vec4(0)), sz(0), life(0), grav(0), nextP(0){}

	Particle(vec3 pos, vec3 mv, vec4 col, float sz, float life, bool grav = 0):
		pos(pos), mv(mv), col(col), sz(sz), life(life), grav(grav), nextP(0){}

	Particle(vec3 pos, vec3 mv, vec3 col, float sz, float life, bool grav = 0):
		pos(pos), mv(mv), col(vec4(col, 1)), sz(sz), life(life), grav(grav), nextP(0){}
};

void addParticle(Particle *p);
void addParticle(vec3 pos, vec3 mv, vec4 col, float sz, float life, bool grav = 0);
void clearParticles();
void updatePartics();
void renderPartics();

void startPartics();
void endPartics();
