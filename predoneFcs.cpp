#include "predoneFcs.h"
#include "Object.h"
#include "GUI/RenderLines2D.h"
#include "GUI/Render2D.h"
#include "simplefc/TemplateBuffers.h"

ObjectComponent* createLine(vec2 start, vec2 stop, vec3 col){
	return new RenderLines2D(vector<float>{start.getX(), start.getY(), stop.getX(), stop.getY()}, col);
}

ObjectComponent* createEdgeRect(vec2 pos, vec2 size){
	return new RenderLines2D(template_buffer_rectangle_boundaries, 8, vec3(0), pos, quat(), size);
}

ObjectComponent* createInsideRect(vec2 pos, vec2 size){
	return new Render2D(template_buffer_rectangle_background, 0, 6, 0, vec3(1), pos, quat(), size);
}

void createBlankRect(Object *parent, vec2 pos, vec2 size){
	parent->addComponent(createEdgeRect(pos, size));
	parent->addComponent(createInsideRect(pos, size));
}
