#pragma once

#include "simplefc/mathlib.h"

class Object;
class ObjectComponent;

ObjectComponent* createLine(vec2 start, vec2 stop, vec3 col = vec3(0));

ObjectComponent* createEdgeRect(vec2 pos, vec2 size);
ObjectComponent* createInsideRect(vec2 pos, vec2 size);

void createBlankRect(Object *parent, vec2 pos, vec2 size);
