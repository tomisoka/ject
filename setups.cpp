#include "setups.h"
#include "simplefc/window.h"
#include "simplefc/Shader.h"
#include "simplefc/input.h"
#include "simplefc/sounds.h"
#include "BtInterface/WorldInterface.h"
#include "BtInterface/Shape.h"
#include "gameData.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "particles.h"
#include "simplefc/textures.h"
#include "simplefc/log.h"
#include "simplefc/render.h"
#include "simplefc/settings.h"
#include "simplefc/Text.h"
#include "simplefc/TemplateBuffers.h"

#include <time.h>

using namespace std;

void setup(){
	srand(time(0));

	setupLog();

	printLog("Loading game data");
	setupData();

	printLog("Setting up window (OPENGL)");
	setupWin();
	int major, minor, rev, texture_units;
	major = glfwGetWindowAttrib(window,GLFW_CONTEXT_VERSION_MAJOR);
	minor = glfwGetWindowAttrib(window,GLFW_CONTEXT_VERSION_MINOR);
	rev = glfwGetWindowAttrib(window,GLFW_CONTEXT_REVISION);
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &texture_units);

	printLog("OpenGL version recieved: " + to_string(major) + "." + to_string(minor) + "." + to_string(rev));
	printLog("Max textures: " + to_string(texture_units));

	setupTemplateBuffers();

	printLog("Loading shaders");
	setupShaders();
	printLog("Setting up input");
	setupInput();
	printLog("Setting up sounds");
	setupSounds();

	printLog("Loading textures");
	initTextures();

	printLog("Loading text");
	text = new Text("fonts/OpenSans.ttf", 16);

	printLog("Setting up Bullet Physics");
	world_interface = new WorldInterface();

	printLog("Setting up particles");
	startPartics();

	render::setup();
}

void cleanup(){
	printLog("Ending particles");
	endPartics();

	printLog("Clearing collision shapes");
	clearBtShapes();

	printLog("Cleanup of Bullet Physics");
	delete world_interface;

	printLog("Cleanup of text");
	delete text;

	printLog("Cleanup of textures");
	cleanupTextures();

	printLog("Cleanup of sounds");
	cleanupSounds();
	printLog("Cleanup of shaders");
	cleanupShaders();

	cleanupTemplateBuffers();

	printLog("Cleanup of window(OPENGL)");
	glfwDestroyWindow(window);
	glfwTerminate();

	printLog("Saving data");
	cleanupData();
}
