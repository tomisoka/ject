#pragma once
#include "std.h"
#include <sstream>

//don't add char or string, they are implemented manually
#define LIST_OF_PRINTABLE_TYPES \
	X(int)\
	X(size_t)\
	X(float)\
	X(double)\
	X(bool)\
	X(vec2)\
	X(vec3)\
	X(vec4)\
	X(vec2i)\
	X(vec3i)\
	X(vec4i)

struct StringPart{
private:
	friend class MutString;
	friend ostream& operator<<(ostream &os, StringPart& p);

	enum Type{Tstring, Tcstring, TFstring, TFcstring, Tmanipulator,
		#define X(type) T##type,\
			TF##type,
			LIST_OF_PRINTABLE_TYPES
		#undef X
	};

	union{
		const string* Vstring;
		const char* Vcstring;
		function<string()>* VFstring;
		function<const char *()>* VFcstring;
		ios_base& (*Vmanipulator)(ios_base&); //hex, dec, oct etc.
		#define X(type) const type* V##type;\
			function<type()>* VF##type;
			LIST_OF_PRINTABLE_TYPES
		#undef X
	};
	Type T;
public:
	StringPart(const StringPart& o): T(o.T){
		switch(T){
			case Tstring:
				Vstring = new string(*o.Vstring);
				break;
			case Tcstring:
				Vcstring = o.Vcstring;
				break;
			case TFstring:
				VFstring = new function<string()>(*o.VFstring);
				break;
			case TFcstring:
				VFcstring = new function<const char*()>(*o.VFcstring);
				break;
			case Tmanipulator:
				Vmanipulator = o.Vmanipulator;
				break;
			#define X(type) case T##type:\
					V##type = o.V##type;\
					break;\
				case TF##type:\
					VF##type = new function<type()>(*o.VF##type);\
					break;
				LIST_OF_PRINTABLE_TYPES
			#undef X
		}
	}

	StringPart(const string& v){
		Vstring = new string(v);
		T = Tstring;
	}

	StringPart(const char *v){
		Vcstring = v;
		T = Tcstring;
	}

	StringPart(function<string()> v){
		VFstring = new function<string()>(v);
		T = TFstring;
	}

	StringPart(function<const char *()> v){
		VFcstring = new function<const char *()>(v);
		T = TFcstring;
	}

	StringPart(ios_base& (*v)(ios_base&)){
		Vmanipulator = v;
		T = Tmanipulator;
	}

	#define X(type) StringPart(const type* v){\
			V##type = v;\
			T = T##type;\
		}\
		StringPart(function<type()> v){\
			VF##type = new function<type()>(v);\
			T = TF##type;\
		}\
		StringPart(type (*v)()){\
			VF##type = new function<type()>(v);\
			T = TF##type;\
		}
		LIST_OF_PRINTABLE_TYPES
	#undef X

	~StringPart(){
		switch(T){
			case Tstring:
				delete Vstring;
				break;
			case TFstring:
				delete VFstring;
				break;
			case TFcstring:
				delete VFcstring;
				break;
			#define X(type) case TF##type:\
					delete VF##type;\
					break;
				LIST_OF_PRINTABLE_TYPES
			#undef X
			default:
				break;
		}
	}
};

inline ostream& operator<<(ostream &os, StringPart& p){
	switch (p.T){
		case StringPart::Tstring:
			os << *p.Vstring;
			break;
		case StringPart::Tcstring:
			os << p.Vcstring;
			break;
		case StringPart::TFstring:
			os << (*p.VFstring)();
			break;
		case StringPart::TFcstring:
			os << (*p.VFcstring)();
			break;
		case StringPart::Tmanipulator:
			os << p.Vmanipulator;
			break;
		#define X(type) case StringPart::T##type:\
				os << *p.V##type;\
				break;\
			case StringPart::TF##type:\
				os << (*p.VF##type)();\
				break;
			LIST_OF_PRINTABLE_TYPES
		#undef X
	}
	return os;
}

class MutString{
public:
	MutString(initializer_list<StringPart> s): parts(s){
		constS  = parts.size() == 1 && parts[0].T == StringPart::Tstring;
		constcS = parts.size() == 1 && parts[0].T == StringPart::Tcstring;
	}

	MutString(const string& s): constS(true ), constcS(false), parts({s}){}
	MutString(const char*   s): constS(false), constcS(true ), parts({s}){}

	operator string(){
		if(constS)return *parts[0].Vstring;
		if(constcS)return parts[0].Vcstring;
		stringbuf buffer;
		ostream os(&buffer);
		for(StringPart& p: parts)os << p;
		return buffer.str();
	}
private:
	bool constS;
	bool constcS;
	vector<StringPart> parts;
};
