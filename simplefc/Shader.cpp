#include "Shader.h"
#include "render.h"
#include <sstream>
#include "View.h"

Shader* texture_3D_shader;
Shader* color_transparent_3D_shader;
Shader* color_3D_shader;
Shader* color_per_face_3D_shader;
Shader* line_3D_shader;
Shader* particle_3D_shader;

Shader* texture_2D_shader;
Shader* color_2D_shader;
Shader* line_2D_shader;
Shader* texture_distance_field_2D_shader;
Shader* texture_distance_field_3D_shader;

Shader* outline_3D_shader;

string load_file(string& file);
uint_t compile_shader(string sh, GLenum type);
GLuint compile_program(vector<uint_t> shs);

Shader::Shader(string vs, string fs): custom(0), color(0), invert_colors(0), vert(0), MVP(0), view(0), projection(0), depth_mask(1){
	string vs_f = load_file(vs);
	string fs_f = load_file(fs);
	if(fs_f.find("invert_color") != string::npos)invert_colors = 1;
	if(vs_f.find("MVP") != string::npos)MVP = 1;
	if(vs_f.find("view") != string::npos)view = 1;
	if(vs_f.find("projection") != string::npos)projection = 1;

	if(vs_f.find("layout(location = 4)") != string::npos)layout = 5;
	else if(vs_f.find("layout(location = 3)") != string::npos)layout = 4;
	else if(vs_f.find("layout(location = 2)") != string::npos)layout = 3;
	else if(vs_f.find("layout(location = 1)") != string::npos)layout = 2;
	else if(vs_f.find("layout(location = 0)") != string::npos)layout = 1;
	else layout = 0;

	uint_t vs_c = compile_shader(vs_f, GL_VERTEX_SHADER);
	uint_t fs_c = compile_shader(fs_f, GL_FRAGMENT_SHADER);
	program = compile_program({vs_c, fs_c});
	glDeleteShader(vs_c);
	glDeleteShader(fs_c);
}

Shader::Shader(string vs, string gs, string fs): custom(0), color(0), invert_colors(0), vert(0), MVP(0), view(0), projection(0), depth_mask(1){
	string vs_f = load_file(vs);
	string gs_f = load_file(gs);
	string fs_f = load_file(fs);
	if(fs_f.find("invert_color") != string::npos)invert_colors = 1;
	if(vs_f.find("MVP") != string::npos)MVP = 1;
	if(vs_f.find("view") != string::npos)view = 1;
	if(vs_f.find("projection") != string::npos)projection = 1;
	if(gs_f.find("projection") != string::npos)projection = 1;

	if(vs_f.find("layout(location = 4)") != string::npos)layout = 5;
	else if(vs_f.find("layout(location = 3)") != string::npos)layout = 4;
	else if(vs_f.find("layout(location = 2)") != string::npos)layout = 3;
	else if(vs_f.find("layout(location = 1)") != string::npos)layout = 2;
	else if(vs_f.find("layout(location = 0)") != string::npos)layout = 1;
	else layout = 0;

	uint_t vs_c = compile_shader(vs_f, GL_VERTEX_SHADER);
	uint_t gs_c = compile_shader(gs_f, GL_GEOMETRY_SHADER);
	uint_t fs_c = compile_shader(fs_f, GL_FRAGMENT_SHADER);
	program = compile_program({vs_c, gs_c, fs_c});
	glDeleteShader(vs_c);
	glDeleteShader(gs_c);
	glDeleteShader(fs_c);
}

Shader::Shader(uint8_t vert, uint8_t color, bool invert_colors, bool MVP):program(0), custom(1), color(color), invert_colors(invert_colors), vert(vert), MVP(MVP), view(0), projection(0), depth_mask(1){}

Shader* Shader::build(){
	if(program)return this;

	stringbuf buffer_vs;
	ostream vs(&buffer_vs);
	stringbuf buffer_fs;
	ostream fs(&buffer_fs);
	vs << "#version 330 core\n";
	if(vert)vs << "layout(location = 0) in vec" << to_string(vert) << " vert;\n";
	vs << endl;
	if(MVP)vs << "uniform mat4 MVP;\n";
	vs << "uniform mat4 transform;\n";
	vs << endl;
	vs << "void main(){\n";
	if(vert){
		vs << "gl_Position = ";
		if(MVP)vs << "MVP * ";
		if(vert == 2)vs << "transform * vec4(vert,0,1);\n";
		if(vert == 3)vs << "transform * vec4(vert,1);\n";
	}
	vs << "}\n";

	fs << "#version 330 core\n\n";
	fs << "out vec4 color;\n";
	fs << endl;
	if(color)fs << "uniform vec" << to_string(color) << " col;\n";
	if(invert_colors)fs << "uniform bool invert_color;\n";
	fs << endl;
	fs << "void main(){\n";
	if(color == 3)fs << "\tcolor = vec4(col, 1);\n";
	else if(color == 4)fs << "\tcolor = vec4(col);\n";

	if(invert_colors){
	fs << "\
	if(invert_color){\n\
		color.x = 1-color.x;\n\
		color.y = 1-color.y;\n\
		color.z = 1-color.z;\n\
	}\n";
	}
	fs << "}\n";

	layout = 0;
	if(vert)layout++;

	//cout << "vertex shader:\n" << buffer_vs.str() << endl;
	//cout << "fragment shader:\n" << buffer_fs.str() << endl;


	uint_t vs_c = compile_shader(buffer_vs.str(), GL_VERTEX_SHADER);
	uint_t fs_c = compile_shader(buffer_fs.str(), GL_FRAGMENT_SHADER);
	program = compile_program({vs_c, fs_c});
	glDeleteShader(vs_c);
	glDeleteShader(fs_c);

	return this;
}

Shader::~Shader(){
	if(program)glDeleteProgram(program);
}

uint_t Shader::getLocation(const char* nameID){
	if(program)return glGetUniformLocation(program, nameID);
	else{
		printLogErr("Trying to use invalid shader for location!");
		return 0;
	}
}

void Shader::use(){
	if(program){
		glUseProgram(program);
		if(invert_colors)glUniform1f(getLocation("invert_color"), render::invertCol);
		if(MVP)glUniformMatrix4fv(getLocation("MVP"), 1, GL_FALSE, &get_current_MVP()[0][0]);
		if(view)glUniformMatrix4fv(getLocation("view"), 1, GL_FALSE, &get_current_view_matix()[0][0]);
		if(projection)glUniformMatrix4fv(getLocation("projection"), 1, GL_FALSE, &get_current_projection_matrix()[0][0]);
		render::setVertexAttribArray(layout);
		for(auto enable : gl_enable )glEnable(enable);
		for(auto disable: gl_disable)glDisable(disable);
	}else printLogErr("Trying to use invalid shader!");
}
Shader* Shader::add_gl_enable(uint32_t enable){
	gl_enable.push_back(enable);
	return this;
}
Shader* Shader::add_gl_disable(uint32_t disable){
	gl_disable.push_back(disable);
	return this;
}
Shader* Shader::add_gl_enable(vector<uint32_t> enable){
	gl_enable.insert(gl_enable.end(), enable.begin(), enable.end());
	return this;
}
Shader* Shader::add_gl_disable(vector<uint32_t> disable){
	gl_disable.insert(gl_disable.end(), disable.begin(), disable.end());
	return this;
}

void setupShaders(){
	texture_3D_shader = (new Shader("Shaders/texture_3D.vs", "Shaders/texture_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	//color_3D_shader = (new Shader(3, 3, 1, 1))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	color_3D_shader = (new Shader("Shaders/color_3D.vs", "Shaders/color_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	color_per_face_3D_shader = (new Shader("Shaders/color_per_face_3D.vs", "Shaders/color_per_face_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	color_transparent_3D_shader = (new Shader(3, 4, 1, 1))->add_gl_enable({GL_DEPTH_TEST, GL_BLEND})->add_gl_disable({})->set_depth_mask(0)->build();
	line_3D_shader = (new Shader("Shaders/line_3D.vs", "Shaders/line_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	texture_2D_shader = (new Shader("Shaders/texture_2D.vs", "Shaders/texture_2D.fs"))->add_gl_enable({GL_BLEND})->add_gl_disable({GL_DEPTH_TEST});
	color_2D_shader =(new Shader(2, 3, 1, 0))->add_gl_enable({GL_BLEND})->add_gl_disable({GL_DEPTH_TEST})->build();
	line_2D_shader = (new Shader(2, 3, 1, 0))->add_gl_enable({GL_BLEND})->add_gl_disable({GL_DEPTH_TEST})->build();
	texture_distance_field_2D_shader = (new Shader("Shaders/texture_distance_field_2D.vs", "Shaders/texture_distance_field_2D.fs"))->add_gl_enable({GL_BLEND})->add_gl_disable({GL_DEPTH_TEST});
	texture_distance_field_3D_shader = (new Shader("Shaders/texture_distance_field_3D.vs", "Shaders/texture_distance_field_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
	particle_3D_shader = (new Shader("Shaders/particle_3D.vs", "Shaders/particle_3D.fs"))->add_gl_enable({GL_DEPTH_TEST, GL_BLEND})->add_gl_disable({})->build();

	outline_3D_shader = (new Shader("Shaders/outline_3D.vs", "Shaders/outline_3D.gs", "Shaders/outline_3D.fs"))->add_gl_enable({GL_DEPTH_TEST})->add_gl_disable({GL_BLEND})->build();
}

void cleanupShaders(){
	delete texture_3D_shader;
	delete color_3D_shader;
	delete color_transparent_3D_shader;
	delete line_3D_shader;
	delete particle_3D_shader;

	delete texture_2D_shader;
	delete color_2D_shader;
	delete line_2D_shader;


	delete outline_3D_shader;
}

string load_file(string& file){
	string fl;
	ifstream VertexShaderStream(file, ios::in);
	if(VertexShaderStream.is_open()){
		string Line = "";
		while(getline(VertexShaderStream, Line))
			fl += "\n" + Line;
		VertexShaderStream.close();
	}else printLogErr("Can't to open: " + string(file));
	return fl;
}

uint_t compile_shader(string sh, GLenum type){
	uint_t shID = glCreateShader(type);

	int Result = GL_FALSE;
	int InfoLogLength;

	char const * source = sh.c_str();
	glShaderSource(shID, 1, &source, NULL);
	glCompileShader(shID);

	glGetShaderiv(shID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(shID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		vector<char> shErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(shID, InfoLogLength, NULL, &shErrorMessage[0]);
		printLogWarn(&shErrorMessage[0]);
	}

	return shID;
}

GLuint compile_program(vector<uint_t> shs){
	GLuint ProgramID = glCreateProgram();
	for(uint_t sh: shs)glAttachShader(ProgramID, sh);
	glLinkProgram(ProgramID);

	int Result = GL_FALSE;
	int InfoLogLength;
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printLogWarn(&ProgramErrorMessage[0]);
	}
	return ProgramID;
}
