#pragma once
#include "std.h"

class Shader{
public:
	Shader(string vs, string fs);
	Shader(string vs, string gs, string fs);
	Shader(uint8_t vert, uint8_t color, bool invert_colors, bool MVP);
	~Shader();
	uint_t getLocation(const char* nameID);
	void use();

	Shader* set_depth_mask(bool depth_mask){this->depth_mask = depth_mask; return this;}

	Shader* add_gl_enable(uint32_t enable);
	Shader* add_gl_disable(uint32_t disable);
	Shader* add_gl_enable(vector<uint32_t> enable);
	Shader* add_gl_disable(vector<uint32_t> disable);

	Shader* build();
private:
	uint_t program;
	bool custom;

	uint8_t color;
	bool invert_colors;
	uint8_t vert;
	bool MVP;
	bool view;
	bool projection;


	uint8_t layout;

	bool depth_mask;
	vector<uint32_t> gl_enable;
	vector<uint32_t> gl_disable;
};

extern Shader* texture_3D_shader;
extern Shader* color_transparent_3D_shader;
extern Shader* color_3D_shader;
extern Shader* color_per_face_3D_shader;
extern Shader* line_3D_shader;
extern Shader* particle_3D_shader;

extern Shader* texture_2D_shader;
extern Shader* color_2D_shader;
extern Shader* line_2D_shader;
extern Shader* texture_distance_field_2D_shader;
extern Shader* texture_distance_field_3D_shader;

extern Shader* outline_3D_shader;

void setupShaders();
void cleanupShaders();
