#include "TemplateBuffers.h"
#include "../simplefc/render.h"

uint_t template_buffer_rectangle_boundaries;
uint_t template_buffer_rectangle_background;
uint_t template_buffer_rectangle_positive_background;
uint_t template_buffer_rectangle_cross;
uint_t template_buffer_rectangle_InfoTab;
uint_t template_buffer_editcomp_moveup;
uint_t template_buffer_editcomp_del;
uint_t template_buffer_editcomp_plus;
uint_t template_buffer_editcomp_dot;

void setupTemplateBuffers(){
	template_buffer_rectangle_boundaries = render::getBuffer(vector<float>({
		1,1,
		-1,1,

		1,-1,
		-1,-1,

		-1,-1,
		-1,1,

		1,-1,
		1,1,
	}));

	template_buffer_rectangle_background = render::getBuffer(vector<float>({
		-1, 1,
		-1,-1,
		1, 1,

		1, 1,
		-1,-1,
		1,-1,
	}));

	template_buffer_rectangle_positive_background = render::getBuffer(vector<float>({
		0,0,
		0,1,
		1,0,

		1,0,
		0,1,
		1,1,
	}));

	template_buffer_rectangle_cross = render::getBuffer(vector<float>({
		1,1,
		-1,-1,

		1,-1,
		-1,1,
	}));

	template_buffer_editcomp_moveup = render::getBuffer(vector<float>({
		-0.6f, -0.6f,
		0, 0.6f,

		0.6f, -0.6f,
		0, 0.6f,

		1,1,
		-1,1,

		1,-1,
		-1,-1,

		-1,-1,
		-1,1,

		1,-1,
		1,1,
	}));

	template_buffer_editcomp_del = render::getBuffer(vector<float>({
		0.6f, 0.6f,
		-0.6f, -0.6f,

		-0.6f, 0.6f,
		0.6f, -0.6f,

		1,1,
		-1,1,

		1,-1,
		-1,-1,

		-1,-1,
		-1,1,

		1,-1,
		1,1,
	}));

	template_buffer_editcomp_plus= render::getBuffer(vector<float>({
		0, -0.6f,
		0, 0.6f,

		-0.6f, 0,
		0.6f, 0,

		1,1,
		-1,1,

		1,-1,
		-1,-1,

		-1,-1,
		-1,1,

		1,-1,
		1,1,
	}));

	template_buffer_editcomp_dot = render::getBuffer(vector<float>({
		-0.1, -0.1,
		0.1, 0.1,

		0.1, -0.1,
		-0.1, 0.1,

		1,1,
		-1,1,

		1,-1,
		-1,-1,

		-1,-1,
		-1,1,

		1,-1,
		1,1,
	}));
}

void cleanupTemplateBuffers(){
	render::delBuffer(&template_buffer_rectangle_boundaries);
	render::delBuffer(&template_buffer_rectangle_background);
	render::delBuffer(&template_buffer_rectangle_positive_background);
	render::delBuffer(&template_buffer_rectangle_cross);
	render::delBuffer(&template_buffer_editcomp_moveup);
	render::delBuffer(&template_buffer_editcomp_del);
	render::delBuffer(&template_buffer_editcomp_plus);
	render::delBuffer(&template_buffer_editcomp_dot);
}
