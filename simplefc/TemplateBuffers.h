#pragma once
#include "preprocessor.h"

extern uint_t template_buffer_rectangle_boundaries;
extern uint_t template_buffer_rectangle_background;
extern uint_t template_buffer_rectangle_positive_background;
extern uint_t template_buffer_rectangle_cross;
extern uint_t template_buffer_editcomp_moveup;
extern uint_t template_buffer_editcomp_del;
extern uint_t template_buffer_editcomp_plus;
extern uint_t template_buffer_editcomp_dot;

void setupTemplateBuffers();
void cleanupTemplateBuffers();
