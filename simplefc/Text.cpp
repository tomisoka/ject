#include "Text.h"

#include <freetype-gl/freetype-gl.h>

struct Text::internals{
	texture_font_t* font;
	texture_atlas_t *atlas;
};

#define SZ 32

Text::Text(string ttf_file, uint32_t textureID): textureID(textureID), m(new internals()){
	const char * cache =
		" !\"#$%&'()*+,-./0123456789:;<=>?"
		"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
		"`abcdefghijklmnopqrstuvwxyz{|}~";

	m->atlas = texture_atlas_new(1024, 1024, 1);
	m->font = texture_font_new_from_file(m->atlas, SZ, ttf_file.c_str());
	m->font->rendermode = RENDER_SIGNED_DISTANCE_FIELD;

	//clock_t t = clock();
	texture_font_load_glyphs(m->font, cache);
	//cout << "Text load time: " << (clock() - t)/CLOCKS_PER_SEC << endl;

	glActiveTexture(GL_TEXTURE0 + textureID);

	glGenTextures(1, &m->atlas->id);
	glBindTexture(GL_TEXTURE_2D, m->atlas->id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m->atlas->width, m->atlas->height, 0, GL_RED, GL_UNSIGNED_BYTE, m->atlas->data);
}

Text::~Text(){
	glDeleteTextures(1, &m->atlas->id);

	texture_font_delete(m->font);
	texture_atlas_delete(m->atlas);
	delete m;
}

float Text::getTextVerts(string s, std::vector<mathlib::vec2>& verts, std::vector<mathlib::vec2>& uvs){
	float w = 0;

	const char* text = s.c_str();

	for(size_t i = 0; i < s.size(); i++){
		texture_glyph_t *g = texture_font_get_glyph(m->font, text + i);
		if(g){
			float kerning = i ? texture_glyph_get_kerning(g, text + i - 1) : 0;
			w += kerning;

			float x0  = (w + g->offset_x) / float(SZ);
			float y0  = (g->offset_y) / float(SZ);
			float x1  = x0 + (g->width) / float(SZ);
			float y1  = y0 - (g->height) / float(SZ);
			float s0 = g->s0;
			float t0 = g->t0;
			float s1 = g->s1;
			float t1 = g->t1;

			verts.push_back(mathlib::vec2(x0, y0));
			uvs  .push_back(mathlib::vec2(s0, t0));

			verts.push_back(mathlib::vec2(x0, y1));
			uvs  .push_back(mathlib::vec2(s0, t1));

			verts.push_back(mathlib::vec2(x1, y1));
			uvs  .push_back(mathlib::vec2(s1, t1));

			verts.push_back(mathlib::vec2(x0, y0));
			uvs  .push_back(mathlib::vec2(s0, t0));

			verts.push_back(mathlib::vec2(x1, y1));
			uvs  .push_back(mathlib::vec2(s1, t1));

			verts.push_back(mathlib::vec2(x1, y0));
			uvs  .push_back(mathlib::vec2(s1, t0));

			w += g->advance_x;
		}
	}

	return w/SZ;
}

float Text::getCharWidth(char c){
	texture_glyph_t *g = texture_font_get_glyph(m->font, &c);
	if(!g) return 0;
	return g->advance_x/SZ;
}
float Text::getTextWidth(string s){
	float w = 0;

	const char* text = s.c_str();

	for(size_t i = 0; i < s.size(); i++){
		texture_glyph_t *g = texture_font_get_glyph(m->font, text + i);
		if(g){
			float kerning = i ? texture_glyph_get_kerning(g, text + i - 1) : 0;
			w += kerning + g->advance_x;
		}
	}
	return w/SZ;
}

Text* text;
