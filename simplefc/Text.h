#pragma once
#include "std.h"

class Text{
public:
	Text(string ttf_file, uint32_t textureID);
	~Text();

	//returns width
	float getTextVerts(string s, vector<vec2>& verts, vector<vec2>& uvs);
	float getCharWidth(char c);
	float getTextWidth(string s);

	uint32_t getTextureID(){return textureID;}
private:
	uint32_t textureID;

	struct internals;
	internals *m;
};

extern Text* text;
