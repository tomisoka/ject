#pragma once
#include "trigger.h"
#include <functional>

class TriggerFunction: public Trigger{
public:
	TriggerFunction(function<void(int)> f):f(f){}
	void call(int state){f(state);}
private:
	function<void(int)> f;
};
