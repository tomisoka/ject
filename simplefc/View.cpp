#include "View.h"
#include "window.h"

View::View(float FoV): FoV(FoV), pos(vec3(0)), dir(vec3(1, 0, 0)), up(vec3(0, 1, 0)){
	view_matrix.initIdentity();
}

mat4 View::getProjection(){
	mat4 projection_matrix;
	projection_matrix.initPerspective(FoV, (float)gwinWidth() / (float)gwinHeight(), 1/8.f, 65536.f);
	return projection_matrix;
}

void View::update_view_matrix(){
	view_matrix.lookAt(pos, pos + dir, up);
}

View* view;
