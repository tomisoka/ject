#pragma once
#include "mathlib.h"

class View{
public:
	View(float FoV = 45.);
	mat4 getMVP(){return getProjection() * view_matrix;}
	mat4 getView(){return view_matrix;}
	mat4 getProjection();

	vec3 get_position(){return pos;}
	vec3 get_direction(){return dir;}
	vec3 get_up(){return up;}
	vec3 get_right(){return dir.cross(up);}

	void set_position(vec3 p){pos = p; update_view_matrix();}
	void set_direction(vec3 d){dir = d; update_view_matrix();}
	void set_up(vec3 u){up = u; update_view_matrix();}
	void set_pdu(vec3 p, vec3 d, vec3 u){pos = p; dir = d; up = u; update_view_matrix();}
protected:
	void update_view_matrix();

	float FoV;

	mat4 view_matrix;

	vec3 pos;
	vec3 dir;
	vec3 up;
};

extern View* view;

inline View*get_current_view(){return view;}
inline mat4 get_current_MVP(){return view->getMVP();}
inline mat4 get_current_view_matix(){return view->getView();}
inline mat4 get_current_projection_matrix(){return view->getProjection();}
