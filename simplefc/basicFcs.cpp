#include "basicFcs.h"
#include <fstream>
#include <sstream>

char TEMP[256];

bool file_exist(const char *fileName){
	ifstream infile(fileName);
	return infile.good();
}
string intToHex(int i){
	std::stringstream stream;
	stream << uppercase << hex << i;
	return stream.str();
}

const char* strToDouble(const char * pos, double& returned, char split, uint32_t len){
	uint32_t i=0;
	uint8_t is_dot=0;
	double multiplier = 1;
	returned = 0;

	if(*pos=='-')i++;

	for(; i < len; i++){
		if(pos[i]<=0x20 || pos[i] == split)break;
		else if(pos[i] == '.' || pos[i] == ','){
			if(is_dot)
				return 0;
			else
				is_dot = 1;
		}else if(!is_dot){
			if(pos[i] < '0' || pos[i] > '9')return 0;
			returned *= 10;
			returned += pos[i] - '0';
		}else{
			if(pos[i] < '0' || pos[i] > '9')return 0;
			multiplier /= 10;
			returned += multiplier*(pos[i]-'0');
		}
	}
	if(*pos=='-')returned*=-1;
	return pos + i;
}

const char* strToInt(const char *pos, int32_t& returned, char split, uint32_t len, uint8_t base){
	size_t i=0;
	returned=0;
	unsigned char temp;

	if(base>26*2+10)return 0;

	if(*pos=='-')i++;
	for(; i<len;++i){
		temp = pos[i];
		if(temp<=0x20 || temp == split)break;
		returned*=base;

		if(temp>='0' && temp<='9'){
			temp-='0';
		}else if(temp>='A' && temp <= 'Z'){
			temp = temp-'A'+10;
		}else if(temp>='a' && temp <= 'z'){
			temp = temp-'a'+36;
		}else
			return 0;
		if(base<=temp)return 0;
		returned+=temp;
	}
	if(*pos=='-')returned*=-1;
	return pos + i;
}

char *numStr(char *s, int num){
	int i=1;

	if(num < 0){
		*s++ = '-';
		num = -num;
	}else if(!num){
		*s++ = '0';
		return s;
	}

	while(i<=num)i*=10;
	i/=10;

	while(i){
		*s = num/i+'0';
		s++;
		num %= i;
		i/=10;
	}

	return s;
}

char *floatStr(char *s, float n, uint32_t afterpoint){
	if(n<0){
		*s++ = '-';
		n=-n;
	}
	int i;
	int ipart = (int)n;

	float fpart = n - (float)ipart;


	if (afterpoint){
		i = (int)pow(10, afterpoint-1);

		while(afterpoint--){
			fpart*=10;
		}

		int fp = (int) round(fpart);

		if(fp/i==10){
			ipart++;
			fp=0;
		}

		s = numStr(s, ipart);

		if(fp){
			*s++ = '.';
			while(i && fp){
				*s++ = fp/i+'0';
				fp %= i;
				i/=10;
			}
		}
	}else
		s = numStr(s, ipart);
	return s;
}

vec3 normalRadA(vec3 old){
	while(old.getX() < -M_PI)old.setX(old.getX()+2*M_PI);
	while(old.getY() < -M_PI)old.setY(old.getY()+2*M_PI);
	while(old.getZ() < -M_PI)old.setZ(old.getZ()+2*M_PI);
	while(old.getX() > +M_PI)old.setX(old.getX()-2*M_PI);
	while(old.getY() > +M_PI)old.setY(old.getY()-2*M_PI);
	while(old.getZ() > +M_PI)old.setZ(old.getZ()-2*M_PI);
	return old;
}
