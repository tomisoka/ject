#pragma once
#include "mathlib.h"
#include "preprocessor.h"
#include <string>
#include <vector>
using namespace std;

bool file_exist(const char *fileName);

inline bool ends_with(string const& value, string const& ending){
	if (ending.size() > value.size()) return false;
	return equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline string get_relative_path(string original, string relative){
	auto it = original.end();
	while(it != original.begin() && *(it-1) != '/')it--;
	return ((it == original.begin())?"":string(original.begin(), it)) + relative;
}

inline string trim_end(string original){
	auto it = original.end();
	while(it != original.begin() && *(it-1) < 0x20)it--;
	return (it == original.begin())?"":string(original.begin(), it);
}

string intToHex(int i);

inline bool fisInt(float inp){
	return (int)inp == inp;
}

const char* strToDouble(const char * pos, double& returned, char split = ';', uint32_t len=100000);
inline const char* strToFloat(const char * pos, float& returned, char split = ';', uint32_t len=100000){
	double retu;
	const char* ret = strToDouble(pos, retu, split, len);
	returned = retu;
	return ret;
}
const char* strToInt(const char *pos, int32_t& returned, char split=';', uint32_t len=100000, uint8_t base=10);

char *numStr(char *s, int num);
char *floatStr(char *s, float n, uint32_t afterpoint = 3);

template<unsigned int D>
bool strToVec(const char *str, Vector<float, D> &ret, char split = ';'){
	ret = Vector<float, D>(0);
	double temp;

	for(size_t i = 0;i<D; ++i){
		if(!(str = strToDouble(str, temp, split)))return 1;
		str++;
		ret[i]=float(temp);
	}
	return 0;
}

template<unsigned int D>
bool strToVec(const char *str, Vector<int, D> &ret, char split = ';'){
	ret = Vector<int, D>(0);
	int temp;

	for(size_t i = 0;i<D; ++i){
		if(!(str = strToInt(str, temp, split)))return 1;
		str++;
		ret[i]= temp;
	}
	return 0;
}

inline void strToVec(const char *str, vector<float> &ret, char split = ';'){
	ret.clear();
	double temp;

	for(;;){
		if(!(str = strToDouble(str, temp, split)))return;
		ret.push_back(float(temp));
		if(*str == 0)return;
		str++;
	}
}

inline void strToVec(const char *str, vector<int> &ret, char split = ';'){
	ret.clear();
	int temp;

	for(;;){
		if(!(str = strToInt(str, temp, split)))return;
		ret.push_back(temp);
		if(*str == 0)return;
		str++;
	}
}

template<unsigned int D>
char *vecToStr(char *out, Vector<float, D> in, char split = ';', uint32_t afterpoint = 3){
	for(size_t i = 0; i < D;){
		out = floatStr(out, in[i], afterpoint);
		if(++i != D)*out++ = split;
	}
	return out;
}

extern char TEMP[256];

template<unsigned int D>
string vecToS(Vector<float, D> in, char split = ';', uint32_t afterpoint = 3){
	*vecToStr(TEMP, in, split, afterpoint) = 0;
	return string(TEMP);
}

inline string numStr(int num){
	*numStr(TEMP, num) = 0;
	return string(TEMP);
}

inline string floatStr(float f, uint32_t afterpoint=3){
	*floatStr(TEMP, f, afterpoint) = 0;
	return string(TEMP);
}

template<typename T, uint_t D>
string as_str(Vector<T, D> in){
	*vecToStr(TEMP, in) = 0;
	return string(TEMP);
}

inline string as_str(int v){
	return numStr(v);
}

inline string as_str(float v){
	return floatStr(v);
}

vec3 normalRadA(vec3 old);

inline bool pointInTrian(vec2 p, vec2 p1, vec2 p2, vec2 p3){
	let alpha= ((p2.getY() - p3.getY())*(p.getX() - p3.getX()) + (p3.getX() - p2.getX())*(p.getY() - p3.getY()))/((p2.getY() - p3.getY())*(p1.getX() - p3.getX()) + (p3.getX() - p2.getX())*(p1.getY() - p3.getY()));
	let beta= ((p3.getY() - p1.getY())*(p.getX() - p3.getX()) + (p1.getX() - p3.getX())*(p.getY() - p3.getY())) /((p2.getY() - p3.getY())*(p1.getX() - p3.getX()) + (p3.getX() - p2.getX())*(p1.getY() - p3.getY()));
	let gamma = 1.0f - alpha - beta;

	return alpha >= 0 && beta >= 0 && gamma >= 0;
}
