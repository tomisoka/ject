#include "input.h"
#include "window.h"
#include <GLFW/glfw3.h>
#include <vector>

using namespace std;

vector<InputCallbacks*> registeredInput;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	bool acted = 0;
	for(size_t i=registeredInput.size();i-- > 0;){
		if(registeredInput[i]->input_key(acted, key, action, mods))acted = 1;
	}
}

void char_callback(GLFWwindow* window, unsigned int ch, int mods){
	bool acted = 0;
	for(size_t i=registeredInput.size();i-- > 0;){
		if(registeredInput[i]->input_char(acted, ch, mods))acted = 1;
	}
}

void mouse_callback(GLFWwindow* window, int button, int action, int mods){
	bool acted = 0;
	for(size_t i=registeredInput.size();i-- > 0;){
		if(registeredInput[i]->input_mouse(acted, button, action, mods))acted = 1;
	}
}

void scroll_callback(GLFWwindow* window, double xOff, double yOff){
	bool acted = 0;
	for(size_t i=registeredInput.size();i-- > 0;){
		if(registeredInput[i]->input_scroll(acted, xOff, yOff))acted = 1;
	}
}

void cursorPos_callback(GLFWwindow* window, double xd, double yd){
	bool acted = 0;
	for(size_t i=registeredInput.size();i-- > 0;){
		if(registeredInput[i]->input_cursor_pos(acted, xd, yd))acted = 1;
	}
}

void registInput(InputCallbacks *in){
	registeredInput.push_back(in);
}

void unregistInput(InputCallbacks *in){
	for(size_t i=0;i<registeredInput.size();++i){
		if(registeredInput[i] == in){
			registeredInput.erase(registeredInput.begin() + i);
			i--;
		}
	}
}

void setupInput(){
	glfwSetKeyCallback(window, key_callback);
	glfwSetCharModsCallback(window, char_callback);
	glfwSetMouseButtonCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetCursorPosCallback(window, cursorPos_callback);
}

bool isPressed(int key){
	return glfwGetKey(window, key) == GLFW_PRESS;
}
