#pragma once

class GLFWwindow;

class InputCallbacks;

void registInput(InputCallbacks *in);
void unregistInput(InputCallbacks *in);
void setupInput();

class InputCallbacks{
public:
	InputCallbacks(){registInput(this);}
	virtual ~InputCallbacks(){unregistInput(this);}

private:
	friend void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	friend void char_callback(GLFWwindow* window, unsigned int ch, int mods);
	friend void mouse_callback(GLFWwindow* window, int button, int action, int mods);
	friend void scroll_callback(GLFWwindow* window, double xOff, double yOff);
	friend void cursorPos_callback(GLFWwindow* window, double xd, double yd);

	virtual bool input_key(bool acted, int key, int action, int mods){return 0;}
	virtual bool input_char(bool acted, int ch, int mods){return 0;}
	virtual bool input_mouse(bool acted, int button, int action, int mods){return 0;}
	virtual bool input_scroll(bool acted, double xOff, double yOff){return 0;}
	virtual bool input_cursor_pos(bool acted, double xd, double yd){return 0;}
};

bool isPressed(int key);
