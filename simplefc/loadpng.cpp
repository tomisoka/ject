#include "loadpng.h"
#include "log.h"
#define TEXTURE_LOAD_ERROR 0

using namespace std;
	/** loadTexture
	* 	loads a png file into an opengl texture object, using cstdio , libpng, and opengl.
	*
	* 	\param filename : the png file to be loaded
	* 	\param width : width of png, to be updated as a side effect of this function
	* 	\param height : height of png, to be updated as a side effect of this function
	*
	* 	\return GLuint : an opengl texture id.  Will be 0 if there is a major error,
	* 					should be validated by the client of this function.
	*
	*/

GLuint loadTexture(const char* filename, uint32_t *width, uint32_t *height){
	//header for testing if it is a png
	png_byte header[8];

	//open file as binary
	FILE *fp = fopen(filename, "rb");
	if (!fp){
		return TEXTURE_LOAD_ERROR;
	}

	//read the header
	fread(header, 1, 8, fp);

	//test if png
	int is_png = !png_sig_cmp(header, 0, 8);
	if (!is_png){
		fclose(fp);
		return TEXTURE_LOAD_ERROR;
	}

	//create png struct
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL,	 NULL, NULL);
	if (!png_ptr){
		fclose(fp);
		return (TEXTURE_LOAD_ERROR);
	}

	//create png info struct
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr){
		png_destroy_read_struct(&png_ptr, (png_infopp) NULL, (png_infopp) NULL);
		fclose(fp);
		return (TEXTURE_LOAD_ERROR);
	}

	//create png info struct
	png_infop end_info = png_create_info_struct(png_ptr);
	if (!end_info){
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
		fclose(fp);
		return (TEXTURE_LOAD_ERROR);
	}

	//png error stuff, not sure libpng man suggests this.
	if (setjmp(png_jmpbuf(png_ptr))){
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		fclose(fp);
		return (TEXTURE_LOAD_ERROR);
	}

	//init png reading
	png_init_io(png_ptr, fp);

	//let libpng know you already read the first 8 bytes
	png_set_sig_bytes(png_ptr, 8);

	// read all the info up to the image data
	png_read_info(png_ptr, info_ptr);

	//variables to pass to get info
	int bit_depth, color_type;
	png_uint_32 twidth, theight;

	// get info about png
	png_get_IHDR(png_ptr, info_ptr, &twidth, &theight, &bit_depth, &color_type, NULL, NULL, NULL);

	//update width and height based on png info
	*width = (uint32_t)twidth;
	*height = (uint32_t)theight;

	// Update the png info struct.
	png_read_update_info(png_ptr, info_ptr);

	// Row size in bytes.
	int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

	// Allocate the image_data as a big block, to be given to opengl
	png_byte *image_data = new png_byte[rowbytes * (*height)];
	//row_pointers is for pointing to image_data for reading the png with libpng
	png_bytep *row_pointers = new png_bytep[*height];
	// set the individual row_pointers to point at the correct offsets of image_data
	for (uint32_t i = 0; i < *height; ++i)
		row_pointers[i] = image_data + i * rowbytes;

	//read the png into image_data through row_pointers
	png_read_image(png_ptr, row_pointers);

	//Now generate the OpenGL texture object
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA, *width, *height, 0, (rowbytes/ *width==4)?GL_RGBA:GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*) image_data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//clean up memory and close stuff
	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	delete[] image_data;
	delete[] row_pointers;
	fclose(fp);

	return texture;
}

png_bytep* loadPng(const char* filename, uint32_t& width, uint32_t& height){
	FILE *fp = fopen(filename, "rb");
	if (!fp){
		printLogErr(string("Invalid file: ") + filename + "!");
		return TEXTURE_LOAD_ERROR;
	}

	png_structp png= png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)return 0;
	png_infop info = png_create_info_struct(png);
	if (!info)return 0;
	if (setjmp(png_jmpbuf(png)))return 0;
	png_init_io(png, fp);
	png_read_info(png, info);

	int bit_depth, color_type;
	png_uint_32 twidth, theight;
	png_get_IHDR(png, info, &twidth, &theight, &bit_depth, &color_type, NULL, NULL, NULL);
	/* Read any color_type into 8bit depth, RGBA format. */
	/* See http://www.libpng.org/pub/png/libpng-manual.txt */
	if (bit_depth == 16)png_set_strip_16(png);
	if (color_type == PNG_COLOR_TYPE_PALETTE)png_set_palette_to_rgb(png);
	/* PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth. */
	if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);
	if (png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);
	/* These color_type don't have an alpha channel then fill it with 0xff. */
	if(color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_PALETTE)
	png_set_filler(png, 0xFF, PNG_FILLER_AFTER);
	if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	width = (uint32_t)twidth;
	height = (uint32_t)theight;

	png_read_update_info(png, info);

	int rowbytes = png_get_rowbytes(png, info);
	png_bytep* row_pointers = new png_bytep[height];
	for (size_t y = 0; y < height; y++) {
		row_pointers[y] = new png_byte[rowbytes];
	}
	png_read_image(png, row_pointers);
	fclose(fp);

	return row_pointers;
}

void writePng(char *filename, png_bytep* row_pointers, uint32_t w, uint32_t h) {
	FILE *fp = fopen(filename, "wb");
	if (!fp)return;
	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png)return;
	png_infop info = png_create_info_struct(png);
	if (!info)return;
	if (setjmp(png_jmpbuf(png)))return;
	png_init_io(png, fp);
	png_set_IHDR(png, info, w, h, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	png_write_info(png, info);
	/* To remove the alpha channel for PNG_COLOR_TYPE_RGB format, */
	/* Use png_set_filler(). */
	/*png_set_filler(png, 0, PNG_FILLER_AFTER);*/
	png_write_image(png, row_pointers);
	png_write_end(png, NULL);
	for (size_t y = 0; y < h; y++) {
		delete[] row_pointers[y];
	}
	delete[] row_pointers;
	fclose(fp);
}
