#pragma once
#include <stdint.h>
#include <GL/gl.h>
#include <png.h>
GLuint loadTexture(const char* filename, uint32_t *width, uint32_t *height);
png_bytep* loadPng(const char* filename, uint32_t& width, uint32_t& height);
void writePng(const char *filename, png_bytep* row_pointers, uint32_t w, uint32_t h);
