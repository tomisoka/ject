#include "log.h"
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>

using std::endl;
using std::string;

std::ofstream log("latest.log");

#define dig2 std::setw(2) << std::setfill('0')

void setupLog(){
	time_t t = time(0);
	tm *now = localtime(&t);
	log << "Ject log (" << (now->tm_year+1900) << "-" << dig2 << (now->tm_mon+1) << "-" << dig2 << now->tm_mday << "):" << endl;
}

string get_time(){
	time_t t = time(0);
	tm *now = localtime(&t);

	std::stringbuf buffer;
	std::ostream os(&buffer);
	os << dig2 << now->tm_hour << ":" << dig2 << now->tm_min << ":" << dig2 << now->tm_sec;
	return buffer.str();
}

void printLog(string s){
	string tm = get_time();
	log << tm << " " << "[INFO]" << " " << s << endl;
}

void printLogWarn(string s){
	string tm = get_time();
	std::cerr << tm << " " << "[WARN]" << " " << s << endl;
	log << tm << " " << "[WARN]" << " " << s << endl;
}

void printLogErr(string s){
	string tm = get_time();
	std::cerr << tm << " " << "[ERROR]" << " " << s << endl;
	log << tm << " " << "[ERROR]" << " " << s << endl;
}
