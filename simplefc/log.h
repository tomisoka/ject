#pragma once
#include <string>

void setupLog();

void printLog(std::string s);
void printLogWarn(std::string s);
void printLogErr(std::string s);
