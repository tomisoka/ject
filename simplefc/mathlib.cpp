#include "mathlib.h"

Vector3f Vector3f::rotate(const Quaternion& rotation)const{
	Quaternion conjugateQ = rotation.conjugate();
	Quaternion w = rotation * (*this) * conjugateQ;

	return Vector3f(w.getX(), w.getY(), w.getZ());
}
