#include "mouse.h"
#include <GLFW/glfw3.h>
#include "window.h"
#include "basicFcs.h"

vec2 cursorPos(){
	double X, Y;
	glfwGetCursorPos(window, &X, &Y);
	return vec2(2*X/gwinWidth()-1, -(2*Y/gwinHeight()-1));
}

vec2i cursorPosI(){
	vec2d ret;
	glfwGetCursorPos(window, &ret[0], &ret[1]);
	ret[1] = gwinHeight()-ret[1]-1;
	return ret.roundI();
}
vec2i cursorPosIE(){
	vec2d ret;
	glfwGetCursorPos(window, &ret[0], &ret[1]);
	return ret.roundI();
}

bool mouse_in_rectangle(mat4 transform, vec2 min, vec2 max){
	vec2 MP = cursorPos();

	vec2 pos[4];
	pos[0] = vec2(transform * vec3(min, 0));
	pos[1] = vec2(transform * vec3(vec2(max.getX(), min.getY()), 0));

	pos[2] = vec2(transform * vec3(max, 0));
	pos[3] = vec2(transform * vec3(vec2(min.getX(), max.getY()), 0));

	if(pointInTrian(MP, pos[0], pos[1], pos[2]) || pointInTrian(MP, pos[0], pos[3], pos[2]))return 1;
	return 0;
}
