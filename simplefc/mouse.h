#pragma once
#include "mathlib.h"

vec2 cursorPos();
vec2i cursorPosI();
vec2i cursorPosIE();

bool mouse_in_rectangle(mat4 transform, vec2 min = vec2(-1), vec2 max = vec2(1));
