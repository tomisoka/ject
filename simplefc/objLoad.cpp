#include "objLoad.h"
#include <string.h>
#include "basicFcs.h"
#include "std.h"

static char temp[500];

void loadLineObj(const char *inFile, vector<vec3>& vertices, vector<vec2i>& indices){
	FILE *inF;
	if(!(inF=fopen(inFile, "r"))){
		printLogErr("Obj file (" + string(inFile) + ") not found!");
		return;
	}

	fgets(temp, 500, inF);
	if(!strncmp(temp, "#v 1", 4)){
		do{
			if(temp[0] == 'v' && temp[1] == ' '){
				vertices.push_back(vec3(0));
				strToVec(temp + 2, vertices[vertices.size()-1], ' ');
			}else if(temp[0] == 'l' && temp[1] == ' '){
				indices.push_back(vec2i(0));
				strToVec(temp + 2, indices[indices.size()-1], ' ');
			}
		}while(fgets(temp, 500, inF));
	}else{
		do{
			Vector<float, 6> pses;
			strToVec(temp, pses, ' ');
			indices.push_back(vec2i(vertices.size(), vertices.size()+1));
			vertices.push_back(vec3(pses[0], pses[1], pses[2]));
			vertices.push_back(vec3(pses[3], pses[4], pses[5]));
		}while(fgets(temp, 500, inF));
	}
	fclose(inF);
}

void loadFaceObj(const char *inFile, vector<vec3>& vertices, vector<vec3i>& indices){
	FILE *inF;
	if(!(inF=fopen(inFile, "r"))){
		printLogErr("Obj file (" + string(inFile) + ") not found!");
		return;
	}

	int sub = 1;
	fgets(temp, 500, inF);
	if(!strncmp(temp, "#v 1", 4)){
		sub = 0;
	}

	do{
		if(temp[0] == 'v' && temp[1] == ' '){
			vertices.push_back(vec3(0));
			strToVec(temp + 2, vertices[vertices.size()-1], ' ');
		}else if(temp[0] == 'f' && temp[1] == ' '){
			indices.push_back(vec3i(0));
			strToVec(temp + 2, indices[indices.size()-1], ' ');
			indices[indices.size()-1] -= vec3i(sub);
		}
	}while(fgets(temp, 500, inF));
	fclose(inF);
}

void load_material_colors(string file, map<string, Color>& material_colors){
	FILE *inF;
	if(!(inF=fopen(file.c_str(), "r"))){
		printLogErr("Mtl file (" + file + ") not found!");
		return;
	}
	string current_material = "None";

	do{
		if(!strncmp(temp, "newmtl ", strlen("newmtl "))){
			current_material = temp + strlen("newmtl ");
		}else if(!strncmp(temp, "Kd ", strlen("Kd "))){
			vec3 col;
			strToVec(temp + strlen("Kd "), col, ' ');
			col[0] = sqrt(col[0]);
			col[1] = sqrt(col[1]);
			col[2] = sqrt(col[2]);
			material_colors[current_material] = Color(col);
		}
	}while(fgets(temp, 500, inF));

	fclose(inF);
}

void loadFaceObj(const char *inFile, vector<vec3>& vertices, vector<vec3i>& indices, vector<Color>& out_colors){
	FILE *inF;
	if(!(inF=fopen(inFile, "r"))){
		printLogErr("Obj file (" + string(inFile) + ") not found!");
		return;
	}

	int sub = 1;
	fgets(temp, 500, inF);
	if(!strncmp(temp, "#v 1", 4)){
		sub = 0;
	}

	map<string, Color> material_colors;

	Color current_color = Color(0xFF, 0xFF, 0xFF);

	do{
		if(temp[0] == 'v' && temp[1] == ' '){
			vertices.push_back(vec3(0));
			strToVec(temp + 2, vertices[vertices.size()-1], ' ');
		}else if(temp[0] == 'f' && temp[1] == ' '){
			indices.push_back(vec3i(0));
			strToVec(temp + 2, indices[indices.size()-1], ' ');
			indices[indices.size()-1] -= vec3i(sub);
			out_colors.push_back(current_color);
		}else if(!strncmp(temp, "usemtl ", strlen("usemtl "))){
			auto it = material_colors.find(string(temp + strlen("usemtl ")));
			if(it != material_colors.end())current_color = it->second;
		}else if(!strncmp(temp, "mtllib ", strlen("mtllib "))){
			load_material_colors(get_relative_path(inFile, trim_end(temp + strlen("mtllib "))), material_colors);
		}
	}while(fgets(temp, 500, inF));
	fclose(inF);
}

void build_adjacent_indices(vector<vec3i>& org, vector<Vector<int, 6>>& out){
	map<vec2i, size_t> edges;
	for(size_t i = 0; i < org.size(); i++){
		vec3i o = org[i];
		out.push_back(Vector<int, 6>());
		out[i][0] = o[0];
		out[i][2] = o[1];
		out[i][4] = o[2];
		out[i][1] = o[2];
		out[i][3] = o[0];
		out[i][5] = o[1];

		{
		auto it = edges.find(vec2i(o[1], o[0]));
		if(it != edges.end()){
			vec3i other = org[it->second];
			int ind_v = 0;
			if(o[1] == other[0])ind_v = 0;
			if(o[1] == other[1])ind_v = 1;
			if(o[1] == other[2])ind_v = 2;
			out[it->second][ind_v *2+1] = o[2];
			out[i][1] = other[(ind_v + 2)%3];
			edges.erase(it);
		}else edges[vec2i(o[0], o[1])] = i;
		}

		{
		auto it = edges.find(vec2i(o[2], o[1]));
		if(it != edges.end()){
			vec3i other = org[it->second];
			int ind_v = 0;
			if(o[2] == other[0])ind_v = 0;
			if(o[2] == other[1])ind_v = 1;
			if(o[2] == other[2])ind_v = 2;
			out[it->second][ind_v*2+1] = o[0];
			out[i][3] = other[(ind_v + 2)%3];
			edges.erase(it);
		}else edges[vec2i(o[1], o[2])] = i;
		}

		{
		auto it = edges.find(vec2i(o[0], o[2]));
		if(it != edges.end()){
			vec3i other = org[it->second];
			int ind_v = 0;
			if(o[0] == other[0])ind_v = 0;
			if(o[0] == other[1])ind_v = 1;
			if(o[0] == other[2])ind_v = 2;
			out[it->second][ind_v*2+1] = o[1];
			out[i][5] = other[(ind_v + 2)%3];
			edges.erase(it);
		}else edges[vec2i(o[2], o[0])] = i;
		}
	}
}

void remove_indices(vector<vector<size_t>>& colors, vec3i indices, size_t index){
	for(size_t i = 0; i < 3; i++){
		int ind = indices[i];
		auto it = find(colors[ind].begin(), colors[ind].end(), index);
		if(it != colors[ind].end())colors[ind].erase(it);
	}
}

void round_about(vec3i& v){
	int temp = v[0];
	v[0] = v[1];
	v[1] = v[2];
	v[2] = temp;
}

void distribute_colors_per_face(vector<vec3i>& index, vector<Color>& color_face, vector<Color>& out_colors){
	vector<vector<size_t>> colors;

	for(size_t i = 0; i < index.size();i++){
		for(size_t j = 0; j < 3;j++){
			int ind = index[i][j];
			while(colors.size() <= (size_t)ind)colors.push_back(vector<size_t>());
			colors[ind].push_back(i);
		}
	}

	for(size_t i = 0;i < colors.size();i++)out_colors.push_back(vec3(1, 0, 0));

	bool changed = 1;
	while(changed){
		changed = 0;
		for(size_t i = 0; i < colors.size(); i++){
			if(!colors[i].size())continue;
			bool the_same = 1;
			Color clr = color_face[colors[i][0]];
			for(size_t j = 1; j < colors[i].size();j++){
				if(color_face[colors[i][j]] != clr){
					the_same = 0;
					break;
				}
			}
			if(the_same){
				vector<size_t> ind = colors[i];
				out_colors[i] = clr;

				for(size_t j = 0; j < ind.size(); ++j){
					while((size_t)index[ind[j]][2] != i){
						round_about(index[ind[j]]);
					}
				}



				for(size_t j = 0; j < ind.size(); ++j){
					remove_indices(colors, index[ind[j]], ind[j]);
				}
				changed = 1;
			}
		}
	}

	bool empty = 0;
	size_t allowed_contamination = 1;
	while(!empty){
		empty = 1;
		for(size_t i = 0; i < colors.size(); i++){
			if(!colors[i].size())continue;
			empty = 0;
			map<Color, int> c_n;

			for(size_t j = 1; j < colors[i].size();j++){
				c_n[color_face[colors[i][j]]]++;
			}

			auto x = std::max_element(c_n.begin(), c_n.end(), [](const pair<Color, int>& p1, const pair<Color, int>& p2) {return p1.second < p2.second;});

			if(x->second + allowed_contamination >= colors[i].size()){
				Color clr = x->first;
				vector<size_t> ind = colors[i];
				out_colors[i] = clr;

				for(size_t j = 0; j < ind.size(); ++j){
					while((size_t)index[ind[j]][2] != i){
						round_about(index[ind[j]]);
					}
				}



				for(size_t j = 0; j < ind.size(); ++j){
					if(color_face[ind[j]] == clr)remove_indices(colors, index[ind[j]], ind[j]);
				}
				colors[i].clear();
				changed = 1;
			}
		}
	}

	for(size_t i = 0; i < colors.size();i++){
		if(colors[i].size()){
			cout << "You should fix distribute_colors_per_face, not all faces are distributed correctly" << endl;
		}
		for(size_t c: colors[i]){
			Color clr = color_face[c];
			for(int j = 0; j < 3; j++)cout << to_string(clr[j]) << " ";
			cout << endl;
		}
	}
}
