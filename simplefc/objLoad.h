#pragma once

#include <vector>
#include "mathlib.h"
using std::vector;

void loadLineObj(const char *inFile, vector<vec3>& vertices, vector<vec2i>& indices);
void loadFaceObj(const char *inFile, vector<vec3>& vertices, vector<vec3i>& indices);
void loadFaceObj(const char *inFile, vector<vec3>& vertices, vector<vec3i>& indices, vector<Color>& colors);

void build_adjacent_indices(vector<vec3i>& org, vector<Vector<int, 6>>& out);

void distribute_colors_per_face(vector<vec3i>& index, vector<Color>& color_face, vector<Color>& out_colors);
