#pragma once
#include <cstdlib>

inline uint32_t getRandInt(uint32_t n){return rand()%n;}
inline uint32_t getRandInt(uint32_t start, uint32_t end){return start+rand()%(end-start);}
inline double getRandDouble(){return rand()/float(RAND_MAX);}
inline double getRandDouble2(){return getRandDouble()-getRandDouble();}
