#include "render.h"
#include "Shader.h"
#include <GLFW/glfw3.h>

namespace render{
bool invertCol = 0;

void setVertexAttribArray(int max){
	static int cur = 0;
	while(cur > max){
		cur--;
		glDisableVertexAttribArray(cur);
	}
	while(cur < max){
		glEnableVertexAttribArray(cur);
		cur++;
	}
}

void setup(){
	glfwSwapInterval(0);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
}

void clear(){
	if(invertCol)glClearColor(0, 0, 0, 1);
	else glClearColor(1, 1, 1, 1);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void setInvert(bool inv){
	if(inv){
		glLogicOp(GL_INVERT);
		glEnable(GL_COLOR_LOGIC_OP);
	}else{
		glLogicOp(GL_SET);
		glDisable(GL_COLOR_LOGIC_OP);
	}
}
void setIgnoreLowAlpha(bool ign){
	if(ign){
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
	}else{
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
	}
}

void bind_ibo(GLuint indices){
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
}

void setAttrPoint(GLuint ind, GLint size, GLuint buffer, GLenum type, bool convert){
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glVertexAttribPointer(ind, size, type, convert, 0, 0);
}
void setAttrDiv(GLuint ind, GLuint size){
	glVertexAttribDivisor(ind, size);
}

void delBuffer(GLuint* buffer){
	glDeleteBuffers(1, buffer);
}

void setImage(GLuint texture, GLuint w, GLuint h, void* pixels){
	glActiveTexture(GL_TEXTURE0 + texture);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, pixels);
}

Shader *act_shader = 0;

enum R{Lines, Triangles, Part, TriangleAdjacent};
R actRendering = R::Lines;

void setTexture(uint32_t texID){
	glUniform1i(act_shader->getLocation("tex"), texID);
}

void setVec3(GLuint ID, vec3 data){
	glUniform3f(ID, data.getX(), data.getY(), data.getZ());
}
void setVec4(GLuint ID, vec4 data){
	glUniform4f(ID, data.getX(), data.getY(), data.getZ(), data.getW());
}

void setTrans(mat4 trans){
	glUniformMatrix4fv(act_shader->getLocation("transform"), 1, GL_FALSE, &trans[0][0]);
}

void setColor(vec3 color){
	setVec3(act_shader->getLocation("col"), color);
}

void setColor(vec4 color){
	setVec4(act_shader->getLocation("col"), color);
}

void setNormal(){
	actRendering = Triangles;
	act_shader = texture_3D_shader;
	act_shader->use();
}

void setColor(){
	actRendering = Triangles;
	act_shader = color_3D_shader;
	act_shader->use();
}

void set_color_per_face_3D(){
	actRendering = Triangles;
	act_shader = color_per_face_3D_shader;
	act_shader->use();
}

void setColorTranspar(){
	actRendering = Triangles;
	act_shader = color_transparent_3D_shader;
	act_shader->use();
}

void setLine(bool fade){
	actRendering = Lines;
	act_shader = line_3D_shader;
	act_shader->use();
	glUniform1i(act_shader->getLocation("fade"), fade);
}

void set3D_distance_field(){
	actRendering = Triangles;
	act_shader = texture_distance_field_3D_shader;
	act_shader->use();
}

void set2D(){
	actRendering = Triangles;
	act_shader = texture_2D_shader;
	act_shader->use();
	glUniform1f(act_shader->getLocation("alpha"), 1.f);
}

void set2DColor(){
	actRendering = Triangles;
	act_shader = color_2D_shader;
	act_shader->use();
}

void set2DLine(){
	actRendering = Lines;
	act_shader = line_2D_shader;
	act_shader->use();
}

void set2D_distance_field(){
	actRendering = Triangles;
	act_shader = texture_distance_field_2D_shader;
	act_shader->use();
}

void setParticle(vec3 camUp, vec3 camRight){
	actRendering = Part;
	act_shader = particle_3D_shader;
	act_shader->use();
	setVec3(act_shader->getLocation("CamUp"), camUp);
	setVec3(act_shader->getLocation("CamRight"), camRight);
}


void setOutline(){
	actRendering = TriangleAdjacent;
	act_shader = outline_3D_shader;
	act_shader->use();
}

void draw(size_t len){
	switch(actRendering){
		case Lines:
			glDrawArrays(GL_LINES, 0, len);
			break;
		case Triangles:
			glDrawArrays(GL_TRIANGLES, 0, len);
			break;
		case TriangleAdjacent:
			glDrawElements(GL_TRIANGLES_ADJACENCY, len, GL_UNSIGNED_INT, nullptr);
			break;
		default:
			break;
	}
}

void draw_elements(size_t len){
	switch(actRendering){
		case Lines:
			glDrawElements(GL_LINES, len, GL_UNSIGNED_INT, nullptr);
			break;
		case Triangles:
			glDrawElements(GL_TRIANGLES, len, GL_UNSIGNED_INT, nullptr);
			break;
		case TriangleAdjacent:
			glDrawElements(GL_TRIANGLES_ADJACENCY, len, GL_UNSIGNED_INT, nullptr);
			break;
		default:
			break;
	}
}

void drawInstanced(size_t instLen, size_t len){
	switch(actRendering){
		case Part:
			glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, instLen, len);
			break;
		default:
			break;
	}
}

}//render
