#pragma once
#include "std.h"
#include <GL/glew.h>

namespace render{
extern bool invertCol;

void setup();

void clear();

void setVertexAttribArray(int max);

void setInvert(bool inv);
void setIgnoreLowAlpha(bool ign);

void bind_ibo(GLuint indices);
void setAttrPoint(GLuint ind, GLint size, GLuint buffer, GLenum type = GL_FLOAT, bool convert = false);
void setAttrDiv(GLuint ind, GLuint size);

template<typename T>
GLuint getBuffer(const vector<T>& in, GLenum target = GL_ARRAY_BUFFER){
	if(!in.size())return 0;
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(target, buffer);
	glBufferData(target, sizeof(T) * in.size(), &in[0], GL_STATIC_DRAW);
	return buffer;
}
template<typename T>
GLuint getBuffer(const T* in, size_t len, GLenum target = GL_ARRAY_BUFFER){
	if(!len)return 0;
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(target, buffer);
	glBufferData(target, sizeof(T) * len, in, GL_STATIC_DRAW);
	return buffer;
}

void delBuffer(GLuint* buffer);

void setImage(GLuint texture, GLuint w, GLuint h, void* pixels);

void setTrans(mat4 trans);
void setColor(vec3 color);
void setColor(vec4 color);

void setTexture(uint32_t texID);

void setNormal();
void setColor();
void set_color_per_face_3D();
void setColorTranspar();
void setLine(bool fade=1);
void set3D_distance_field();
void set2D();
void set2DColor();
void set2DLine();
void set2D_distance_field();
void setParticle(vec3 camUp, vec3 camRight);

void setOutline();

void draw(size_t len);
void draw_elements(size_t len);
void drawInstanced(size_t instLen, size_t len);
}
