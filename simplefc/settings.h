#pragma once

//Tick
#define TPS 120
#define TICK_TIME (1./TPS)


//Text
#define TEXT_COLOR vec3(3/5.)

//Level
#define LEVEL_DIR "levels/"
#define DEFAULT_LEVEL "levels/edit_template_do_not_change!"
#define DEFAULT_BOUNCE 0
#define DEFAULT_FRICTION 0.5
#define DEFAULT_ROLLING_FRICTION 0
#define DEFAULT_MASS 0


//Physics
//linear, angular
#define SLEEPING_THRESHOLD 0.5,0.25
#define GRAVACC btVector3(0,-9.81,0)
