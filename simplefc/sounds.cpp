#include "sounds.h"
#include "rand.h"
#include "log.h"
#include <SFML/Audio.hpp>
#include <map>

sf::Music *stepSound = 0;

std::map<collisionID, sf::Music *> collisionSounds;

void playStep(){
	return;
	if(stepSound){
		if(stepSound->getStatus() != sf::Music::Stopped){
			return;
		}
	}else{
		stepSound = new sf::Music();
	}

	static char TEMP[128];
	//sprintf(TEMP, "audio/step/step%d.wav", getRandInt(22)+1);
	sprintf(TEMP, "audio/step/step%d.wav", 11+getRandInt(11)+1);

	if(!stepSound->openFromFile(TEMP)){
		delete stepSound;
		return;
	}

	stepSound->play();
}



void checkSounds(){
	std::map<collisionID, sf::Music *>::iterator it = collisionSounds.end();
	while(it!=collisionSounds.begin()){
		it--;

		if(it->second->getStatus()==sf::Music::Stopped){
			delete it->second;
			collisionSounds.erase(it);
			it = collisionSounds.end();
		}
	}
}
void killSounds(){
	std::map<collisionID, sf::Music *>::iterator it = collisionSounds.end();
	while(it!=collisionSounds.begin()){
		it--;

		delete it->second;
	}
	collisionSounds.clear();

	if(stepSound){
		delete stepSound;
		stepSound = 0;
	}
}


void addCollisionSound(collisionID collID, float volume, vec3 pos){
	checkSounds();

	std::map<collisionID, sf::Music *>::iterator it = collisionSounds.find(collID);

	if(it == collisionSounds.end()){
		sf::Music *collSound = new sf::Music();
		if(!collSound->openFromFile("audio/collision/coll.wav")){
			delete collSound;
			printLogErr("missing coll sound file or another audio crash");
			return;
		}
		collisionSounds[collID] = collSound;

		//collSound->setVolume(volume);
		collSound->setVolume(0);
		collSound->setPosition(pos.getX(), pos.getY(), pos.getZ());
		collSound->setAttenuation(0.8f);
		collSound->play();
	}
}


void soundSetPosDirUp(vec3 pos, vec3 dir, vec3 up){
	sf::Listener::setPosition(pos.getX(), pos.getY(), pos.getZ());
	sf::Listener::setDirection(dir.getX(), dir.getY(), dir.getZ());
	sf::Listener::setUpVector(up.getX(), up.getY(), up.getZ());
}

void soundSetVolume(float vol){
	sf::Listener::setGlobalVolume(vol);
}

void setupSounds(){

}

void cleanupSounds(){
	killSounds();
}
