#pragma once
#include "mathlib.h"

class btRigidBody;
struct collisionID{
	btRigidBody *a;
	btRigidBody *b;

	collisionID(btRigidBody *a, btRigidBody *b):a(a), b(b){}

	bool operator<(const collisionID& oth) const{
		if(a < oth.a){
			return true;
		}else if(a == oth.a){
			if(b < oth.b)return true;
		}
		return false;
	};
};

void checkSounds();
void killSounds();

void addCollisionSound(collisionID collID, float volume, vec3 pos);

void soundSetPosDirUp(vec3 pos, vec3 dir, vec3 up);
void soundSetVolume(float vol);

void playStep();

void setupSounds();
void cleanupSounds();
