#pragma once
#include <algorithm>
#include <map>
#include <vector>
#include <string>
#include <cassert>
#include <functional>
#include <iostream>
#include <fstream>
#include <ostream>
#include <ctime>
#include <chrono>
#include <thread>
#include <memory>
#include <numeric>

using namespace std;

#include "std_improved/Containers.h"

#include "preprocessor.h"
#include "mathlib.h"
#include "log.h"
