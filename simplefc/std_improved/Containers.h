#pragma once
#include <vector>

template<typename T>
void erase(vector<T>& c, T r){
	auto it = find(c.begin(), c.end(), r);
	if(it != c.end())c.erase(it);
}

template<typename T>
void push_new(vector<T>& c, T r){
	if(find(c.begin(), c.end(), r) == c.end())c.push_back(r);
}

template<typename T>
void includes(vector<T>& c, T r){
	return find(c.begin(), c.end(), r) != c.end();
}
