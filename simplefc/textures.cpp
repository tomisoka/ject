#include <GL/glew.h>
#include <GL/gl.h>
#include "loadpng.h"

uint32_t Thight[32];
uint32_t Twidth[32];

GLuint Ttexture[32];

void initTextures(){
	glActiveTexture(GL_TEXTURE0+1);
	Ttexture[1] = loadTexture("textures/Menu_backgrond.png", Twidth+1, Thight+1);
}

void cleanupTextures(){
	glDeleteTextures(1, Ttexture+1);
}
