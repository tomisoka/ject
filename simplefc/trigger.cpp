#include "trigger.h"
#include "xml.h"
#include "basicFcs.h"
#include "std.h"

vector<string> triggPref;
map<string, Trigger*> triggers;

TriggerData::TriggerData(xml_node xml, string pref):tCall(0), delete_call(0){
	string temp = string(xml.text().as_string());
	size_t name_len = temp.find(";");

	if(name_len)nameCall = pref + temp.substr(0, name_len);
	else nameCall = "";

	if(temp.c_str()[name_len] == ';')strToVec(temp.c_str() + name_len + 1, retState);
}

Trigger::Trigger(const Trigger& o){
	triggerRedirectAdd((Trigger *) &o, this);
}

void Trigger::updateTriggerName(string nm){
	nameT = accumulate(triggPref.begin(), triggPref.end(), string(""), [](string o, string p){return o + p + "/";}) + nm;
}

void Trigger::registTriggerSelf(){
	triggers[nameT] = this;
}

void TriggerData::requestTrigger(){
	tCall = 0;
	triggerRequest(this);
}


void TriggerCaller::init(xml_node xml){
	for(xml_node dat: xml.children("callData")){
		int32_t argsI = xmlARInt(dat, "args", 0);

		string pref="";
		for(int32_t i=0;i<int32_t(triggPref.size())-argsI-1;++i){
			pref += triggPref[i] + "/";
		}

		data.push_back(new TriggerData(dat, pref));
	}
}

void TriggerCaller::save(xml_node xml){
	static char Temp[256];

	for(TriggerData *dat: data){
		xml_node callData = xml.append_child("callData");
		sprintf(Temp, "%s", dat->nameCall.c_str());
		char *ps = Temp + strlen(Temp);

		for(int ret: dat->retState){
			*ps++ = ';';
			*(ps = floatStr(ps, ret)) = 0;
		}

		callData.text().set(Temp);
	}
}

Trigger *getTrigger(string name){
	map<string, Trigger *>::iterator it = triggers.find(name);
	if(it==triggers.end())return 0;
	return it->second;
}

void triggerRequest(TriggerData *data){
	data->tCall = getTrigger(data->nameCall);
}

void triggersClear(){
	triggers.clear();
}
