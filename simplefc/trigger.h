#pragma once

#include "triggerRedirect.h"
#include <vector>
#include <map>
#include <string>

namespace pugi{class xml_node;}
using pugi::xml_node;

using namespace std;

extern vector<string> triggPref;

class Trigger{
public:
	Trigger(){}
	Trigger(const Trigger& o);
	virtual ~Trigger(){}

	virtual void call(int state){}

	void updateTriggerName(string nm);
	void registTriggerSelf();

	string nameT;
private:
};

struct TriggerData;
void removeTrigReq(TriggerData *data);

struct TriggerData{
	TriggerData():tCall(0), delete_call(0){}
	TriggerData(xml_node xml, string pref);
	TriggerData(Trigger *tCall, int retSt, bool delete_call = 0):tCall(tCall), delete_call(delete_call){retState.push_back(retSt);}
	TriggerData(const TriggerData& o):tCall(o.tCall), nameCall(o.nameCall), delete_call(o.delete_call){
		for (int retSt: o.retState){
			retState.push_back(retSt);
		}
	}
	~TriggerData(){if(delete_call)delete tCall;}
	void requestTrigger();

	Trigger *tCall;
	string nameCall;
	string oldnameCall;

	vector<int> retState;

	bool delete_call;
};

void triggerRequest(TriggerData *data);
void triggersClear();


class TriggerCaller{
public:
	TriggerCaller(vector<TriggerData *> callData = {}){data = callData;}
	TriggerCaller(const TriggerCaller& o){
		for (auto dat: o.data){
			TriggerData* dt = new TriggerData(*dat);
			data.push_back(dt);
			triggerRedirectFind(dt->tCall, dt);
		}
	}
	void init(xml_node xml);

	virtual ~TriggerCaller(){
		for(auto dat: data){
			delete dat;
		}
	}

	void requestTriggerAClear(){
		for(auto dat: data){
			dat->requestTrigger();
		}
	}

	void addData(TriggerData *dat){data.push_back(dat);}

	map<string, string> stringChangeF(){
		map<string, string> ch;
		for(auto dat: data){
			if(dat->nameCall != dat->oldnameCall){
				ch[dat->oldnameCall] = dat->nameCall;
			}
		}
		return ch;
	}
protected:
	void save(xml_node xml);

	virtual void callit(int state){
		for(auto dat: data){
			if(dat->tCall){
				if((int)dat->retState.size()>state)dat->tCall->call(dat->retState[state]);
				else dat->tCall->call(state);
			}
		}
	}
protected:
	vector<TriggerData *> data;
};
