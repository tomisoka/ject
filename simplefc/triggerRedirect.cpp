#include <map>
#include "trigger.h"

using std::map;

//org, new
map <Trigger *, Trigger *> redir;

//org, dest
map <Trigger *, TriggerData *> findT;

void triggerRedirectAdd(Trigger *org, Trigger *nw){
	redir[org] = nw;
}

void triggerRedirectFind(Trigger *org, TriggerData *dest){
	findT[org] = dest;
}

void triggerRedirectFindAClear(){
	auto it = findT.begin();
	while(it != findT.end()){
		auto itF = redir.find(it->first);
		if(itF != redir.end()){
			it->second->tCall = itF->second;
			it->second->oldnameCall = it->second->nameCall;
			it->second->nameCall = itF->second->nameT;
		}
		++it;
	}
	redir.clear();
	findT.clear();
}
