#pragma once

class Trigger;
class TriggerData;

void triggerRedirectAdd(Trigger *org, Trigger* nw);
void triggerRedirectFind(Trigger *org, TriggerData* dest);
void triggerRedirectFindAClear();
