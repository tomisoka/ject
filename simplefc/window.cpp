#include "window.h"
#include "log.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>

void error_callback(int error, const char* description){
	printLogErr(description);
}
void gl_debug_message_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void *_null){
	printLogErr(std::string(message));
}

GLFWwindow* window;
const int winWidthNorm=1200;
const int winHeightNorm=750;
int winWidth=winWidthNorm, winHeight=winHeightNorm;

int gwinWidth(){return winWidth;}
int gwinHeight(){return winHeight;}

void fbsize_callback(GLFWwindow* window, int width, int height){
	if(width==winWidth && height == winHeight)return;
	winWidth=width;
	winHeight=height;

	glViewport(0, 0, width, height);
	glScissor(0, 0, winWidth, winHeight);
}

void set_cursor_to_middle(){
	glfwSetCursorPos(window, winWidth/2, winHeight/2);
}

bool fullscreen = 0;

static bool cursorHidden = 0;
bool isHiddenCursor(){return cursorHidden;}
void hideCursor(bool toHide){
	if(toHide){
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		set_cursor_to_middle();
	}else{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	cursorHidden = toHide;
}




void setupWin(){

	glfwSetErrorCallback(error_callback);
	if(!glfwInit()){
		printLogErr("Failed to init GLFW");
		exit(1);
	}

	glfwWindowHint(GLFW_SAMPLES, 32);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL

	window = glfwCreateWindow(winWidth, winHeight, "Ject", fullscreen?glfwGetPrimaryMonitor():NULL, NULL);

	if(window == NULL){
		printLogErr("Failed to open window");
		glfwTerminate();
		exit(1);
	}

	glfwSetFramebufferSizeCallback(window, fbsize_callback);

	glfwMakeContextCurrent(window);

	glewExperimental=true;
	if(glewInit() != GLEW_OK){
		printLogErr("Failed to init GLEW");
		exit(1);
	}

	glDebugMessageCallback(gl_debug_message_callback, 0);
	//glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glEnable(GL_SCISSOR_TEST);

	glLineWidth(2);
	glEnable(GL_LINE_SMOOTH);


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
