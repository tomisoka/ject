#pragma once

class GLFWwindow;
extern GLFWwindow* window;

int gwinWidth();
int gwinHeight();
void set_cursor_to_middle();

void setupWin();

bool isHiddenCursor();
void hideCursor(bool toHide);
