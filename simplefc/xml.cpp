#include "xml.h"
#include "../simplefc/basicFcs.h"

bool xmlARBool(xml_node parent, const char *name, bool defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	return valN.as_int();
}
int xmlARInt(xml_node parent, const char *name, int defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	return valN.as_int();
}
float xmlARFloat(xml_node parent, const char *name, float defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	return valN.as_float();
}
string xmlARString(xml_node parent, const char *name, string defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	return string(valN.as_string());
}
vec3 xmlARVec3(xml_node parent, const char *name, vec3 defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	vec3 ret;
	if(strToVec(valN.as_string(), ret))return defaultV;
	return ret;
}
vec2 xmlARVec2(xml_node parent, const char *name, vec2 defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;
	vec2 ret;
	if(strToVec(valN.as_string(), ret))return defaultV;
	return ret;
}
quat xmlARRot(xml_node parent, const char *name, quat defaultV){
	xml_attribute valN = parent.attribute(name);
	if(!valN)return defaultV;

	const char* text = valN.as_string();

	int nums = 1;
	for(size_t i = 0;i < strlen(text); ++i){
		if(text[i] == ';')nums++;
	}
	if(nums == 4){
		vec4 rot;
		if(strToVec(text, rot))return defaultV;
		return rot;
	}else{
		vec3 rot;
		if(strToVec(text, rot))return defaultV;
		rot *= M_PI/180;
		return quat(rot);
	}
}


bool xmlRBool(xml_node parent, const char *name, bool defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	return valN.text().as_int();
}
int xmlRInt(xml_node parent, const char *name, int defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	return valN.text().as_int();
}
float xmlRFloat(xml_node parent, const char *name, float defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	return valN.text().as_float();
}
string xmlRString(xml_node parent, const char *name, string defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	return string(valN.text().as_string());
}
vec3 xmlRVec3(xml_node parent, const char *name, vec3 defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	vec3 ret;
	if(strToVec(valN.text().as_string(), ret))return defaultV;
	return ret;
}
vec2 xmlRVec2(xml_node parent, const char *name, vec2 defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;
	vec2 ret;
	if(strToVec(valN.text().as_string(), ret))return defaultV;
	return ret;
}
quat xmlRRot(xml_node parent, const char *name, quat defaultV){
	xml_node valN = parent.child(name);
	if(!valN)return defaultV;

	const char* text = valN.text().as_string();

	int nums = 1;
	for(size_t i = 0;i < strlen(text); ++i){
		if(text[i] == ';')nums++;
	}
	if(nums == 4){
		vec4 rot;
		if(strToVec(text, rot))return defaultV;
		return rot;
	}else{
		vec3 rot;
		if(strToVec(text, rot))return defaultV;
		rot *= M_PI/180;
		return quat(rot);
	}
}

void xmlRBool(xml_node node, const char *name, vector<bool>& ret){
	for(xml_node v: node.children(name)){
		ret.push_back(v.text().as_int());
	}
}
void xmlRInt(xml_node node, const char *name, vector<int>& ret){
	for(xml_node v: node.children(name)){
		ret.push_back(v.text().as_int());
	}
}
void xmlRFloat(xml_node node, const char *name, vector<float>& ret){
	for(xml_node v: node.children(name)){
		ret.push_back(v.text().as_float());
	}
}
void xmlRString(xml_node node, const char *name, vector<string>& ret){
	for(xml_node v: node.children(name)){
		ret.push_back(string(v.text().as_string()));
	}
}
void xmlRVec3(xml_node node, const char *name, vector<vec3>& ret){
	vec3 r;
	for(xml_node v: node.children(name)){
		if(strToVec(v.text().as_string(), r))continue;
		ret.push_back(r);
	}
}
void xmlRVec2(xml_node node, const char *name, vector<vec2>& ret){
	vec2 r;
	for(xml_node v: node.children(name)){
		if(strToVec(v.text().as_string(), r))continue;
		ret.push_back(r);
	}
}
void xmlRRot(xml_node node, const char *name, vector<quat>& ret){
	for(xml_node v: node.children(name)){
		const char* text = v.text().as_string();

		int nums = 1;
		for(size_t i = 0;i < strlen(text); ++i){
			if(text[i] == ';')nums++;
		}
		if(nums == 4){
			vec4 rot;
			if(strToVec(text, rot))continue;
			ret.push_back(rot);
		}else{
			vec3 rot;
			if(strToVec(text, rot))continue;
			rot *= M_PI/180;
			ret.push_back(quat(rot));
		}
	}
}




char XMLWTEMP[1024];
void xmlAWBool(xml_node parent, const char *name, bool saveV){
	xml_attribute valN = parent.append_attribute(name);
	sprintf(XMLWTEMP, "%d", saveV);
	valN.set_value(XMLWTEMP);
}
void xmlAWInt(xml_node parent, const char *name, int saveV){
	xml_attribute valN = parent.append_attribute(name);
	sprintf(XMLWTEMP, "%d", saveV);
	valN.set_value(XMLWTEMP);
}
void xmlAWFloat(xml_node parent, const char *name, float saveV){
	xml_attribute valN = parent.append_attribute(name);
	*floatStr(XMLWTEMP, saveV)=0;
	valN.set_value(XMLWTEMP);
}
void xmlAWString(xml_node parent, const char *name, string saveV){
	xml_attribute valN = parent.append_attribute(name);
	valN.set_value(saveV.c_str());
}
void xmlAWVec3(xml_node parent, const char *name, vec3 saveV){
	xml_attribute valN = parent.append_attribute(name);
	*(vecToStr(XMLWTEMP, saveV))=0;
	valN.set_value(XMLWTEMP);
}
void xmlAWVec2(xml_node parent, const char *name, vec2 saveV){
	xml_attribute valN = parent.append_attribute(name);
	*(vecToStr(XMLWTEMP, saveV))=0;
	valN.set_value(XMLWTEMP);
}
void xmlAWRot(xml_node parent, const char *name, quat saveV){
	xml_attribute valN = parent.append_attribute(name);
	*vecToStr(XMLWTEMP, saveV, ';', 6) = 0;
	valN.set_value(XMLWTEMP);
}

void xmlWBool(xml_node parent, const char *name, bool saveV){
	xml_node valN = parent.append_child(name);
	sprintf(XMLWTEMP, "%d", saveV);
	valN.text().set(XMLWTEMP);
}
void xmlWInt(xml_node parent, const char *name, int saveV){
	xml_node valN = parent.append_child(name);
	sprintf(XMLWTEMP, "%d", saveV);
	valN.text().set(XMLWTEMP);
}
void xmlWFloat(xml_node parent, const char *name, float saveV){
	xml_node valN = parent.append_child(name);
	*floatStr(XMLWTEMP, saveV)=0;
	valN.text().set(XMLWTEMP);
}
void xmlWString(xml_node parent, const char *name, string saveV){
	xml_node valN = parent.append_child(name);
	valN.text().set(saveV.c_str());
}
void xmlWVec3(xml_node parent, const char *name, vec3 saveV){
	xml_node valN = parent.append_child(name);
	*(vecToStr(XMLWTEMP, saveV))=0;
	valN.text().set(XMLWTEMP);
}
void xmlWVec2(xml_node parent, const char *name, vec2 saveV){
	xml_node valN = parent.append_child(name);
	*(vecToStr(XMLWTEMP, saveV))=0;
	valN.text().set(XMLWTEMP);
}
void xmlWRot(xml_node parent, const char *name, quat saveV){
	xml_node valN = parent.append_child(name);
	*vecToStr(XMLWTEMP, saveV, ';', 6) = 0;
	valN.text().set(XMLWTEMP);
}

void xmlWBool(xml_node parent, const char *name, const vector<bool>& saveV){
	for(bool v: saveV){
		xml_node valN = parent.append_child(name);
		sprintf(XMLWTEMP, "%d", v);
		valN.text().set(XMLWTEMP);
	}
}
void xmlWInt(xml_node parent, const char *name, const vector<int>& saveV){
	for(int v: saveV){
		xml_node valN = parent.append_child(name);
		sprintf(XMLWTEMP, "%d", v);
		valN.text().set(XMLWTEMP);
	}
}
void xmlWFloat(xml_node parent, const char *name, const vector<float>& saveV){
	for(float v: saveV){
		xml_node valN = parent.append_child(name);
		*floatStr(XMLWTEMP, v)=0;
		valN.text().set(XMLWTEMP);
	}
}
void xmlWString(xml_node parent, const char *name, const vector<string>& saveV){
	for(const string& v: saveV){
		xml_node valN = parent.append_child(name);
		valN.text().set(v.c_str());
	}
}
void xmlWVec3(xml_node parent, const char *name, const vector<vec3>& saveV){
	for(vec3 v: saveV){
		xml_node valN = parent.append_child(name);
		*(vecToStr(XMLWTEMP, v))=0;
		valN.text().set(XMLWTEMP);
	}
}
void xmlWVec2(xml_node parent, const char *name, const vector<vec2>& saveV){
	for(vec2 v: saveV){
		xml_node valN = parent.append_child(name);
		*(vecToStr(XMLWTEMP, v))=0;
		valN.text().set(XMLWTEMP);
	}
}
void xmlWRot(xml_node parent, const char *name, const vector<quat>& saveV){
	for(quat v: saveV){
		xml_node valN = parent.append_child(name);
		*vecToStr(XMLWTEMP, v, ';', 6) = 0;
		valN.text().set(XMLWTEMP);
	}
}
