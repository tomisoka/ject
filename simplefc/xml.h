#pragma once

#include <pugixml.hpp>
#include <vector>
#include <string.h>
#include "mathlib.h"

using std::vector;
using std::string;
using namespace pugi;

//attribute
bool xmlARBool(xml_node parent, const char *name, bool defaultV);
int xmlARInt(xml_node parent, const char *name, int defaultV);
float xmlARFloat(xml_node parent, const char *name, float defaultV);
string xmlARString(xml_node parent, const char *name, string defaultV);
vec3 xmlARVec3(xml_node parent, const char *name, vec3 defaultV);
vec2 xmlARVec2(xml_node parent, const char *name, vec2 defaultV);
quat xmlARRot(xml_node parent, const char *name, quat defaultV);

bool xmlRBool(xml_node parent, const char *name, bool defaultV);
int xmlRInt(xml_node parent, const char *name, int defaultV);
float xmlRFloat(xml_node parent, const char *name, float defaultV);
string xmlRString(xml_node parent, const char *name, string defaultV);
vec3 xmlRVec3(xml_node parent, const char *name, vec3 defaultV);
vec2 xmlRVec2(xml_node parent, const char *name, vec2 defaultV);
quat xmlRRot(xml_node parent, const char *name, quat defaultV);

void xmlRBool(xml_node node, const char *name, vector<bool>& ret);
void xmlRInt(xml_node node, const char *name, vector<int>& ret);
void xmlRFloat(xml_node node, const char *name, vector<float>& ret);
void xmlRString(xml_node node, const char *name, vector<string>& ret);
void xmlRVec3(xml_node node, const char *name, vector<vec3>& ret);
void xmlRVec2(xml_node node, const char *name, vector<vec2>& ret);
void xmlRRot(xml_node node, const char *name, vector<quat>& ret);


//attribute
void xmlAWBool(xml_node parent, const char *name, bool saveV);
void xmlAWInt(xml_node parent, const char *name, int saveV);
void xmlAWFloat(xml_node parent, const char *name, float saveV);
void xmlAWString(xml_node parent, const char *name, string saveV);
void xmlAWVec3(xml_node parent, const char *name, vec3 saveV);
void xmlAWVec2(xml_node parent, const char *name, vec2 saveV);
void xmlAWRot(xml_node parent, const char *name, quat saveV);

void xmlWBool(xml_node parent, const char *name, bool saveV);
void xmlWInt(xml_node parent, const char *name, int saveV);
void xmlWFloat(xml_node parent, const char *name, float saveV);
void xmlWString(xml_node parent, const char *name, string saveV);
void xmlWVec3(xml_node parent, const char *name, vec3 saveV);
void xmlWVec2(xml_node parent, const char *name, vec2 saveV);
void xmlWRot(xml_node parent, const char *name, quat saveV);

void xmlWBool(xml_node parent, const char *name, const vector<bool>& saveV);
void xmlWInt(xml_node parent, const char *name, const vector<int>& saveV);
void xmlWFloat(xml_node parent, const char *name, const vector<float>& saveV);
void xmlWString(xml_node parent, const char *name, const vector<string>& saveV);
void xmlWVec3(xml_node parent, const char *name, const vector<vec3>& saveV);
void xmlWVec2(xml_node parent, const char *name, const vector<vec2>& saveV);
void xmlWRot(xml_node parent, const char *name, const vector<quat>& saveV);
